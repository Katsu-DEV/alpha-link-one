package com.alpha.alphalink.LinkCore.NodeServer;

import android.util.Log;

import com.alpha.alphalink.BaseActivity;
import com.alpha.alphalink.LinkCore.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class ChallengeN implements Serializable {
    private static final long serialVersionUID = 2L;

    @SerializedName("uuid")
    @Expose
    private String uuid = null;

    @SerializedName("A")
    @Expose
    private String a = null;

    @SerializedName("aslot")
    @Expose
    private long aSlot;

    @SerializedName("awins")
    @Expose
    private int aWins;

    @SerializedName("aloss")
    @Expose
    private int aLoss;

    @SerializedName("acommType")
    @Expose
    private int aCommType;

    @SerializedName("B")
    @Expose
    private String b = null;

    @SerializedName("bslot")
    @Expose
    private long bSlot;

    @SerializedName("bwins")
    @Expose
    private int bWins;

    @SerializedName("bloss")
    @Expose
    private int bLoss;

    @SerializedName("bcommType")
    @Expose
    private int bCommType;

    @SerializedName("draws")
    @Expose
    private int draws;

    @SerializedName("created")
    @Expose
    private long created;

    @SerializedName("lastseen")
    @Expose
    private long lastseen;

    @SerializedName("digiROM")
    @Expose
    private String digiROM = null;

    @SerializedName("winner")
    @Expose
    private int winner;

    @SerializedName("commType")
    @Expose
    private int commType;

    @SerializedName("state")
    @Expose
    private int state;

    @SerializedName("level")
    @Expose
    private String level = null;

    public ChallengeN() {}
    public ChallengeN(String opponent, String digiROM, int commType, long slot) {
        setA(BaseActivity.getInstance().getUser().getName());
        setAWins(0);
        setALoss(0);
        setASlot(slot);

        setB(opponent);
        setBWins(0);
        setBLoss(0);
        setBSlot(slot);

        setDraws(0);

        setCreated(Utility.getTime());
        setLastseen(created);

        setDigiROM(digiROM);
        setCommType(commType);
        setWinner(-1);
        setState(1);
        setLevel("");
    }
    public ChallengeN(String opponent, String digiROM, int commType, long slot, String level) {
        setA(BaseActivity.getInstance().getUser().getName());
        setAWins(0);
        setALoss(0);
        setASlot(slot);
        setACommType(commType);

        setB(opponent);
        setBWins(0);
        setBLoss(0);
        setBSlot(-1);
        setBCommType(-1);

        setDraws(0);

        setCreated(Utility.getTime());
        setLastseen(created);

        setDigiROM(digiROM);
        setCommType(commType);
        setWinner(-1);
        setState(1);
        setLevel(level);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public long getASlot() {
        return aSlot;
    }

    public void setASlot(long aSlot) {
        this.aSlot = aSlot;
    }

    public int getAWins() {
        return aWins;
    }

    public void setAWins(int aWins) {
        this.aWins = aWins;
    }

    public int getALoss() {
        return aLoss;
    }

    public void setALoss(int aLoss) {
        this.aLoss = aLoss;
    }

    public int getACommType() {
        return aCommType;
    }

    public void setACommType(int aCommType) {
        this.aCommType = aCommType;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public long getBSlot() {
        return bSlot;
    }

    public void setBSlot(long bSlot) {
        this.bSlot = bSlot;
    }

    public int getBWins() {
        return bWins;
    }

    public void setBWins(int bWins) {
        this.bWins = bWins;
    }

    public int getBLoss() {
        return bLoss;
    }

    public void setBLoss(int bLoss) {
        this.bLoss = bLoss;
    }

    public int getBCommType() {
        return bCommType;
    }

    public void setBCommType(int bCommType) {
        this.bCommType = bCommType;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getLastseen() {
        return lastseen;
    }

    public void setLastseen(long lastseen) {
        this.lastseen = lastseen;
    }

    public void updateLastseen() {
        lastseen = Utility.getTime();
    }

    public String getDigiROM() {
        return digiROM;
    }

    public void setDigiROM(String digiROM) {
        this.digiROM = digiROM;
    }

    public int getWinner() {
        return winner;
    }

    public void setWinner(int winner) {
        this.winner = winner;
    }

    public int getCommType() {
        return commType;
    }

    public void setCommType(int commType) {
        this.commType = commType;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void increaseAWins() { aWins++; }
    public void increaseBWins() { bWins++; }
    public void increaseALoss() { aLoss++; }
    public void increaseBLoss() { bLoss++; }
    public void increaseDraws() { draws++; }


    public String getOpp() {
        return (BaseActivity.getInstance().getUser().getName().equals(b) ? a:b);
    }
    public boolean isA() {
        return a.equals(BaseActivity.getInstance().getUser().getName());
    }
    public int getOppCommType() { return isA() ? bCommType : aCommType; }
    public int getMyCommType() { return isA() ? aCommType : bCommType; }
    public void setMyCommType(int myCommType) {
        if(isA()) setACommType(myCommType);
        else setBCommType(myCommType);
    }

    public boolean isActive() {
        return (state == 1 || state == 4 || state == 2 || state == 5 );
    }
    public boolean isActiveRequiresAttention() {
        return (state == 1 || (!isA() && state == 2) || state == 4 || (isA() && state == 5));
    }

    public String toJsonString() {
        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create()
                .toJson(this);
    }

    public void updateChallengeInDatabase() {
        updateChallengeInDatabase(null);
    }
    public void updateChallengeInDatabase(final Runnable task) {
        updateLastseen();
        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updateChallenge",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            Log.e("UpdateChallengeInDatabaseScs", response);
                            if(task != null) task.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("UpdateChallengeInDatabaseErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() { return toJsonString().getBytes(); }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        try {
            Log.e("Body", new String(strRe.getBody()));
        }
        catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        rQ.add(strRe);
    }

    public void deleteChallengeInDatabase() {
        updateLastseen();

        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.GET, "https://www.alphahub.site/alphalink/api/protected/deleteChallengeByUUID/"+this.getUuid(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("DeleteChallengeInDatabaseScs", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("DeleteChallengeInDatabaseErr", error.getMessage());
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(strRe);
    }
}