package com.alpha.alphalink;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.alpha.alphalink.LinkCore.NodeServer.MQTTInterface;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;
import com.alpha.alphalink.LinkCore.NodeServer.UserNotify;
import com.alpha.alphalink.LinkCore.SettingsStore;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import static com.alpha.alphalink.App.CHANNEL_ID;

public class BackgroundService extends Service {
    private static BackgroundService k;
    private UserN user;
    public int keepConfirmingInterval = 20000;
    private boolean listening = false, boot = true, wait = false, keepConfirming;
    private String FILENAME_CH = "filech.txt", FILENAME_DM = "filedm.txt", command = "";
    private SettingsStore settings;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        stopListening();
        user = BaseActivity.getInstance().getUser();
        generatePermanentNotification();

        MQTTInterface.connectMqtt(user, true);
        startListening();
        if(boot) checkOldNotifs();
        else boot = false;
        return START_NOT_STICKY;
    }

    private void generatePermanentNotification() {
        Intent notificationIntent = new Intent(this, BaseActivity.class);
        notificationIntent.putExtra("command", "newmessage");
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Alpha Link is running in the background")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_not_icotwo)
                .build();

        startForeground(1, notification);
    }

    public void startListening() {
        if(!listening) {
            listening = true;
            MQTTInterface.subscribe(user.getName());
            //keepConfirming();
        }
    }

    public void stopListening() {
        if(listening) {
            Log.e("Stop", "Listening");
            listening = false;
            keepConfirming = false;
            MQTTInterface.disconnect();
        }
    }

    public boolean isListening() { return listening; }

    public void heardNotification(final UserNotify notif) {
        settings = loadSettings();

        try { Thread.sleep(750); } catch (InterruptedException e) { e.printStackTrace(); }

        Runnable task = new Runnable() {
            @Override
            public void run() {
                if(notif.getType().equals("challenge")) {
                    saveChalChk(notif.toJsonString());
                    if(settings.getNotify().equals("on")) {
                        if(Challenges.getInstance() == null || (BaseActivity.getInstance() != null && !BaseActivity.getInstance().isScreenOpen("challenges"))) {
                            String st = notif.getWhat();
                            if(st.equals("1") || st.equals("4") || st.equals("2") || st.equals("5") || st.equals("7") || st.equals("8") || st.equals("12")) {
                                command = "newchallenge";
                                buildNotification(notif);
                            }
                        }
                    }
                    if(BaseActivity.getInstance() != null)  BaseActivity.getInstance().onNewChallenge();
                }
                if(notif.getType().equals("message")) {
                    user.setDmchk(notif.toJsonString());
                    saveDmChk(notif.toJsonString());
                    if(BaseActivity.getInstance() != null) BaseActivity.getInstance().onNewMessage(notif);
                    if(settings.getNotify().equals("on")){
//                if(!(Profile.getInstance() != null && Profile.getInstance().isLinkChatOpen()) &&
//                        !(Challenges.getInstance() != null && Challenges.getInstance().isMessagesOpen())) {
                        //if(BaseActivity.getInstance().isScreenOpen("profile")) Log.e("EE2", ""+Profile.getInstance().isLinkChatOpen());
                        if(!(BaseActivity.getInstance().isScreenOpen("profile") && Profile.getInstance().isLinkChatOpen()) &&
                            !(BaseActivity.getInstance().isScreenOpen("challenges") && Challenges.getInstance().isMessagesOpen())) {
                            command = "newmessage";
                            buildNotification(notif);
                        }
                    }
                }
            }
        };

        getUserFromDatabase(task, false);
    }

    private void buildNotification(UserNotify notif) {
        if(notif == null || notif.equals(UserNotify.getEmpty()) || notif.getDetails().equals("ERROR")) return;
        String message = notif.getDetails(), title = notif.getType().equals("challenge") ? "Challenge" : notif.getFrom();

        if(settings.getNotificationsound().equals("on")){
            MediaPlayer beep = MediaPlayer.create(getApplicationContext(), R.raw.snd_digivicebeep);
            beep.start();
        }
        if(settings.getNotificationvibrate().equals("on")) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            long[] ptn = {0, 200, 100, 200, 100};
            v.vibrate(ptn, -1);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(getApplicationContext(), BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_not_icotwo)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        notificationManager.notify(100, builder.build());
        Log.e("Notif", "Built");
    }

    private void keepConfirming() {
        keepConfirming = true;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(keepConfirming) {
                    try { Thread.sleep(keepConfirmingInterval); }
                    catch (InterruptedException e) { e.printStackTrace(); }
                    Log.e("KEEP", "CONF");
                    //LinkChatMQTT.confirmConnection(user);
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }

    private void saveChalChk(String text) {
        if(text == null || text.equals("")) text="";
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(FILENAME_CH, MODE_PRIVATE);
            fos.write(text.getBytes());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e("FOS", "FOS == NULL");
        }
    }
    private String loadChalChk() {
        String ret = "";
        FileInputStream fis = null;

        try {
            fis = openFileInput(FILENAME_CH);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();

            while ( (ret = br.readLine()) !=null ) {
                sb.append(ret).append("\n");
            }
            ret = sb.toString();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fis != null) {
                try {
                    fis.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if(ret == null || ret.equals("")) ret = "empty";
        return ret;
    }
    private void saveDmChk(String text) {
        if(text == null || text.equals("")) text="";
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(FILENAME_DM, MODE_PRIVATE);
            fos.write(text.getBytes());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e("FOS", "FOS == NULL");
        }
    }
    private String loadDmChk() {
        String ret = "";
        FileInputStream fis = null;

        try {
            fis = openFileInput(FILENAME_DM);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();

            while ( (ret = br.readLine()) !=null ) {
                sb.append(ret).append("\n");
            }
            ret = sb.toString();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fis != null) {
                try {
                    fis.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if(ret == null || ret.equals("")) ret = "empty";
        return ret;
    }
    private SettingsStore loadSettings() {
        String ret = "";
        FileInputStream fis = null;

        try {
            fis = openFileInput("settingsstore.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();

            while ( (ret = br.readLine()) !=null ) {
                sb.append(ret).append("\n");
            }
            ret = sb.toString();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fis != null) {
                try {
                    fis.close();

                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        SettingsStore retSets = null;
        if(ret == null || ret.equals("")) retSets = SettingsStore.getEmpty();
        else retSets = new Gson().fromJson(ret, SettingsStore.class);

        return retSets;
    }

    public static BackgroundService getInstance() { return k; }
    public boolean getBoot() { return boot; }
    public String getCommand() { return command; }
    public void resetCommand() { command = "";}
    public UserN getUser() { return user; }
    public void setUser(UserN user) { this.user = user; }
    public void getUserFromDatabase(final Runnable task, final boolean check) {
        wait = true;
        String URL = "https://www.alphahub.site/alphalink/api/protected/getUserByUsername/"+user.getName();

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        wait = false;
                        String resp = response.toString();
                        if(resp.contains("'user doesn't exist'")) Log.e("GetUserBGS", "Doesn't Exist");
                        else if(resp != null) {
                            user = new Gson().fromJson(resp, UserN.class);
                            if(BaseActivity.getInstance() != null) BaseActivity.getInstance().setUser(user);
                            if(check) checkOldNotifs();
                        }
                        if(task != null) task.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        wait = false;
                        Log.e("eRESPBGSGU", ": "+error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }

    public void checkOldNotifs() {
        settings = loadSettings();
        String chalchk = loadChalChk(),dmchk = loadDmChk();
        if(chalchk != null && user.getChalchk() != null && !chalchk.contains(user.getChalchk())) {
            buildNotification(new Gson().fromJson(user.getChalchk(), UserNotify.class));
        }
        if(dmchk != null && user.getDmchk() != null && !dmchk.contains(user.getDmchk())) {
            buildNotification(new Gson().fromJson(user.getDmchk(), UserNotify.class));
        };
    }

    private BaseActivity base() { return BaseActivity.getInstance(); }
    private Profile profile() { return Profile.getInstance(); }
    private Challenges challenges() { return Challenges.getInstance(); }

    @Override public void onCreate() {
        super.onCreate();
        k = this;
    }
    @Override public void onDestroy() { super.onDestroy(); }
    @Nullable  @Override  public IBinder onBind(Intent intent) { return null; }
}