package com.alpha.alphalink.AlphaCore;

import com.alpha.alphalink.LinkCore.RomManager;

public class DM20BattleCalculator {
    private String[] shotSizePatterns;
    private int p1Presses, p2Presses, p1Life = 10, p2Life = 10;;
    private String p1ShotPattern, p2ShotPattern, p1HitPattern, p2HitPattern;
    private int[][] lifePoints = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
    private boolean winner;

    public DM20BattleCalculator(String romA, String romB) {
        initShotSizePatterns();
        String temp;

        // Presses and Shot Pattern
        p1Presses = RomManager.getDM20Presses(romA);
        p1ShotPattern = shotSizePatterns[p1Presses];

        p2Presses = RomManager.getDM20Presses(romB);
        p2ShotPattern = shotSizePatterns[p2Presses];

        // hit pattern
        temp = leadingZeros(Integer.toHexString(RomManager.getDM20HitPattern(romA)), 2);
        p1HitPattern = leadingZeros(Integer.toBinaryString(Integer.parseInt(temp.substring(0,1), 16)), 4);
        p2HitPattern = leadingZeros(Integer.toBinaryString(Integer.parseInt(temp.substring(1), 16)), 4);

        // hitPatternBoost
        int p1Boost = (RomManager.getDM20Power(romA) >= Integer.parseInt(RomManager.getDm20BasePower(RomManager.getDM20Index(romA)))+16) ? 1 : 0,
            p2Boost = (RomManager.getDM20Power(romB) >= Integer.parseInt(RomManager.getDm20BasePower(RomManager.getDM20Index(romB)))+16) ? 1 : 0;


        int round = 0;

        do {
            for(int j = 0; j<4; j++) {
                p1Life -= (Integer.parseInt(""+p1HitPattern.toCharArray()[3-j]) * (Integer.parseInt(""+p2ShotPattern.toCharArray()[j]) +p2Boost ));
                p2Life -= (Integer.parseInt(""+p2HitPattern.toCharArray()[3-j]) * (Integer.parseInt(""+p1ShotPattern.toCharArray()[j]) +p1Boost ));
                lifePoints[round][0] = p1Life;
                lifePoints[round][1] = p2Life;
                round++;

                if(p1Life <= 0) {
                    winner = false;
                    return;
                }
                else if (p2Life <= 0) {
                    winner = true;
                    return;
                }
                else if (round >= 5 && p2Life != p1Life) {
                    winner = (p2Life<p1Life);
                    return;
                }
            }
        } while (true /*p1Life > 0 && p2Life > 0*/);
    }

    public boolean getWinner() {
        return winner;
    }

    private void initShotSizePatterns() {
        shotSizePatterns = new String[]{
                "1231",
                "1231",
                "1231",
                "2323",
                "2323",
                "3124",
                "3124",
                "4141",
                "4141",
                "1434",
                "3244",
                "4144",
                "4344",
                "4434",
                "4434",
                "4434"};

//                {"2342",
//                "2432",
//                "2432",
//                "3434",
//                "3434",
//                "3245",
//                "3245",
//                "5252",
//                "5252",
//                "2545",
//                "3455",
//                "5255",
//                "5455",
//                "5545",
//                "5545",
//                "5545",
//                "5545"};
    }
    private static String leadingZeros(String input, int length) {
        if(input.length() < length){
            for(int j = input.length(); j<length ; j++) input = "0"+input;
        }
        return input;
    }
}
