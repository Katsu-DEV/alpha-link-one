package com.alpha.alphalink;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.alphalink.AlphaCore.AlphaEngine;
import com.alpha.alphalink.AlphaCore.DM20BattleCalculator;
import com.alpha.alphalink.AlphaCore.Pen20BattleCalculator;
import com.alpha.alphalink.AlphaCore.PenOGBattleCalculator;
import com.alpha.alphalink.AlphaCore.PenXBattleCalculator;
import com.alpha.alphalink.LinkCore.NodeServer.ChallengeN;
import com.alpha.alphalink.LinkCore.NodeServer.Conversation;
import com.alpha.alphalink.LinkCore.NodeServer.MQTTInterface;
import com.alpha.alphalink.LinkCore.NodeServer.Message;
import com.alpha.alphalink.LinkCore.NodeServer.MessageIdentifier;
import com.alpha.alphalink.LinkCore.NodeServer.SendMessageResponse;
import com.alpha.alphalink.LinkCore.NodeServer.SlotN;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;
import com.alpha.alphalink.LinkCore.NodeServer.UserNotify;
import com.alpha.alphalink.LinkCore.RomManager;
import com.alpha.alphalink.LinkCore.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.google.gson.Gson;
import com.physicaloid.lib.Physicaloid;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static android.view.WindowManager.*;

public class Challenges extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static Challenges k;
    private Context th = this;
    private UserN user;
    private ConstraintLayout[] challengeCls, challengeTxts, uslcls;
    private ImageView[] challengeIcos, uslstatus;
    private TextView[] usltxts, uslspcrs, challengeUsrs, challengeSubtxt, fltrpills, fltrspcs;
    private Runnable filter, expandList, showAll, reduceList;
    private int[] open = {9, 9};
    private Physicaloid ardu;
    private AlphaEngine alpha;
    private Thread findThread, scanThread;
    private DM20BattleCalculator dm20BCalc;
    private Pen20BattleCalculator pen20BCalc;
    private PenXBattleCalculator penxBCalc;
    private PenOGBattleCalculator penogBCalc;
    private Animation expOne, expTwo, expThree;
    private LinearLayout messageslist;
    private Conversation conversation;
    private UserNotify lastNotif;
    private ArrayList<String> challengesKeys;
    private LinkedHashMap<String, ChallengeN> challenges;
    private UserN[] leaderboard;
    private Message reportMessage = null;
    private boolean[] filters;
    private float x1 = 0, x2 = 0, y1, y2;
    private long t1 = 0, t2;
    private int slotSel = 9, chalSel = -1, slotCount, backCnt = 0, chrgcnt = 0, commTypeSel = 0;
    private String ncOppName, extRom, chalOpp = "";
    private boolean srch = false, thrd = false, retry = true, ringsSpin = false, ringsSpinTwo,
            hasUsersList = false, dropDown = false, pressed = false, isNewChallenge = false,
            canPress = true, del = false, debugLog = true, msgChIsExp = false, boot = true,
            hasPopulatedChallenges = false, messagesIsOpen = false, highlightChalTitle = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenges);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Utility.catchCrash("[Challenges : Uncaught]", paramThrowable, getApplicationContext());
                //Catch your exception
                // Without System.exit() this will not work.
                System.exit(2);
            }
        });

        try {
            init();
            obeyCommand(getIntent().getStringExtra("command"));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    boot = false;
                }
            }, 750);
        }
        catch (Exception e) {
            Utility.catchCrash("[CHALLENGES : ONSTART]", e, this);
//            Utility.appendToCrashLog("[CHALLENGES : ONSTART]\n" + e.toString());
//            e.printStackTrace();
//            ProcessPhoenix.triggerRebirth(this);
        }
    }

    private void obeyCommand(final String command) {
        switch(command) {
            case "newchallenge":
                final UserNotify notif = new Gson().fromJson(user.getChalchk(), UserNotify.class);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            int i;
                            for(i = 0; i< challengeUsrs.length; i++) {
                                if(challengeUsrs[i].getText().toString().equals(notif.getFrom())) break;
                            }

                            challengeCls[i].setFocusable(true);
                            challengeCls[i].setFocusableInTouchMode(true);
                            challengeCls[i].requestFocus();

                            anims("pulse", challengeCls[i]);
                            chalSel = Integer.parseInt(challengeCls[i].getTag().toString());
                            //chalOpp = notif.getFrom();

                            onClickChallenge();
                            challengeUsrs[Integer.parseInt(challengeCls[i].getTag().toString())].setTextColor(Color.parseColor("#383838"));
                        }
                        catch(Exception e) {
                            obeyCommand(command);
                        }
                    }
                }, 500);
                break;
            default: break;
        }
    }

    private void init() {
        k = this;

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = ((int) (dm.widthPixels - (50*dm.density))), height = dm.heightPixels;
        getWindow().setLayout(width, height);
        LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.RIGHT;
        getWindow().setFlags(LayoutParams.FLAG_NOT_TOUCH_MODAL, LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.swipe_container);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh.setRefreshing(false);
                if(!boot) {
                    base().getChallengesFromDatabase(new Runnable() {
                        @Override
                        public void run() {
                            Challenges.getInstance().populateChallenges(1);
                        }
                    });
//                    base().getChallengesFromDatabase();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            populateChallenges(1);
//                        }
//                    }, 1000);
                }
            }
        });

        user = (UserN) getIntent().getSerializableExtra("User");
        try {
            challenges = base().getChalsByActivity();
            challengesKeys = new ArrayList<>(challenges.keySet());
        }
        catch (Exception e) {
            sleep(250);
            challenges = base().getChalsByActivity();
            challengesKeys = new ArrayList<>(challenges.keySet());

        }

        //getOpponentsList();

        initialiseViews();
        initialiseSlots();

        filter = new Runnable() {
            @Override
            public void run() {
                // Filter out users according to filter bools
                for(int j = 0; j<usltxts.length; j++) {
                    boolean remove = true;
                    for(UserN u : leaderboard) {
                        if(u.hasPartners() && u.getName().equals(usltxts[j].getText().toString())) {
                            boolean ctsRemove = true, stgRemove = true;

                            // CommTypes
                            int[] cts = new int[u.getSlots().length];
                            for(int k = 0; k<cts.length; k++) cts[k] = u.getSlots()[k].getCommType();

                            if((cts.length > 0) && (
                                    (filters[0] && cts[0] == 1) ||
                                    (filters[1] && cts[0] == 3) ||
                                    (filters[2] && cts[0] == 7) ||
                                    (filters[3] && cts[0] == 9) ||
                                    (filters[4] && cts[0] == 91) || (filters[4] && cts[0] == 92) || (filters[4] && cts[0] == 93) ||
                                    (filters[5] && cts[0] == 8) ))
                                ctsRemove = false;
                            if((cts.length > 1) && (
                                    (filters[0] && cts[1] == 1) ||
                                    (filters[1] && cts[1] == 3) ||
                                    (filters[2] && cts[1] == 7) ||
                                    (filters[3] && cts[1] == 9) ||
                                    (filters[4] && cts[1] == 91) || (filters[4] && cts[1] == 92) || (filters[4] && cts[1] == 93) ||
                                    (filters[5] && cts[1] == 8) ))
                                ctsRemove = false;
                            if((cts.length > 2) && (
                                    (filters[0] && cts[2] == 1) ||
                                    (filters[1] && cts[2] == 3) ||
                                    (filters[2] && cts[2] == 7) ||
                                    (filters[3] && cts[2] == 9) ||
                                    (filters[4] && cts[2] == 91) || (filters[4] && cts[2] == 92) || (filters[4] && cts[2] == 93) ||
                                    (filters[5] && cts[2] == 8) ))
                                ctsRemove = false;
                            if((cts.length > 3) && (
                                    (filters[0] && cts[3] == 1) ||
                                    (filters[1] && cts[3] == 3) ||
                                    (filters[2] && cts[3] == 7) ||
                                    (filters[3] && cts[3] == 9) ||
                                    (filters[4] && cts[3] == 91) || (filters[4] && cts[3] == 92) || (filters[4] && cts[3] == 93) ||
                                    (filters[5] && cts[3] == 8) ))
                                ctsRemove = false;
                            if((cts.length > 4) && (
                                    (filters[0] && cts[4] == 1) ||
                                    (filters[1] && cts[4] == 3) ||
                                    (filters[2] && cts[4] == 7) ||
                                    (filters[3] && cts[4] == 9) ||
                                    (filters[4] && cts[4] == 91) || (filters[4] && cts[4] == 92) || (filters[4] && cts[4] == 93) ||
                                    (filters[5] && cts[4] == 8) ))
                                ctsRemove = false;
                            if((cts.length > 5) && (
                                    (filters[0] && cts[5] == 1) ||
                                    (filters[1] && cts[5] == 3) ||
                                    (filters[2] && cts[5] == 7) ||
                                    (filters[3] && cts[5] == 9) ||
                                    (filters[4] && cts[5] == 91) || (filters[4] && cts[5] == 92) || (filters[4] && cts[5] == 93) ||
                                    (filters[5] && cts[5] == 8) ))
                                ctsRemove = false;

                            // Stages
                            int[] stages = new int[u.getSlots().length];
                            for(int k = 0; k<stages.length; k++) stages[k] = u.getSlots()[k].getLevelVal();

                            if((stages.length > 0) && (
                                    (filters[6] &&  stages[0] == 6) ||
                                    (filters[7] &&  stages[0] == 5) ||
                                    (filters[8] &&  stages[0] == 4) ||
                                    (filters[9] &&  stages[0] == 3) ||
                                    (filters[10] && stages[0] == 2) ||
                                    (filters[11] && stages[0] == 1) ))
                                stgRemove = false;
                            if((stages.length > 1) && (
                                    (filters[6] &&  stages[1] == 6) ||
                                    (filters[7] &&  stages[1] == 5) ||
                                    (filters[8] &&  stages[1] == 4) ||
                                    (filters[9] &&  stages[1] == 3) ||
                                    (filters[10] && stages[1] == 2) ||
                                    (filters[11] && stages[1] == 1) ))
                                stgRemove = false;
                            if((stages.length > 2) && (
                                    (filters[6] &&  stages[2] == 6) ||
                                    (filters[7] &&  stages[2] == 5) ||
                                    (filters[8] &&  stages[2] == 4) ||
                                    (filters[9] &&  stages[2] == 3) ||
                                    (filters[10] && stages[2] == 2) ||
                                    (filters[11] && stages[2] == 1) ))
                                stgRemove = false;
                            if((stages.length > 3) && (
                                    (filters[6] &&  stages[3] == 6) ||
                                    (filters[7] &&  stages[3] == 5) ||
                                    (filters[8] &&  stages[3] == 4) ||
                                    (filters[9] &&  stages[3] == 3) ||
                                    (filters[10] && stages[3] == 2) ||
                                    (filters[11] && stages[3] == 1) ))
                                stgRemove = false;
                            if((stages.length > 4) && (
                                    (filters[6] &&  stages[4] == 6) ||
                                    (filters[7] &&  stages[4] == 5) ||
                                    (filters[8] &&  stages[4] == 4) ||
                                    (filters[9] &&  stages[4] == 3) ||
                                    (filters[10] && stages[4] == 2) ||
                                    (filters[11] && stages[4] == 1) ))
                                stgRemove = false;
                            if((stages.length > 5) && (
                                    (filters[6] &&  stages[5] == 6) ||
                                    (filters[7] &&  stages[5] == 5) ||
                                    (filters[8] &&  stages[5] == 4) ||
                                    (filters[9] &&  stages[5] == 3) ||
                                    (filters[10] && stages[5] == 2) ||
                                    (filters[11] && stages[5] == 1) ))
                                stgRemove = false;

                            if(!ctsRemove && !stgRemove) remove = false;
                        }
                    }

                    if(remove){
                        final int indtx = j;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uslcls[indtx].setVisibility(View.GONE);
                                uslspcrs[indtx].setVisibility(View.GONE);
                            }
                        });
                    }
                }
            }
        };
        expandList = new Runnable() {
            @Override
            public void run() {
                String ss = ((TextView)findViewById(R.id.chopptext)).getText().toString();
                for(int j = 0; j<usltxts.length; j++) {
                    if(usltxts[j].getText().toString().toLowerCase().contains(ss.trim().toLowerCase())){
                        final int indtx = j;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String typed = ((EditText) findViewById(R.id.chopptext)).getText().toString();
                                if(usltxts[indtx].getText().toString().toLowerCase().contains(typed.toLowerCase().trim())){
                                    uslcls[indtx].setVisibility(View.VISIBLE);
                                    uslspcrs[indtx].setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                }
            }
        };
        showAll = new Runnable() {
            @Override
            public void run() {
                for(int j = 0; j<usltxts.length; j++) {
                    final int indtx = j;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String typed = ((EditText) findViewById(R.id.chopptext)).getText().toString();
                            if(usltxts[indtx].getText().toString().toLowerCase().contains(typed.toLowerCase().trim())){
                                uslcls[indtx].setVisibility(View.VISIBLE);
                                uslspcrs[indtx].setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
        };
        reduceList = new Runnable() {
            @Override
            public void run() {
                String ss = ((TextView)findViewById(R.id.chopptext)).getText().toString();
                for(int j = 0; j<usltxts.length; j++) {
                    if(!usltxts[j].getText().toString().toLowerCase().contains(ss.trim().toLowerCase())){
                        final int indtx = j;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uslcls[indtx].setVisibility(View.GONE);
                                uslspcrs[indtx].setVisibility(View.GONE);
                            }
                        });
                    }
                }
            }
        };

        EditText chopp = (EditText) findViewById(R.id.chopptext);
        chopp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    populateOpponentsList(0, true, true);
                }
            }
        });

        initialiseMessages();
        base().setRefreshTime(60*1000);
        base().refreshLeaderboard();
    }
    private void populateOpponentsList(int anim, boolean initChoppText, boolean initFilters) {
        findViewById(R.id.filtersscrl).setVisibility(View.VISIBLE);
        findViewById(R.id.filtertypes).setVisibility(View.VISIBLE);
        findViewById(R.id.filtertitle).setVisibility(View.VISIBLE);

        leaderboard = base().getLeaderboard();
        if(!hasUsersList) {
            hasUsersList = true;
            int i = 0;
            uslcls = new ConstraintLayout[leaderboard.length];
            uslstatus = new ImageView[leaderboard.length];
            usltxts = new TextView[leaderboard.length];
            uslspcrs = new TextView[leaderboard.length];

            // Device Filter
            TextView    fltitle = new TextView(getApplicationContext()),
                    flpent = new TextView(getApplicationContext()),
                    fldmt = new TextView(getApplicationContext()),
                    fldmog = new TextView(getApplicationContext()),
                    flpenx = new TextView(getApplicationContext()),
                    flmini = new TextView(getApplicationContext()),
                    flpenog = new TextView(getApplicationContext()),
                    flbabyii = new TextView(getApplicationContext()),
                    flchild = new TextView(getApplicationContext()),
                    fladult = new TextView(getApplicationContext()),
                    flperfect = new TextView(getApplicationContext()),
                    flultimate = new TextView(getApplicationContext()),
                    flsuperultimate = new TextView(getApplicationContext());

            if(initFilters) {
                try {
                    if (fltrpills != null && fltrpills[0] != null) {
                        LinearLayout fltrlin = (LinearLayout) findViewById(R.id.filtertypes);
                        for (int index = 0; index<fltrpills.length; index++) {
                            fltrlin.removeView(fltrpills[index]);
                            fltrlin.removeView(fltrspcs[index]);
                        }
                    }
                }
                catch (Exception e) {
                    //Log.e("Filter Init Error", ": "+e.toString());
                }

                int size = 12;

                fltrpills = new TextView[size];
                fltrspcs = new TextView[size+1];

                // Setup filter
                filters = new boolean[size];
                for(int j = 0; j<filters.length; j++) filters[j] = true;

                for(int j = 0; j<fltrspcs.length; j++) {
                    fltrspcs[j] = new TextView(getApplicationContext());
                    LayoutParams spparams = new LayoutParams();
                    spparams.width = 32;
                    spparams.height = 20;
                    fltrspcs[j].setLayoutParams(spparams);
                }

                // Setup Filter Buttons
                // Pen20
                flpent.setText("  Pen20  ");
                flpent.setTextSize(11);
                flpent.setBackgroundResource(R.drawable.commtypebgpent);
                flpent.setTextColor(0xFFFFFFFF);
                flpent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[0]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            filters[0] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgpent);
                            filters[0] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });

                fltrpills[0] = flpent;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[0]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[0]);

                // DM20
                fldmt.setText("  DM20  ");
                fldmt.setTextSize(11);
                fldmt.setBackgroundResource(R.drawable.commtypebgdmt);
                fldmt.setTextColor(0xFFFFFFFF);
                fldmt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[1]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            filters[1] = false;
                        } else {
                            v.setBackgroundResource(R.drawable.commtypebgdmt);
                            filters[1] = true;
                            expandList.run();
                        }
                        filter.run();

                    }
                });
                fltrpills[1] = fldmt;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[1]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[1]);

                // DMOG
                fldmog.setText("  DMOG  ");
                fldmog.setTextSize(11);
                fldmog.setBackgroundResource(R.drawable.commtypebgdmog);
                fldmog.setTextColor(0xFFFFFFFF);
                fldmog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[2]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            filters[2] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgdmog);
                            filters[2] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[2] = fldmog;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[2]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[2]);

                // PenX
                flpenx.setText("  PenX  ");
                flpenx.setTextSize(11);
                flpenx.setBackgroundResource(R.drawable.commtypebgpenx);
                flpenx.setTextColor(0xFFFFFFFF);
                flpenx.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[3]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            filters[3] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgpenx);
                            filters[3] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[3] = flpenx;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[3]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[3]);

                // Mini
                flmini.setText("  MINI  ");
                flmini.setTextSize(11);
                flmini.setBackgroundResource(R.drawable.commtypebgmini);
                flmini.setTextColor(0xFFFFFFFF);
                flmini.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[4]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            filters[4] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgmini);
                            filters[4] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[4] = flmini;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[4]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[4]);

                // PenOG
                flpenog.setText("  PenOG  ");
                flpenog.setTextSize(11);
                flpenog.setBackgroundResource(R.drawable.commtypebgpenog);
                flpenog.setTextColor(0xFFFFFFFF);
                flpenog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[5]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            filters[5] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgpenog);
                            filters[5] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[5] = flpenog;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[5]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[5]);


                // Super Ultimate
                flsuperultimate.setText("  S Ultimate  ");
                flsuperultimate.setTextSize(10);
                flultimate.setPadding(Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this), Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this));
                flsuperultimate.setBackgroundResource(R.drawable.commtypebgstage);
                flsuperultimate.setTextColor(0xFFFFFFFF);
                flsuperultimate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[6]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            //((TextView)v).setTextColor(0xFFFFFFFF);
                            filters[6] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgstage);
                            //((TextView)v).setTextColor(0xFF555555);
                            filters[6] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[6] = flsuperultimate;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[6]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[6]);

                // Ultimate
                flultimate.setText("  Ultimate  ");
                flultimate.setTextSize(10);
                flultimate.setPadding(Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this), Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this));
                flultimate.setBackgroundResource(R.drawable.commtypebgstage);
                flultimate.setTextColor(0xFFFFFFFF);
                flultimate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[7]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            //((TextView)v).setTextColor(0xFFFFFFFF);
                            filters[7] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgstage);
                            //((TextView)v).setTextColor(0xFF555555);
                            filters[7] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[7] = flultimate;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[7]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[7]);

                // Perfect
                flperfect.setText("  Perfect  ");
                flperfect.setTextSize(10);
                flperfect.setPadding(Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this), Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this));
                flperfect.setBackgroundResource(R.drawable.commtypebgstage);
                flperfect.setTextColor(0xFFFFFFFF);
                flperfect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[8]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            //((TextView)v).setTextColor(0xFFFFFFFF);
                            filters[8] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgstage);
                            //((TextView)v).setTextColor(0xFF555555);
                            filters[8] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[8] = flperfect;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[8]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[8]);


                // Adult
                fladult.setText("  Adult  ");
                fladult.setTextSize(10);
                fladult.setPadding(Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this), Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this));
                fladult.setBackgroundResource(R.drawable.commtypebgstage);
                fladult.setTextColor(0xFFFFFFFF);
                fladult.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[9]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            //((TextView)v).setTextColor(0xFFFFFFFF);
                            filters[9] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgstage);
                            //((TextView)v).setTextColor(0xFF555555);
                            filters[9] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[9] = fladult;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[9]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[9]);


                // Child
                flchild.setText("  Child  ");
                flchild.setTextSize(10);
                flchild.setPadding(Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this), Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this));
                flchild.setBackgroundResource(R.drawable.commtypebgstage);
                flchild.setTextColor(0xFFFFFFFF);
                flchild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[10]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            //((TextView)v).setTextColor(0xFFFFFFFF);
                            filters[10] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgstage);
                            //((TextView)v).setTextColor(0xFF555555);
                            filters[10] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[10] = flchild;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[10]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[10]);

                // Baby II
                flbabyii.setText("  Baby II  ");
                flbabyii.setTextSize(10);
                flbabyii.setPadding(Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this), Utility.dpToPx(0.25f, this), Utility.dpToPx(0.5f, this));
                flbabyii.setBackgroundResource(R.drawable.commtypebgstage);
                flbabyii.setTextColor(0xFFFFFFFF);
                flbabyii.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        if (filters[11]) {
                            v.setBackgroundResource(R.drawable.commtypebgempty);
                            //((TextView)v).setTextColor(0xFFFFFFFF);
                            filters[11] = false;
                        }
                        else {
                            v.setBackgroundResource(R.drawable.commtypebgstage);
                            //((TextView)v).setTextColor(0xFF555555);
                            filters[11] = true;
                            expandList.run();
                        }
                        filter.run();
                    }
                });
                fltrpills[11] = flbabyii;
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrpills[11]);
                ((LinearLayout) findViewById(R.id.filtertypes)).addView(fltrspcs[11]);


                TextView flspcb = new TextView(getApplicationContext());
                LayoutParams flspcbparams = new LayoutParams();
                flspcbparams.width = 1000;
                flspcbparams.height = 1;
                flspcb.setLayoutParams(flspcbparams);
                flspcb.setBackgroundColor(0xFF383838);
                fltrspcs[fltrspcs.length - 1] = flspcb;
                ((LinearLayout) findViewById(R.id.userslistli)).addView(fltrspcs[fltrspcs.length - 1]);
            }

            //  Name List
            for (UserN u : leaderboard) {
                ConstraintLayout cl = new ConstraintLayout(getApplicationContext());
                cl.setId(View.generateViewId());
                cl.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                uslcls[i] = cl;

                ImageView status = new ImageView(getApplicationContext());
                status.setId(View.generateViewId());
                status.setVisibility(View.VISIBLE);

                LayoutParams stparams = new LayoutParams();
                stparams.width = dpToPx(12);
                stparams.height = dpToPx(12);
                status.setLayoutParams(stparams);

                uslstatus[i] = status;

                TextView tv = new TextView(getApplicationContext());
                tv.setText(u.getName());
                tv.setTextSize(15);
                tv.setPadding(10, dpToPx(16), 0, dpToPx(16));
                tv.setTextColor(0xE0383838);
                tv.setId(View.generateViewId());
                usltxts[i] = tv;

                //((LinearLayout) findViewById(R.id.userslistli)).addView(tv);
                cl.addView(status);
                cl.addView(tv);
                ((LinearLayout) findViewById(R.id.userslistli)).addView(cl);


                // Constraints
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(cl);

                constraintSet.connect(status.getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT, dpToPx(6));
                constraintSet.connect(tv.getId(), ConstraintSet.LEFT, status.getId(), ConstraintSet.RIGHT, dpToPx(6));

                constraintSet.connect(status.getId(), ConstraintSet.TOP, tv.getId(), ConstraintSet.TOP, dpToPx(1.5f));
                constraintSet.connect(status.getId(), ConstraintSet.BOTTOM, tv.getId(), ConstraintSet.BOTTOM);

                constraintSet.applyTo(cl);


                TextView sp = new TextView(getApplicationContext());

                LayoutParams spparams = new LayoutParams();
                spparams.width = 1000;
                spparams.height = 1;
                sp.setLayoutParams(spparams);
                sp.setBackgroundColor(0xFF383838);
                //sp.setPadding(0, 2, 0, 2);

                ((LinearLayout) findViewById(R.id.userslistli)).addView(sp);
                uslspcrs[i] = sp;

                i++;

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        onClickUser(((TextView)v).getText().toString());
                    }
                });
            }

            if(initChoppText) {
                ((EditText) findViewById(R.id.chopptext)).addTextChangedListener(new TextWatcher() {
                    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    @Override public void afterTextChanged(Editable s) { }
                    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (before < count) {
                            if (s.length() != 0) {
                                String ss = s.toString();
                                for (int j = 0; j < usltxts.length; j++) {
                                    if (!usltxts[j].getText().toString().toLowerCase().contains(ss.trim().toLowerCase())) {
                                        final int indtx = j;
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                uslcls[indtx].setVisibility(View.GONE);
                                                uslspcrs[indtx].setVisibility(View.GONE);
//                                                        usltxts[indtx].setVisibility(View.GONE);
//                                                        uslspcrs[indtx].setVisibility(View.GONE);


                                            }
                                        });
                                    }
                                }
                            }
                        } else {
                            if (count == 0) showAll.run();
                            else expandList.run();
                            filter.run();
                        }
                    }
                });
            }
        }

        long ti = Utility.getTime();
        for(int i = 0; i<leaderboard.length; i++) {
            //if(leaderboard[i].getName().equals(user.getName())) Log.e("Time: "+ti, ": "+leaderboard[i].getLastseen());

            if( (leaderboard[i].getName().equals(user.getName())) || (ti - leaderboard[i].getLastseen() <= (1000*60*15) ) && leaderboard[i].getLastseen() > 0) uslstatus[i].setBackgroundResource(R.drawable.ic_online);
            else uslstatus[i].setBackgroundResource(R.drawable.ic_offline);
        }
        if(anim == 0) anims("dropdownopen", findViewById(R.id.userslistscrl));
        if(anim == 1) {}
    }
    private void initialiseViews() {
        populateChallenges(0);
        initialiseWindows();
    }
    private void initialiseWindows() {
        findViewById(R.id.inbbg).setVisibility(View.GONE);
        findViewById(R.id.inbchallenge).setVisibility(View.GONE);
        findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
        findViewById(R.id.inbchallengecard).setVisibility(View.VISIBLE);
        findViewById(R.id.inbinfotextpl).setVisibility(View.GONE);
        findViewById(R.id.inbchallengecancelearly).setVisibility(View.GONE);
        findViewById(R.id.msgcommtypeid).setVisibility(View.GONE);
        findViewById(R.id.inbchallengecontrols).setVisibility(View.VISIBLE);


        findViewById(R.id.inbchallengewin).setVisibility(View.GONE);
        findViewById(R.id.inbmon).setVisibility(View.VISIBLE);

        findViewById(R.id.inbsndcenterico).setVisibility(View.GONE);

        findViewById(R.id.inbmonlist).setVisibility(View.VISIBLE);
        findViewById(R.id.inbsndlwrcntrls).setVisibility(View.VISIBLE);

        findViewById(R.id.inbscanvpet).setVisibility(View.GONE);
        findViewById(R.id.inbcbig).setVisibility(View.INVISIBLE);
        findViewById(R.id.inbcsml).setVisibility(View.INVISIBLE);
        findViewById(R.id.inbinfocenterico).setVisibility(View.INVISIBLE);
        ((ImageView)findViewById(R.id.inbinfocenterico)).setScaleY(1f);
        ((ImageView)findViewById(R.id.inbinfocenterico)).setScaleX(1f);

        findViewById(R.id.inbcnctardu).setVisibility(View.GONE);

        findViewById(R.id.inbpresswin).setVisibility(View.VISIBLE);
        findViewById(R.id.inbncmon).setVisibility(View.GONE);
        findViewById(R.id.inbnccenterico).setVisibility(View.GONE);

        findViewById(R.id.inblevellockcl).setVisibility(View.GONE);
    }
    private void initialiseMessages() {
        ConstraintLayout mw = findViewById(R.id.messageswin);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = ((int) (dm.widthPixels - (50*dm.density)));

        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) mw.getLayoutParams();
        lp.width = width;
        mw.setLayoutParams(lp);
        mw.setTranslationX(width);

        messageslist = findViewById(R.id.messageslist);
        anims("closeChallengeStats", null);
    }
    private void initialiseSlots() {
        slotCount = user.getSlots().length;
    }

    private void populateConversation() {
        // Stats
        findViewById(R.id.msgchallengetopbg).setPivotY(0);
        findViewById(R.id.msgchallengetopbg).animate()
                .scaleY(0f)
                .setDuration(1);

        findViewById(R.id.messageschallengescorestats).animate()
                .alpha(0)
                .setDuration(1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        findViewById(R.id.messageschallengescorestats).setVisibility(View.GONE);
                    }});

        ChallengeN chal = currentChallenge();
        String opp = chal.getOpp();

        if(chal.isA()) {
            ((TextView)findViewById(R.id.messageschallengescoreusername)).setText(chal.getA());
            ((TextView)findViewById(R.id.messageschallengescoreuserwin)).setText("W: " + chal.getAWins());
            ((TextView)findViewById(R.id.messageschallengescoreuserlos)).setText("L: " + chal.getALoss());
            ((TextView)findViewById(R.id.messageschallengescoreuserdrw)).setText("D: " + chal.getDraws());

            ((TextView)findViewById(R.id.msgtitlebartitle)).setText(opp);
            ((TextView)findViewById(R.id.messageschallengescoreoppname)).setText(opp);
            ((TextView)findViewById(R.id.messageschallengescoreoppwin)).setText("W: " + chal.getBWins());
            ((TextView)findViewById(R.id.messageschallengescoreopplos)).setText("L: " + chal.getBLoss());
            ((TextView)findViewById(R.id.messageschallengescoreoppdrw)).setText("D: " + chal.getDraws());
        }
        else {
            ((TextView)findViewById(R.id.messageschallengescoreusername)).setText(user.getName());
            ((TextView)findViewById(R.id.messageschallengescoreuserwin)).setText("W: " + chal.getBWins());
            ((TextView)findViewById(R.id.messageschallengescoreuserlos)).setText("L: " + chal.getBLoss());
            ((TextView)findViewById(R.id.messageschallengescoreuserdrw)).setText("D: " + chal.getDraws());

            ((TextView)findViewById(R.id.msgtitlebartitle)).setText(opp);
            ((TextView)findViewById(R.id.messageschallengescoreoppname)).setText(opp);
            ((TextView)findViewById(R.id.messageschallengescoreoppwin)).setText("W: " + chal.getAWins());
            ((TextView)findViewById(R.id.messageschallengescoreopplos)).setText("L: " + chal.getALoss());
            ((TextView)findViewById(R.id.messageschallengescoreoppdrw)).setText("D: " + chal.getDraws());
        }

        // Conversation
        ConstraintLayout filler = new ConstraintLayout(this);
        filler.setId(View.generateViewId());
        filler.setLayoutParams(new LayoutParams(0, 0));
        filler.getLayoutParams().height = dpToPx(144);
        messageslist.addView(filler);

        if(conversation.getMessages() != null){
            int tag = 0;
            for(Message message: conversation.getShortMessages()) {
                // Message
                ConstraintLayout box = new ConstraintLayout(this);
                box.setId(View.generateViewId());
                MessageIdentifier mi = new MessageIdentifier(message.getFrom(), message.getWhen());
                box.setTag(mi.toJsonString());
                //box.setTag(tag++);

                int alignment = 0;

                messageslist.addView(box);

                TextView msg = new TextView(this);
                msg.setId(View.generateViewId());
                msg.setText(message.getWhat());
                if(!message.getFrom().equals(user.getName())) {
                    msg.setBackgroundResource(R.drawable.messageleft);
                    alignment = ConstraintSet.LEFT;
                }
                else {
                    msg.setBackgroundResource(R.drawable.messageright);
                    alignment = ConstraintSet.RIGHT;
                }

                msg.setTextColor(Color.parseColor("#FFFFFF"));
                msg.setPadding(dpToPx(17) , dpToPx(6), dpToPx(17) , dpToPx(6));
                //msg.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                box.addView(msg);

                ViewGroup.MarginLayoutParams params =
                        (ViewGroup.MarginLayoutParams) msg.getLayoutParams();
                params.bottomMargin = (int)(dpToPx(4));
                //params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                msg.setLayoutParams(params);
                msg.setMaxWidth((int)(messageslist.getWidth()*0.85f));

                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(box);
                constraintSet.connect( msg.getId() , alignment, box.getId(), alignment,0);
                constraintSet.applyTo(box);

                // Timestamp
                String time;
                int msgDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(message.getWhen()))),
                        todayDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(Utility.getTime())));

                if(msgDate<todayDate)
                    time = new SimpleDateFormat("dd MMM HH:mm").format(new Date(message.getWhen()));
                else time = new SimpleDateFormat("HH:mm").format(new Date(message.getWhen()));

                TextView timestamp = new TextView(this);
                timestamp.setText(time);
                timestamp.setTextColor(Color.parseColor("#858585"));
                timestamp.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                timestamp.setTextSize(9.5f);
                messageslist.addView(timestamp);

                ViewGroup.MarginLayoutParams tsparams =
                        (ViewGroup.MarginLayoutParams) timestamp.getLayoutParams();
                tsparams.topMargin = (int)(dpToPx(2));
                tsparams.bottomMargin = (int)(dpToPx(18));
                timestamp.setLayoutParams(tsparams);

                box.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(final View v, MotionEvent event) {
                        int CLICK_DURATION = 750;

                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                x1 = event.getX();
                                y1 = event.getY();
                                t1 = System.currentTimeMillis();
                                return true;
                            case MotionEvent.ACTION_UP:
                                x2 = event.getX();
                                y2 = event.getY();
                                t2 = System.currentTimeMillis();

                                if (x1 == x2 && y1 == y2 && (t2 - t1) < CLICK_DURATION) {
                                    //Toast.makeText(getApplicationContext(), "Click", Toast.LENGTH_SHORT).show();
                                } else if ((t2 - t1) >= CLICK_DURATION) {
                                    //Toast.makeText(getApplicationContext(), "Long click", Toast.LENGTH_SHORT).show();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            MessageIdentifier mi = new Gson().fromJson(v.getTag().toString(), MessageIdentifier.class);
                                            reportMessage = conversation.getMessageFromWhen(mi.getFrom(), mi.getWhen());
                                            anims("pulse", v);
                                            anims("expand", findViewById(R.id.inbreportmessage));
                                            anims("fadein", findViewById(R.id.inbbg));
                                        }
                                    });
                                } else if (x1 > x2) {
                                    //Toast.makeText(getApplicationContext(), "Left swipe", Toast.LENGTH_SHORT).show();
                                } else if (x2 > x1) {
                                    //Toast.makeText(getApplicationContext(), "Right swipe", Toast.LENGTH_SHORT).show();
                                }
                                return true;
                        }
                        return false;
                    }
                });
            }
        }

        // Footer
        TextView footer = new TextView(this);
        footer.setFocusable(true);
        footer.setFocusableInTouchMode(true);
        footer.setHeight(1);
        messageslist.addView(footer);
        footer.requestFocus();
    }
    private void cleanMessages() {
        ((LinearLayout)findViewById(R.id.messageslist)).removeAllViews();
    }
    private void appendMessage(Message message, boolean isMe) {
        conversation.appendMessage(message);

        ConstraintLayout box = new ConstraintLayout(this);
        box.setId(View.generateViewId());
        box.setVisibility(View.GONE);

        MessageIdentifier mi = new MessageIdentifier(message.getFrom(), message.getWhen());
        box.setTag(message.toJsonString());

        messageslist.addView(box);

        TextView msg = new TextView(this);
        msg.setId(View.generateViewId());
        msg.setText(message.getWhat());
        msg.setBackgroundResource(isMe ? R.drawable.messageright : R.drawable.messageleft);
        msg.setTextColor(Color.parseColor("#FFFFFF"));
        msg.setPadding(dpToPx(17) , dpToPx(6), dpToPx(17) , dpToPx(6));
        box.addView(msg);

        ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams) msg.getLayoutParams();
        params.bottomMargin = (int)(dpToPx(2));
        params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        msg.setLayoutParams(params);
        msg.setMaxWidth((int)(messageslist.getWidth()*0.85f));


        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(box);
        if(isMe) constraintSet.connect( msg.getId() , ConstraintSet.RIGHT, box.getId(), ConstraintSet.RIGHT,0);
        else constraintSet.connect( msg.getId() , ConstraintSet.LEFT, box.getId(), ConstraintSet.LEFT,0);
        constraintSet.applyTo(box);

        // Timestamp
        String time;
        int msgDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(message.getWhen()))),
            todayDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(Utility.getTime())));

        if(msgDate<todayDate)
             time = new SimpleDateFormat("dd MMM HH:mm").format(new Date(message.getWhen()));
        else time = new SimpleDateFormat("HH:mm").format(new Date(message.getWhen()));

        TextView timestamp = new TextView(this);
        timestamp.setText(time);
        timestamp.setTextColor(Color.parseColor("#656565"));
        timestamp.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        timestamp.setTextSize(9f);
        messageslist.addView(timestamp);

        ViewGroup.MarginLayoutParams tsparams =
                (ViewGroup.MarginLayoutParams) timestamp.getLayoutParams();
        tsparams.bottomMargin = (int)(dpToPx(2));
        tsparams.bottomMargin = (int)(dpToPx(12));
        timestamp.setLayoutParams(tsparams);

        timestamp.setFocusable(true);
        timestamp.setFocusableInTouchMode(true);
        timestamp.requestFocus();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EditText tb = findViewById(R.id.messagetextfield);

                tb.setFocusable(true);
                tb.setFocusableInTouchMode(true);
                tb.requestFocus();
            }
        }, 250);

        box.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View v, MotionEvent event) {
                int CLICK_DURATION = 750;

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        y1 = event.getY();
                        t1 = System.currentTimeMillis();
                        return true;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        y2 = event.getY();
                        t2 = System.currentTimeMillis();

                        if (x1 == x2 && y1 == y2 && (t2 - t1) < CLICK_DURATION) {
                            //Toast.makeText(getApplicationContext(), "Click", Toast.LENGTH_SHORT).show();
                        } else if ((t2 - t1) >= CLICK_DURATION) {
                            //Toast.makeText(getApplicationContext(), "Long click", Toast.LENGTH_SHORT).show();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MessageIdentifier mi = new Gson().fromJson(v.getTag().toString(), MessageIdentifier.class);
                                    reportMessage = conversation.getMessageFromWhen(mi.getFrom(), mi.getWhen());
                                    anims("pulse", v);
                                    anims("expand", findViewById(R.id.inbreportmessage));
                                    anims("fadein", findViewById(R.id.inbbg));
                                }
                            });
                        } else if (x1 > x2) {
                            //Toast.makeText(getApplicationContext(), "Left swipe", Toast.LENGTH_SHORT).show();
                        } else if (x2 > x1) {
                            //Toast.makeText(getApplicationContext(), "Right swipe", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                }
                return false;
            }
        });
        anims("expand", box);
    }
    private void reportMessage() {
        final Message msg = new Message(user.getName(), "reportMessage", Utility.getTime(), reportMessage.toJsonString());

        String URL = "https://www.alphahub.site/alphalink/api/protected/sendmessage/";

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRe = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final SendMessageResponse resp = (new Gson()).fromJson(response, SendMessageResponse.class);

                        if(resp != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    String  mm = "New Reported Message",
                                            type = "reportedMessage";
                                    UserNotify oppNotify = new UserNotify(user.getName(), resp.getRowid(), mm, type);
                                    notifyUser("reportedMessage", oppNotify);
                                }
                            }, 250);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() {
                return msg.toJsonString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        rQ.add(strRe);
    }
    public void cleanOpponentsList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!boot) {
                        boolean[] tempFilters = filters.clone();
                        LinearLayout lin = (LinearLayout) findViewById(R.id.userslistli);

                        for (int i = 0; i < uslcls.length; i++) {
                            lin.removeView(uslcls[i]);
                            lin.removeView(uslspcrs[i]);
                        }

                        hasUsersList = false;
                        if (dropDown) {
                            populateOpponentsList(1, false, false);
                            int count = ((EditText) findViewById(R.id.chopptext)).getText().toString().trim().length();
                            if (count == 0) showAll.run();
                            else reduceList.run();

                            filters = tempFilters.clone();
                            for (int i = 0; i < filters.length; i++) {
                                if (!filters[i]) {
                                    fltrpills[i].setBackgroundResource(R.drawable.commtypebgempty);
                                }
                                else {
                                    switch (i) {
                                        case 0:
                                            fltrpills[i].setBackgroundResource(R.drawable.commtypebgpent);
                                            break;
                                        case 1:
                                            fltrpills[i].setBackgroundResource(R.drawable.commtypebgdmt);
                                            break;
                                        case 2:
                                            fltrpills[i].setBackgroundResource(R.drawable.commtypebgdmog);
                                            break;
                                        case 3:
                                            fltrpills[i].setBackgroundResource(R.drawable.commtypebgpenx);
                                            break;
                                        case 4:
                                            fltrpills[i].setBackgroundResource(R.drawable.commtypebgmini);
                                            break;
                                        case 5:
                                            fltrpills[i].setBackgroundResource(R.drawable.commtypebgpenog);
                                            break;
                                        case 6:
                                        case 7:
                                        case 8:
                                        case 9:
                                        case 10:
                                        case 11:
                                            fltrpills[i].setBackgroundResource(R.drawable.commtypebgstage);
                                            break;
                                        default:
                                            fltrpills[i].setBackgroundResource(R.drawable.commtypebgempty);
                                            break;
                                    }
                                }
                            }
                            filter.run();
                        }
                    }
            }
        });
    }

    private void populateChallenges(int anim) {
        challenges = base().getChalsByActivity();
        challengesKeys = new ArrayList<>(challenges.keySet());

        if(challenges.size() <= 0) {
            findViewById(R.id.emptychals).setVisibility(View.VISIBLE);
            findViewById(R.id.emptychals).animate()
                    .setStartDelay(50)
                    .setDuration(1600)
                    .alpha(1)
                    .start();
            return;
        }
        else findViewById(R.id.emptychals).setVisibility(View.GONE);

        if(!boot && ((challenges == null || challengeCls == null) || (challenges.size() != challengeCls.length))) {
            hasPopulatedChallenges = false;
        }

        if(true/*!hasPopulatedChallenges*/) {
            cleanChallenges();
            hasPopulatedChallenges = true;
            int len = challenges.size();

            challengeCls = new ConstraintLayout[len];
            challengeIcos = new ImageView[len];
            challengeTxts = new ConstraintLayout[len];
            challengeUsrs = new TextView[len];
            challengeSubtxt = new TextView[len];

            ArrayList<String> keys = new ArrayList<>(challenges.keySet());

            for(int i = 0; i<len; i++) {
                //  Basic Setup
                challengeCls[i] = new ConstraintLayout(getApplicationContext());
                challengeCls[i].setId(View.generateViewId());
                challengeCls[i].setLayoutParams(new LayoutParams(0, 0));
                challengeCls[i].getLayoutParams().height = dpToPx(78);
                challengeCls[i].getLayoutParams().width = dpToPx(300);
                challengeCls[i].setPadding(dpToPx(4), dpToPx(4), dpToPx(4), dpToPx(4));
                challengeCls[i].setTag(Integer.toString(i));
                //challengeCls[i].setTag(chal.getOpp());
                challengeCls[i].setAlpha(0f);

                // icos
                challengeIcos[i] = new ImageView(getApplicationContext());
                challengeIcos[i].setLayoutParams(new LayoutParams(0, 0));
                challengeIcos[i].setId(View.generateViewId());
                challengeIcos[i].getLayoutParams().height = dpToPx(50);
                challengeIcos[i].getLayoutParams().width = dpToPx(50);
                challengeIcos[i].setAlpha(0.75f);

                // txts
                challengeTxts[i] = new ConstraintLayout(getApplicationContext());
                challengeTxts[i].setId(View.generateViewId());
                challengeTxts[i].setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                //challengeTxts[i].setId(View.generateViewId());


                // usrs
                challengeUsrs[i] = new TextView(getApplicationContext());
                challengeUsrs[i].setId(View.generateViewId());
                challengeUsrs[i].setTextColor(Color.parseColor("#383838"/*"#3C0E05"*/));
                challengeUsrs[i].setTextSize(15);

                // subtxt
                challengeSubtxt[i] = new TextView(getApplicationContext());
                challengeSubtxt[i].setId(View.generateViewId());
                challengeSubtxt[i].setTextColor(Color.parseColor("#D0383838"/*"#A43C0E05"*/));
                challengeSubtxt[i].setTextSize(10);

                challengeTxts[i].addView(challengeUsrs[i]);
                challengeTxts[i].addView(challengeSubtxt[i]);
                challengeCls[i].addView(challengeIcos[i]);
                challengeCls[i].addView(challengeTxts[i]);

                // Constraints
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(challengeCls[i]);

                constraintSet.connect(challengeIcos[i].getId(), ConstraintSet.TOP, challengeCls[i].getId(), ConstraintSet.TOP);
                constraintSet.connect(challengeIcos[i].getId(), ConstraintSet.BOTTOM, challengeCls[i].getId(), ConstraintSet.BOTTOM);
                constraintSet.connect(challengeIcos[i].getId(), ConstraintSet.LEFT, challengeCls[i].getId(), ConstraintSet.LEFT, dpToPx(5));

                constraintSet.connect(challengeTxts[i].getId(), ConstraintSet.TOP, challengeIcos[i].getId(), ConstraintSet.TOP);
                constraintSet.connect(challengeTxts[i].getId(), ConstraintSet.BOTTOM, challengeIcos[i].getId(), ConstraintSet.BOTTOM);
                constraintSet.connect(challengeTxts[i].getId(), ConstraintSet.START, challengeIcos[i].getId(), ConstraintSet.END, dpToPx(12));

                constraintSet.applyTo(challengeCls[i]);


                ConstraintSet constraintSet2 = new ConstraintSet();
                constraintSet2.clone(challengeTxts[i]);

                constraintSet2.connect(challengeUsrs[i].getId(), ConstraintSet.TOP, challengeTxts[i].getId(), ConstraintSet.TOP);
                constraintSet2.connect(challengeUsrs[i].getId(), ConstraintSet.LEFT, challengeTxts[i].getId(), ConstraintSet.LEFT);

                constraintSet2.connect(challengeSubtxt[i].getId(), ConstraintSet.TOP, challengeUsrs[i].getId(), ConstraintSet.BOTTOM, dpToPx(4));
                constraintSet2.connect(challengeSubtxt[i].getId(), ConstraintSet.LEFT, challengeTxts[i].getId(), ConstraintSet.LEFT);

                constraintSet2.applyTo(challengeTxts[i]);



                ((LinearLayout) findViewById(R.id.challLinLayout)).addView(challengeCls[i]);


                challengeCls[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        chalSel = Integer.parseInt(v.getTag().toString());
                        //chalOpp = v.getTag().toString();
                        onClickChallenge();
                    }
                });

                ChallengeN challengesi = challenges.get( keys.get(i) );

                // Content
                String opp = challengesi.getOpp(), index = "", rom = challengesi.getDigiROM();
                int commType = challengesi.getCommType(), state = challengesi.getState();
                challengeUsrs[i].setText(opp);
                /*switch(commType) {
                    case 1:
                        index = RomManager.getPen20Index(chals[i].getDigiROM());
                        break;
                    case 3:
                        index = RomManager.getDM20Index(chals[i].getDigiROM());
                        break;
                    case 7:
                        index = RomManager.getDmogIndex(chals[i].getDigiROM());
                        break;
                    case 91:
                        index = RomManager.getIndexGeneral(chals[i].getDigiROM(), commType);
                        break;
                    default: break;
                }*/
//                if(challengesi.isA() && challengesi.getWinner() == 0 && (state == 2 || state == 3) ) {
//                    commType = user.getSlotFromIdentifier(challengesi.getASlot()).getCommType();
//                }
//                else if(!challengesi.isA() && challengesi.getWinner() == 1 && (state == 5 || state == 6) )
//                    commType = user.getSlotFromIdentifier(challengesi.getBSlot()).getCommType();

                if((state == 1 && challengesi.isA()) || (state == 4 && !challengesi.isA())) // Challenge Sent
                {
                    commType = challengesi.getMyCommType();
                    challengeIcos[i].setImageResource(getImageId(getIMGFileName(rom, commType)));
                    challengeSubtxt[i].setText("Challenged "+opp+" to Battle!");
                }
                if((state == 1 && !challengesi.isA()) || (state == 4 && challengesi.isA())) // Challenge Received
                {
                    String monName = "";
                    commType = challengesi.getOppCommType();
                    index = RomManager.getIndexGeneral(challengesi.getDigiROM(), commType);
                    monName = getMonNameFromIndex(index, commType);

                    challengeIcos[i].setImageResource(getImageId(getIMGFileName(rom, commType)));
                    challengeSubtxt[i].setText(opp+" Has Challenged you to Battle\nwith their "+ monName + "!");
                }

                if((state == 2 && challengesi.isA()) || (state == 5 && !challengesi.isA())) // Accept Sent
                {
                    int win = challengesi.getWinner();
                    if(win == 0 && challengesi.isA() || win == 1 && !challengesi.isA()) {
                        commType = challengesi.getMyCommType();
                        challengeSubtxt[i].setText("You have Defeated " + opp + "!");
                    }
                    else if (win == 0 && !challengesi.isA() || win == 1 && challengesi.isA()) {
                        commType = challengesi.getOppCommType();
                        challengeSubtxt[i].setText(opp+" Won the Battle");
                    }
                    challengeIcos[i].setImageResource(getImageId(getIMGFileName(rom, commType)));
                }
                if((state == 2 && !challengesi.isA()) || (state == 5 && challengesi.isA())) // Accept Received
                {
                    challengeIcos[i].setImageResource(R.drawable.ic_ico);
                    challengeSubtxt[i].setText(opp+" Has Accepted your Challenge!");
                }

                if((state == 3 && challengesi.isA()) || (state == 6 && !challengesi.isA())) // Challenge Resolved
                {
                    int win = challengesi.getWinner();
                    if(win == 0 && challengesi.isA() || win == 1 && !challengesi.isA()) {
                        commType = challengesi.getMyCommType();
                        challengeSubtxt[i].setText("You have Defeated " + opp + "!");
                    }
                    else if (win == 0 && !challengesi.isA() || win == 1 && challengesi.isA()) {
                        commType = challengesi.getOppCommType();
                        challengeSubtxt[i].setText(opp+" Won the Battle");
                    }
                    challengeIcos[i].setImageResource(getImageId(getIMGFileName(rom, commType)));

                }
                if((state == 3 && !challengesi.isA()) || (state == 6 && challengesi.isA())) // Challenge Resolved
                {
                    int win = challengesi.getWinner();
                    if(win == 0 && challengesi.isA() || win == 1 && !challengesi.isA()) {
                        challengeSubtxt[i].setText("You have Defeated " + opp + "!");
                        commType = challengesi.getMyCommType();
                    }
                    else if (win == 0 && !challengesi.isA() || win == 1 && challengesi.isA()) {
                        challengeSubtxt[i].setText(opp+" Won the Battle");
                        commType = challengesi.getOppCommType();
                    }
                    challengeIcos[i].setImageResource(getImageId(getIMGFileName(rom, commType)));
                }

                if((state == 7 && challengesi.isA()) || (state == 8 && !challengesi.isA())) // Player Rejected
                {
                    challengeIcos[i].setImageResource(R.drawable.ic_ico);
                    challengeSubtxt[i].setText("You Have Declined a Challenge from "+opp);
                }
                if((state == 7 && !challengesi.isA()) || (state == 8 && challengesi.isA())) // Opponent Rejected
                {
                    challengeIcos[i].setImageResource(R.drawable.ic_ico);
                    challengeSubtxt[i].setText(opp + " Has Declined your Challenge");
                }

                if((state == 9 && challengesi.isA()) || (state == 10 && !challengesi.isA())) // Mon Deleted
                {
                    challengeIcos[i].setImageResource(R.drawable.ic_ico);
                    challengeSubtxt[i].setText("Challenge Cancelled");
                }
                if((state == 9 && !challengesi.isA()) || (state == 10 && challengesi.isA())) // Mon Deleted
                {
                    challengeIcos[i].setImageResource(R.drawable.ic_ico);
                    challengeSubtxt[i].setText("Challenge Cancelled");
                }

                if(state == 11) // Expired
                {
                    challengeIcos[i].setImageResource(R.drawable.ic_ico);
                    challengeSubtxt[i].setText("Challenge Expired");
                }

                if(state == 12) // Challenge Cancelled by initiator
                {
                    challengeIcos[i].setImageResource(R.drawable.ic_ico);
                    challengeSubtxt[i].setText("Challenge Cancelled");
                }

                if(challengesi.isActiveRequiresAttention()) {
                    challengeCls[i].setBackgroundColor(0x0a611e10);
                }
                else {
                    challengeCls[i].setBackgroundColor(0x00000000);
                }
                //i++;
            }
        }

        if(anim == 0) {
            for(int i = 0; i<challengeCls.length; i++) {
                challengeCls[i].animate()
                        .setStartDelay(440+(i*60))
                        .setDuration(250)
                        .alpha(1.0f)
                        .start();
            }
        }
        else if(anim == 1) {
            for(ConstraintLayout c : challengeCls) c.setAlpha(1.0f);
        }
    }
    private void cleanChallenges() {
        ((LinearLayout)findViewById(R.id.challLinLayout)).removeAllViews();
    }
    private void initialiseMonList() {
        slotSel = 9;
        String rom;
        int commType;
        slotCount = user.getSlots().length;

        if(slotCount >= 1){
            if(user.getSlots()[0].isAway() && isNewChallenge) {
                findViewById(R.id.inbmloneico).setVisibility(View.GONE);
                findViewById(R.id.inbmloneaway).setVisibility(View.VISIBLE);
            }
            else {
                findViewById(R.id.inbmloneico).setVisibility(View.VISIBLE);
                findViewById(R.id.inbmloneaway).setVisibility(View.GONE);

                rom = user.getSlots()[0].getDigiROM();
                commType = user.getSlots()[0].getCommType();
                ( (ImageView) findViewById(R.id.inbmloneico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
            }
        }
        else {
            findViewById(R.id.inbmloneico).setVisibility(View.GONE);
            findViewById(R.id.inbmloneaway).setVisibility(View.GONE);
        }

        if(slotCount >= 2){
            if(user.getSlots()[1].isAway() && isNewChallenge) {
                findViewById(R.id.inbmltwoico).setVisibility(View.GONE);
                findViewById(R.id.inbmltwoaway).setVisibility(View.VISIBLE);
            }
            else {
                findViewById(R.id.inbmltwoico).setVisibility(View.VISIBLE);
                findViewById(R.id.inbmltwoaway).setVisibility(View.GONE);

                rom = user.getSlots()[1].getDigiROM();
                commType = user.getSlots()[1].getCommType();
                ((ImageView) findViewById(R.id.inbmltwoico)).setImageResource(getImageId(getIMGFileName(rom, commType)));
            }
        }
        else {
            findViewById(R.id.inbmltwoico).setVisibility(View.GONE);
            findViewById(R.id.inbmltwoaway).setVisibility(View.GONE);

        }

        if(slotCount >= 3) {
            if(user.getSlots()[2].isAway() && isNewChallenge) {
                findViewById(R.id.inbmlthreeico).setVisibility(View.GONE);
                findViewById(R.id.inbmlthreeaway).setVisibility(View.VISIBLE);
            }
            else {
                findViewById(R.id.inbmlthreeico).setVisibility(View.VISIBLE);
                findViewById(R.id.inbmlthreeaway).setVisibility(View.GONE);

                rom = user.getSlots()[2].getDigiROM();
                commType = user.getSlots()[2].getCommType();
                ((ImageView) findViewById(R.id.inbmlthreeico)).setImageResource(getImageId(getIMGFileName(rom, commType)));
            }
        }
        else {
            findViewById(R.id.inbmlthreeico).setVisibility(View.GONE);
            findViewById(R.id.inbmlthreeaway).setVisibility(View.GONE);

        }

        if(slotCount >= 4) {
            if(user.getSlots()[3].isAway() && isNewChallenge) {
                findViewById(R.id.inbmlfourico).setVisibility(View.GONE);
                findViewById(R.id.inbmlfouraway).setVisibility(View.VISIBLE);
            }
            else {
                findViewById(R.id.inbmlfourico).setVisibility(View.VISIBLE);
                findViewById(R.id.inbmlfouraway).setVisibility(View.GONE);

                rom = user.getSlots()[3].getDigiROM();
                commType = user.getSlots()[3].getCommType();
                ((ImageView) findViewById(R.id.inbmlfourico)).setImageResource(getImageId(getIMGFileName(rom, commType)));
            }
        }
        else {
            findViewById(R.id.inbmlfourico).setVisibility(View.GONE);
            findViewById(R.id.inbmlfouraway).setVisibility(View.GONE);

        }

        if(slotCount >= 5) {
            if(user.getSlots()[4].isAway() && isNewChallenge) {
                findViewById(R.id.inbmlfiveico).setVisibility(View.GONE);
                findViewById(R.id.inbmlfiveaway).setVisibility(View.VISIBLE);
            }
            else {
                findViewById(R.id.inbmlfiveico).setVisibility(View.VISIBLE);
                findViewById(R.id.inbmlfiveaway).setVisibility(View.GONE);

                rom = user.getSlots()[4].getDigiROM();
                commType = user.getSlots()[4].getCommType();
                ((ImageView) findViewById(R.id.inbmlfiveico)).setImageResource(getImageId(getIMGFileName(rom, commType)));
            }
        }
        else {
            findViewById(R.id.inbmlfiveico).setVisibility(View.GONE);
            findViewById(R.id.inbmlfiveaway).setVisibility(View.GONE);

        }

        if(slotCount >= 6){
            if(user.getSlots()[5].isAway() && isNewChallenge) {
                findViewById(R.id.inbmlsixico).setVisibility(View.GONE);
                findViewById(R.id.inbmlsixaway).setVisibility(View.VISIBLE);
            }
            else {
                findViewById(R.id.inbmlsixico).setVisibility(View.VISIBLE);
                findViewById(R.id.inbmlsixaway).setVisibility(View.GONE);

                rom = user.getSlots()[5].getDigiROM();
                commType = user.getSlots()[5].getCommType();
                ( (ImageView) findViewById(R.id.inbmlsixico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
            }
        }
        else {
            findViewById(R.id.inbmlsixico).setVisibility(View.GONE);
            findViewById(R.id.inbmlsixaway).setVisibility(View.GONE);

        }
    }
    private void setMonListColours(boolean countCommType){
        ColorMatrix monomatrix = new ColorMatrix(), clrmatrix = new ColorMatrix();
        monomatrix.setSaturation(0);
        clrmatrix.setSaturation(1f);
        ColorMatrixColorFilter monofilter = new ColorMatrixColorFilter(monomatrix);
        ColorMatrixColorFilter clrfilter = new ColorMatrixColorFilter(clrmatrix);

        ImageView[] icos = new ImageView[] {
                (ImageView) findViewById(R.id.inbmloneico),
                (ImageView) findViewById(R.id.inbmltwoico),
                (ImageView) findViewById(R.id.inbmlthreeico),
                (ImageView) findViewById(R.id.inbmlfourico),
                (ImageView) findViewById(R.id.inbmlfiveico),
                (ImageView) findViewById(R.id.inbmlsixico)
        };

        int commType = (countCommType) ? currentChallenge().getCommType() : -1;
        for(int i = 0; i<slotCount; i++){
            //(user.getSlots()[i].getCommType() != commType && countCommType)
            if((!RomManager.checkCommTypesCompatibility(user.getSlots()[i].getCommType(), commType) && countCommType) || user.getSlots()[i].isLocked() || ( currentChallenge() != null && ( !currentChallenge().getLevel().equals("") && RomManager.levelVal(currentChallenge().getLevel()) < user.getSlots()[i].getLevelVal()) )) icos[i].setColorFilter(monofilter);
            else  icos[i].setColorFilter(clrfilter);
        }
    }

    private void slotClick() {
//                 if(!isNewChallenge && user.getSlots()[slotSel].getCommType() != currentChallenge().getCommType() || user.getSlots()[slotSel].isLocked() || (RomManager.levelVal(currentChallenge().getLevel()) < currentSlot().getLevelVal())) return;

        if(user.getSlots()[slotSel].isLocked()) {
            slotSel = 9;
            return;
        }
        else if(!isNewChallenge) {
            if (!RomManager.checkCommTypesCompatibility(user.getSlots()[slotSel].getCommType(), currentChallenge().getCommType()) || ( !currentChallenge().getLevel().equals("") && RomManager.levelVal(currentChallenge().getLevel()) < currentSlot().getLevelVal()))
                return;
        }


        //for(ChallengeN ch : challenges) {
        for(ChallengeN ch : challenges.values()) {
            if( (ch.getA().equals(user.getName()) && ch.getASlot() == slotSel && (ch.getState() == 1 || ch.getState() == 2)) ||
                    (ch.getB().equals(user.getName()) && ch.getBSlot() == slotSel && (ch.getState() == 4 || ch.getState() == 5)) ) {
                return;
            }
        }

        open[0] = open[1];
        open[1] = slotSel;

        anims("expand", findViewById(R.id.inbsndcenterico));
        ( (TextView) findViewById(R.id.inbsndinfotext)).setText(user.getSlots()[slotSel].getName());

        ( (ImageView) findViewById(R.id.inbsndcenterico) ).setImageResource(getImageId(getIMGFileName(user.getSlots()[slotSel].getDigiROM(), user.getSlots()[slotSel].getCommType())));

        if(open[0] < 6 && open[0] > -1) {
            if(!user.getSlots()[open[0]].isAway()) {
                switch(open[0]){
                    case 0:
                        anims("loseFocus", findViewById(R.id.inbmlbone));
                        anims("loseFocus", findViewById(R.id.inbmlboneshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmloneico));
                        break;
                    case 1:
                        anims("loseFocus", findViewById(R.id.inbmlbtwo));
                        anims("loseFocus", findViewById(R.id.inbmlbtwoshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmltwoico));
                        break;
                    case 2:
                        anims("loseFocus", findViewById(R.id.inbmlbthree));
                        anims("loseFocus", findViewById(R.id.inbmlbthreeshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmlthreeico));
                        break;
                    case 3:
                        anims("loseFocus", findViewById(R.id.inbmlbfour));
                        anims("loseFocus", findViewById(R.id.inbmlbfourshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmlfourico));
                        break;
                    case 4:
                        anims("loseFocus", findViewById(R.id.inbmlbfive));
                        anims("loseFocus", findViewById(R.id.inbmlbfiveshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmlfiveico));
                        break;
                    case 5:
                        anims("loseFocus", findViewById(R.id.inbmlbsix));
                        anims("loseFocus", findViewById(R.id.inbmlbsixshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmlsixico));
                        break;

                }
            }
        }
        switch(open[1]){
            case 0:
                anims("focus", findViewById(R.id.inbmlbone));
                anims("focus", findViewById(R.id.inbmlboneshadow));
                anims("negFocus", findViewById(R.id.inbmloneico));
                break;
            case 1:
                anims("focus", findViewById(R.id.inbmlbtwo));
                anims("focus", findViewById(R.id.inbmlbtwoshadow));
                anims("negFocus", findViewById(R.id.inbmltwoico));
                break;
            case 2:
                anims("focus", findViewById(R.id.inbmlbthree));
                anims("focus", findViewById(R.id.inbmlbthreeshadow));
                anims("negFocus", findViewById(R.id.inbmlthreeico));
                break;
            case 3:
                anims("focus", findViewById(R.id.inbmlbfour));
                anims("focus", findViewById(R.id.inbmlbfourshadow));
                anims("negFocus", findViewById(R.id.inbmlfourico));
                break;
            case 4:
                anims("focus", findViewById(R.id.inbmlbfive));
                anims("focus", findViewById(R.id.inbmlbfiveshadow));
                anims("negFocus", findViewById(R.id.inbmlfiveico));
                break;
            case 5:
                anims("focus", findViewById(R.id.inbmlbsix));
                anims("focus", findViewById(R.id.inbmlbsixshadow));
                anims("negFocus", findViewById(R.id.inbmlsixico));
                break;

        }

    }
    private void findArduino() {
        srch = true;
        if(ardu != null && ardu.isOpened()) ardu.close();
        Runnable task = new Runnable()
        {
            public void run()
            {
                do{
                    if(!srch) break;
                    sleep(900);
                    if(!srch) break;
                    ardu = new Physicaloid(th);
                    if(!ardu.isOpened()) ardu.open();
                    sleep(150);
                }
                while(!ardu.isOpened() && srch);

                if(ardu.isOpened()){
                    alpha = new AlphaEngine(ardu);
                    if(((TextView)findViewById(R.id.inbinfotext)).getText().toString().contains("Accept Challenge")){
                        challengeConnect(0);
                    }
                    else if(((TextView)findViewById(R.id.inbinfotext)).getText().toString().contains("Complete Challenge")){
                        challengeConnect(1);
                    }
                    sleep(750);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.inbcsml).setVisibility(View.GONE);
                            findViewById(R.id.inbcbig).setVisibility(View.GONE);

                            anims("close", findViewById(R.id.inbcnctardu));
                            anims("expandslow", findViewById(R.id.inbcountdownloading));

                            Animation loadbar = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.loadingboxinsideexpand);
                            findViewById(R.id.inbloadingboxinside).setVisibility(View.VISIBLE);
                            loadbar.setAnimationListener(new Animation.AnimationListener() {
                                @Override public void onAnimationStart(Animation animation) {}
                                @Override public void onAnimationRepeat(Animation animation) {}
                                @Override public void onAnimationEnd(Animation animation) {
                                    ((TextView)findViewById(R.id.inbcancelscan)).setText("CANCEL");
                                    anims("close", findViewById(R.id.inbcountdownloading));
                                    anims("expandslow", findViewById(R.id.inbscanvpet));
                                    findViewById(R.id.inbscanvpet).getAnimation().setAnimationListener(new Animation.AnimationListener() {
                                        @Override public void onAnimationStart(Animation animation) {}
                                        @Override public void onAnimationRepeat(Animation animation) {}
                                        @Override public void onAnimationEnd(Animation animation) {
                                            switch(currentChallenge().getCommType()) {
                                                case 1:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("<Pendulum 20th VPet>");
                                                    break;
                                                case 3:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("<Digimon 20th VPet>");
                                                    break;
                                                case 7:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("<Digimon Original VPet>");
                                                    break;
                                                case 8:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("<Pendulum Original VPet>");
                                                    break;
                                                case 9:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("<Pendulum X VPet>");
                                                    break;
                                                case 91:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("<Digimon Mini VPet V1>");
                                                    break;
                                                case 92:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("<Digimon Mini VPet V2>");
                                                    break;
                                                case 93:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("<Digimon Mini VPet V3>");
                                                    break;
                                                default:
                                                    ((TextView)findViewById(R.id.inbcommtext)).setText("ERROR");
                                                    break;
                                            }
                                            animateCountDown();
                                        }
                                    });
                                }
                            });
                            findViewById(R.id.inbloadingboxinside).startAnimation(loadbar);
                        }
                    });

                }
                else if(ardu != null && !srch) {
                        findArduino();
                }
            }
        };
        findThread = new Thread(task);
        findThread.setDaemon(true);
        findThread.start();
    }
    public void presses() {
        del = true;
        chrgcnt = 0;
        findViewById(R.id.inbchargeone).setVisibility(View.VISIBLE);
        findViewById(R.id.inbchargetwo).setVisibility(View.GONE);
        findViewById(R.id.inbchargethree).setVisibility(View.GONE);
        findViewById(R.id.inbchargefour).setVisibility(View.GONE);
        findViewById(R.id.inbchargefive).setVisibility(View.GONE);
        findViewById(R.id.inbchargesix).setVisibility(View.GONE);
        findViewById(R.id.inbchargeseven).setVisibility(View.GONE);

        anims("close", findViewById(R.id.inbmon));
        anims("expand", findViewById(R.id.inbpresswin));

        Handler h = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                canPress = false;
                findViewById(R.id.inbpresswin).setVisibility(View.INVISIBLE);
                findViewById(R.id.inbncmon).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.inbinfotext)).setText("Sending...");
                anims("ringsTwo", null);

                postNewChallenge(ncOppName);
                canPress = true;
            }
        };
        h.postDelayed(r, 4200);
    }

    private void onClickUser(String unm) {
        UserN opp = new UserN();
        for(UserN u : base().getLeaderboard()) {
            if(u.getName().equals(unm)){
                opp = u;
                break;
            }
        }

        closeDropDown();
        ((TextView)findViewById(R.id.opptitle)).setText(opp.getName());
        ncOppName = opp.getName();

        // Opponent Tamer Card
        int[] themes = new int[] {  R.drawable.card_tamercard_grey, R.drawable.card_tamercard_blue,
                R.drawable.card_tamercard_pink, R.drawable.card_tamercard_green,
                R.drawable.card_tamercard_orange, R.drawable.card_tamercard_red,
                R.drawable.card_tamercard_black };

        findViewById(R.id.chaloppcardprofile).setBackgroundResource(themes[opp.getTheme()]);

        int bg = R.drawable.tamercard_box_grey;
        switch(opp.getTheme()) {
            case 0:
                bg = R.drawable.tamercard_box_grey;
                break;
            case 1:
                bg = R.drawable.tamercard_box_blue;
                break;
            case 2:
                bg = R.drawable.tamercard_box_pink;
                break;
            case 3:
                bg = R.drawable.tamercard_box_green;
                break;
            case 4:
                bg = R.drawable.tamercard_box_orange;
                break;
            case 5:
                bg = R.drawable.tamercard_box_red;
                break;
            case 6:
                bg = R.drawable.tamercard_box_black;
                break;
            default: break;
        }
        ((TextView) findViewById(R.id.chaloppcardunm)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcarduserwins)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcarduserloss)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcarduserdraws)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcardrank)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcardalphaid)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcardposition)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcardpoints)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcardpartner)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcardpartner2)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcardjoineddate)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.chaloppcardpartner2)).setAlpha(opp.getTheme() == 6 ? 0.2f : 0.4f);

        ((TextView) findViewById(R.id.chaloppcardunm)).setText(opp.getName());
        ((TextView) findViewById(R.id.chaloppcarduserwins)).setText(""+opp.getStats().getWin());
        ((TextView) findViewById(R.id.chaloppcarduserloss)).setText(""+opp.getStats().getLoss());
        ((TextView) findViewById(R.id.chaloppcarduserdraws)).setText(""+opp.getStats().getDraw());
        ((TextView) findViewById(R.id.chaloppcardrank)).setText(opp.getStats().getRank().toUpperCase());
        ((TextView) findViewById(R.id.chaloppcardalphaid)).setText(opp.getAid());
        ((TextView) findViewById(R.id.chaloppcardposition)).setText("# " + opp.getPosition(base().getLeaderboard()));
        ((TextView) findViewById(R.id.chaloppcardpoints)).setText(""+opp.getStats().getScore());

        String joined = new SimpleDateFormat("dd/MM/yyyy").format(new Date(opp.getJoined()));
        ((TextView) findViewById(R.id.chaloppcardjoineddate)).setText(joined);

        SlotN partner = opp.getMostBattledPartner();
        if (partner != null) {
            ((TextView) findViewById(R.id.chaloppcardpartner)).setText(partner.getName().equals("") ? getName(partner.getDigiROM(), partner.getCommType()) : partner.getName());
            ((ImageView) findViewById(R.id.chaloppcardpartnermonico) ).setImageResource(getImageId(getIMGFileName(partner.getDigiROM(), partner.getCommType())));
            ((ImageView) findViewById(R.id.chaloppcardpartnermonico) ).setVisibility(View.VISIBLE);
        }
        else {
            ((TextView) findViewById(R.id.chaloppcardpartner)).setText("N/A");
            ((ImageView) findViewById(R.id.chaloppcardpartnermonico) ).setVisibility(View.GONE);
        }

        // Opponent Slots
        if(opp.getName().equals(user.getName())) findViewById(R.id.oppchallengebtn).setVisibility(View.GONE);
        else findViewById(R.id.oppchallengebtn).setVisibility(View.VISIBLE);

        ImageView[] sls = new ImageView[] {
                findViewById(R.id.oppmloneico),
                findViewById(R.id.oppmltwoico),
                findViewById(R.id.oppmlthreeico),
                findViewById(R.id.oppmlfourico),
                findViewById(R.id.oppmlfiveico),
                findViewById(R.id.oppmlsixico) };

        int oppSlotCount = opp.getSlots().length;
        if(oppSlotCount > 0) {
            SlotN s = opp.getSlots()[0];
            sls[0].setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.oppctidone)).setVisibility(View.VISIBLE);
            sls[0].setImageResource(getImageId(getIMGFileNameFromRom(s.getDigiROM(), s.getCommType())));

            String ct = "";
            switch(s.getCommType()) {
                case 1:
                    ct = "  Pen20  ";
                    ((TextView)findViewById(R.id.oppctidone)).setBackgroundResource(R.drawable.commtypebgpent);
                    break;
                case 3:
                    ct = "  DM20  ";
                    ((TextView)findViewById(R.id.oppctidone)).setBackgroundResource(R.drawable.commtypebgdmt);
                    break;
                case 7:
                    ct = "  DMOG  ";
                    ((TextView)findViewById(R.id.oppctidone)).setBackgroundResource(R.drawable.commtypebgdmog);
                    break;
                case 8:
                    ct = "  PenOG  ";
                    ((TextView)findViewById(R.id.oppctidone)).setBackgroundResource(R.drawable.commtypebgpenog);
                    break;
                case 9:
                    ct = "  PenX  ";
                    ((TextView)findViewById(R.id.oppctidone)).setBackgroundResource(R.drawable.commtypebgpenx);
                    break;
                case 91:
                case 92:
                case 93:
                    ct = "  MINI  ";
                    ((TextView)findViewById(R.id.oppctidone)).setBackgroundResource(R.drawable.commtypebgmini);
                    break;
                default: break;
            }
            ((TextView)findViewById(R.id.oppctidone)).setText(ct);
        }
        else {
            sls[0].setVisibility(View.GONE);
            findViewById(R.id.oppctidone).setVisibility(View.GONE);
        }

        if(oppSlotCount > 1) {
            SlotN s = opp.getSlots()[1];
            sls[1].setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.oppctidtwo)).setVisibility(View.VISIBLE);
            sls[1].setImageResource(getImageId(getIMGFileNameFromRom(s.getDigiROM(), s.getCommType())));

            String ct = "";
            switch(s.getCommType()) {
                case 1:
                    ct = "  Pen20  ";
                    ((TextView)findViewById(R.id.oppctidtwo)).setBackgroundResource(R.drawable.commtypebgpent);
                    break;
                case 3:
                    ct = "  DM20  ";
                    ((TextView)findViewById(R.id.oppctidtwo)).setBackgroundResource(R.drawable.commtypebgdmt);
                    break;
                case 7:
                    ct = "  DMOG  ";
                    ((TextView)findViewById(R.id.oppctidtwo)).setBackgroundResource(R.drawable.commtypebgdmog);
                    break;
                case 8:
                    ct = "  PenOG  ";
                    ((TextView)findViewById(R.id.oppctidtwo)).setBackgroundResource(R.drawable.commtypebgpenog);
                    break;
                case 9:
                    ct = "  PenX  ";
                    ((TextView)findViewById(R.id.oppctidtwo)).setBackgroundResource(R.drawable.commtypebgpenx);
                    break;
                case 91:
                case 92:
                case 93:
                    ct = "  MINI  ";
                    ((TextView)findViewById(R.id.oppctidtwo)).setBackgroundResource(R.drawable.commtypebgmini);
                    break;
                default:
                    ct = "  ERR  ";
                    break;
            }
            ((TextView)findViewById(R.id.oppctidtwo)).setTextColor(Color.WHITE);
            ((TextView)findViewById(R.id.oppctidtwo)).setText(ct);
        }
        else {
            sls[1].setVisibility(View.GONE);
            findViewById(R.id.oppctidtwo).setVisibility(View.GONE);
        }

        if(oppSlotCount > 2) {
            SlotN s = opp.getSlots()[2];
            sls[2].setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.oppctidthree)).setVisibility(View.VISIBLE);
            sls[2].setImageResource(getImageId(getIMGFileNameFromRom(s.getDigiROM(), s.getCommType())));


            String ct = "";
            switch(s.getCommType()) {
                case 1:
                    ct = "  Pen20  ";
                    ((TextView)findViewById(R.id.oppctidthree)).setBackgroundResource(R.drawable.commtypebgpent);
                    break;
                case 3:
                    ct = "  DM20  ";
                    ((TextView)findViewById(R.id.oppctidthree)).setBackgroundResource(R.drawable.commtypebgdmt);
                    break;
                case 7:
                    ct = "  DMOG  ";
                    ((TextView)findViewById(R.id.oppctidthree)).setBackgroundResource(R.drawable.commtypebgdmog);
                    break;
                case 8:
                    ct = "  PenOG  ";
                    ((TextView)findViewById(R.id.oppctidthree)).setBackgroundResource(R.drawable.commtypebgpenog);
                    break;
                case 9:
                    ct = "  PenX  ";
                    ((TextView)findViewById(R.id.oppctidthree)).setBackgroundResource(R.drawable.commtypebgpenx);
                    break;
                case 91:
                case 92:
                case 93:
                    ct = "  MINI  ";
                    ((TextView)findViewById(R.id.oppctidthree)).setBackgroundResource(R.drawable.commtypebgmini);
                    break;
                default: break;
            }
            ((TextView)findViewById(R.id.oppctidthree)).setText(ct);
        }
        else {
            sls[2].setVisibility(View.GONE);
            findViewById(R.id.oppctidthree).setVisibility(View.GONE);
        }

        if(oppSlotCount > 3) {
            SlotN s = opp.getSlots()[3];
            sls[3].setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.oppctidfour)).setVisibility(View.VISIBLE);
            sls[3].setImageResource(getImageId(getIMGFileNameFromRom(s.getDigiROM(), s.getCommType())));


            String ct = "";
            switch(s.getCommType()) {
                case 1:
                    ct = "  Pen20  ";
                    ((TextView)findViewById(R.id.oppctidfour)).setBackgroundResource(R.drawable.commtypebgpent);
                    break;
                case 3:
                    ct = "  DM20  ";
                    ((TextView)findViewById(R.id.oppctidfour)).setBackgroundResource(R.drawable.commtypebgdmt);
                    break;
                case 7:
                    ct = "  DMOG  ";
                    ((TextView)findViewById(R.id.oppctidfour)).setBackgroundResource(R.drawable.commtypebgdmog);
                    break;
                case 8:
                    ct = "  PenOG  ";
                    ((TextView)findViewById(R.id.oppctidfour)).setBackgroundResource(R.drawable.commtypebgpenog);
                    break;
                case 9:
                    ct = "  PenX  ";
                    ((TextView)findViewById(R.id.oppctidfour)).setBackgroundResource(R.drawable.commtypebgpenx);
                    break;
                case 91:
                case 92:
                case 93:
                    ct = "  MINI  ";
                    ((TextView)findViewById(R.id.oppctidfour)).setBackgroundResource(R.drawable.commtypebgmini);
                    break;
                default: break;
            }
            ((TextView)findViewById(R.id.oppctidfour)).setText(ct);
        }
        else {
            sls[3].setVisibility(View.GONE);
            findViewById(R.id.oppctidfour).setVisibility(View.GONE);
        }

        if(oppSlotCount > 4) {
            SlotN s = opp.getSlots()[4];
            sls[4].setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.oppctidfive)).setVisibility(View.VISIBLE);
            sls[4].setImageResource(getImageId(getIMGFileNameFromRom(s.getDigiROM(), s.getCommType())));


            String ct = "";
            switch(s.getCommType()) {
                case 1:
                    ct = "  Pen20  ";
                    ((TextView)findViewById(R.id.oppctidfive)).setBackgroundResource(R.drawable.commtypebgpent);
                    break;
                case 3:
                    ct = "  DM20  ";
                    ((TextView)findViewById(R.id.oppctidfive)).setBackgroundResource(R.drawable.commtypebgdmt);
                    break;
                case 7:
                    ct = "  DMOG  ";
                    ((TextView)findViewById(R.id.oppctidfive)).setBackgroundResource(R.drawable.commtypebgdmog);
                    break;
                case 8:
                    ct = "  PenOG  ";
                    ((TextView)findViewById(R.id.oppctidfive)).setBackgroundResource(R.drawable.commtypebgpenog);
                    break;
                case 9:
                    ct = "  PenX  ";
                    ((TextView)findViewById(R.id.oppctidfive)).setBackgroundResource(R.drawable.commtypebgpenx);
                    break;
                case 91:
                case 92:
                case 93:
                    ct = "  MINI  ";
                    ((TextView)findViewById(R.id.oppctidfive)).setBackgroundResource(R.drawable.commtypebgmini);
                    break;
                default: break;
            }
            ((TextView)findViewById(R.id.oppctidfive)).setText(ct);
        }
        else {
            sls[4].setVisibility(View.GONE);
            findViewById(R.id.oppctidfive).setVisibility(View.GONE);
        }

        if(oppSlotCount > 5) {
            SlotN s = opp.getSlots()[5];
            sls[5].setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.oppctidsix)).setVisibility(View.VISIBLE);
            sls[5].setImageResource(getImageId(getIMGFileNameFromRom(s.getDigiROM(), s.getCommType())));


            String ct = "";
            switch(s.getCommType()) {
                case 1:
                    ct = "  Pen20  ";
                    ((TextView)findViewById(R.id.oppctidsix)).setBackgroundResource(R.drawable.commtypebgpent);
                    break;
                case 3:
                    ct = "  DM20  ";
                    ((TextView)findViewById(R.id.oppctidsix)).setBackgroundResource(R.drawable.commtypebgdmt);
                    break;
                case 7:
                    ct = "  DMOG  ";
                    ((TextView)findViewById(R.id.oppctidsix)).setBackgroundResource(R.drawable.commtypebgdmog);
                    break;
                case 8:
                    ct = "  PenOG  ";
                    ((TextView)findViewById(R.id.oppctidsix)).setBackgroundResource(R.drawable.commtypebgpenog);
                    break;
                case 9:
                    ct = "  PenX  ";
                    ((TextView)findViewById(R.id.oppctidsix)).setBackgroundResource(R.drawable.commtypebgpenx);
                    break;
                case 91:
                case 92:
                case 93:
                    ct = "  MINI  ";
                    ((TextView)findViewById(R.id.oppctidsix)).setBackgroundResource(R.drawable.commtypebgmini);
                    break;
                default: break;
            }
            ((TextView)findViewById(R.id.oppctidsix)).setText(ct);
        }
        else {
            sls[5].setVisibility(View.GONE);
            findViewById(R.id.oppctidsix).setVisibility(View.GONE);
        }

        // Animate Slots
        findViewById(R.id.oppctidtwo).setScaleX(0.001f);
        findViewById(R.id.oppmlbtwoshadow).setScaleX(0);
        findViewById(R.id.oppmlbtwo).setScaleX(0);
        findViewById(R.id.oppmltwoico).setScaleX(0);
        findViewById(R.id.oppctidfour).setScaleX(0);
        findViewById(R.id.oppmlbfourshadow).setScaleX(0);
        findViewById(R.id.oppmlbfour).setScaleX(0);
        findViewById(R.id.oppmlfourico).setScaleX(0);
        findViewById(R.id.oppctidsix).setScaleX(0);
        findViewById(R.id.oppmlbsixshadow).setScaleX(0);
        findViewById(R.id.oppmlbsix).setScaleX(0);
        findViewById(R.id.oppmlsixico).setScaleX(0);
        findViewById(R.id.oppctidfive).setScaleX(0);
        findViewById(R.id.oppmlbfiveshadow).setScaleX(0);
        findViewById(R.id.oppmlbfive).setScaleX(0);
        findViewById(R.id.oppmlfiveico).setScaleX(0);
        findViewById(R.id.oppctidthree).setScaleX(0);
        findViewById(R.id.oppmlbthreeshadow).setScaleX(0);
        findViewById(R.id.oppmlbthree).setScaleX(0);
        findViewById(R.id.oppmlthreeico).setScaleX(0);
        findViewById(R.id.oppctidone).setScaleX(0);
        findViewById(R.id.oppmlboneshadow).setScaleX(0);
        findViewById(R.id.oppmlbone).setScaleX(0);
        findViewById(R.id.oppmloneico).setScaleX(0);

        findViewById(R.id.oppctidtwo).setScaleY(0.001f);
        findViewById(R.id.oppmlbtwoshadow).setScaleY(0);
        findViewById(R.id.oppmlbtwo).setScaleY(0);
        findViewById(R.id.oppmltwoico).setScaleY(0);
        findViewById(R.id.oppctidfour).setScaleY(0);
        findViewById(R.id.oppmlbfourshadow).setScaleY(0);
        findViewById(R.id.oppmlbfour).setScaleY(0);
        findViewById(R.id.oppmlfourico).setScaleY(0);
        findViewById(R.id.oppctidsix).setScaleY(0);
        findViewById(R.id.oppmlbsixshadow).setScaleY(0);
        findViewById(R.id.oppmlbsix).setScaleY(0);
        findViewById(R.id.oppmlsixico).setScaleY(0);
        findViewById(R.id.oppctidfive).setScaleY(0);
        findViewById(R.id.oppmlbfiveshadow).setScaleY(0);
        findViewById(R.id.oppmlbfive).setScaleY(0);
        findViewById(R.id.oppmlfiveico).setScaleY(0);
        findViewById(R.id.oppctidthree).setScaleY(0);
        findViewById(R.id.oppmlbthreeshadow).setScaleY(0);
        findViewById(R.id.oppmlbthree).setScaleY(0);
        findViewById(R.id.oppmlthreeico).setScaleY(0);
        findViewById(R.id.oppctidone).setScaleY(0);
        findViewById(R.id.oppmlboneshadow).setScaleY(0);
        findViewById(R.id.oppmlbone).setScaleY(0);
        findViewById(R.id.oppmloneico).setScaleY(0);

        int cnt = 0;
        findViewById(R.id.oppctidtwo).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*55))
                .setDuration(195)
                .start();
        findViewById(R.id.oppmlbtwoshadow).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlbtwo).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmltwoico).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt++*50))
                .setDuration(180)
                .start();

        findViewById(R.id.oppctidfour).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*55))
                .setDuration(195)
                .start();
        findViewById(R.id.oppmlbfourshadow).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlbfour).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlfourico).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt++*50))
                .setDuration(180)
                .start();

        findViewById(R.id.oppctidsix).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*55))
                .setDuration(195)
                .start();
        findViewById(R.id.oppmlbsixshadow).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlbsix).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlsixico).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt++*50))
                .setDuration(180)
                .start();

        findViewById(R.id.oppctidfive).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*55))
                .setDuration(195)
                .start();
        findViewById(R.id.oppmlbfiveshadow).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlbfive).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlfiveico).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt++*50))
                .setDuration(180)
                .start();

        findViewById(R.id.oppctidthree).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*55))
                .setDuration(195)
                .start();
        findViewById(R.id.oppmlbthreeshadow).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlbthree).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlthreeico).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt++*50))
                .setDuration(180)
                .start();

        findViewById(R.id.oppctidone).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*55))
                .setDuration(195)
                .start();
        findViewById(R.id.oppmlboneshadow).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmlbone).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt*50))
                .setDuration(180)
                .start();
        findViewById(R.id.oppmloneico).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(750+(cnt++*50))
                .setDuration(180)
                .start();

        // Other Animations
        findViewById(R.id.chaloppcardprofile).setScaleX(0);
        findViewById(R.id.chaloppcardprofile).setScaleY(0);
        findViewById(R.id.chaloppcardprofile).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(400)
                .setDuration(220)
                .start();

        findViewById(R.id.oppchallengebtn).setScaleX(0);
        findViewById(R.id.oppchallengebtn).setScaleY(0);
        findViewById(R.id.oppchallengebtn).animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(1150)
                .setDuration(220)
                .start();



        anims("fadein", findViewById(R.id.inbbg));
        //anims("fadein", findViewById(R.id.inbopponentwin));
        anims("expand", findViewById(R.id.inbopponentwin));
    }
    private void onClickUserChallenge() {
        for(ChallengeN ch : challenges.values()) {
            if((ch.getA().equals(ncOppName) || ch.getB().equals(ncOppName)) && (ch.isActive())){
                makeToast("A Challenge is Already Active with this User", 1);
                return;
            }
        }
        isNewChallenge = true;
        findViewById(R.id.inblevellockcl).setVisibility(View.VISIBLE);

        initialiseMonList();
        setMonListColours(false);

        anims("close", findViewById(R.id.inbopponentwin));
        anims("expandslow", findViewById(R.id.inbchallengewin));
    }
    private void onClickChallengeCancel() {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                closeWindows();
                base().getChallengesFromDatabase(new Runnable() {
                    @Override
                    public void run() {
                        populateChallenges(1);
                    }
                });
            }
        };

        currentChallenge().setState(12);
        currentChallenge().updateChallengeInDatabase(task);
        notifyUser(currentChallenge().getOpp(), new UserNotify(user.getName(), -1, "12", "challenge"));
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                closeWindows();
//            }
//        }, 800);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                base().getChallengesFromDatabase();
//            }
//        }, 1400);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                populateChallenges(1);
//            }
//        }, 2000);
    }
    private void onClickChallenge() {
        initialiseWindows();
        int state = currentChallenge().getState(), commType = currentChallenge().getCommType(), chind;
        String index = RomManager.getIndexGeneral(currentChallenge().getDigiROM(), commType),
                opp = currentChallenge().getOpp();
        boolean isA = currentChallenge().isA();

//        for(chind = 0; chind<challenges.length; chind++) {
//            if(challenges[chind].getOpp().equals(chalOpp)) break;
//        }
//        challengeUsrs[chind].setTextColor(Color.parseColor("#383838"));


//        if(currentChallenge().isA() && currentChallenge().getWinner() == 0 && (state == 2 || state == 3) ) {
//            commType = user.getSlotFromIdentifier(currentChallenge().getASlot()).getCommType();
//        }
//        else if(!currentChallenge().isA() && currentChallenge().getWinner() == 1 && (state == 5 || state == 6) )
//            commType = user.getSlotFromIdentifier(currentChallenge().getBSlot()).getCommType();

        anims("fadein", findViewById(R.id.inbbg));
        anims("expand", findViewById(R.id.inbchallenge));

        if(currentChallenge().isA()) {
            ((TextView)findViewById(R.id.inbusername)).setText(currentChallenge().getA());
            ((TextView)findViewById(R.id.inbuserwin)).setText("W: " + currentChallenge().getAWins());
            ((TextView)findViewById(R.id.inbuserlos)).setText("L: " + currentChallenge().getALoss());
            ((TextView)findViewById(R.id.inbuserdrw)).setText("D: " + currentChallenge().getDraws());

            ((TextView)findViewById(R.id.inboppname)).setText(opp);
            ((TextView)findViewById(R.id.inbplwintitle)).setText(opp);
            ((TextView)findViewById(R.id.inboppwin)).setText("W: " + currentChallenge().getBWins());
            ((TextView)findViewById(R.id.inbopplos)).setText("L: " + currentChallenge().getBLoss());
            ((TextView)findViewById(R.id.inboppdrw)).setText("D: " + currentChallenge().getDraws());
        }
        else {
            ((TextView)findViewById(R.id.inbusername)).setText(user.getName());
            ((TextView)findViewById(R.id.inbuserwin)).setText("W: " + currentChallenge().getBWins());
            ((TextView)findViewById(R.id.inbuserlos)).setText("L: " + currentChallenge().getBLoss());
            ((TextView)findViewById(R.id.inbuserdrw)).setText("D: " + currentChallenge().getDraws());

            ((TextView)findViewById(R.id.inboppname)).setText(opp);
            ((TextView)findViewById(R.id.inbplwintitle)).setText(opp);
            ((TextView)findViewById(R.id.inboppwin)).setText("W: " + currentChallenge().getAWins());
            ((TextView)findViewById(R.id.inbopplos)).setText("L: " + currentChallenge().getALoss());
            ((TextView)findViewById(R.id.inboppdrw)).setText("D: " + currentChallenge().getDraws());
        }

        if(lastNotif != null && lastNotif.getFrom().contains(opp) && highlightChalTitle)
            ((TextView)findViewById(R.id.inbplwintitle)).setTextColor(Color.parseColor("#cf401d"));
        else ((TextView)findViewById(R.id.inbplwintitle)).setTextColor(Color.parseColor("#383838"/*"#BF3C0E05"*/));
        ((TextView)findViewById(R.id.inbchallengecardname)).setText(opp);
        findViewById(R.id.levellockid).setVisibility(View.GONE);


        if((state == 1 && isA) || (state == 4 && !isA)) // Challenge Sent
        {
            ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getMyCommType())));
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText("Challenged "+opp+" to Battle!");
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);
            findViewById(R.id.inbchallengecancelearly).setVisibility(View.VISIBLE);

            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            switch(commType) {
                case 1:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  Pen20  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgpent);
                    break;
                case 3:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  DM20  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgdmt);

                    break;
                case 7:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  DMOG  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgdmog);
                    break;
                case 8:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  PenOG  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgpenog);
                    break;
                case 9:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  PenX  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgpenx);
                    break;
                case 91:
                case 92:
                case 93:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  MINI  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgmini);
                    break;
            }
            findViewById(R.id.msgcommtypeid).setVisibility(View.VISIBLE);
            if(!currentChallenge().getLevel().equals("")) findViewById(R.id.levellockid).setVisibility(View.VISIBLE);
            else findViewById(R.id.levellockid).setVisibility(View.GONE);


        }
        if((state == 1 && !isA) || (state == 4 && isA)) // Challenge Received
        {
            String monName = "";

            index = RomManager.getIndexGeneral(currentChallenge().getDigiROM(), currentChallenge().getOppCommType());
            monName = getMonNameFromIndex(index, currentChallenge().getOppCommType());

            switch(commType) {
                case 1:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  Pen20  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgpent);
                    break;
                case 3:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  DM20  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgdmt);
                    break;
                case 7:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  DMOG  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgdmog);
                    break;
                case 8:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  PenOG  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgpenog);
                    break;
                case 9:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  PenX  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgpenx);
                    break;
                case 91:
                case 92:
                case 93:
                    ((TextView)findViewById(R.id.msgcommtypeid)).setText("  MINI  ");
                    ((TextView)findViewById(R.id.msgcommtypeid)).setBackgroundResource(R.drawable.commtypebgmini);
                    break;
                default: break;
            }

            ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getOppCommType())));
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText(opp+" Has Challenged you to Battle\nwith their "+ monName + "!");
            findViewById(R.id.inbchallengecontrols).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.inbchallengeaccept)).setText("ACCEPT");
            ((TextView)findViewById(R.id.inbchallengedecline)).setText("DECLINE");
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            findViewById(R.id.inbchallengecontrols).setVisibility(View.VISIBLE);
            findViewById(R.id.msgcommtypeid).setVisibility(View.VISIBLE);

            if(!currentChallenge().getLevel().equals("")) findViewById(R.id.levellockid).setVisibility(View.VISIBLE);
            else findViewById(R.id.levellockid).setVisibility(View.GONE);
        }

        if((state == 2 && isA) || (state == 5 && !isA)) // Accept Sent
        {
            int win = currentChallenge().getWinner();

            if(win == 0 && isA || win == 1 && !isA) {
                ((TextView)findViewById(R.id.inbchallengeinfo)).setText("You have Defeated " + opp + "!");
                ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getMyCommType())));
            }
            else if (win == 0 && !isA || win == 1 && isA) {
                ((TextView)findViewById(R.id.inbchallengeinfo)).setText(opp+" Won the Battle");
                ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getOppCommType())));
            }

            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);
        }
        if((state == 2 && !isA) || (state == 5 && isA)) // Accept Received
        {
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText(opp+" Has Accepted your Challenge!");
            ((ImageView) findViewById(R.id.challengeico)).setImageResource(R.drawable.ic_ico);

            findViewById(R.id.inbstats).setVisibility(View.GONE);

            findViewById(R.id.inbchallengecontrols).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.inbchallengeaccept)).setText("CONNECT");
            ((TextView)findViewById(R.id.inbchallengedecline)).setText("CANCEL");
        }

        if((state == 3 && isA) || (state == 6 && !isA)) // Challenge Resolved
        {
            int win = currentChallenge().getWinner();

            if(win == 0 && isA || win == 1 && !isA) {
                ((TextView)findViewById(R.id.inbchallengeinfo)).setText("You have Defeated " + opp + "!");
                ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getMyCommType())));
            }
            else if (win == 0 && !isA || win == 1 && isA) {
                ((TextView)findViewById(R.id.inbchallengeinfo)).setText(opp+" Won the Battle");
                ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getOppCommType())));
            }

//            ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getCommType())));
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);

        }
        if((state == 3 && !isA) || (state == 6 && isA)) // Challenge Resolved
        {
            int win = currentChallenge().getWinner();

            if(win == 0 && isA || win == 1 && !isA) {
                ((TextView)findViewById(R.id.inbchallengeinfo)).setText("You have Defeated " + opp + "!");
                ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getMyCommType())));
            }
            else if (win == 0 && !isA || win == 1 && isA) {
                ((TextView)findViewById(R.id.inbchallengeinfo)).setText(opp+" Won the Battle");
                ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getOppCommType())));
            }

//            ((ImageView) findViewById(R.id.challengeico)).setImageResource(getImageId(getIMGFileName(currentChallenge().getDigiROM(), currentChallenge().getCommType())));
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);
        }

        if((state == 7 && isA) || (state == 8 && !isA)) // Player Rejected
        {
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.challengeico)).setImageResource(R.drawable.ic_ico);
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText("You Have Declined a Challenge from "+opp);
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);

        }
        if((state == 7 && !isA) || (state == 8 && isA)) // Opponent Rejected
        {
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.challengeico)).setImageResource(R.drawable.ic_ico);
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText(opp + " Has Declined your Challenge");
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);
        }

        if((state == 9 && isA) || (state == 10 && !isA)) // Mon Deleted
        {
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.challengeico)).setImageResource(R.drawable.ic_ico);
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText("Challenged Cancelled");
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);

        }
        if((state == 9 && !isA) || (state == 10 && isA)) // Mon Deleted
        {
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.challengeico)).setImageResource(R.drawable.ic_ico);
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText("Challenge Cancelled");
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);
        }

        if(state == 11)                                      // Expired
        {
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.challengeico)).setImageResource(R.drawable.ic_ico);
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText("Challenge Expired");
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);

        }

        if(state == 12)                                      // Initiator Cancelled
        {
            findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.challengeico)).setImageResource(R.drawable.ic_ico);
            ((TextView)findViewById(R.id.inbchallengeinfo)).setText("Challenge Cancelled");
            findViewById(R.id.inbchallengecontrols).setVisibility(View.GONE);

        }
    }
    private void onClickChallengeAccept()  {
        isNewChallenge = false;
        int state = currentChallenge().getState();
        boolean isA = currentChallenge().isA();

        if((state == 1 && !isA) || (state == 4 && isA)) // Challenge Received
        {
            initialiseMonList();
            setMonListColours(true);

            anims("close", findViewById(R.id.inbchallenge));
            anims("expandslow", findViewById(R.id.inbchallengewin));
        }
    }
    private void onClickChallengeDecline() {
        int state = currentChallenge().getState();
        boolean isA = currentChallenge().isA();

        if((state == 1 && !isA) || (state == 4 && isA)) { // Challenge Received
            reject();
        }
    }
    private void challengeConnect(int s) {
        // 0 = accept challenge, 1 = complete challenge
        if(s == 0) {
            if(!thrd) {
                scanThread = new Thread(new Runnable()
                {
                    public void run()
                    {
                        final int commType = currentChallenge().getCommType();
                        final String oppRom = currentChallenge().getDigiROM();

                        alpha.setCommType(commType);
                        extRom = alpha.sendROM(oppRom);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                switch(extRom) {
                                    case "-1":
                                        anims("fail", null);
                                        ((TextView)findViewById(R.id.inbinfotext)).setText("Error connecting to device - try again");
                                        ((TextView)findViewById(R.id.inbcancelscan)).setText("RETRY");
                                        break;
                                    case "-2":  // Killed
                                        break;
                                    case "-3":  // ROM too short
                                        ((TextView)findViewById(R.id.inbinfotext)).setText(extRom);
                                        break;
                                    case "-4":  // Contains illegal characters
                                        ((TextView)findViewById(R.id.inbinfotext)).setText(extRom);
                                        break;
                                    default:
                                        String  scanned = getName(extRom, commType),
                                                slotmon = getMonName(user.getSlots()[slotSel].getDigiROM(), commType/*user.getSlots()[slotSel].getCommType()*/);
                                        log("MON", "scanned: " + scanned + ", " + slotmon);

                                        if(!scanned.equals(slotmon)) {
                                            ((TextView)findViewById(R.id.inbinfotext)).setText("Invalid Digimon Scanned");
                                            ((TextView)findViewById(R.id.inbcancelscan)).setText("RETRY");
                                            anims("closeRings", null);
                                        }
                                        else {
                                            anims("closeRings", null);
                                            findViewById(R.id.inbcbig).setVisibility(View.INVISIBLE);
                                            findViewById(R.id.inbcsml).setVisibility(View.INVISIBLE);
                                            boolean hasWon = false;
                                            switch(commType) {
                                                case 1:
                                                    pen20BCalc = new Pen20BattleCalculator(extRom, oppRom);
                                                    hasWon = pen20BCalc.getWinner();
                                                    break;
                                                case 3:
                                                    dm20BCalc = new DM20BattleCalculator(extRom, oppRom);
                                                    hasWon = dm20BCalc.getWinner();
                                                    break;
                                                case 7:
                                                    hasWon = RomManager.getDmogWinner(extRom);
                                                    break;
                                                case 8:
                                                    penogBCalc = new PenOGBattleCalculator(extRom, oppRom);
                                                    hasWon = penogBCalc.getWinner();
                                                    break;
                                                case 9:
                                                case 91:
                                                case 92:
                                                case 93:
                                                    penxBCalc = new PenXBattleCalculator(extRom, oppRom);
                                                    hasWon = penxBCalc.getWinner();
                                                    break;
                                                default: break;
                                            }

                                            if(currentChallenge().isA()) currentChallenge().setASlot(user.getSlots()[slotSel].getBirthdate());
                                            else currentChallenge().setBSlot(user.getSlots()[slotSel].getBirthdate());
                                            challengeConnectSuccess(hasWon);
                                        }
                                        break;
                                }
                            }
                        });
                    }
                });
                scanThread.setDaemon(true);
                scanThread.start();
            }
        }
        else if(s == 1) {
            if(!thrd) {
                scanThread = new Thread(new Runnable()
                {
                    public void run()
                    {
                        final int commType = currentChallenge().getCommType(), winner = currentChallenge().getWinner();
                        String preOppRom = currentChallenge().getDigiROM();
                        boolean isA = currentChallenge().isA(), win = true;

                        String index = RomManager.getIndexGeneral(currentChallenge().getDigiROM(), currentChallenge().getCommType());

                        if(winner == 0 && isA || winner == 1 && !isA) win = true;
                        else if (winner == 0 && !isA || winner == 1 && isA) win = false;

                        final String oppRom = RomManager.generateForcedV1FromBaseGeneral(preOppRom, commType, win);

                        alpha.setCommType(commType);
                        extRom = alpha.sendV1Rom(oppRom);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                switch(extRom) {
                                    case "-1":
                                        anims("fail", null);
                                        ((TextView)findViewById(R.id.inbinfotext)).setText("Do Not Initiate on VPet");
                                        ((TextView)findViewById(R.id.inbcancelscan)).setText("RETRY");
                                        break;
                                    case "-2":  // Killed
                                        break;
                                    case "-3":  // ROM too short
                                        ((TextView)findViewById(R.id.inbinfotext)).setText(extRom);
                                        break;
                                    case "-4":  // Contains illegal characters
                                        ((TextView)findViewById(R.id.inbinfotext)).setText(extRom);
                                        break;
                                    default:
                                        long ss = 0;
                                        if(currentChallenge().isA()) ss = currentChallenge().getASlot();
                                        else ss = currentChallenge().getBSlot();


                                        String scanned = getName(extRom, commType), slotmon;

                                        if(ss == -1) slotmon = scanned;
                                        else slotmon = getMonName(user.getSlotFromIdentifier(ss).getDigiROM(), user.getSlotFromIdentifier(ss).getCommType());

                                        log("MON", "scanned: " + scanned + ", " + slotmon);

                                        if(!slotmon.equals(scanned)){
                                            ((TextView)findViewById(R.id.inbinfotext)).setText("Invalid Digimon Scanned");
                                            anims("closeRings", null);
                                            anims("closeDelayed", findViewById(R.id.inbscanvpet));

                                            if(alpha != null) alpha.setkl();
                                            srch = false;
                                            if(ardu != null) {
                                                if(ardu.isOpened()) ardu.close();
                                            }
                                            if (findThread != null) findThread.interrupt();
                                            if(scanThread != null) {
                                                scanThread.interrupt();
                                            }
                                            findThread = null;
                                            scanThread = null;
                                        }
                                        else {
                                            anims("closeRings", null);
                                            findViewById(R.id.inbcbig).setVisibility(View.INVISIBLE);
                                            findViewById(R.id.inbcsml).setVisibility(View.INVISIBLE);

                                            challengeCompleteConnectSuccess();
                                        }

                                        break;
                                }
                            }
                        });
                    }
                });
                scanThread.setDaemon(true);
                scanThread.start();
            }
        }
    }
    private void challengeConnectSuccess(final boolean hasWon) {
        long sl;
        boolean isA = currentChallenge().isA();
        UserN opp = Utility.getUserFromLeaderboard(currentChallenge().getOpp());

        if(isA) {
            currentChallenge().setState(2);
            sl = currentChallenge().getASlot();
            currentChallenge().setACommType(user.getSlotFromIdentifier(currentChallenge().getASlot()).getCommType());
        }
        else {
            currentChallenge().setState(5);
            sl = currentChallenge().getBSlot();
            currentChallenge().setBCommType(user.getSlotFromIdentifier(currentChallenge().getBSlot()).getCommType());
        }

        if(hasWon){
            // player won
            user.winsUp(opp.getStats().getRank());
            user.getSlotFromIdentifier(sl).winsUp();
            user.updateUserInDatabase();

            if(isA) {
                currentChallenge().setWinner(0);
                currentChallenge().setDigiROM(extRom);
                currentChallenge().increaseAWins();
                currentChallenge().increaseBLoss();
            }
            else {
                currentChallenge().setWinner(1);
                currentChallenge().setDigiROM(extRom);
                currentChallenge().increaseBWins();
                currentChallenge().increaseALoss();
            }
        }
        else {
            // player lost
            user.lossUp();
            SlotN sss = user.getSlotFromIdentifierTwo(sl);
            user.getSlotFromIdentifierTwo(sl).lossUp();
            user.updateUserInDatabase();

            if(isA) {
                currentChallenge().setWinner(1);
                //currentChallenge().setDigiROM(extRom);
                currentChallenge().increaseBWins();
                currentChallenge().increaseALoss();
            }
            else {
                currentChallenge().setWinner(0);
                //currentChallenge().setDigiROM(extRom);
                currentChallenge().increaseAWins();
                currentChallenge().increaseBLoss();
            }
        }

        currentChallenge().updateLastseen();
        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updateChallenge",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            closeWindows();
                            notifyUser(currentChallenge().getOpp(), new UserNotify(user.getName(), -1, ""+currentChallenge().getState(), "challenge"));
                            populateChallenges(1);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("UpdateChallengeInDatabaseErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() { return currentChallenge().toJsonString().getBytes(); }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        rQ.add(strRe);
    }
    private void challengeCompleteConnectSuccess() {
        int win = currentChallenge().getWinner();
        long sl;
        boolean isA = currentChallenge().isA();
        UserN opp = Utility.getUserFromLeaderboard(currentChallenge().getOpp());

        if(isA) {
            currentChallenge().setState(3);
            sl = currentChallenge().getASlot();
            currentChallenge().setASlot(currentChallenge().getASlot()*-1);
            currentChallenge().setBSlot(currentChallenge().getBSlot()*-1);
            currentChallenge().setACommType(user.getSlotFromIdentifier(currentChallenge().getASlot()).getCommType());
        }
        else {
            currentChallenge().setState(6);
            sl = currentChallenge().getBSlot();
            currentChallenge().setASlot(currentChallenge().getASlot()*-1);
            currentChallenge().setBSlot(currentChallenge().getBSlot()*-1);
            currentChallenge().setBCommType(user.getSlotFromIdentifier(currentChallenge().getBSlot()).getCommType());
        }

        // Update Win/Loss
        if(win == 0 && isA || win == 1 && !isA) {
            user.winsUp(opp.getStats().getRank());
            user.getSlotFromIdentifier(sl).winsUp();
            user.updateUserInDatabase();
        }
        else if (win == 0 && !isA|| win == 1 && isA) {
            user.lossUp();
            user.getSlotFromIdentifier(sl).lossUp();
            user.updateUserInDatabase();
        }

        currentChallenge().updateLastseen();

        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updateChallenge",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            closeWindows();
                            populateChallenges(1);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("UpdateChallengeInDatabaseErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() { return currentChallenge().toJsonString().getBytes(); }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        rQ.add(strRe);
    }
    private void reject() {
        currentChallenge().setState(currentChallenge().isA() ? 7:8);
        currentChallenge().updateLastseen();

        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updateChallenge",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            closeWindows();
                            notifyUser(currentChallenge().getOpp(), new UserNotify(user.getName(), -1, ""+currentChallenge().getState(), "challenge"));
                            populateChallenges(1);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("UpdateChallengeInDatabaseErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() { return currentChallenge().toJsonString().getBytes(); }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        rQ.add(strRe);
    }

    private void postNewChallenge(final String oppName) {
        int pr = (chrgcnt>31) ? 31 : chrgcnt;
        if(BaseActivity.getInstance().getAllChallengesN().getChallengeWith(oppName) != null) {
            if(currentSlot() == null) {
                makeToast("Something went wrong... [PNC0]", 1);
                closeWindows();
                return;
            }

            for(int i = 0; i<challenges.size(); i++) {
                ChallengeN chali = challenges.get(challengesKeys.get(i));
                if( chali.getOpp().equals(oppName)) {
                    chalSel = i;
                    String drr = chali.getDigiROM();
                    switch(currentSlot().getCommType()) {
                        case 1:
                            pr = (int) Math.floor(((float)pr)/1.9375f);
                            currentChallenge().setDigiROM(RomManager.generatePen20RomV2FromBase(currentSlot().getDigiROM(), pr));
                            currentChallenge().setCommType(1);
                            break;
                        case 3:
                            currentChallenge().setDigiROM(RomManager.generateDM20RomV2FromBase(currentSlot().getDigiROM(), pr));
                            currentChallenge().setCommType(3);
                            break;
                        case 7:
                            currentChallenge().setDigiROM(currentSlot().getDigiROM());
                            currentChallenge().setCommType(7);
                            break;
                        case 8:
                            pr = (int) Math.floor(((float)pr)/1.9375f);
                            currentChallenge().setDigiROM(RomManager.generatePenOGRomV2FromBase(currentSlot().getDigiROM(), pr));
                            currentChallenge().setCommType(8);
                            break;
                        case 9:
                            currentChallenge().setDigiROM(RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr));
                            currentChallenge().setCommType(9);
                            break;
                        case 91:
                            currentChallenge().setDigiROM(RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr));
                            currentChallenge().setCommType(91);
                            break;
                        case 92:
                            currentChallenge().setDigiROM(RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr));
                            currentChallenge().setCommType(92);
                            break;
                        case 93:
                            currentChallenge().setDigiROM(RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr));
                            currentChallenge().setCommType(93);
                            break;
                        default: break;
                    }

                    if(currentChallenge() == null) {
                        makeToast("Something went wrong... [PNC1]", 1);
                        closeWindows();
                        return;
                    }

                    if(currentChallenge().isA()){
                        currentChallenge().setASlot(currentSlot().getBirthdate());
                        currentChallenge().setState(1);
                    }
                    else{
                        currentChallenge().setBSlot(currentSlot().getBirthdate());
                        currentChallenge().setState(4);
                    }

                    currentChallenge().updateLastseen();

                    break;
                }
            }

            if(currentChallenge() != null) currentChallenge().updateLastseen();

            String level = RomManager.getLevelGeneral(currentSlot().getDigiROM(), currentSlot().getCommType());
            currentChallenge().setLevel((((Switch)findViewById(R.id.inblevellockswitch)).isChecked()) ? level : "");

            currentChallenge().setMyCommType(currentSlot().getCommType());

            RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
            StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updateChallenge",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response != null) {
                                del = false;
                                closeWindows();
                                notifyUser(oppName, new UserNotify(user.getName(), -1, ""+currentChallenge().getState(), "challenge"));
                                populateChallenges(1);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override public void onErrorResponse(VolleyError error) { Log.e("UpdateUserInDatabaseErr", error.getMessage()); }
                    }
            ) {
                @Override public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type","application/json");
                    return headers;
                }
                @Override public byte[] getBody() { return currentChallenge().toJsonString().getBytes(); }
                @Override public String getBodyContentType() {
                    return "application/json";
                }
            };

            rQ.add(strRe);

        }
        else {
            String rom = "";
            switch(currentSlot().getCommType()) {
                case 1:
                    pr = (int) Math.floor(((float)pr)/1.9375f);
                    rom = RomManager.generatePen20RomV2FromBase(currentSlot().getDigiROM(), pr);
                    break;
                case 3:
                    rom = RomManager.generateDM20RomV2FromBase(currentSlot().getDigiROM(), pr);
                    break;
                case 7:
                    rom = currentSlot().getDigiROM();
                    break;
                case 8:
                    pr = (int) Math.floor(((float)pr)/1.9375f);
                    rom = RomManager.generatePenOGRomV2FromBase(currentSlot().getDigiROM(), pr);
                    break;
                case 9:
                case 91:
                case 92:
                case 93:
                    rom = RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr);
                    break;
                default: break;
            }

            String level = (((Switch)findViewById(R.id.inblevellockswitch)).isChecked()) ?
                    RomManager.getLevelGeneral(currentSlot().getDigiROM(), currentSlot().getCommType()) : "";

            final ChallengeN ch = new ChallengeN(oppName, rom, currentSlot().getCommType(), currentSlot().getBirthdate(), level);

            RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
            StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/addChallenge",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response != null) {
                                del = false;

                                notifyUser(oppName, new UserNotify(user.getName(), -1, ""+ch.getState(), "challenge"));
                                base().addChallenge(ch);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        populateChallenges(1);
                                    }
                                }, 1000);

                                closeWindows();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override public void onErrorResponse(VolleyError error) { Log.e("UpdateUserInDatabaseErr", error.getMessage()); }
                    }
            ) {
                @Override public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type","application/json");
                    return headers;
                }
                @Override public byte[] getBody() { return ch.toJsonString().getBytes(); }
                @Override public String getBodyContentType() {
                    return "application/json";
                }
            };

            rQ.add(strRe);
        }
    }
    public void onNewChallenge() {
        Utility.postDelayed(1500, new Runnable() {
            @Override
            public void run() {
                Challenges.getInstance().populateChallenges(1);
            }
        });
    }

    private void getConversationWith(String other) {
        String URL = "https://www.alphahub.site/alphalink/api/protected/getmessageswith/"+user.getName()+"/"+other;

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String conversationString = response.toString();
                        conversation = (new Gson()).fromJson(response.toString(), Conversation.class);
                        populateConversation();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        conversation = new Conversation();
                        populateConversation();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };
        
        rQ.add(obRe);
    }
    private void getMessageFromId(final int rowid) {
        String URL = "https://www.alphahub.site/alphalink/api/protected/getmessagesfromid/"+rowid;

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Message mess = new Gson().fromJson(response.toString(), Message.class);

                        if(messagesIsOpen){
                            if( !user.getName().equals(mess.getFrom()) &&
                                !mess.equals(conversation.getLastMessage())  &&
                                mess.getFrom().equals(conversation.getConversant(user.getName()))   )
                                    appendMessage(mess, false);
                        }
                        else {
                            newMessageHighlightName(mess);
                        }

                        //appendMessage(mess, false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        log("Get From ID", "Null");
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }
    private void sendMessageTo(String recipient) {
    Message msg = new Message();
    msg.setFrom(user.getName());
    msg.setTo(recipient);
    msg.setWhen(Utility.getTime());
    msg.setWhat(((EditText)findViewById(R.id.messagetextfield)).getText().toString());

    final Message msgTwo = msg;
    String URL = "https://www.alphahub.site/alphalink/api/protected/sendmessage/";

    RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
    StringRequest strRe = new StringRequest(Request.Method.POST, URL,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    final SendMessageResponse resp = (new Gson()).fromJson(response, SendMessageResponse.class);

                    if(resp != null) {
                        appendMessage(msgTwo, true);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                String mm = msgTwo.getShortWhat();
                                UserNotify oppNotify = new UserNotify(msgTwo.getFrom(), resp.getRowid(), mm, "message");
                                notifyUser(msgTwo.getTo(), oppNotify);
                            }
                        }, 250);
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // Handle error
                }
            })  {

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type","application/json");
            return headers;
        }

        @Override
        public byte[] getBody() {
            return msgTwo.toJsonString().getBytes();
        }

        @Override
        public String getBodyContentType() {
            return "application/json";
        }
    };

    rQ.add(strRe);
}
    public void onNewMessage(UserNotify dmNotif) {
        lastNotif = dmNotif;
        highlightChalTitle = true;
        getMessageFromId(dmNotif.getRowid());
    }

    private void newMessageHighlightName(final Message mess) {
        for(int i = 0; i < challengeCls.length; i++) {
            if(challengeUsrs[i].getText().toString().equals(mess.getFrom())) {
                final int z = i;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        challengeUsrs[z].setTextColor(Color.parseColor("#cf401d"));
                    }
                });
            }
        }
    }
    private void notifyUser(final String name, UserNotify notif) {
        MQTTInterface.publishMessage(name, notif);
    }

    private void anims(String cmd, final View v) {
        Animation
                focus = AnimationUtils.loadAnimation(this, R.anim.clickfocus),
                loseFocus = AnimationUtils.loadAnimation(this, R.anim.clicklosefocus),
                pulse = AnimationUtils.loadAnimation(this, R.anim.pulse),
                expand = AnimationUtils.loadAnimation(this, R.anim.expandone),
                close = AnimationUtils.loadAnimation(this, R.anim.closeone),
                expandslow = AnimationUtils.loadAnimation(this, R.anim.expandoneslow),
                closefast = AnimationUtils.loadAnimation(this, R.anim.closefast),
                closedelayed = AnimationUtils.loadAnimation(this, R.anim.closedelayed),
                fade = AnimationUtils.loadAnimation(this, R.anim.fade),
                fadein = AnimationUtils.loadAnimation(this, R.anim.fadein),
                rcw = AnimationUtils.loadAnimation(this, R.anim.rotatecw),
                rccw = AnimationUtils.loadAnimation(this, R.anim.rotateccw),
                dropdownopen = AnimationUtils.loadAnimation(this, R.anim.dropdownopen),
                dropdownclose = AnimationUtils.loadAnimation(this, R.anim.dropdownclose),
                pulsering = AnimationUtils.loadAnimation(this, R.anim.pulsering);

        if(v != null) v.setVisibility(View.VISIBLE);

        switch(cmd) {
            case "focus":
                v.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200);
                break;
            case "loseFocus":
                v.animate().scaleX(1.0f).scaleY(1.0f).setDuration(200);
                break;
            case "negFocus":
                v.animate().scaleX(-1.2f).scaleY(1.2f).setDuration(200);
                break;
            case "negLoseFocus":
                v.animate().scaleX(-1.0f).scaleY(1.0f).setDuration(200);
                break;
            case "pulse":
                v.startAnimation(pulse);
                break;
            case "expand":
                v.startAnimation(expand);
                break;
            case "close":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(close);
                break;
            case "expandslow":
                v.startAnimation(expandslow);
                break;
            case "closefast":
                v.startAnimation(closefast);
                v.setVisibility(View.GONE);
                break;
            case "closeDelayed":
                anims("fade", findViewById(R.id.inbbg));
                closedelayed.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}

                });
                v.startAnimation(closedelayed);
                break;
            case "fade":
                fade.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(fade);
                break;
            case "fadein":
                v.startAnimation(fadein);
                break;
            case "rings":
                ringsSpin = true;
                findViewById(R.id.inbscanvpet).setVisibility(View.VISIBLE);
                findViewById(R.id.inbinformation).setVisibility(View.VISIBLE);
                findViewById(R.id.inbimghldr).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.inbcsml)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.inbcbig)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.inbcsml)).startAnimation(rcw);
                ((ImageView) findViewById(R.id.inbcbig)).startAnimation(rccw);
                break;
            case "closeRings":
                ringsSpin = false;
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.inbcsml).setVisibility(View.GONE);
                        findViewById(R.id.inbcbig).setVisibility(View.GONE);}
                });
                ((ImageView) findViewById(R.id.inbcsml)).startAnimation(close);
                ((ImageView) findViewById(R.id.inbcbig)).startAnimation(close);
                break;
            case "ringsTwo":
                ringsSpinTwo = true;
                ((ImageView) findViewById(R.id.inbnccsml)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.inbnccbig)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.inbnccsml)).startAnimation(rcw);
                ((ImageView) findViewById(R.id.inbnccbig)).startAnimation(rccw);
                break;
            case "closeRingsTwo":
                ringsSpinTwo = false;
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.sndcsml).setVisibility(View.GONE);
                        findViewById(R.id.sndcbig).setVisibility(View.GONE);}
                });
                ((ImageView) findViewById(R.id.inbnccsml)).startAnimation(close);
                ((ImageView) findViewById(R.id.inbnccbig)).startAnimation(close);
                break;
            case "fail":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.inbcsml).setVisibility(View.GONE);
                        findViewById(R.id.inbcbig).setVisibility(View.GONE);
                        findViewById(R.id.inbinfocenterico).setVisibility(View.GONE);}
                });
                ((ImageView) findViewById(R.id.inbcsml)).startAnimation(close);
                ((ImageView) findViewById(R.id.inbcbig)).startAnimation(close);
                break;
            case "scs":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.inbcsml).setVisibility(View.GONE);
                        findViewById(R.id.inbcbig).setVisibility(View.GONE);}
                });
                ((ImageView) findViewById(R.id.inbcsml)).startAnimation(close);
                ((ImageView) findViewById(R.id.inbcbig)).startAnimation(close);
                ((ImageView) findViewById(R.id.inbinfocenterico)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.inbinfocenterico)).startAnimation(expand);
                break;

            case "dropdownopen":
                ConstraintLayout constraintLayout = findViewById(R.id.screentwo);
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(constraintLayout);
                constraintSet.connect(R.id.searchbar,ConstraintSet.TOP,R.id.screentwo,ConstraintSet.TOP,0);
                constraintSet.applyTo(constraintLayout);

                v.startAnimation(dropdownopen);
                dropDown = true;
                findViewById(R.id.chtitle).setVisibility(View.GONE);
                break;
            case "dropdownclose":
                findViewById(R.id.chtitle).setVisibility(View.VISIBLE);
                dropdownclose.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);
                    }
                });
                v.startAnimation(dropdownclose);
                dropDown = false;

                ConstraintLayout constraintLayouto = findViewById(R.id.screentwo);
                ConstraintSet constraintSeto = new ConstraintSet();
                constraintSeto.clone(constraintLayouto);
                constraintSeto.connect(R.id.searchbar,ConstraintSet.TOP,R.id.screentwo,ConstraintSet.TOP,(int) dpToPx(82));
                constraintSeto.applyTo(constraintLayouto);
                break;
            case "pulsering":
                findViewById(R.id.inbpresspulsering).setVisibility(View.VISIBLE);

                pulsering.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.inbpresspulsering).setVisibility(View.GONE);}
                });

                findViewById(R.id.inbpresspulsering).startAnimation(pulsering);
                break;
            case "openMessages":
                messagesIsOpen = true;
                ConstraintLayout mwo = findViewById(R.id.messageswin);

                mwo.animate()
                        .setStartDelay(220)
                        .translationX(0)
                        .setDuration(200)
                        .start();

                break;
            case "closeMessages":
                messagesIsOpen = false;
                ConstraintLayout mwc = findViewById(R.id.messageswin);

                mwc.animate()
                        .setStartDelay(100)
                        .translationX(mwc.getWidth())
                        .setDuration(200);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cleanMessages();
                        if(msgChIsExp) anims("closeChallengeStats", null);
                        ((TextView) findViewById(R.id.msgtitlebartitle)).setText("");
                    }
                }, 300);
                break;
            case "expandChallengeStats":
                findViewById(R.id.msgchallengetopbg).setPivotY(0);
                findViewById(R.id.msgchallengetopbg).animate()
                        .scaleY(1.0f)
                        .setDuration(200);

                msgChIsExp = true;
                findViewById(R.id.messageschallengescorestats).animate()
                        .alpha(1)
                        .setDuration(150)
                        .setStartDelay(240)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                findViewById(R.id.messageschallengescorestats).setVisibility(View.VISIBLE);
                            }});

                break;
            case "closeChallengeStats":
                msgChIsExp = false;
                findViewById(R.id.messageschallengescorestats).setVisibility(View.GONE);

                findViewById(R.id.msgchallengetopbg).setPivotY(0);
                findViewById(R.id.msgchallengetopbg).animate()
                        .setStartDelay(150)
                        .scaleY(0f)
                        .setDuration(200);
                break;
            default:
                break;
        }
    }
    private void animateCountDown() {
        ((ImageView) findViewById(R.id.inbinfocenterico))
                .setImageResource(R.drawable.ic_cntdwn_three);

        findViewById(R.id.inbcsml).clearAnimation();
        findViewById(R.id.inbcbig).clearAnimation();

        expOne = AnimationUtils.loadAnimation(this, R.anim.expandtwo);
        expTwo = AnimationUtils.loadAnimation(this, R.anim.expandtwo);
        expThree = AnimationUtils.loadAnimation(this, R.anim.expandtwo);

        ((ImageView) findViewById(R.id.inbinfocenterico))
                .setImageResource(R.drawable.ic_cntdwn_three);
        ((ImageView) findViewById(R.id.inbinfocenterico))
                .setVisibility(View.VISIBLE);

        expOne.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
                ((ImageView) findViewById(R.id.inbinfocenterico))
                        .setImageResource(R.drawable.ic_cntdwn_two);
                ((ImageView) findViewById(R.id.inbinfocenterico))
                        .startAnimation(expTwo);
            }
        });

        expTwo.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
                ((ImageView) findViewById(R.id.inbinfocenterico))
                        .setImageResource(R.drawable.ic_cntdwn_one);

                ((ImageView) findViewById(R.id.inbinfocenterico))
                        .startAnimation(expThree);
            }
        });

        expThree.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
                anims("close", findViewById(R.id.inbinfocenterico));

                anims("expandslow", findViewById(R.id.inbimghldr));
                anims("rings", null);
                ((TextView)findViewById(R.id.inbcancelscan)).setText("CANCEL");

                if(((TextView)findViewById(R.id.inbinfotext)).getText().toString().contains("Accept Challenge")){
                    ((ImageView)findViewById(R.id.inbinfocenterico)).setVisibility(View.INVISIBLE);
                }
                else if(((TextView)findViewById(R.id.inbinfotext)).getText().toString().contains("Complete Challenge")){
                    ChallengeN chal = currentChallenge();
                    final long sl = (chal.isA()) ? chal.getASlot() : chal.getBSlot();
                    final int ct = chal.getCommType();
                    final String rom = chal.isA() ? user.getSlotFromIdentifierTwo(chal.getASlot()).getDigiROM() : user.getSlotFromIdentifierTwo(chal.getBSlot()).getDigiROM();

                    anims("expand", findViewById(R.id.inbinfocenterico));

                    findViewById(R.id.inbinfocenterico).getAnimation().setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            ((ImageView)findViewById(R.id.inbinfocenterico)).setScaleY(0.5f);
                            ((ImageView)findViewById(R.id.inbinfocenterico)).setScaleX(0.5f);
                            ((ImageView)findViewById(R.id.inbinfocenterico)).setImageResource(getImageId(getIMGFileName(rom, ct)));

                        }
                        @Override public void onAnimationEnd(Animation animation) {

                        }
                        @Override public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }

                if(((TextView)findViewById(R.id.inbinfotext)).getText().toString().contains("Accept Challenge")){
                    retry = true;
                    ((TextView)findViewById(R.id.inbinfotext)).setText(/*Scan VPet to Accept Challenge\n*/"Initiate on VPet");
                }
                else if(((TextView)findViewById(R.id.inbinfotext)).getText().toString().contains("Complete Challenge")){
                    retry = false;
                    ((TextView)findViewById(R.id.inbinfotext)).setText(/*Scan VPet to Complete Challenge\n*/"Do Not Initiate on VPet");
                }
            }
        });

        ((ImageView) findViewById(R.id.inbinfocenterico)).startAnimation(expOne);
    }

    private String getIMGFileNameFromRom(String rom, int commType) { return Utility.getIMGFileNameFromRom(rom, commType); }
    private String getIndex(String rom, int commType) { return Utility.getIndex(rom, commType); }
    private String getMonName(String rom, int commType) { return Utility.getMonName(rom, commType); }
    private String getMonNameFromIndex(String index, int commType) { return Utility.getMonNameFromIndex(index, commType); }
    private int getImageId(String imageName) { return Utility.getImageId(imageName, this); }
    private String getIMGFileName(String rom, int commType) { return Utility.getIMGFileNameFromRom(rom, commType);}
    private String getName(String rom, int commType) { return Utility.getMonName(rom, commType); }
    private int dpToPx(float dp) { return Utility.dpToPx(dp, this); }
    private int pxToDp(float px) { return Utility.pxToDp(px, this); }

    public SlotN currentSlot() { return slotSel < 6 ? user.getSlots()[slotSel] : null; }
    public ChallengeN currentChallenge() { return (challenges != null && challenges.size() > 0 && chalSel < challenges.size() && chalSel != -1) ? challenges.get(challengesKeys.get(chalSel)) : null; }

    private void closeDropDown() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideKeyboard();
                anims("dropdownclose", findViewById(R.id.userslistscrl));
                backCnt = 0;
                getCurrentFocus().clearFocus();
            }
        });
    }

    @Override public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.inbmloneico:
                    if (!isNewChallenge && RomManager.checkCommTypesCompatibility(user.getSlots()[0].getCommType(), currentChallenge().getCommType())) {
                        slotSel = 0;
                        slotClick();
                    }
                    if (isNewChallenge) {
                        slotSel = 0;
                        slotClick();
                    }
                    break;
                case R.id.inbmltwoico:
                    if (!isNewChallenge && RomManager.checkCommTypesCompatibility(user.getSlots()[1].getCommType(), currentChallenge().getCommType())) {
                        slotSel = 1;
                        slotClick();
                    }
                    if (isNewChallenge) {
                        slotSel = 1;
                        slotClick();
                    }
                    break;
                case R.id.inbmlthreeico:
                    if (!isNewChallenge && RomManager.checkCommTypesCompatibility(user.getSlots()[2].getCommType(), currentChallenge().getCommType())) {
                        slotSel = 2;
                        slotClick();
                    }
                    if (isNewChallenge) {
                        slotSel = 2;
                        slotClick();
                    }
                    break;
                case R.id.inbmlfourico:
                    if (!isNewChallenge && RomManager.checkCommTypesCompatibility(user.getSlots()[3].getCommType(), currentChallenge().getCommType())) {
                        slotSel = 3;
                        slotClick();
                    }
                    if (isNewChallenge) {
                        slotSel = 3;
                        slotClick();
                    }
                    break;
                case R.id.inbmlfiveico:
                    if (!isNewChallenge && RomManager.checkCommTypesCompatibility(user.getSlots()[4].getCommType(), currentChallenge().getCommType())) {
                        slotSel = 4;
                        slotClick();
                    }
                    if (isNewChallenge) {
                        slotSel = 4;
                        slotClick();
                    }
                    break;
                case R.id.inbmlsixico:
                    if (!isNewChallenge && RomManager.checkCommTypesCompatibility(user.getSlots()[5].getCommType(), currentChallenge().getCommType())) {
                        slotSel = 5;
                        slotClick();
                    }
                    if (isNewChallenge) {
                        slotSel = 5;
                        slotClick();
                    }
                    break;
                case R.id.inbbg:
                    closeWindows();
                    break;
                case R.id.inbchallengeaccept:
                    anims("pulse", v);

                    if (((TextView) v).getText().toString().contains("ACCEPT")) {
                        onClickChallengeAccept();
                    }
                    else if (((TextView) v).getText().toString().contains("CONNECT")) {
                        anims("close", findViewById(R.id.inbchallenge));
                        anims("expand", findViewById(R.id.inbcnctardu));
                        ((TextView) findViewById(R.id.inbinfotext)).setText("Scan VPet to Complete Challenge");
                        findArduino();
                    }
                    break;
                case R.id.inbchallengedecline:
                    anims("pulse", v);

                    if (((TextView) v).getText().toString().contains("DECLINE")) {
                        onClickChallengeDecline();
                    } else if (((TextView) v).getText().toString().contains("CANCEL")) {
                        closeWindows();
                    }
                    break;
                case R.id.inbchallengeslotselect:
                    anims("pulse", v);
                    if (slotSel <= 6) {
                        anims("close", findViewById(R.id.inbchallengewin));
                        switch (slotSel) {
                            case 0:
                                anims("loseFocus", findViewById(R.id.inbmlbone));
                                anims("loseFocus", findViewById(R.id.inbmlboneshadow));
                                anims("negLoseFocus", findViewById(R.id.inbmloneico));
                                break;
                            case 1:
                                anims("loseFocus", findViewById(R.id.inbmlbtwo));
                                anims("loseFocus", findViewById(R.id.inbmlbtwoshadow));
                                anims("negLoseFocus", findViewById(R.id.inbmltwoico));
                                break;
                            case 2:
                                anims("loseFocus", findViewById(R.id.inbmlbthree));
                                anims("loseFocus", findViewById(R.id.inbmlbthreeshadow));
                                anims("negLoseFocus", findViewById(R.id.inbmlthreeico));
                                break;
                            case 3:
                                anims("loseFocus", findViewById(R.id.inbmlbfour));
                                anims("loseFocus", findViewById(R.id.inbmlbfourshadow));
                                anims("negLoseFocus", findViewById(R.id.inbmlfourico));
                                break;
                            case 4:
                                anims("loseFocus", findViewById(R.id.inbmlbfive));
                                anims("loseFocus", findViewById(R.id.inbmlbfiveshadow));
                                anims("negLoseFocus", findViewById(R.id.inbmlfiveico));
                                break;
                            case 5:
                                anims("loseFocus", findViewById(R.id.inbmlbsix));
                                anims("loseFocus", findViewById(R.id.inbmlbsixshadow));
                                anims("negLoseFocus", findViewById(R.id.inbmlsixico));
                                break;

                        }

                        anims("close", findViewById(R.id.inbsndcenterico));
                        ((TextView) findViewById(R.id.inbsndinfotext)).setText("Select a Digimon");

                        if (!isNewChallenge) {
                            anims("expand", findViewById(R.id.inbcnctardu));
                            ((TextView) findViewById(R.id.inbinfotext)).setText("Scan VPet to Accept Challenge");
                            findArduino();
                        }
                        else {
                            // NEW CHALLENGE
                            anims("expand", findViewById(R.id.inbsndwin));
                            switch (user.getSlots()[slotSel].getCommType()) {
                                case 1:
                                case 3:
                                    presses();
                                    break;
                                case 7:
                                    findViewById(R.id.inbpresswin).setVisibility(View.INVISIBLE);
                                    findViewById(R.id.inbncmon).setVisibility(View.VISIBLE);
                                    ((TextView) findViewById(R.id.inbsendtitle)).setText("Sending...");
                                    anims("ringsTwo", null);
                                    postNewChallenge(ncOppName);
                                    break;
                                case 8:
                                case 9:
                                case 91:
                                case 92:
                                case 93:
                                    presses();
                                    break;
                            }
                        }

                    }
                    else {
                        ((TextView) findViewById(R.id.inbsndinfotext)).setText("Select a Digimon");
                    }
                    break;
                case R.id.inbchallengecancel:
                    anims("pulse", v);
                    closeWindows();
                    break;
                case R.id.inbcancelscan:
                    anims("pulse", v);
                    if (((TextView) v).getText().toString().contains("CANCEL")) {
                        anims("closeRings", null);
                        closeWindows();
                    } else if (((TextView) v).getText().toString().contains("RETRY")) {
                        ((TextView) findViewById(R.id.inbinfotext)).setText("");
                        if (retry) {
                            anims("rings", null);
                            challengeConnect(0);
                        } else {
                            anims("rings", null);
                            challengeConnect(1);
                        }
                    }
                    break;
                case R.id.inbchallengecancelearly:
                    anims("pulse", v);
                    if (!pressed) onClickChallengeCancel();
                    pressTimeout();
                    break;
                case R.id.choppsrch:
                    anims("pulse", v);
                    break;
                case R.id.oppchallengebtn:
                    anims("pulse", v);
                    if (!pressed) onClickUserChallenge();
                    pressTimeout();
                    break;
                case R.id.inbpressbtn:
                    if (canPress) {
                        anims("pulse", v);
                        anims("pulsering", null);
                        chrgcnt++;

                        switch (chrgcnt) {
                            case 0:
                                findViewById(R.id.inbchargeone).setVisibility(View.VISIBLE);
                                break;
                            case 3:
                                findViewById(R.id.inbchargetwo).setVisibility(View.VISIBLE);
                                break;
                            case 5:
                                findViewById(R.id.inbchargethree).setVisibility(View.VISIBLE);
                                break;
                            case 7:
                                findViewById(R.id.inbchargefour).setVisibility(View.VISIBLE);
                                break;
                            case 9:
                                findViewById(R.id.inbchargefive).setVisibility(View.VISIBLE);
                                break;
                            case 11:
                                findViewById(R.id.inbchargesix).setVisibility(View.VISIBLE);
                                break;
                            case 13:
                                findViewById(R.id.inbchargeseven).setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                    break;
                case R.id.messagesexit:
                    anims("pulse", v);
                    anims("closeMessages", null);
                    break;
                case R.id.inbplwintitle:
                    getConversationWith(currentChallenge().getOpp());
                    ((TextView) findViewById(R.id.inbplwintitle)).setTextColor(Color.parseColor("#383838"/*"#3C0E05"*/));
                    highlightChalTitle = false;
                    closeWindows();
                    anims("pulse", v);
                    anims("openMessages", null);
                    break;
                case R.id.msgtitlebartitle:
                    anims("pulse", v);

                    if (msgChIsExp) anims("closeChallengeStats", null);
                    else anims("expandChallengeStats", null);
                    break;

                case R.id.messagessend:
                    anims("pulse", v);
                    String ms = ((TextView) findViewById(R.id.messagetextfield)).getText().toString();

                    if (ms.equals("") || ms.equals(" ") || ms.contains("`")) return;
                    sendMessageTo(currentChallenge().getOpp());
                    ((TextView) findViewById(R.id.messagetextfield)).setText("");
                    break;

                case R.id.inbreportmessageselect:
                    anims("pulse", v);
                    closeWindows();
                    reportMessage();
                    break;

                case R.id.inbreportmessagecancel:
                    anims("pulse", v);
                    closeWindows();
                    break;

                case R.id.inblevellockswitch:
                    if (((Switch) v).isChecked()) v.setAlpha(0.7f);
                    else v.setAlpha(0.3f);
                    break;

                default:
                    break;
            }
        }
        catch (Exception e) {
            Utility.catchCrash("[CHALLENGES : ONCLICK]", e, this);
        }
    }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        closeWindows();
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(dropDown) {
                if(backCnt++ < 1) {
                    closeDropDown();
                }
            }
            else backCnt = 0;

            if(messagesIsOpen) anims("closeMessages", null);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    private void closeWindows() {
        if(findViewById(R.id.inbbg).getVisibility() == View.VISIBLE && !del) {
            if(slotSel <= 6) {
                switch(slotSel){
                    case 0:
                        anims("loseFocus", findViewById(R.id.inbmlbone));
                        anims("loseFocus", findViewById(R.id.inbmlboneshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmloneico));
                        break;
                    case 1:
                        anims("loseFocus", findViewById(R.id.inbmlbtwo));
                        anims("loseFocus", findViewById(R.id.inbmlbtwoshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmltwoico));
                        break;
                    case 2:
                        anims("loseFocus", findViewById(R.id.inbmlbthree));
                        anims("loseFocus", findViewById(R.id.inbmlbthreeshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmlthreeico));
                        break;
                    case 3:
                        anims("loseFocus", findViewById(R.id.inbmlbfour));
                        anims("loseFocus", findViewById(R.id.inbmlbfourshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmlfourico));
                        break;
                    case 4:
                        anims("loseFocus", findViewById(R.id.inbmlbfive));
                        anims("loseFocus", findViewById(R.id.inbmlbfiveshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmlfiveico));
                        break;
                    case 5:
                        anims("loseFocus", findViewById(R.id.inbmlbsix));
                        anims("loseFocus", findViewById(R.id.inbmlbsixshadow));
                        anims("negLoseFocus", findViewById(R.id.inbmlsixico));
                        break;

                }
                slotSel = 12;
                ((TextView)findViewById(R.id.inbsndinfotext)).setText("");
            }
            anims("fade", findViewById(R.id.inbbg));

            if(findViewById(R.id.inbchallenge).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.inbchallenge));

            if(findViewById(R.id.inbchallengewin).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.inbchallengewin));
            }

            if(findViewById(R.id.inbcnctardu).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.inbcnctardu));
            }

            if(findViewById(R.id.inbscanvpet).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.inbscanvpet));
            }

            if(findViewById(R.id.inbcountdownloading).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.inbcountdownloading));
            }

            if(findViewById(R.id.inbopponentwin).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.inbopponentwin));
            }

            if(findViewById(R.id.inbsndwin).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.inbsndwin));
            }

            if(findViewById(R.id.inbreportmessage).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.inbreportmessage));
            }

        }

        if(ringsSpin) {
            findViewById(R.id.inbcsml).clearAnimation();
            findViewById(R.id.inbcbig).clearAnimation();

            findViewById(R.id.inbcsml).setVisibility(View.GONE);
            findViewById(R.id.inbcbig).setVisibility(View.GONE);
        }
        if(ringsSpinTwo) {
            findViewById(R.id.inbnccsml).clearAnimation();
            findViewById(R.id.inbnccbig).clearAnimation();

            findViewById(R.id.inbnccsml).setVisibility(View.GONE);
            findViewById(R.id.inbnccbig).setVisibility(View.GONE);
        }
        if(alpha != null) alpha.setkl();
        srch = false;
        if(ardu != null) {
            if(ardu.isOpened()) ardu.close();
        }
        if (findThread != null) findThread.interrupt();
        if(scanThread != null) {
            scanThread.interrupt();
        }
        findThread = null;
        scanThread = null;
    }
    @Override public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect(), listRect = new Rect(), srchicRect = new Rect();
                //v.getGlobalVisibleRect(outRect);
                findViewById(R.id.searchbar).getGlobalVisibleRect(outRect);
                findViewById(R.id.userslistscrl).getGlobalVisibleRect(listRect);
                findViewById(R.id.choppsrch).getGlobalVisibleRect(srchicRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY()) &&
                        !listRect.contains((int)event.getRawX(), (int)event.getRawY()) &&
                        !srchicRect.contains((int)event.getRawX(), (int)event.getRawY()) ) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    if(dropDown) {
                        findViewById(R.id.chopptext).clearFocus();
                        anims("dropdownclose", findViewById(R.id.userslistscrl));
                        backCnt = 0;
                    }
                }
            }
        }
        try{
            return super.dispatchTouchEvent(event);
        }
        catch(Exception e) {
            try{
                sleep(750);
                return super.dispatchTouchEvent(event);
            }
            catch(Exception ee) {
                sleep(750);
                return super.dispatchTouchEvent(event);
            }
        }
    }
    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        switch(item) {
            case "<Digimon Original VPet>":
                commTypeSel = 7;
                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
                break;
            case "<Digimon 20th VPet>":
                commTypeSel = 3;
                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
                break;
            case "<Pendulum 20th VPet>":
                commTypeSel = 1;
                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
                break;
            case "<Pendulum Original VPet>":
                commTypeSel = 8;
                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
                break;
            case "<Pendulum X VPet>":
                commTypeSel = 9;
                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
                break;
            case "<Digimon Mini VPet V1>":
                commTypeSel = 91;
                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
                break;
            case "<Digimon Mini VPet V2>":
                commTypeSel = 92;
                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
                break;
            case "<Digimon Mini VPet V3>":
                commTypeSel = 93;
                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
                break;
            default:
                ((EditText)findViewById(R.id.opp)).setText(item);
                break;
        }
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Challenges getInstance(){ return k; }
    private BaseActivity base() { return BaseActivity.getInstance(); }
    public boolean isMessagesOpen() { return messagesIsOpen; }
    public void setUser(UserN user) { this.user = user; }
    public void updateChallenges(LinkedHashMap<String, ChallengeN> challenges) {
        this.challenges = challenges;
        challengesKeys = new ArrayList<>(challenges.keySet());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(messagesIsOpen) anims("closeMessages", null);
        base().stopRefreshingLeaderboard();
        base().setRefreshTime(14*60*1000);
//        base().setRefreshTime(10*1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(messagesIsOpen) anims("closeMessages", null);
        base().setRefreshTime(14*60*1000);
//        base().setRefreshTime(10*1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        base().refreshLeaderboard();
        base().getChallengesFromDatabase();
        base().setRefreshTime(60*1000);
//        base().setRefreshTime(10*1000);
    }

    public void pressTimeout() {
        pressed = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pressed = false;
            }
        }, 1250);
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void log(String T, String M) { if(debugLog) Log.e(T, M); }
    private void log(String T, String M, Exception e) { if(debugLog) Log.e(T, M, e); }
    private void makeToast(final String t, final int len) {
        TextView tv = new TextView(getApplicationContext());
        tv.setGravity(Gravity.CENTER);
        tv.setText(t);
        tv.setPadding(30, 35, 35, 30);
        tv.setTextColor(Color.WHITE);
        tv.setBackgroundResource(R.drawable.toastbg);

        Toast toast = new Toast(getApplicationContext());
        toast.setView(tv);
        toast.setDuration(len);
        toast.show();
    }
}