
package com.alpha.alphalink.LinkCore.NodeServer;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Stats implements Serializable {
    @SerializedName("rank")
    @Expose
    private String rank;

    @SerializedName("score")
    @Expose
    private int score;

    @SerializedName("win")
    @Expose
    private int win;

    @SerializedName("loss")
    @Expose
    private int loss;

    @SerializedName("draw")
    @Expose
    private int draw;

    public Stats() {
        setRank("E");
        setScore(0);
        setWin(0);
        setLoss(0);
        setDraw(0);
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLoss() {
        return loss;
    }

    public void setLoss(int loss) {
        this.loss = loss;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public void genRank() {
        if(((win+loss+draw) >= 1500.0f) && win >= 1000.0f)      setRank("s");
        else if(((win+loss+draw) >= 800.0f) && win >= 500.0f)   setRank("a");
        else if(((win+loss+draw) >= 150.0f) && win >= 90.0f)    setRank("b");
        else if(((win+loss+draw) >= 25.0f) && win >= 10.0f)     setRank("c");
        else if(((win+loss+draw) >= 7.0f) && win >= 4.0f)       setRank("d");
        else                                                    setRank("e");
    }
}
