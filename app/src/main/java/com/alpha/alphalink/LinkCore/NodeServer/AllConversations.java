
package com.alpha.alphalink.LinkCore.NodeServer;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alpha.alphalink.BaseActivity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class AllConversations {

    @SerializedName("allmessages")
    @Expose
    private List<Message> allmessages = null;

    private ArrayList<Conversation> conversations;
    private ArrayList<String> names;

    private void genNameList(String user) {
        names = new ArrayList<>();

        for(Message m : allmessages) {
            String n = m.getFrom().equals(user) ? m.getTo():m.getFrom();
            if(!names.contains(n)) names.add(n);
        }
    }

    public int assembleConversations(String user) {
        if(allmessages != null) {
            genNameList(user);
            conversations = new ArrayList<>();
            for (String n : names) conversations.add(new Conversation());

            for (Message m : allmessages) {
                String n = m.getFrom().equals(user) ? m.getTo() : m.getFrom();
                int ii = names.indexOf(n);
                conversations.get(ii).appendMessage(m);
            }

            for(Conversation c : conversations) c.sortMessages();
            sortConversations();
            return 0;
        }
        else return -1;
    }

    public void sortConversations() {
        if (conversations != null) {
            if (conversations.size() == 2) {
                if ((conversations.get(0).getMessages().get((conversations.get(0).getMessages().size()-1)).getWhen() < conversations.get(1).getMessages().get((conversations.get(1).getMessages().size()-1)).getWhen())) {
                    Conversation a = conversations.get(0);
                    conversations.set(0, conversations.get(1));
                    conversations.set(1, a);
                }
            }
            else {
                for (int i = conversations.size() - 1; i > 1; i--) {
                    for (int j = 0; j < i; j++) {
                        if (conversations.get(j).getMessages().get((conversations.get(j).getMessages().size()-1)).getWhen() < conversations.get(j + 1).getMessages().get((conversations.get(j+1).getMessages().size()-1)).getWhen()) {
                            Conversation c = conversations.get(j);
                            conversations.set(j, conversations.get(j + 1));
                            conversations.set(j + 1, c);
                        }
                    }
                }
            }
        }
    }

    public List<Message> getMessages() {
        return allmessages;
    }

    public Conversation getConversationWith(String conversant) {
        if(conversations != null) {
            for (Conversation conv : conversations) {
                if (conv.getConversant(BaseActivity.getInstance().getUser().getName()).equals(conversant))
                    return conv;
            }
        }
        return null;
    }

    public void setMessages(List<Message> messages) {
        this.allmessages = messages;
    }

    public Conversation[] getConversations() {
        Conversation[] cc = new Conversation[conversations.size()];
        return conversations.toArray(cc);
    }
}
