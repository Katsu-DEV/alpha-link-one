package com.alpha.alphalink.LinkCore;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Link {
    private String originator, slotString, commType, timePosted, aSlot, bSlot;

    public Link(String originator, String romString, String aSlot, String bSlot, String commType, String timePosted) {
        this.originator = originator;
        this.slotString = romString;
        this.aSlot = aSlot;
        this.bSlot = bSlot;
        this.commType = commType;
        this.timePosted = timePosted;
    }
    public Link() {}

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public String getSlotString() {
        return slotString;
    }

    public void setSlotString(String slotString) {
        this.slotString = slotString;
    }

    public String getCommType() {
        return commType;
    }

    public void setCommType(String commType) {
        this.commType = commType;
    }

    public String getTimePosted() {
        return timePosted;
    }

    public void setTimePosted(String timePosted) {
        this.timePosted = timePosted;
    }
}







