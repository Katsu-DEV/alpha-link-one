
package com.alpha.alphalink.LinkCore.NodeServer;

import android.util.Log;

import com.alpha.alphalink.BaseActivity;
import com.alpha.alphalink.LinkCore.RomManager;
import com.alpha.alphalink.LinkCore.Utility;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SlotN implements Serializable {
    public static final long serialVersionUID = -3345824290892466358L;

    @SerializedName("name")
    @Expose
    private String name = null;

    @SerializedName("digirom")
    @Expose
    private String digiROM = null;

    @SerializedName("commtype")
    @Expose
    private int commType;

    @SerializedName("birthdate")
    @Expose
    private long birthdate;

    @SerializedName("synchdate")
    @Expose
    private long synchdate;

    @SerializedName("wins")
    @Expose
    private int wins;

    @SerializedName("loss")
    @Expose
    private int loss;

    @SerializedName("draw")
    @Expose
    private int draw;

    @SerializedName("stats")
    @Expose
    private int[] stats;

    @SerializedName("inactive")
    @Expose
    private int inactive;

    public SlotN() {}

    public static SlotN slotN(String name, String digiROM, int commType) {
        SlotN ret = new SlotN();

        ret.name = name;
        ret.digiROM = digiROM;
        ret.commType = commType;

        ret.birthdate = Utility.getTime();
        ret.synchronise();

        ret.wins = 0;
        ret.loss = 0;
        ret.draw = 0;

        ret.stats = new int[] {0,0,0,0,0,0,0};

        ret.inactive = 0;
        return ret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDigiROM() {
        return digiROM;
    }

    public void setDigiROM(String digiROM) {
        this.digiROM = digiROM;
    }

    public int getCommType() {
        return commType;
    }

    public void setCommType(int commType) {
        this.commType = commType;
    }

    public long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(int birthdate) {
        this.birthdate = birthdate;
    }

    public long getSynchdate() {
        return synchdate;
    }

    public void setSynchdate(int synchdate) {
        this.synchdate = synchdate;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLoss() {
        return loss;
    }

    public void setLoss(int loss) {
        this.loss = loss;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int[] getStats() {
        return stats;
    }

    public void setStats(int[] stats) {
        this.stats = stats;
    }

    public int getInactive() {
        return inactive;
    }

    public void setInactive(int inactive) {
        this.inactive = inactive;
    }

    public int getAge(){
        return (int) ((Utility.getTime() - birthdate)/(1000*60*60)) - inactive;
    }

    public int getSynchTime() {
        return (int) ((Utility.getTime() - synchdate)/(1000*60*60));
    }

    public void synchronise() {
        synchdate = Utility.getTime();
    }

    public boolean isLocked() {
        return ((int) ((Utility.getTime() - synchdate)/(1000*60*60))) > 72;
    }

    public int getLevelVal() {
        return RomManager.levelVal(RomManager.getLevelGeneral(digiROM, commType));
    }

    public void winsUp() {
        wins++;
    }
    public void lossUp() {
        loss++;
    }
    public void drawUp() {
        draw++;
    }

    public boolean checkEvo(String digimonName) {
        boolean ret = true;
        String thisName = Utility.getNameRaw(digiROM, commType).trim();

        Log.e("NAME", ": "+thisName+" : "+digimonName);

        if(!thisName.equals(digimonName.trim())) {
            switch(commType) {
                case 1:
                    ret = RomManager.checkEvoPen20(RomManager.getIndexGeneral(digiROM, commType), digimonName);
                    break;
                case 3:
                    ret = RomManager.checkEvoDM20(RomManager.getIndexGeneral(digiROM, commType), digimonName);
                    break;
                case 7:
                    ret = RomManager.checkEvoDmog(RomManager.getIndexGeneral(digiROM, commType), digimonName);
                    break;
                case 8:
                    ret = RomManager.checkEvoPenOG(RomManager.getIndexGeneral(digiROM, commType), digimonName);
                    break;
                case 9:
                    ret = RomManager.checkEvoPenx(RomManager.getIndexGeneral(digiROM, commType), digimonName);
                    break;
                case 91:
                case 92:
                case 93:
                    ret = RomManager.checkEvoMini(RomManager.getIndexGeneral(digiROM, commType), digimonName, commType);
                    Log.e("===== SlotN Switch Mini Check Evo ====", "[ "+ commType + " | "+ ret + " ]");
                    break;
                default: break;
            }
        }
        return ret;
    }

    public boolean isAway() {
        for (ChallengeN ch : BaseActivity.getInstance().getAllChallengesN().getAllChallenges()) {
            if ((ch.isA() && (ch.getASlot() == birthdate && (ch.getState() == 1 || /*ch.getState() == 2 ||*/ ch.getState() == 5))) ||
                (!ch.isA() && (ch.getBSlot() == birthdate && (ch.getState() == 4 || ch.getState() == 2/* || ch.getState() == 5*/)))) {
                return true;
            }
        }
        return false;
    }

    public String toJsonString() {
        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create()
                .toJson(this);
    }
}
