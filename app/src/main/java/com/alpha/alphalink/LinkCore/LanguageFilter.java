package com.alpha.alphalink.LinkCore;

public class LanguageFilter {
    private static String[] badWords =
            {
                "fuck",   "Fuck",   "FUCK",
                "bitch",  "Bitch",  "BITCH",
                "nigger", "Nigger", "NIGGER",
                "nigga",  "Nigga",  "NIGGA",
                "fag",    "Fag",    "FAG",
                "shit",   "Shit",   "SHIT",
                "cunt",   "Cunt",   "CUNT",
                "whore",  "Whore",  "WHORE",
                "dick",   "Dick",   "DICK",
                "cock",   "Cock",   "COCK",
                "crap",   "Crap",   "CRAP",
                "slut",   "Slut",   "SLUT",
                "pussy",  "Pussy",  "PUSSY",
                "sex",    "Sex",    "SEX"
            },
    badChars = {"\\", "`", "\"",  "\'"};

    public static String bobbify(String text) {
        text =  text.replaceAll("fuck you", "I'm really sorry")
                    .replaceAll("Fuck You", "I'm really sorry")
                    .replaceAll("FUCK YOU", "I'm really sorry")

                    .replaceAll("daddy", "Gennai")
                    .replaceAll("Daddy", "Gennai")
                    .replaceAll("DADDY", "Gennai")

                    .replaceAll("hentai", "a really good book")
                    .replaceAll("Hentai", "A really good book")
                    .replaceAll("HENTAI", "A REALLY GOOD BOOK")

                .replaceAll("mark zuckerberg", "zark markerberg")
                    .replaceAll("Mark Zuckerberg", "Zark Markerberg")
                    .replaceAll("mark Zuckerberg", "Zark Markerberg")
                    .replaceAll("zuckerberg", "markerberg")
                    .replaceAll("Zuckerberg", "Markerberg")

                .replaceAll("bitch", "noot noot");

        for(String bad : badWords) {
            text = text.replaceAll(bad, "bobba");
        }

        if(containsBadWord(text)) text = "[REDACTED]";

        return text;
    }

    public static boolean containsBadWord(String text) {
        for(String bad : badWords) {
            if(text.replaceAll(" ", "").toLowerCase().contains(bad)) return true;
        }
        return false;
    }

    public static boolean containsBadChar(String text) {
        for(String bad : badChars) {
            if(text.replaceAll(" ", "").toLowerCase().contains(bad)) return true;
        }
        return false;
    }

    public static String removeBadChar(String text) {
        for(int i = 0; i<text.length(); i++) {
            if(containsBadChar(text.substring(i, i+1))) text = text.substring(0, i) + "" + text.substring(i+1);
        }
        for(int i = 0; i<text.length(); i++) {
            if(containsBadChar(text.substring(i, i+1))) text = text.substring(0, i) + "" + text.substring(i+1);
        }
        for(int i = 0; i<text.length(); i++) {
            if(containsBadChar(text.substring(i, i+1))) text = text.substring(0, i) + "" + text.substring(i+1);
        }
        for(int i = 0; i<text.length(); i++) {
            if(containsBadChar(text.substring(i, i+1))) text = text.substring(0, i) + "" + text.substring(i+1);
        }
        return text;
    }

    public static boolean checkText(String text) {
        if (text.equals("") || containsBadWord(text) || containsBadChar(text) ) return false;
        return true;
    }

    public static int checkName(String name) {
        name = name.replace(" ", "");
        if (name.length() > 26) {
            return -1;
        }
        if (name.equals("")) {
            return -2;
        }
        if (containsBadWord(name)) {
            return -3;
        }
        if (!(name.matches("[a-zA-Z0-9]+"))) {
            return -4;
        }

        return 0;
    }
}
