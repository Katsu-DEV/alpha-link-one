package com.alpha.alphalink;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.alpha.alphalink.LinkCore.LanguageFilter;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;
import com.alpha.alphalink.LinkCore.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateUser extends AppCompatActivity implements View.OnClickListener {
    private CreateUser k;
    private TextView unstate;
    private EditText username;
    private String uid, un;
    private boolean pressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        k = this;

        username = findViewById(R.id.unm);
        unstate = findViewById(R.id.unstatus);
        ((CheckBox)findViewById(R.id.createuseruacheck)).setClickable(false);

        anims("boot", null);
    }

    private void createUser()  {
        // Retrieve account UID
        uid = getIntent().getExtras().getString("UID");

        // Create new user object instant
        final UserN newUser = new UserN(un, uid);
        Log.e("New User", newUser.toJsonString());

        String URL = "https://www.alphahub.site/alphalink/api/protected/addUser/";

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRe = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pressed = false;
                        mainact().setUser(newUser);
                        mainact().accountFound();
                        k.finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pressed = false;
                        Log.e("CreateUser","Bad Request: " + error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() {
                return newUser.toJsonString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        rQ.add(strRe);
    }
    private int checkUsername(){
        un = username.getText().toString().trim();

        switch(LanguageFilter.checkName(un))  {
            case 0: break;
            case -1:
                unstate.setText("Username too long");
                return -1;
            case -2:
                unstate.setText("Username Invalid");
                return -2;
            case -3:
                unstate.setText("Keep it clean!");
                return -3;
            case -4:
                unstate.setText("Username contains an illegal character");
                return -4;
        }

        String URL = "https://www.alphahub.site/alphalink/api/protected/getUserByUsername/"+un;
        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String resp = response.toString();
                        if(resp.contains("'user doesn't exist'")) {
                            Log.e("User", "Doesnt exist");
                            //createUser();
                            anims("expand", findViewById(R.id.createuseruawin));
                        }
                        else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("User", "Exists");
                                    unstate.setText("An account with this username already exists.");
                                }
                            });
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("eRESP", ": "+error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);

        return 0;
    }

    public void acceptUserAgreement() {
        if( ((CheckBox)findViewById(R.id.createuseruapromise4)).isChecked() &&
                ((CheckBox)findViewById(R.id.createuseruapromise5)).isChecked() &&
                ((CheckBox)findViewById(R.id.createuseruapromise6)).isChecked() &&
                ((CheckBox)findViewById(R.id.createuseruapromise7)).isChecked() &&
                ((CheckBox)findViewById(R.id.createuseruapromise8)).isChecked() &&
                ((CheckBox)findViewById(R.id.createuseruacheck)).isChecked()) {
            anims("close", findViewById(R.id.createuseruawin));

            createUser();
        }
        else {
            makeToast("Please accept all parts of the User Agreement");
        }

    }

    private void anims(String cmd, final View v) {
        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);

        if(v != null) v.setVisibility(View.VISIBLE);

        switch(cmd) {
            case "pulse":
                v.startAnimation(pulse);
                break;
            case "boot":
                findViewById(R.id.createusertitle).animate()
                        .alpha(1.0f)
                        .setDuration(650)
                        .setStartDelay(825)
                        .start();

                findViewById(R.id.unm).animate()
                        .alpha(1.0f)
                        .setDuration(650)
                        .setStartDelay(1400)
                        .start();

                findViewById(R.id.createbtn).animate()
                        .alpha(1.0f)
                        .setDuration(650)
                        .setStartDelay(2000)
                        .start();

                findViewById(R.id.createuserbgico).animate()
                        .alpha(0.04f)
                        .setDuration(3400)
                        .setStartDelay(1000)
                        .start();
                break;
            default: break;
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.createbtn:
                anims("pulse", view);
                if(!pressed) {
                    pressed = true;
                    if(checkUsername() != 0) pressed = false;
                    else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                pressed = false;
                            }
                        }, 3000);
                    }
                }
                break;
            case R.id.createuseruapromise4:
            case R.id.createuseruapromise5:
            case R.id.createuseruapromise6:
            case R.id.createuseruapromise7:
                if( ((CheckBox)findViewById(R.id.createuseruapromise4)).isChecked() &&
                        ((CheckBox)findViewById(R.id.createuseruapromise5)).isChecked() &&
                        ((CheckBox)findViewById(R.id.createuseruapromise6)).isChecked() &&
                        ((CheckBox)findViewById(R.id.createuseruapromise7)).isChecked()) {
                    ((CheckBox)findViewById(R.id.createuseruacheck)).setClickable(true);
                    findViewById(R.id.createuseruacontrols).setAlpha(0.75f);
                }
                else {
                    ((CheckBox)findViewById(R.id.createuseruacheck)).setClickable(false);
                    findViewById(R.id.createuseruacontrols).setAlpha(0.25f);
                }
                break;
            case R.id.createuseruaaccept:
                anims("pulse", view);
                acceptUserAgreement();
                break;
            default: break;
        }
    }

    private void makeToast(final String t) {
        Toast.makeText(this, t, Toast.LENGTH_LONG).show();
    }

    private MainActivity mainact() { return MainActivity.getInstance(); }
}