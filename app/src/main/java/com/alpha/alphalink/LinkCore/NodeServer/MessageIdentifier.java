
package com.alpha.alphalink.LinkCore.NodeServer;

import com.alpha.alphalink.LinkCore.ChallengeManager;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class MessageIdentifier {
    @SerializedName("from")
    @Expose
    private String from;

    @SerializedName("when")
    @Expose
    private Long when;

    public MessageIdentifier() { }

    public MessageIdentifier(String from, long when) {
        setFrom(from);
        setWhen(when);
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Long getWhen() {
        return when;
    }

    public void setWhen(Long when) {
        this.when = when;
    }

    public String toJsonString() {
        return new Gson().toJson(this);
    }

    public static MessageIdentifier getEmpty() {
        return new MessageIdentifier("`!`", -1);
    }
}
