package com.alpha.alphalink;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.alpha.alphalink.AlphaCore.AlphaEngine;
import com.alpha.alphalink.LinkCore.LanguageFilter;
import com.alpha.alphalink.LinkCore.NodeServer.ChallengeN;
import com.alpha.alphalink.LinkCore.NodeServer.MQTTInterface;
import com.alpha.alphalink.LinkCore.NodeServer.SlotN;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;
import com.alpha.alphalink.LinkCore.NodeServer.UserNotify;
import com.alpha.alphalink.LinkCore.RomManager;
import com.alpha.alphalink.LinkCore.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.physicaloid.lib.Physicaloid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Partners extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    static Partners k;
    private Context th = this;
    private UserN user;
    private ChallengeN ch;
    private int[] open = {9, 9};
    private Physicaloid ardu;
    private AlphaEngine alpha;
    private Thread findThread, scanThread;
    private List<String> opponentsList;
    private Animation expOne, expTwo, expThree;
    private int slotCount = 0, MAX_TIME = 72, BAR_WIDTH = 0, slotSel = 0, chrgcnt = 0, commTypeSel = 3;
    private boolean srch = true, canPress = true, thrd = false, dontCloseWindows = false, ringsSpinOne = false,
            ringsSpinTwo = false, debugLog = true, isChallenge = false, isSync = false;
    private String extRom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partners);
        k = this;

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Utility.catchCrash("[Partners : Uncaught]", paramThrowable, getApplicationContext());
                System.exit(2);
            }
        });

        try{
            user = (UserN) getIntent().getSerializableExtra("User");
            getFriends();

            init();
            initialiseSlots();
            animateLoad();
            slotClick(false);
        }
        catch (Exception e) {
            Utility.catchCrash("[PARTNERS : ONSTART]", e, this);
        }
    }

    private void init() {
        findViewById(R.id.partnersbg).setBackgroundColor(getResources().getColor(R.color.colorBg));
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = ((int) (dm.widthPixels - (50*dm.density))), height = dm.heightPixels;
        getWindow().setLayout(width, height);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.RIGHT;

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        BAR_WIDTH = (findViewById(R.id.syncbar)).getLayoutParams().width;

        findViewById(R.id.prtnrslevellockcl).setVisibility(View.VISIBLE);

        initialiseSpinner();
    }
    private void getFriends() { opponentsList = base().getAllChallengesN().getNamesList(); }
    private void initialiseSpinner() {
        Spinner commTypes, friends;

        commTypes = findViewById(R.id.spinner);
        commTypes.setOnItemSelectedListener(this);

        List<String> categories = new ArrayList<String>();
        categories.add("<Digimon 20th VPet>");
        categories.add("<Digimon Original VPet>");
        categories.add("<Pendulum 20th VPet>");
        categories.add("<Pendulum Original VPet>");
        categories.add("<Pendulum X VPet>");
        categories.add("<Digimon Mini VPet V1>");
        categories.add("<Digimon Mini VPet V2>");
        categories.add("<Digimon Mini VPet V3>");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        commTypes.setAdapter(dataAdapter);

        friends = findViewById(R.id.friends);
        friends.setOnItemSelectedListener(this);

        ArrayAdapter<String> friendsDataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, opponentsList);
        friendsDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        friends.setAdapter(friendsDataAdapter);
    }
    private void initialiseSlots() {
        slotCount = user.getSlots().length;

        if(slotCount < 1) {
            ((ImageView) findViewById(R.id.mloneico)).setImageResource(getImageId("ic_add"));
            findViewById(R.id.mloneico).setVisibility(View.VISIBLE);
        }
        if(slotCount < 2) {
            if(slotCount == 1) {
                ((ImageView) findViewById(R.id.mltwoico)).setImageResource(getImageId("ic_add"));
                findViewById(R.id.mltwoico).setVisibility(View.VISIBLE);
            }
            else findViewById(R.id.mltwoico).setVisibility(View.GONE);
        }
        if(slotCount < 3) {
            if(slotCount == 2) {
                ((ImageView) findViewById(R.id.mlthreeico)).setImageResource(getImageId("ic_add"));
                findViewById(R.id.mlthreeico).setVisibility(View.VISIBLE);
            }
            else findViewById(R.id.mlthreeico).setVisibility(View.GONE);
        }
        if(slotCount < 4) {
            if(slotCount == 3) {
                ((ImageView) findViewById(R.id.mlfourico)).setImageResource(getImageId("ic_add"));
                findViewById(R.id.mlfourico).setVisibility(View.VISIBLE);
            }
            else findViewById(R.id.mlfourico).setVisibility(View.GONE);
        }
        if(slotCount < 5) {
            if(slotCount == 4) {
                ((ImageView) findViewById(R.id.mlfiveico)).setImageResource(getImageId("ic_add"));
                findViewById(R.id.mlfiveico).setVisibility(View.VISIBLE);
            }
            else findViewById(R.id.mlfiveico).setVisibility(View.GONE);
        }
        if(slotCount < 6) {
            if(slotCount == 5) {
                ((ImageView) findViewById(R.id.mlsixico)).setImageResource(getImageId("ic_add"));
                findViewById(R.id.mlsixico).setVisibility(View.VISIBLE);
            }
            else findViewById(R.id.mlsixico).setVisibility(View.GONE);
        }

        findViewById(R.id.mlonelock).setVisibility(View.GONE);
        findViewById(R.id.mltwolock).setVisibility(View.GONE);
        findViewById(R.id.mlthreelock).setVisibility(View.GONE);
        findViewById(R.id.mlfourlock).setVisibility(View.GONE);
        findViewById(R.id.mlfivelock).setVisibility(View.GONE);
        findViewById(R.id.mlsixlock).setVisibility(View.GONE);

        if(slotCount > 0 && user.getSlots()[0].isLocked()) {
            findViewById(R.id.mlonelock).setVisibility(View.VISIBLE);
        }
        if(slotCount > 1 && user.getSlots()[1].isLocked()) {
            findViewById(R.id.mltwolock).setVisibility(View.VISIBLE);
        }
        if(slotCount > 2 && user.getSlots()[2].isLocked()) {
            findViewById(R.id.mlthreelock).setVisibility(View.VISIBLE);
        }
        if(slotCount > 3 && user.getSlots()[3].isLocked()) {
            findViewById(R.id.mlfourlock).setVisibility(View.VISIBLE);
        }
        if(slotCount > 4 && user.getSlots()[4].isLocked()) {
            findViewById(R.id.mlfivelock).setVisibility(View.VISIBLE);
        }
        if(slotCount > 5 && user.getSlots()[5].isLocked()) {
            findViewById(R.id.mlsixlock).setVisibility(View.VISIBLE);
        }
    }
    private void animateLoad() {
        findViewById(R.id.bg).setVisibility(View.GONE);
        findViewById(R.id.check).setVisibility(View.GONE);
        findViewById(R.id.cnctardu).setVisibility(View.GONE);
        findViewById(R.id.scanvpet).setVisibility(View.GONE);
        findViewById(R.id.plainwin).setVisibility(View.GONE);
        findViewById(R.id.sndwin).setVisibility(View.GONE);
        findViewById(R.id.monlist).setVisibility(View.VISIBLE);
        //Focused
        anims("expand", findViewById(R.id.focused));

        // Slots
        anims("expand", findViewById(R.id.mlbone));
        anims("expand", findViewById(R.id.mlboneshadow));
        if(findViewById(R.id.mloneico).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mloneico));
        if(findViewById(R.id.mlonelock).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlonelock));

        anims("expand", findViewById(R.id.mlbtwo));
        anims("expand", findViewById(R.id.mlbtwoshadow));
        if(findViewById(R.id.mltwoico).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mltwoico));
        if(findViewById(R.id.mltwolock).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mltwolock));

        anims("expand", findViewById(R.id.mlbthree));
        anims("expand", findViewById(R.id.mlbthreeshadow));
        if(findViewById(R.id.mlthreeico).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlthreeico));
        if(findViewById(R.id.mlthreelock).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlthreelock));

        anims("expand", findViewById(R.id.mlbfour));
        anims("expand", findViewById(R.id.mlbfourshadow));
        if(findViewById(R.id.mlfourico).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlfourico));
        if(findViewById(R.id.mlfourlock).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlfourlock));

        anims("expand", findViewById(R.id.mlbfive));
        anims("expand", findViewById(R.id.mlbfiveshadow));
        if(findViewById(R.id.mlfiveico).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlfiveico));
        if(findViewById(R.id.mlfivelock).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlfivelock));

        anims("expand", findViewById(R.id.mlbsix));
        anims("expand", findViewById(R.id.mlbsixshadow));
        if(findViewById(R.id.mlsixico).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlsixico));
        if(findViewById(R.id.mlsixlock).getVisibility() == View.VISIBLE)
            anims("expand", findViewById(R.id.mlsixlock));
    }
    private void setSpriteColours(){
        ColorMatrix monomatrix = new ColorMatrix(), clrmatrix = new ColorMatrix();
        monomatrix.setSaturation(0);
        clrmatrix.setSaturation(1f);
        ColorMatrixColorFilter monofilter = new ColorMatrixColorFilter(monomatrix);
        ColorMatrixColorFilter clrfilter = new ColorMatrixColorFilter(clrmatrix);

        ImageView[] icos = new ImageView[] {
                (ImageView) findViewById(R.id.mloneico),
                (ImageView) findViewById(R.id.mltwoico),
                (ImageView) findViewById(R.id.mlthreeico),
                (ImageView) findViewById(R.id.mlfourico),
                (ImageView) findViewById(R.id.mlfiveico),
                (ImageView) findViewById(R.id.mlsixico)
        };

        for(int i = 0; i<slotCount; i++){
            if(i!=slotSel) icos[i].setColorFilter(monofilter);
            else  icos[i].setColorFilter(clrfilter);
        }
    }
    private void populateData() {
        if(user.getSlots()[slotSel] == null && slotSel > 1) slotSel = slotCount-1;

        ( (TextView) findViewById(R.id.monname) ).setText(user.getSlots()[slotSel].getName());
        ( (TextView) findViewById(R.id.focuswins) ).setText("WIN: "+user.getSlots()[slotSel].getWins());
        ( (TextView) findViewById(R.id.focusloss) ).setText("LOSS: "+ user.getSlots()[slotSel].getLoss());
        ( (TextView) findViewById(R.id.focusdraws) ).setText("DRAW: "+ user.getSlots()[slotSel].getDraw());

        TextView commTypeTxt = findViewById(R.id.commtypeid);
        commTypeTxt.setVisibility(View.VISIBLE);
        switch(user.getSlots()[slotSel].getCommType()) {
            case 1:
                commTypeTxt.setText("  Pen20  ");
                commTypeTxt.setBackgroundResource(R.drawable.commtypebgpent);
                break;
            case 3:
                commTypeTxt.setText("  DM20  ");
                commTypeTxt.setBackgroundResource(R.drawable.commtypebgdmt);
                break;
            case 7:
                commTypeTxt.setText("  DMOG  ");
                commTypeTxt.setBackgroundResource(R.drawable.commtypebgdmog);
                break;
            case 8:
                commTypeTxt.setText("  PenOG  ");
                commTypeTxt.setBackgroundResource(R.drawable.commtypebgpenog);
                break;
            case 9:
                commTypeTxt.setText("  PenX  ");
                commTypeTxt.setBackgroundResource(R.drawable.commtypebgpenx);
                break;
            case 91:
            case 92:
            case 93:
                commTypeTxt.setText("  MINI  ");
                commTypeTxt.setBackgroundResource(R.drawable.commtypebgmini);
                break;
            default:
                commTypeTxt.setText("");
                break;
        }

        String rom;
        int age, syncTime, commType;
        float rem;
        String alp, colorString;

        age = user.getSlots()[slotSel].getAge();
        ( (TextView) findViewById(R.id.focusage) ).setText(String.valueOf( (int) (age/24) ) + "YR");
        syncTime = user.getSlots()[slotSel].getSynchTime();
        rem = 1f - ((float) syncTime / (float) MAX_TIME);
        if(rem < 0.075f) rem = 0.075f;
        ( (ImageView) findViewById(R.id.syncbar) ).getLayoutParams().width =  ((int) (BAR_WIDTH*rem));
        if(rem+0.2f <= 1f) rem+=0.2f;
        alp = Integer.toHexString((int) (255* (1-rem)));
        if(alp.length() == 1) alp = "0" + alp;
        colorString = "#" + alp + "F33000";
        ( (ImageView) findViewById(R.id.syncbar) ).setColorFilter(Color.parseColor(colorString));

        ColorMatrix matrix = new ColorMatrix();
        if(user.getSlots()[slotSel].isLocked()) matrix.setSaturation(0);
        else matrix.setSaturation(1f);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        ((ImageView)findViewById(R.id.monico)).setColorFilter(filter);
        ((ImageView)findViewById(R.id.monicomirror)).setColorFilter(filter);

        if(slotCount > 0){
            rom = user.getSlots()[0].getDigiROM();
            commType = user.getSlots()[0].getCommType();
            ( (ImageView) findViewById(R.id.mloneico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
        }
        if(slotCount > 1){
            rom = user.getSlots()[1].getDigiROM();
            commType = user.getSlots()[1].getCommType();
            ( (ImageView) findViewById(R.id.mltwoico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
        }
        if(slotCount > 2){
            rom = user.getSlots()[2].getDigiROM();
            commType = user.getSlots()[2].getCommType();
            ( (ImageView) findViewById(R.id.mlthreeico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
        }
        if(slotCount > 3){
            rom = user.getSlots()[3].getDigiROM();
            commType = user.getSlots()[3].getCommType();
            ( (ImageView) findViewById(R.id.mlfourico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
        }
        if(slotCount > 4){
            rom = user.getSlots()[4].getDigiROM();
            commType = user.getSlots()[4].getCommType();
            ( (ImageView) findViewById(R.id.mlfiveico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
        }
        if(slotCount > 5){
            rom = user.getSlots()[5].getDigiROM();
            commType = user.getSlots()[5].getCommType();
            ( (ImageView) findViewById(R.id.mlsixico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
        }
    }

    private void slotClick(boolean click) {
        open[0] = open[1];
        open[1] = slotSel;
        setSpriteColours();

        if(slotSel != slotCount){
            String rom = user.getSlots()[slotSel].getDigiROM();
            int commType = user.getSlots()[slotSel].getCommType();
            ( (ImageView) findViewById(R.id.monico) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
            ( (ImageView) findViewById(R.id.monicomirror) ).setImageResource(getImageId(getIMGFileName(rom, commType)));
        }

        switch(open[0]){
            case 0:
                anims("loseFocus", findViewById(R.id.mlbone));
                anims("loseFocus", findViewById(R.id.mlboneshadow));
                anims("negLoseFocus", findViewById(R.id.mloneico));
                break;
            case 1:
                anims("loseFocus", findViewById(R.id.mlbtwo));
                anims("loseFocus", findViewById(R.id.mlbtwoshadow));
                anims("negLoseFocus", findViewById(R.id.mltwoico));
                break;
            case 2:
                anims("loseFocus", findViewById(R.id.mlbthree));
                anims("loseFocus", findViewById(R.id.mlbthreeshadow));
                anims("negLoseFocus", findViewById(R.id.mlthreeico));
                break;
            case 3:
                anims("loseFocus", findViewById(R.id.mlbfour));
                anims("loseFocus", findViewById(R.id.mlbfourshadow));
                anims("negLoseFocus", findViewById(R.id.mlfourico));
                break;
            case 4:
                anims("loseFocus", findViewById(R.id.mlbfive));
                anims("loseFocus", findViewById(R.id.mlbfiveshadow));
                anims("negLoseFocus", findViewById(R.id.mlfiveico));
                break;
            case 5:
                anims("loseFocus", findViewById(R.id.mlbsix));
                anims("loseFocus", findViewById(R.id.mlbsixshadow));
                anims("negLoseFocus", findViewById(R.id.mlsixico));
                break;

        }
        switch(open[1]){
            case 0:
                anims("focus", findViewById(R.id.mlbone));
                anims("focus", findViewById(R.id.mlboneshadow));
                anims("negFocus", findViewById(R.id.mloneico));
                break;
            case 1:
                anims("focus", findViewById(R.id.mlbtwo));
                anims("focus", findViewById(R.id.mlbtwoshadow));
                anims("negFocus", findViewById(R.id.mltwoico));
                break;
            case 2:
                anims("focus", findViewById(R.id.mlbthree));
                anims("focus", findViewById(R.id.mlbthreeshadow));
                anims("negFocus", findViewById(R.id.mlthreeico));
                break;
            case 3:
                anims("focus", findViewById(R.id.mlbfour));
                anims("focus", findViewById(R.id.mlbfourshadow));
                anims("negFocus", findViewById(R.id.mlfourico));
                break;
            case 4:
                anims("focus", findViewById(R.id.mlbfive));
                anims("focus", findViewById(R.id.mlbfiveshadow));
                anims("negFocus", findViewById(R.id.mlfiveico));
                break;
            case 5:
                anims("focus", findViewById(R.id.mlbsix));
                anims("focus", findViewById(R.id.mlbsixshadow));
                anims("negFocus", findViewById(R.id.mlsixico));
                break;

        }

        if(slotSel == slotCount) {
            ( (ImageView) findViewById(R.id.del) ).setVisibility(View.GONE);
            ( (ImageView) findViewById(R.id.sync) ).setVisibility(View.GONE);
            ( (ImageView) findViewById(R.id.send) ).setVisibility(View.GONE);

            ( (ImageView) findViewById(R.id.monico) ).setImageResource(R.drawable.ic_add);
            ( (ImageView) findViewById(R.id.monicomirror) ).setImageResource(R.drawable.ic_add);
            findViewById(R.id.commtypeid).setVisibility(View.GONE);
            ( (TextView) findViewById(R.id.monname) ).setText("E M P T Y");
            ( (TextView) findViewById(R.id.focuswins) ).setText("");
            ( (TextView) findViewById(R.id.focusloss) ).setText("");
            ( (TextView) findViewById(R.id.focusdraws) ).setText("");
            ( (TextView) findViewById(R.id.focusage) ).setText("");
            findViewById(R.id.syncbar).setVisibility(View.GONE);

            if(click) {
                ((TextView)findViewById(R.id.attitle)).setText("A D D   P A R T N E R");
                ((TextView)findViewById(R.id.checktitle)).setText("A D D   P A R T N E R");
                anims("fadein", findViewById(R.id.bg));
                anims("expand", findViewById(R.id.check));
            }
        }
        else {
            ( (ImageView) findViewById(R.id.del) ).setVisibility(View.VISIBLE);
            ( (ImageView) findViewById(R.id.sync) ).setVisibility(View.VISIBLE);
            ( (ImageView) findViewById(R.id.send) ).setVisibility(View.VISIBLE);
            findViewById(R.id.syncbar).setVisibility(View.VISIBLE);
            populateData();
        }
    }

    private void findArduino() {
        srch = true;
        Runnable task = new Runnable()
        {
            public void run()
            {
                do{
                    if(!srch) break;
                    sleep(1000);
                    if(!srch) break;
                    ardu = new Physicaloid(th);
                    if(!ardu.isOpened()) ardu.open();
                }while(!ardu.isOpened() && srch);

                if(ardu.isOpened()){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            alpha = new AlphaEngine(ardu);
                            connectVisibility(0);

                            sleep(750);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    anims("close", findViewById(R.id.cnctardu));
                                    anims("expandslow", findViewById(R.id.prtnrscountdownloading));

                                    Animation loadbar = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.loadingboxinsideexpand);
                                    findViewById(R.id.prtnrsloadingboxinside).setVisibility(View.VISIBLE);
                                    loadbar.setAnimationListener(new Animation.AnimationListener() {
                                        @Override public void onAnimationStart(Animation animation) {}
                                        @Override public void onAnimationRepeat(Animation animation) {}
                                        @Override public void onAnimationEnd(Animation animation) {
                                            anims("close", findViewById(R.id.prtnrscountdownloading));
                                            anims("expand", findViewById(R.id.scanvpet));

                                            findViewById(R.id.scanvpet).getAnimation().setAnimationListener(new Animation.AnimationListener() {
                                                @Override public void onAnimationStart(Animation animation) {}
                                                @Override public void onAnimationRepeat(Animation animation) {}
                                                @Override public void onAnimationEnd(Animation animation) {
                                                    if(((TextView)findViewById(R.id.attitle)).getText().toString().contains("S Y N C H")) {
                                                        anims("expand", findViewById(R.id.imghldr));
                                                        findViewById(R.id.imghldr).getAnimation().setAnimationListener(new Animation.AnimationListener() {
                                                            @Override public void onAnimationStart(Animation animation) {}
                                                            @Override public void onAnimationRepeat(Animation animation) {}
                                                            @Override public void onAnimationEnd(Animation animation) {
                                                                if(((TextView)findViewById(R.id.attitle)).getText().toString().contains("S Y N C H")) {
                                                                    animateCountDown();
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    });
                                    findViewById(R.id.prtnrsloadingboxinside).startAnimation(loadbar);
                                }
                            });
                        }
                    });
                }
                else if(ardu != null) {
                    if(ardu.isOpened()) ardu.close();
                }
            }
        };
        findThread = new Thread(task);
        findThread.setDaemon(true);
        findThread.start();
    }

    private void onClickScan() {
        isSync = false;
        isChallenge = false;
        connectVisibility(2);
        animateCountDown();
    }
    private void onClickSync() {
        isSync = true;
        switch(user.getSlots()[slotSel].getCommType()) {
            case 1:
                ((TextView) findViewById(R.id.commtext)).setText("Pendulum 20th VPet");
                break;
            case 3:
                ((TextView) findViewById(R.id.commtext)).setText("Digimon 20th VPet");
                break;
            case 7:
                ((TextView) findViewById(R.id.commtext)).setText("Digimon Original VPet");
                break;
            case 8:
                ((TextView) findViewById(R.id.commtext)).setText("Pendulum Original VPet");
                break;
            case 9:
                ((TextView) findViewById(R.id.commtext)).setText("<Pendulum X VPet>");
                break;
            case 91:
                ((TextView) findViewById(R.id.commtext)).setText("<Digimon Mini VPet V1>");
                break;
            case 92:
                ((TextView) findViewById(R.id.commtext)).setText("<Digimon Mini VPet V2>");
                break;
            case 93:
                ((TextView) findViewById(R.id.commtext)).setText("<Digimon Mini VPet V3>");
                break;
            default: break;
        }

        ((TextView)findViewById(R.id.attitle)).setText("S Y N C H R O N I S E");
        ((TextView)findViewById(R.id.checktitle)).setText("S Y N C H R O N I S E");

        anims("fadein", findViewById(R.id.bg));
        anims("expand", findViewById(R.id.check));
    }
    private void connectVisibility(int select){
        ((ImageView) findViewById(R.id.csml)).clearAnimation();
        ((ImageView) findViewById(R.id.cbig)).clearAnimation();
        ((ImageView) findViewById(R.id.csml)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.cbig)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.centerico)).setVisibility(View.INVISIBLE);
        ((TextView) findViewById(R.id.infoText)).setText("");
        findViewById(R.id.presswin).setVisibility(View.GONE);

        if(select == 0){  // Connect
            findViewById(R.id.imghldr).setVisibility(View.INVISIBLE);
            findViewById(R.id.uppercontrols).setVisibility(View.VISIBLE);
            findViewById(R.id.lowercontrols).setVisibility(View.GONE);

            float factor = this.getResources().getDisplayMetrics().density;
            findViewById(R.id.imghldr).getLayoutParams().height = (int) (155*factor);
            findViewById(R.id.imghldr).getLayoutParams().width = (int) (155*factor);
            findViewById(R.id.centerico).getLayoutParams().height = (int) (100*factor);
            findViewById(R.id.centerico).getLayoutParams().width = (int) (100*factor);

            if( ((TextView)findViewById(R.id.checktitle)).getText().toString().contains("A D D") ) {
                findViewById(R.id.btnSCAN).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.infoText)).setText("Select Device and Press Scan");
            }
            else if( ((TextView)findViewById(R.id.checktitle)).getText().toString().contains("S Y N C H") ) {
                //((TextView) findViewById(R.id.infoText)).setText("Press Scan");
                findViewById(R.id.btnSCAN).setVisibility(View.GONE);
            }
        }

        if(select == 1){   //
            anims("expand", findViewById(R.id.imghldr));
            ((TextView) findViewById(R.id.infoText)).setText("Initiate On VPet");
            anims("rings", null);
        }

        if(select == 2) {
            anims("expand", findViewById(R.id.imghldr));
        }
    }
    private void connect(final int s) {
        if(!thrd) {
            scanThread = new Thread(new Runnable()
            {
                public void run()
                {
                    alpha.setCommType(isSync ? currentSlot().getCommType() : commTypeSel );
                    extRom = alpha.getROMNoInert();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            switch(extRom) {
                                case "-1":
                                    anims("fail", null);
                                    ((TextView)findViewById(R.id.infoText)).setText("Error connecting to device - try again");
                                    break;
                                case "-2":  // Killed
                                    break;
                                case "-3":  // ROM too short
                                    break;
                                case "-4":  // Contains illegal characters
                                    break;
                                default:
                                    anims("closeRings", null);
                                    ((TextView)findViewById(R.id.infoText)).setText("");
                                    if (s == 0) connectSuccess();
                                    break;
                            }
                        }
                    });
                }
            });
            scanThread.setDaemon(true);
            scanThread.start();
        }
    }
    private void connectSuccess() {
        boolean invalid = false;
        final String t = ((TextView)findViewById(R.id.attitle)).getText().toString(),
                rom = isSync ? currentSlot().getDigiROM() : extRom;
        final int ct = isSync ? currentSlot().getCommType() : commTypeSel;

        if(t.contains("A D D")){
            if(isDuplicate()) {
                anims("close", findViewById(R.id.uppercontrols));
                findViewById(R.id.lowercontrols).setVisibility(View.GONE);
                invalid = true;
                ((TextView) findViewById(R.id.infoText)).setText("Digimon Already Added");

                Handler h = new Handler();
                Runnable r = new Runnable() {
                    public void run() {
                        closeWindows();
                    }
                };
                h.postDelayed(r, 2500);
            }
            else if(getName(extRom, commTypeSel).equals("Errmon")) {
                invalid = true;
                ((TextView) findViewById(R.id.infoText)).setText("Digimon Not Recognised");

                Handler h = new Handler();
                Runnable r = new Runnable() {
                    public void run() {
                        closeWindows();
                    }
                };
                h.postDelayed(r, 2500);
            }
            else {
                anims("close", findViewById(R.id.uppercontrols));
                anims("expandslow", findViewById(R.id.lowercontrols));
            }
        }
        else if(t.contains("S Y N C H")){
            anims("close", findViewById(R.id.uppercontrols));
        }

        if(!invalid) {
            Thread thr = new Thread(new Runnable() {
                public void run() {
                    for (int i = 0; i < 560; i++) sleep(1);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            float factor = th.getResources().getDisplayMetrics().density;
                            findViewById(R.id.imghldr).getLayoutParams().height = (int) (85 * factor);
                            findViewById(R.id.imghldr).getLayoutParams().width = (int) (185 * factor);
                            findViewById(R.id.centerico).getLayoutParams().height = (int) (60 * factor);
                            findViewById(R.id.centerico).getLayoutParams().width = (int) (60 * factor);

                            ((TextView) findViewById(R.id.infoText)).setText(getName(rom, ct));
                            ((ImageView) findViewById(R.id.centerico)).setImageResource(getImageId(getIMGFileName(rom, ct)));
                            anims("expand", findViewById(R.id.centerico));

                            if(t.contains("S Y N C H")) {
                                String b = "";
                                switch(currentSlot().getCommType()) {
                                    case 1:
                                        b = RomManager.getPen20MonFromRom(extRom)[0];
                                        break;
                                    case 3:
                                        b = RomManager.getDm20MonFromRom(extRom);
                                        break;
                                    case 7:
                                        b = RomManager.getDmogMonFromRom(extRom);
                                        break;
                                    case 8:
                                        b = RomManager.getPenOGMonFromRom(extRom);
                                        break;
                                    case 9:
                                        b = RomManager.getPenxMonFromRom(extRom);
                                        break;
                                    case 91:
                                        b = RomManager.getMiniMonFromRom(extRom, 1);
                                        break;
                                    case 92:
                                        b = RomManager.getMiniMonFromRom(extRom, 2);
                                        break;
                                    case 93:
                                        b = RomManager.getMiniMonFromRom(extRom, 3);
                                        break;
                                    default: break;
                                }

                                if(!b.equals("err") && currentSlot().checkEvo(b)) {
                                    ((TextView)findViewById(R.id.infoText)).setText("Synchronising...");
                                    sync();
                                }
                                else {
                                    ((TextView)findViewById(R.id.infoText)).setText("Failed to Synchronise\nIncompatible Match");
                                    //makeToast("| "+currentSlot().getName()+" | "+b+" |", 1);
                                    anims("closeDelayed", findViewById(R.id.scanvpet));
                                    if(alpha != null) alpha.setkl();
                                    srch = false;
                                    if(ardu != null) {
                                        if(ardu.isOpened()) ardu.close();
                                    }
                                    if (findThread != null) findThread.interrupt();
                                    if(scanThread != null) scanThread.interrupt();
                                    findThread = null;
                                    scanThread = null;
                                }
                            }
                        }
                    });
                }
            });
            thr.setDaemon(true);
            thr.start();
        }
    }
    private void onClickOKAdd() {
        String name = ((EditText)findViewById(R.id.namefield)).getText().toString().trim();
        if(name.equals("")) name = getName(extRom, commTypeSel);
        switch(LanguageFilter.checkName(name))  {
            case 0: break;
            case -1:
                ((TextView) findViewById(R.id.infoText)).setText("Nickname too long");
                return;
            case -2:
                //makeToast(name, 1);
                ((TextView) findViewById(R.id.infoText)).setText("Nickname Invalid");
                return;
            case -3:
                ((TextView) findViewById(R.id.infoText)).setText("Keep it clean!");
                return;
            case -4:
                ((TextView) findViewById(R.id.infoText)).setText("Nickname contains an illegal character");
                return;
        }


        anims("close", findViewById(R.id.scanvpet));

        ((TextView)findViewById(R.id.plainwintitle)).setText("A D D   P A R T N E R");
        ((TextView)findViewById(R.id.infotextplainwin)).setText("Add partner to this slot?");
        anims("expandslow", findViewById(R.id.plainwin));
    }
    private void add() {
        String name = ((EditText)findViewById(R.id.namefield)).getText().toString().trim();
        if(name.equals("")) name = getName(extRom, commTypeSel);

        Runnable task = new Runnable() {
            @Override
            public void run() {
                initialiseSlots();
                animateLoad();
                slotClick(false);
                closeWindows();
            }
        };

        user.addSlot(SlotN.slotN(name, extRom, commTypeSel));
        user.updateUserInDatabase(task);


    }
    private void sync() {
        if(user.getSlots()[slotSel].getName().equals(getName(user.getSlots()[slotSel].getDigiROM(), user.getSlots()[slotSel].getCommType())))
            user.getSlots()[slotSel].setName(getName(extRom, commTypeSel));

        user.getSlots()[slotSel].setDigiROM(extRom);
        user.getSlots()[slotSel].synchronise();
        user.updateUserInDatabase(new Runnable() {
            @Override
            public void run() {
                initialiseSlots();
                animateLoad();
                slotClick(false);
                closeWindows();
            }
        });
    }

    private void onClickDelete() {
        ((TextView)findViewById(R.id.plainwintitle)).setText("D E L E T E   P A R T N E R");
        ((TextView)findViewById(R.id.infotextplainwin)).setText("Permanently delete data from this slot?");

        anims("fadein", findViewById(R.id.bg));
        anims("expand", findViewById(R.id.plainwin));
    }
    private void delete() {
        // Check for a message that involves this slot, and update
        boolean needToUpdateChallenges = false;
        for(ChallengeN chal : BaseActivity.getInstance().getAllChallengesN().getAllChallenges()) {
            boolean updateMessage = false, notifyOpponent = false, updateUser = false;

            if      ((chal.getState() == 2 ) || (chal.getState() == 5) ) {
                if(chal.isA() && chal.getASlot() == currentSlot().getBirthdate() && chal.getState() == 5) {
                    log("DeleteMon", "ChallengeExpire -A");

                    if(chal.getWinner() == 0) user.winsUp(Utility.getUserFromLeaderboard(chal.getOpp()).getStats().getRank());
                    else if(chal.getWinner() == 1) user.lossUp();
                    if(chal.getWinner() != -1) user.updateUserInDatabase();

                    chal.setASlot(-1);
                    chal.setState(9);
                    updateMessage = true;
                    notifyOpponent = true;
                }
                if(!chal.isA() && chal.getBSlot() == currentSlot().getBirthdate() && chal.getState() == 2) {
                    log("DeleteMon", "ChallengeExpire -B");

                    if(chal.getWinner() == 1) user.winsUp(Utility.getUserFromLeaderboard(chal.getOpp()).getStats().getRank());
                    else if(chal.getWinner() == 0) user.lossUp();
                    if(chal.getWinner() != -1) user.updateUserInDatabase();

                    chal.setBSlot(-1);
                    chal.setState(10);
                    updateMessage = true;
                    notifyOpponent = true;
                }
            }
            else if ((chal.getState() == 1 && chal.isA()) || (chal.getState() == 4 && !chal.isA()) ) {
                if(chal.isA() && chal.getASlot() == currentSlot().getBirthdate()) {
                    chal.setASlot(-1);
                    chal.setState(9);
                    updateMessage = true;
                    notifyOpponent = true;
                }
                if(!chal.isA() && chal.getBSlot() == currentSlot().getBirthdate()) {
                    chal.setBSlot(-1);
                    chal.setState(10);
                    updateMessage = true;
                    notifyOpponent = true;
                }
            }

//            if(chal.isA() && chal.getASlot() == currentSlot().getBirthdate()) {
//                chal.setASlot(-1);
//                chal.setState(9);
//                updateMessage = true;
//            }
//            if(!chal.isA() && chal.getBSlot() == currentSlot().getBirthdate()){
//                chal.setBSlot(-1);
//                chal.setState(10);
//                updateMessage = true;
//            }

            if(updateMessage) {
                needToUpdateChallenges = true;
                chal.updateChallengeInDatabase();
            }
        }
        if(needToUpdateChallenges) base().getChallengesFromDatabase();

        // remove from local user
        user.deleteSlot(slotSel);
        slotCount = user.getSlots().length;
        user.updateUserInDatabase(new Runnable() {
            @Override
            public void run() {
                dontCloseWindows = false;
                closeWindows();
                initialiseSlots();
                slotClick(false);
                animateLoad();
            }
        });
    }

    private void onClickSend() {
        isChallenge = true;
        for(ChallengeN ch : BaseActivity.getInstance().getAllChallengesN().getAllChallenges()) {
            if( (ch.getA().equals(user.getName()) && ch.getASlot() == currentSlot().getBirthdate() && (ch.getState() == 1 || /*ch.getState() == 2 || */ch.getState() == 5)) ||
                (ch.getB().equals(user.getName()) && ch.getBSlot() == currentSlot().getBirthdate() && (ch.getState() == 4 || /*ch.getState() == 5 ||*/ ch.getState() == 2)) ) {
                return;
            }
        }

        findViewById(R.id.mon).setVisibility(View.GONE);
        findViewById(R.id.sndlwrcntrls).setVisibility(View.GONE);
        findViewById(R.id.sndcbig).setVisibility(View.GONE);
        findViewById(R.id.sndcsml).setVisibility(View.GONE);
        findViewById(R.id.sndcenterico).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.sndinfotext)).setText("");
        findViewById(R.id.prtnrslevellockcl).setVisibility(View.VISIBLE);

        String rom = user.getSlots()[slotSel].getDigiROM();
        int commType = user.getSlots()[slotSel].getCommType();
        ((ImageView) findViewById(R.id.sndcenterico)).setImageResource(getImageId(getIMGFileName(rom, commType)));

        anims("fadein", findViewById(R.id.bg));
        anims("expand", findViewById(R.id.sndwin));

        anims("expandslow", findViewById(R.id.mon));
        anims("expand", findViewById(R.id.sndlwrcntrls));
        anims("expandslow", findViewById(R.id.sndcenterico));
    }
    private void onClickSendOK() {
        ((TextView) findViewById(R.id.sndinfotext)).setText("");
        final String name = ((EditText)findViewById(R.id.opp)).getText().toString().trim();

        if(LanguageFilter.checkName(name)!=0) ((TextView) findViewById(R.id.sndinfotext)).setText("Invalid Username Enterred");
        else if(name.equals(user.getName())) ((TextView) findViewById(R.id.sndinfotext)).setText("You Cannot Challenge Yourself");
        else {
            for(ChallengeN c: BaseActivity.getInstance().getAllChallengesN().getAllChallenges()) {
                if ((c.getA().equals(name) || c.getB().equals(name)) &&
                        (c.getState() == 1 || c.getState() == 4 || c.getState() == 2 || c.getState() == 5 )){
                            ((TextView) findViewById(R.id.sndinfotext)).setText("A Challenge is Already Active");
                            return;
                }
            }

            RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
            StringRequest strRe = new StringRequest(Request.Method.GET, "https://www.alphahub.site/alphalink/api/protected/getUserByUsername/"+name,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response != null) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((EditText) findViewById(R.id.namefield)).setText("");
                                        anims("close", findViewById(R.id.sndlwrcntrls));

                                        switch(currentSlot().getCommType()) {
                                            case 1:
                                            case 3:
                                                presses(name);
                                                break;
                                            case 7:
                                                findViewById(R.id.presswin).setVisibility(View.INVISIBLE);
                                                findViewById(R.id.mon).setVisibility(View.VISIBLE);
                                                ((TextView) findViewById(R.id.sndinfotext)).setText("Sending...");
                                                anims("ringsTwo", null);
                                                postChallenge(name);
                                                break;
                                            case 8:
                                            case 9:
                                            case 91:
                                            case 92:
                                            case 93:
                                                presses(name);
                                                break;
                                        }
                                    }
                                });
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override public void onErrorResponse(VolleyError error) { Log.e("UpdateUserInDatabaseErr", error.getMessage()); }
                    }
            ) {
                @Override public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type","application/json");
                    return headers;
                }
            };

            rQ.add(strRe);
        }
    }
    private void presses(final String name) {
        dontCloseWindows = true;
        chrgcnt = 0;
        findViewById(R.id.prtnrslevellockcl).setVisibility(View.GONE);
        findViewById(R.id.chargeone).setVisibility(View.VISIBLE);
        findViewById(R.id.chargetwo).setVisibility(View.GONE);
        findViewById(R.id.chargethree).setVisibility(View.GONE);
        findViewById(R.id.chargefour).setVisibility(View.GONE);
        findViewById(R.id.chargefive).setVisibility(View.GONE);
        findViewById(R.id.chargesix).setVisibility(View.GONE);
        findViewById(R.id.chargeseven).setVisibility(View.GONE);

        anims("close", findViewById(R.id.mon));
        anims("expand", findViewById(R.id.presswin));


        new Handler().postDelayed(new Runnable() {
            public void run() {
                try {
                    canPress = false;
                    findViewById(R.id.presswin).setVisibility(View.INVISIBLE);
                    findViewById(R.id.mon).setVisibility(View.VISIBLE);
                    ((TextView) findViewById(R.id.sndinfotext)).setText("Sending...");
                    anims("ringsTwo", null);
                    postChallenge(name);
                    canPress = true;
                } catch(Exception e) {
                    Utility.catchCrash("[Partners : Presses]", e, getApplicationContext());
                }

            }
        }, 4200);
    }

    private void postChallenge(final String oppName) {
        int pr = (chrgcnt>31) ? 31 : chrgcnt;
        ch = null;

        if(opponentsList.contains(oppName)) {
            for(ChallengeN c : BaseActivity.getInstance().getAllChallengesN().getAllChallenges()) {
                if( c.getOpp().equals(oppName)) {
                    switch(currentSlot().getCommType()) {
                        case 1:
                            pr = (int) Math.floor(((float)pr)/1.9375f);
                            c.setDigiROM(RomManager.generatePen20RomV2FromBase(currentSlot().getDigiROM(), pr));
                            c.setCommType(1);
                            break;
                        case 3:
                            c.setDigiROM(RomManager.generateDM20RomV2FromBase(currentSlot().getDigiROM(), pr));
                            c.setCommType(3);
                            break;
                        case 7:
                            c.setDigiROM(currentSlot().getDigiROM());
                            c.setCommType(7);
                            break;
                        case 8:
                            pr = (int) Math.floor(((float)pr)/1.9375f);
                            c.setDigiROM(RomManager.generatePenOGRomV2FromBase(currentSlot().getDigiROM(), pr));
                            c.setCommType(8);
                            break;
                        case 9:
                            c.setDigiROM(RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr));
                            c.setCommType(9);
                            break;
                        case 91:
                            c.setDigiROM(RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr));
                            c.setCommType(91);
                            break;
                        case 92:
                            c.setDigiROM(RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr));
                            c.setCommType(92);
                            break;
                        case 93:
                            c.setDigiROM(RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr));
                            c.setCommType(93);
                            break;
                        default: break;
                    }
                    if(c.isA()){
                        c.setASlot(currentSlot().getBirthdate());
                        c.setState(1);
                    }
                    else{
                        c.setBSlot(currentSlot().getBirthdate());
                        c.setState(4);
                    }
                    c.updateLastseen();
                    ch = c;
                    break;
                }
                else log("PostMessage", "Opponent not in Friends List");
            }

            if(ch != null) {
                String level = ((Switch)findViewById(R.id.prtnrslevellockswitch)).isChecked() ?
                        (RomManager.getLevelGeneral(currentSlot().getDigiROM(), currentSlot().getCommType())) : "";
                ch.setLevel(level);
                ch.setMyCommType(currentSlot().getCommType());

                RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
                StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updateChallenge",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                if(response != null) {
                                    dontCloseWindows = false;
                                    closeWindows();
                                    //base().addChallenge(ch);
                                    notifyUser(oppName, new UserNotify(user.getName(), -1, ""+ch.getState(), "challenge"));
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override public void onErrorResponse(VolleyError error) { Log.e("UpdateUserInDatabaseErr", error.getMessage()); }
                        }
                 ) {
                    @Override public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Content-Type","application/json");
                        return headers;
                    }
                    @Override public byte[] getBody() { return ch.toJsonString().getBytes(); }
                    @Override public String getBodyContentType() {
                        return "application/json";
                    }
                };

                rQ.add(strRe);
            }
            else {
                makeToast("Something went wrong... [P0]", 1);
                closeWindows();
            }
        }
        else {
            String rom = "";
            switch(currentSlot().getCommType()) {
                case 1:
                    pr = (int) Math.floor(((float)pr)/1.9375f);
                    rom = RomManager.generatePen20RomV2FromBase(currentSlot().getDigiROM(), pr);
                    break;
                case 3:
                    rom = RomManager.generateDM20RomV2FromBase(currentSlot().getDigiROM(), pr);
                    break;
                case 7:
                    rom = currentSlot().getDigiROM();
                    break;
                case 8:
                    pr = (int) Math.floor(((float)pr)/1.9375f);
                    rom = RomManager.generatePenOGRomV2FromBase(currentSlot().getDigiROM(), pr);
                    break;
                case 9:
                case 91:
                case 92:
                case 93:
                    rom = RomManager.generatePenxRomV2FromBase(currentSlot().getDigiROM(), pr);
                    break;
                default: break;
            }

            String level = ((Switch)findViewById(R.id.prtnrslevellockswitch)).isChecked() ?
                    (RomManager.getLevelGeneral(currentSlot().getDigiROM(), currentSlot().getCommType())) : "";

            ch = new ChallengeN(oppName, rom, currentSlot().getCommType(), currentSlot().getBirthdate(), level);

            RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
            StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/addChallenge",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response != null) {
                                dontCloseWindows = false;
                                if(!opponentsList.contains(oppName)) { initialiseSpinner(); }
                                closeWindows();
                                base().addChallenge(ch);
                                notifyUser(oppName, new UserNotify(user.getName(), -1, ""+ch.getState(), "challenge"));
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override public void onErrorResponse(VolleyError error) { Log.e("UpdateUserInDatabaseErr", error.getMessage()); }
                    }
            ) {
                @Override public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type","application/json");
                    return headers;
                }
                @Override public byte[] getBody() { return ch.toJsonString().getBytes(); }
                @Override public String getBodyContentType() {
                    return "application/json";
                }
            };

            rQ.add(strRe);
        }
    }
    private void notifyUser(final String name, UserNotify notif) {
        MQTTInterface.publishMessage(name, notif);
    }

    private void anims(String cmd, final View v) {
        Animation
                focus = AnimationUtils.loadAnimation(this, R.anim.clickfocus),
                loseFocus = AnimationUtils.loadAnimation(this, R.anim.clicklosefocus),
                pulse = AnimationUtils.loadAnimation(this, R.anim.pulse),
                expand = AnimationUtils.loadAnimation(this, R.anim.expandone),
                close = AnimationUtils.loadAnimation(this, R.anim.closeone),
                expandslow = AnimationUtils.loadAnimation(this, R.anim.expandoneslow),
                closefast = AnimationUtils.loadAnimation(this, R.anim.closefast),
                closedelayed = AnimationUtils.loadAnimation(this, R.anim.closedelayed),
                fade = AnimationUtils.loadAnimation(this, R.anim.fade),
                fadein = AnimationUtils.loadAnimation(this, R.anim.fadein),
                rcw = AnimationUtils.loadAnimation(this, R.anim.rotatecw),
                rccw = AnimationUtils.loadAnimation(this, R.anim.rotateccw),
                pulsering = AnimationUtils.loadAnimation(this, R.anim.pulsering);

        if(v != null) v.setVisibility(View.VISIBLE);

        switch(cmd) {
            case "focus":
                v.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200);
                break;
            case "loseFocus":
                v.animate().scaleX(1.0f).scaleY(1.0f).setDuration(200);
                break;
            case "negFocus":
                v.animate().scaleX(-1.2f).scaleY(1.2f).setDuration(200);
                break;
            case "negLoseFocus":
                v.animate().scaleX(-1.0f).scaleY(1.0f).setDuration(200);
                break;
            case "pulse":
                v.startAnimation(pulse);
                break;
            case "expand":
                v.startAnimation(expand);
                break;
            case "close":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(close);
                break;
            case "expandslow":
                v.startAnimation(expandslow);
                break;
            case "closefast":
                v.startAnimation(closefast);
                v.setVisibility(View.GONE);
                break;
            case "closeDelayed":
                anims("fade", findViewById(R.id.bg));
                closedelayed.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}

                });
                v.startAnimation(closedelayed);
                break;
            case "fade":
                fade.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(fade);
                break;
            case "fadein":
                v.startAnimation(fadein);
                break;
            case "rings":
                ringsSpinOne = true;
                ((ImageView) findViewById(R.id.csml)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.cbig)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.csml)).startAnimation(rcw);
                ((ImageView) findViewById(R.id.cbig)).startAnimation(rccw);
                ((TextView) findViewById(R.id.infoText)).startAnimation(fadein);
                break;
            case "closeRings":
                ringsSpinOne = false;
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.csml).setVisibility(View.GONE);
                        findViewById(R.id.cbig).setVisibility(View.GONE);}
                });
                ((ImageView) findViewById(R.id.csml)).startAnimation(close);
                ((ImageView) findViewById(R.id.cbig)).startAnimation(close);
                break;
            case "ringsTwo":
                ringsSpinTwo = true;
                ((ImageView) findViewById(R.id.sndcsml)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.sndcbig)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.sndcsml)).startAnimation(rcw);
                ((ImageView) findViewById(R.id.sndcbig)).startAnimation(rccw);
                break;
            case "closeRingsTwo":
                ringsSpinTwo = false;
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.sndcsml).setVisibility(View.GONE);
                        findViewById(R.id.sndcbig).setVisibility(View.GONE);}
                });
                ((ImageView) findViewById(R.id.sndcsml)).startAnimation(close);
                ((ImageView) findViewById(R.id.sndcbig)).startAnimation(close);
                break;
            case "fail":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.csml).setVisibility(View.GONE);
                        findViewById(R.id.cbig).setVisibility(View.GONE);
                        findViewById(R.id.centerico).setVisibility(View.INVISIBLE);}
                });
                ((ImageView) findViewById(R.id.csml)).startAnimation(close);
                ((ImageView) findViewById(R.id.cbig)).startAnimation(close);
                break;
            case "scs":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.csml).setVisibility(View.GONE);
                        findViewById(R.id.cbig).setVisibility(View.GONE);}
                });
                ((ImageView) findViewById(R.id.csml)).startAnimation(close);
                ((ImageView) findViewById(R.id.cbig)).startAnimation(close);
                ((ImageView) findViewById(R.id.centerico)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.centerico)).startAnimation(expand);
                break;
            case "pulsering":
                findViewById(R.id.presspulsering).setVisibility(View.VISIBLE);

                pulsering.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.presspulsering).setVisibility(View.GONE);}
                });

                findViewById(R.id.presspulsering).startAnimation(pulsering);
                break;
            case "bop":
                v.setPivotY(v.getHeight());
                v.animate()
                    .setStartDelay(150)
                    .setDuration(20)
                    .scaleY(0.93f);

                v.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) { }
                    @Override
                    public void onAnimationRepeat(Animation animation) { }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.monico).animate()
                            .setStartDelay(400)
                            .setDuration(20)
                            .scaleY(1);

                        findViewById(R.id.monico).getAnimation().setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) { }
                            @Override
                            public void onAnimationRepeat(Animation animation) { }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                findViewById(R.id.monico).clearAnimation();
                            }
                        });
                    }
                });
                break;
            default:
                break;
        }
    }
    private void animateCountDown() {
        connect(0);

        alpha.setCommType(isSync ? currentSlot().getCommType() : commTypeSel);
        alpha.sendInert();

        expOne = AnimationUtils.loadAnimation(this, R.anim.expandtwo);
        expTwo = AnimationUtils.loadAnimation(this, R.anim.expandtwo);
        expThree = AnimationUtils.loadAnimation(this, R.anim.expandtwo);

        ((ImageView) findViewById(R.id.centerico)).setImageResource(R.drawable.ic_cntdwn_three);
        ((ImageView) findViewById(R.id.centerico)).setVisibility(View.VISIBLE);

        expOne.setStartOffset(500);
        expOne.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
                ((ImageView) findViewById(R.id.centerico))
                        .setImageResource(R.drawable.ic_cntdwn_two);
                ((ImageView) findViewById(R.id.centerico))
                        .startAnimation(expTwo);
            }
        });
        expTwo.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
                ((ImageView) findViewById(R.id.centerico))
                        .setImageResource(R.drawable.ic_cntdwn_one);

                ((ImageView) findViewById(R.id.centerico))
                        .startAnimation(expThree);
            }
        });
        expThree.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
                anims("close", findViewById(R.id.centerico));
                connectVisibility(1);
            }
        });

        ((ImageView) findViewById(R.id.centerico)).startAnimation(expOne);
    }

    private int getImageId(String imageName) { return Utility.getImageId(imageName, this); }
    private String getIMGFileName(String rom, int commType) { return Utility.getIMGFileNameFromRom(rom, commType);}
    private String getName(String rom, int commType) { return Utility.getMonName(rom, commType); }
    private boolean isDuplicate() {
        switch(commTypeSel) {
            case 3:
                for(SlotN s : user.getSlots()) {
                    if(s != null && slotCount != 0) {
                        if( (s.getCommType() == commTypeSel) &&
                                (s.getDigiROM().substring(0, 9).equals(extRom.substring(0, 9))) && // Check Name field
                                (getName(s.getDigiROM(), s.getCommType()).equals(getName(extRom, commTypeSel))) )
                                    return true;
                    }
                }
                return false;
            case 1:
            case 7:
                for(SlotN s : user.getSlots()) {
                    if(s != null && slotCount != 0) {
                        if( (s.getCommType() == commTypeSel) &&
                            (getName(s.getDigiROM(), s.getCommType()).equals(getName(extRom, commTypeSel))) )
                                return true;
                    }
                }
                return false;

            default: return false;
        }
    }
    private SlotN currentSlot() { return user.getSlots().length > slotSel ? user.getSlots()[slotSel]  : null; }

    @Override public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.mloneico:
                case R.id.mlonelock:
                    slotSel = 0;
                    slotClick(true);
                    break;
                case R.id.mltwoico:
                case R.id.mltwolock:
                    slotSel = 1;
                    slotClick(true);
                    //Utility.makeCrash();
                    break;
                case R.id.mlthreeico:
                case R.id.mlthreelock:
                    slotSel = 2;
                    slotClick(true);
                    break;
                case R.id.mlfourico:
                case R.id.mlfourlock:
                    slotSel = 3;
                    slotClick(true);
                    break;
                case R.id.mlfiveico:
                case R.id.mlfivelock:
                    slotSel = 4;
                    slotClick(true);
                    break;
                case R.id.mlsixico:
                case R.id.mlsixlock:
                    slotSel = 5;
                    slotClick(true);
                    break;
                case R.id.del:
                    anims("pulse", v);
                    onClickDelete();
                    break;
                case R.id.sync:
                    anims("pulse", v);
                    onClickSync();
                    break;
                case R.id.send:
                    anims("pulse", v);
                    if (!user.getSlots()[slotSel].isLocked()) onClickSend();
                    break;
                case R.id.bg:
                case R.id.checkcancel:
                case R.id.declineplainwin:
                case R.id.sndcancel:
                case R.id.decline:
                    anims("pulse", v);
                    closeWindows();
                    break;
                case R.id.checkok:
                    anims("pulse", v);

                    if (((TextView) findViewById(R.id.checktitle)).getText().toString().contains("A D D")) {
                        findViewById(R.id.spinner).setVisibility(View.VISIBLE);
                        anims("close", findViewById(R.id.check));
                        anims("expand", findViewById(R.id.cnctardu));
                        findArduino();
                    } else if (((TextView) findViewById(R.id.checktitle)).getText().toString().contains("S Y N C H")) {
                        findViewById(R.id.spinner).setVisibility(View.GONE);
                        anims("close", findViewById(R.id.check));
                        anims("expand", findViewById(R.id.cnctardu));
                        commTypeSel = currentSlot().getCommType();
                        findArduino();
                    }
                    break;
                case R.id.btnSCAN:
                    anims("pulse", findViewById(R.id.btnSCAN));
                    onClickScan();
                    break;
                case R.id.accept:
                    anims("pulse", v);
                    onClickOKAdd();
                    break;
                case R.id.acceptplainwin:
                    anims("pulse", v);
                    if (((TextView) findViewById(R.id.plainwintitle)).getText().toString().contains("D E L E T E"))
                        delete();
                    if (((TextView) findViewById(R.id.plainwintitle)).getText().toString().contains("A D D"))
                        add();
                    break;
                case R.id.sndaccept:
                    anims("pulse", v);
                    onClickSendOK();
                    break;
                case R.id.pressbtn:
                    if (canPress) {
                        anims("pulse", v);
                        anims("pulsering", null);
                        chrgcnt++;

                        switch (chrgcnt) {
                            case 0:
                                findViewById(R.id.chargeone).setVisibility(View.VISIBLE);
                                break;
                            case 3:
                                findViewById(R.id.chargetwo).setVisibility(View.VISIBLE);
                                break;
                            case 5:
                                findViewById(R.id.chargethree).setVisibility(View.VISIBLE);
                                break;
                            case 7:
                                findViewById(R.id.chargefour).setVisibility(View.VISIBLE);
                                break;
                            case 9:
                                findViewById(R.id.chargefive).setVisibility(View.VISIBLE);
                                break;
                            case 11:
                                findViewById(R.id.chargesix).setVisibility(View.VISIBLE);
                                break;
                            case 13:
                                findViewById(R.id.chargeseven).setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                    break;
                case R.id.monico:
                    anims("pulse", v);
                    if (slotCount == 0 || slotSel == slotCount) {
                        slotClick(true);
                    } else {
                        anims("bop", v);
                    }
                    break;
                case R.id.prtnrslevellockswitch:
                    if (((Switch) v).isChecked()) v.setAlpha(0.7f);
                    else v.setAlpha(0.3f);
//                (RomManager.getLevelGeneral(currentSlot().getDigiROM(), currentSlot().getCommType()));
                    break;
                default:
                    break;
            }
        }
        catch (Exception e) {
            Utility.catchCrash("[PARTNERS : ONCLICK]", e, this);
        }
    }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            closeWindows();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    private void closeWindows() {
        if(findViewById(R.id.bg).getVisibility() == View.VISIBLE && !dontCloseWindows) {
            switch(slotCount){
                case 0:
                    anims("loseFocus", findViewById(R.id.mlbone));
                    anims("loseFocus", findViewById(R.id.mlboneshadow));
                    anims("negLoseFocus", findViewById(R.id.mloneico));
                    break;
                case 1:
                    anims("loseFocus", findViewById(R.id.mlbtwo));
                    anims("loseFocus", findViewById(R.id.mlbtwoshadow));
                    anims("negLoseFocus", findViewById(R.id.mltwoico));
                    break;
                case 2:
                    anims("loseFocus", findViewById(R.id.mlbthree));
                    anims("loseFocus", findViewById(R.id.mlbthreeshadow));
                    anims("negLoseFocus", findViewById(R.id.mlthreeico));
                    break;
                case 3:
                    anims("loseFocus", findViewById(R.id.mlbfour));
                    anims("loseFocus", findViewById(R.id.mlbfourshadow));
                    anims("negLoseFocus", findViewById(R.id.mlfourico));
                    break;
                case 4:
                    anims("loseFocus", findViewById(R.id.mlbfive));
                    anims("loseFocus", findViewById(R.id.mlbfiveshadow));
                    anims("negLoseFocus", findViewById(R.id.mlfiveico));
                    break;
                case 5:
                    anims("loseFocus", findViewById(R.id.mlbsix));
                    anims("loseFocus", findViewById(R.id.mlbsixshadow));
                    anims("negLoseFocus", findViewById(R.id.mlsixico));
                    break;

            }
            //findViewById(R.id.bg).setVisibility(View.GONE);
            anims("fade", findViewById(R.id.bg));

            if(findViewById(R.id.check).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.check));

            if(findViewById(R.id.cnctardu).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.cnctardu));

            if(findViewById(R.id.scanvpet).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.scanvpet));

            if(findViewById(R.id.plainwin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.plainwin));

            if(findViewById(R.id.presswin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.presswin));

            if(findViewById(R.id.sndwin).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.sndwin));
                anims("closeRingsTwo", null);
            }

            if(findViewById(R.id.prtnrscountdownloading).getVisibility() == View.VISIBLE){
                anims("close", findViewById(R.id.prtnrscountdownloading));
            }
        }

        ((EditText)findViewById(R.id.namefield)).setText("");
        if(alpha != null) alpha.setkl();
        srch = false;
        if(ardu != null) {
            if(ardu.isOpened()) ardu.close();
        }
        if (findThread != null) findThread.interrupt();
        if(scanThread != null) scanThread.interrupt();
        findThread = null;
        scanThread = null;
    }
    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        switch(item) {
            case "<Digimon Original VPet>":
                commTypeSel = 7;
                ((TextView) findViewById(R.id.commtext)).setText(item);
                break;
            case "<Digimon 20th VPet>":
                commTypeSel = 3;
                ((TextView) findViewById(R.id.commtext)).setText(item);
                break;
            case "<Pendulum 20th VPet>":
                commTypeSel = 1;
                ((TextView) findViewById(R.id.commtext)).setText(item);
                break;
            case "<Pendulum Original VPet>":
                commTypeSel = 8;
                ((TextView) findViewById(R.id.commtext)).setText(item);
                break;
            case "<Pendulum X VPet>":
                commTypeSel = 9;
                ((TextView) findViewById(R.id.commtext)).setText(item);
                break;
            case "<Digimon Mini VPet V1>":
                commTypeSel = 91;
                ((TextView) findViewById(R.id.commtext)).setText(item);
                break;
            case "<Digimon Mini VPet V2>":
                commTypeSel = 92;
                ((TextView) findViewById(R.id.commtext)).setText(item);
                break;
            case "<Digimon Mini VPet V3>":
                commTypeSel = 93;
                ((TextView) findViewById(R.id.commtext)).setText(item);
                break;
            default:
                ((EditText)findViewById(R.id.opp)).setText(item);
                break;
        }

        if(ringsSpinOne) {
            anims("closeRings", null);
            ((TextView) findViewById(R.id.infoText)).setText("Select Device and Press Scan");
        }
        if(ringsSpinTwo) {
            anims("closeRingsTwo", null);
            ((TextView) findViewById(R.id.infoText)).setText("Select Device and Press Scan");
        }

        if(!isChallenge){
            //if(commTypeSel == 1) makeToast("WARNING: Pen20 is still experimental. Contact the developer if you experience issues.", 1);
            if(commTypeSel == 8) makeToast("WARNING: PenOG is still experimental. Contact the developer if you experience issues.", 1);
            else if(commTypeSel == 7) makeToast("WARNING: Do NOT use this option for a DM20th or Pen20th VPet", 1);

        }
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public static Partners getInstance(){ return k; }
    private BaseActivity base() { return BaseActivity.getInstance(); }
    public void setUser(UserN user) { this.user = user; }
    public UserN getUser() { return user; }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void makeToast(final String t, final int len) {
        TextView tv = new TextView(getApplicationContext());
        tv.setGravity(Gravity.CENTER);
        tv.setText(t);
        tv.setPadding(30, 35, 35, 30);
        tv.setTextColor(Color.WHITE);
        tv.setBackgroundResource(R.drawable.toastbg);

        Toast toast = new Toast(getApplicationContext());
        toast.setView(tv);
        toast.setDuration(len);
        toast.show();
    }
    private void log(String T, String M) {
        if(debugLog) Log.e(T, M);
    }
}