package com.alpha.alphalink;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Info extends AppCompatActivity implements View.OnClickListener {
    static Info k;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        k = this;
        init();

        title = (TextView) findViewById(R.id.title);
    }

    private void init() {
        findViewById(R.id.infobg).setBackgroundColor(getResources().getColor(R.color.colorBg));

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = ((int) (dm.widthPixels - (50*dm.density))), height = dm.heightPixels;
        getWindow().setLayout(width, height);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.RIGHT;


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        // ...but notify us that it happened.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        setVersionNumber();
    }

    public void goTo (View view) {
        Uri uriUrl = Uri.parse("http://alphaterminal.netlify.com/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void sinobaliGoTo (View view) {
        Uri uriUrl = Uri.parse("https://ko-fi.com/sinobali");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    private void setVersionNumber() {
        String vers = "";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            vers = pInfo.versionName;
            ((TextView)findViewById(R.id.versionnumber)).setText(vers);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            closeWindows();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public static Info getInstance(){
        return   k;
    }

    private void closeWindows() {
        if(findViewById(R.id.bg).getVisibility() == View.VISIBLE) {
            anims("fade", findViewById(R.id.bg));

            if (findViewById(R.id.creditswin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.creditswin));


            if (findViewById(R.id.infouawin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.infouawin));
        }
    }
    private void anims(String cmd, final View v) {
        Animation
        open = AnimationUtils.loadAnimation(this, R.anim.openone),
        expand = AnimationUtils.loadAnimation(this, R.anim.expandone),
        fadein = AnimationUtils.loadAnimation(this, R.anim.fadein),
        pulse = AnimationUtils.loadAnimation(this, R.anim.pulse),
        close = AnimationUtils.loadAnimation(this, R.anim.closeone),
        fade = AnimationUtils.loadAnimation(this, R.anim.fade);

        if(v != null) v.setVisibility(View.VISIBLE);

        switch(cmd){
            case "expand":
                v.startAnimation(expand);
                break;
            case "pulse":
                v.startAnimation(pulse);
                break;
            case "close":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(close);
                break;
            case "fade":
                fade.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(fade);
                break;
            case "fadein":
                v.startAnimation(fadein);
                break;
            default: break;
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.bg:
                closeWindows();
                break;
            case R.id.box2:
                anims("fadein", findViewById(R.id.bg));
                anims("expand", findViewById(R.id.creditswin));
                break;
            case R.id.box4:
                anims("fadein", findViewById(R.id.bg));
                anims("expand", findViewById(R.id.infouawin));
                break;
            default:
                break;
        }
    }
}
