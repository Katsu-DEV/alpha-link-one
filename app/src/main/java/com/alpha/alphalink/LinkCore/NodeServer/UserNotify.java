
package com.alpha.alphalink.LinkCore.NodeServer;

import com.alpha.alphalink.LinkCore.ChallengeManager;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UserNotify {

    @SerializedName("rowid")
    @Expose
    private Integer rowid;

    @SerializedName("from")
    @Expose
    private String from;

    @SerializedName("what")
    @Expose
    private String what;

    @SerializedName("notchk")
    @Expose
    private String notchk;

    @SerializedName("type")
    @Expose
    private String type;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getNotchk() {
        return notchk;
    }

    public void setNotchk(String notchk) {
        this.notchk = notchk;
    }

    public UserNotify() {}

    public UserNotify(String from, Integer rowid, String what, String type) {
        this.from = from;
        this.rowid = rowid;
        this.what = what;
        this.notchk = ChallengeManager.messageStamp(new Date());
        this.type = type;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public String getDetails() {
        if(type.equals("message")) {
            return what.length() > 38 ? what.substring(0, 38)+"..." : what;
        }
        else if(type.equals("challenge")) {
            String ret = "New Challenge activity with " + from;
            return ret.length() > 34 ? ret.substring(0, 34)+"..." : ret;
        }
        else return "ERROR";
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isGlobalLink() {
        return (type.equals("globalLinkGeneral") || type.equals("globalLinkBattle") || type.equals("globalLinkHelp"));
    }

    public String toJsonString() {
        return new Gson().toJson(this);
    }

    public static UserNotify getEmpty() {
        return new UserNotify("`!`", -1, "`!`", "`1`");
    }
}
