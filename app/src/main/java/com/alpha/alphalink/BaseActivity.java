package com.alpha.alphalink;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.alpha.alphalink.LinkCore.NodeServer.AllChallengesN;
import com.alpha.alphalink.LinkCore.NodeServer.ChallengeN;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;
import com.alpha.alphalink.LinkCore.NodeServer.UserNotify;
import com.alpha.alphalink.LinkCore.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class BaseActivity extends AppCompatActivity {
    static BaseActivity k;
    private TextView title;
    private ImageView profile, challenges, livelink, alphacom, settings;
    private UserN user;
    private UserN[] leaderboard;
    private long lastLeaderboardRefresh = 0;
    private int refreshTime = (5*60*1000);
    private int[] open = {9, 9};
    private boolean boot = true, keepRefreshingLeaderboard = false;
    private String FILENAME_CH = "filech.txt", FILENAME_DM = "filedm.txt", command = "";
    private AllChallengesN allChallenges;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        k = this;

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Utility.catchCrash("[Base : Uncaught]", paramThrowable, getApplicationContext());
                System.exit(2);
            }
        });

        Utility.resetSerialLog();
        Utility.resetCrashLog();

        try {
            init();
        }
        catch (Exception e) {
            Utility.catchCrash("[BASE : ONSTART]", e, this);
        }
    }

    public void init() {
        title = (TextView) findViewById(R.id.title);
        profile = (ImageView) findViewById(R.id.profile);
        challenges = (ImageView) findViewById(R.id.challenges);
        livelink = (ImageView) findViewById(R.id.livelink);
        alphacom = (ImageView) findViewById(R.id.partners);
        settings = (ImageView) findViewById(R.id.settings);

        setIcoAlphas();

        if(BackgroundService.getInstance() != null) user = BackgroundService.getInstance().getUser();
        else if(MainActivity.getInstance() != null) user = MainActivity.getInstance().getUser();
        if (user == null) {
            if(BackgroundService.getInstance() != null) {
                BackgroundService.getInstance().getUserFromDatabase(new Runnable() {
                    @Override
                    public void run() {
                        BaseActivity.getInstance().setUser(BackgroundService.getInstance().getUser());
                    }
                }, true);
            }
        }

        if(MainActivity.getInstance() != null) MainActivity.getInstance().finish();

        if(user == null) makeToast("Something went wrong... [B0]", 1);

        title.setText("Welcome back,\n" + user.getName() + "!");

        anims("boot");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        makeToast("Welcome to Alpha Link! This app is still in Beta. Please report crashes or strange behaviour to the developer.", 1);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                getChallengesFromDatabase();
                getLeaderboardFromDatabase();
            }
        });
        t.setDaemon(true);
        t.start();

        startBackgroundService();
        obeyCommand();
        MainActivity.getInstance().finish();
        testBench();
    }

    public void obeyCommand() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try{
                    command = BackgroundService.getInstance().getCommand();
                    switch(command) {
                        case "cleanall":
                            Log.e("Command", "cleanall");
                            cleanActivities();
                            user = BackgroundService.getInstance().getUser();
                            BackgroundService.getInstance().resetCommand();
                            break;
                        case "newmessage":
                            Log.e("command", "New Message");
                            cleanActivities();
                            screenSelect("profile", "newmessage");
                            //BackgroundService.getInstance().resetCommand();
                            break;
                        case "newchallenge":
                            Log.e("command", "New Challenge");
                            cleanActivities();
                            screenSelect("challenges", "newchallenge");
                            BackgroundService.getInstance().resetCommand();
                            break;
                        default: break;
                    }
                }
                catch(Exception e) {
                    obeyCommand();
                }
            }
        }, 400);
    }

    public void startBackgroundService() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if( ((BackgroundService.getInstance() != null && (!BackgroundService.getInstance().isListening()))
                        || BackgroundService.getInstance() == null)
                        && command.equals("")) {
                    Intent serviceIntent = new Intent(getApplicationContext(), BackgroundService.class);
                    serviceIntent.putExtra("User", user);
                    serviceIntent.putExtra("uid", user.getUid());
                    ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);
                    Log.e("Made", "New BG Service");
                }
            }
        });

        t.setDaemon(true);
        t.start();
    }

    private void anims(String command) {
        Animation dropIna, dropInb, dropInc, dropInd, dropIne, fadeIn;

        dropIna = AnimationUtils.loadAnimation(this, R.anim.dropin);
        dropInb = AnimationUtils.loadAnimation(this, R.anim.dropin);
        dropInc = AnimationUtils.loadAnimation(this, R.anim.dropin);
        dropInd = AnimationUtils.loadAnimation(this, R.anim.dropin);
        dropIne = AnimationUtils.loadAnimation(this, R.anim.dropin);
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);

        if(command.equals("boot")){
            dropIna.setStartOffset(970);
            profile.startAnimation(dropIna);

            dropInb.setStartOffset(840);
            challenges.startAnimation(dropInb);

            dropInc.setStartOffset(750);
            livelink.startAnimation(dropInc);

            dropInd.setStartOffset(600);
            alphacom.startAnimation(dropInd);

            dropIne.setStartOffset(150);
            settings.startAnimation(dropInd);

            fadeIn.setStartOffset(900);
            title.startAnimation(fadeIn);

            findViewById(R.id.basebglogoico).animate()
                    .alpha(0.04f)
                    .setDuration(2200)
                    .setStartDelay(600)
                    .start();
        }

        if(command.equals("bootold")){
            dropIna.setStartOffset(970);
            profile.startAnimation(dropIna);

            dropInb.setStartOffset(840);
            challenges.startAnimation(dropInb);

            dropInc.setStartOffset(750);
            livelink.startAnimation(dropInc);

            dropInd.setStartOffset(600);
            alphacom.startAnimation(dropInd);

            //dropIne.setStartOffset(150);
            //settings.startAnimation(dropIne);

            fadeIn.setStartOffset(900);
            title.startAnimation(fadeIn);
        }
    }

    public void getChallengesFromDatabase() {
        getChallengesFromDatabase(null);
    }
    public void getChallengesFromDatabase(final Runnable task)  {
        String URL = "https://www.alphahub.site/alphalink/api/protected/getChallengesByName/"+user.getName();

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,  URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        allChallenges = (new Gson()).fromJson(response.toString(), AllChallengesN.class);
                        //chals = allChallenges.getChallengesArray();
                        if(task != null) task.run();
                        if(Challenges.getInstance() != null) Challenges.getInstance().updateChallenges(getChalsByActivity());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Get All Challenges Error", error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }
    public void getUserFromDatabase(final Runnable task) {
        String URL = "https://www.alphahub.site/alphalink/api/protected/getUserByUsername/"+user.getName();

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String resp = response.toString();
                        if(resp.contains("'user doesn't exist'")) Log.e("GetUser", "Doesn't Exist");
                        else {
                            user = new Gson().fromJson(resp, UserN.class);
                            switch(open[1]) {
                                case 0:
                                    Profile.getInstance().setUser(user);
                                    break;
                                case 1:
                                    Challenges.getInstance().setUser(user);
                                    break;
                                case 2:
                                    //LiveLink.getInstance().setUser(user);
                                    break;
                                case 3:
                                    Partners.getInstance().setUser(user);
                                    break;
                                case 4:
                                    Settings.getInstance().setUser(user);
                                    break;

                                default: break;
                            }

                            if(task != null) task.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("eRESP", ": "+error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }
    public void getLeaderboardFromDatabase() { getLeaderboardFromDatabase(null); }
    public void getLeaderboardFromDatabase(final Runnable task) {
        lastLeaderboardRefresh = Utility.getTime();
        String URL = "https://www.alphahub.site/alphalink/api/leaderboard";

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            leaderboard = new Gson().fromJson(response.getString("users"), UserN[].class);
                            if(task != null) task.run();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                            Log.e("Error", " > "+Log.getStackTraceString(e.getCause()));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("eRESP", error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }

    public void screenSelect(final String screen) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch(screen) {
                    case "profile":
                        screenSelect(findViewById(R.id.profile));
                        break;

                    case "challenges":
                        screenSelect(findViewById(R.id.challenges));
                        break;

                    case "partners":
                        screenSelect(findViewById(R.id.partners));
                        break;

                    case "settings":
                        screenSelect(findViewById(R.id.settings));
                        break;
                    default: break;
                }
            }
        });
    }
    public void screenSelect(final String screen, final String command) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch(screen) {
                    case "profile":
                        screenSelect(findViewById(R.id.profile), command);
                        break;
                    case "challenges":
                        screenSelect(findViewById(R.id.challenges), command);
                        break;
                    case "partners":
                        screenSelect(findViewById(R.id.partners), command);
                        break;
                    case "settings":
                        screenSelect(findViewById(R.id.settings), command);
                        break;
                    default: break;
                }
            }
        });
    }
    public void screenSelect(View v) { screenSelect(v, ""); }
    public void screenSelect(View v, String command){
        //stopService(new Intent(BaseActivity.this, BackgroundService.class));
        title.setText("");
        setIcoAlphas();
        v.setAlpha(0.85f);
        Intent intent = new Intent();
        int id = v.getId(), pgSel = 0;

        switch (id) {
            case R.id.profile:
                pgSel = 0;
                break;
            case R.id.challenges:
                ( (ImageView) findViewById(R.id.challenges) ).setColorFilter(Color.parseColor("#000000"));
                pgSel = 1;
                break;
            case R.id.livelink:
                pgSel = 2;
                break;
            case R.id.partners:
                pgSel = 3;
                break;
            case R.id.settings:
                pgSel = 4;
                break;
            case R.id.info:
                pgSel = 5;
                break;
            default: break;
        }

        if(Math.abs(open[1]) != pgSel) {
            switch (pgSel) {
                case 0:
                    intent = new Intent(BaseActivity.this, Profile.class);
                    break;
                case 1:
                    intent = new Intent(BaseActivity.this, Challenges.class);
                    break;
                case 2:
                    //intent = new Intent(BaseActivity.this, LiveLink.class);
                    break;
                case 3:
                    intent = new Intent(BaseActivity.this, Partners.class);
                    break;
                case 4:
                    intent = new Intent(BaseActivity.this, Settings.class);
                    break;
                case 5:
                    intent = new Intent(BaseActivity.this, Info.class);
                    break;
            }

            cleanActivities();

            open[0] = open[1];
            open[1] = pgSel;

            try {
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("User", user);
                intent.putExtra("chals", getChals());
                intent.putExtra("leaderboard", leaderboard);
                intent.putExtra("command", command);
            } catch(Exception e) {
                sleep(200);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("User", user);
                intent.putExtra("chals", getChals());
                intent.putExtra("leaderboard", leaderboard);
                intent.putExtra("command", command);
            }


            startActivity(intent);
        }
    }
    private void cleanActivity(int s) {
        switch(s) {
            case 0:
                Profile.getInstance().finish();
                break;
            case 1:
                Challenges.getInstance().finish();
                break;
            case 2:
                //LiveLink.getInstance().finish();
                break;
            case 3:
                Partners.getInstance().finish();
                break;
            case 4:
                Settings.getInstance().finish();
                break;
            case 5:
                Info.getInstance().finish();
                break;
            default: break;
        }
    }
    private void cleanActivities() {
        if(Profile.getInstance() != null) Profile.getInstance().finish();
        if(Challenges.getInstance() != null) Challenges.getInstance().finish();
        //if(LiveLink.getInstance() != null) LiveLink.getInstance().finish();
        if(Partners.getInstance() != null) Partners.getInstance().finish();
        if(Settings.getInstance() != null) Settings.getInstance().finish();
        if(Info.getInstance() != null) Info.getInstance().finish();
        open[1] *= -1;
    }
    public boolean isScreenOpen(String screen) {
        switch(screen) {
            case "profile":
                return (open[1] == 0) && Profile.getInstance() != null;
            case "challenges":
                return (open[1] == 1) && Challenges.getInstance() != null;
            case "partners":
                return (open[1] == 3) && Partners.getInstance() != null;
            case "settings":
                return (open[1] == 4) && Settings.getInstance() != null;
            default: return false;
        }
    }
    private void setIcoAlphas(){
        ImageView[] icos = {(ImageView) findViewById(R.id.profile),
                (ImageView) findViewById(R.id.challenges),
                (ImageView) findViewById(R.id.livelink),
                (ImageView) findViewById(R.id.settings),
                (ImageView) findViewById(R.id.partners),
                (ImageView) findViewById(R.id.info)
        };

        for(ImageView ico : icos) ico.setAlpha(0.25f);
    }

    public void setUser(UserN user) { this.user = user; }
    public UserN getUser() { return user; }
    public AllChallengesN getAllChallengesN() { return allChallenges; }
    public LinkedHashMap<String, ChallengeN> getChals() { return allChallenges.getChallengesArrayOrderedByEvent(); }
    public LinkedHashMap<String, ChallengeN> getChalsByActivity() { return allChallenges.getChallengesArrayOrderedByActivity(); }
    public void addChallenge(ChallengeN challenge) {allChallenges.addChal(challenge);}
    public UserN[] getLeaderboard() {
        if(leaderboard == null) {
            leaderboard = new UserN[] {};
        }

        return leaderboard;
    }
    public void setLeaderboard(UserN[] leaderboard) { this.leaderboard = leaderboard; }

    public void notifySelf(ChallengeN m) {
        user.setChalchk(UserNotify.getEmpty().toJsonString());
        if(m != null) allChallenges.addChal(m);

        if(open[1] == 1); //chals = Challenges.getInstance().getChals();

        if(open[1] != 1){
            findViewById(R.id.challenges).setAlpha(0.75f);
            ( (ImageView) findViewById(R.id.challenges) ).setColorFilter(Color.parseColor("#cf401d"));
        }
    }

    public void onNewChallenge() {
        getChallengesFromDatabase();
        if(open[1] != 1){
            findViewById(R.id.challenges).setAlpha(0.75f);
            ( (ImageView) findViewById(R.id.challenges) ).setColorFilter(Color.parseColor("#cf401d"));
        }
        else if(isScreenOpen("challenges")) Challenges.getInstance().onNewChallenge();
    }

    public void onNewMessage(UserNotify notif) {
        if ( open[1] == 0 && Profile.getInstance().isLinkChatOpen()) Profile.getInstance().onNewMessage(notif);
        else if ( open[1] == 1 ) Challenges.getInstance().onNewMessage(notif);
    }

    public void updateLocalChallenges() {
        if(open[1] == 1); //chals = Challenges.getInstance().getChals();
    }

    public void refreshLeaderboard() {
        if(!keepRefreshingLeaderboard){
            keepRefreshingLeaderboard = true;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(keepRefreshingLeaderboard) {
                        if(Utility.getTime() - lastLeaderboardRefresh >= ((int) (refreshTime*0.98f)) ){
                            getUserFromDatabase(new Runnable() {
                                @Override
                                public void run() {
                                    user.updateUserInDatabase(new Runnable() {
                                        @Override
                                        public void run() {
                                            BaseActivity.getInstance().getLeaderboardFromDatabase(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if(isScreenOpen("challenges")) Challenges.getInstance().cleanOpponentsList();
                                                    if(isScreenOpen("profile")) Profile.getInstance().resetLinkChatSearchWinList();
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                        sleep(refreshTime);
                    }
                }
            });
            t.setDaemon(true);
            t.start();
        }
    }
    public void stopRefreshingLeaderboard() {keepRefreshingLeaderboard = false;}
    public void setRefreshTime(int refreshTime) { this.refreshTime = refreshTime; }

    public void makeToast(final String t) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int duration = Toast.LENGTH_SHORT;
                Toast.makeText(k, t, duration).show();
            }
        });
    }
    void makeToast(final String t, final int len) {
        TextView tv = new TextView(getApplicationContext());
        tv.setGravity(Gravity.CENTER);
        tv.setText(t);
        tv.setPadding(25, 30, 30, 25);
        tv.setTextColor(Color.WHITE);
        tv.setBackgroundResource(R.drawable.toastbg);

        Toast toast = new Toast(getApplicationContext());
        toast.setView(tv);
        toast.setDuration(len);
        toast.show();
    }
    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static BaseActivity getInstance(){
        return k;
    }
    public void terminateService() {
        BackgroundService.getInstance().onDestroy();
        stopService(new Intent(this, BackgroundService.class));
    }

    public void resetCommand() {
        command = "";
        BackgroundService.getInstance().resetCommand();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) return true;
        else return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cleanActivities();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utility.postDelayed(1000, new Runnable() {
            @Override
            public void run() {
                Log.e("OnPause", ": "+keepRefreshingLeaderboard);
            }
        });
    }

    private void testBench() {}
}