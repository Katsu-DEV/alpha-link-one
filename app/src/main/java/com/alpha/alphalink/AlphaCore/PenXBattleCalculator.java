package com.alpha.alphalink.AlphaCore;

import android.util.Log;

import com.alpha.alphalink.LinkCore.RomManager;

public class PenXBattleCalculator {
    private int p1Presses, p2Presses, p1Life = 6, p2Life = 6;
    private String p1ShotPattern, p2ShotPattern, p1HitPattern, p2HitPattern;
    private int[][] lifePoints = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
    private boolean winner;

    public PenXBattleCalculator(String romA, String romB) {
        Log.e(romA, romB);
        // Shot Pattern
        p1ShotPattern = shotBitsToDamage(leadingZeros(Integer.toBinaryString(Integer.parseInt(romA.substring(10, 13), 16)), 12).substring(2));
        p2ShotPattern = shotBitsToDamage(leadingZeros(Integer.toBinaryString(Integer.parseInt(romB.substring(10, 13), 16)), 12).substring(2));;

        //2099-4089-0009-91F9

        // hit pattern
        p1HitPattern = leadingZeros(Integer.toBinaryString(Integer.parseInt(romA.substring(16,18), 16)), 8).substring(3).replace("0", "z").replace("1", "0").replace("z", "1");
        p2HitPattern = p1HitPattern.replace("0", "z").replace("1", "0").replace("z", "1");

        int round = 0;

        do {
            for(int j = 0; j<5; j++) {
                p1Life -= (Integer.parseInt(""+p1HitPattern.toCharArray()[4-j]) * Integer.parseInt(""+p2ShotPattern.toCharArray()[j]));
                p2Life -= (Integer.parseInt(""+p2HitPattern.toCharArray()[4-j]) * Integer.parseInt(""+p1ShotPattern.toCharArray()[j]));
                lifePoints[round][0] = p1Life;
                lifePoints[round][1] = p2Life;
                round++;

                if(p1Life <= 0) {
                    winner = false;
                    return;
                }
                else if (p2Life <= 0) {
                    winner = true;
                    return;
                }
                else if (round >= 10 && p2Life != p1Life) {
                    winner = (p2Life<p1Life);
                    return;
                }
            }
        }
        while (true/*p1Life > 0 && p2Life > 0*/);
    }
    public boolean getWinner() {
        return winner;
    }

    public static String shotBitsToDamage(String bits) {
        return
                (""+(1+Integer.parseInt(bits.substring(0, 2),2)) + "" +
                    (1+Integer.parseInt(bits.substring(2, 4), 2)) + "" +
                    (1+Integer.parseInt(bits.substring(4, 6), 2)) + "" +
                    (1+Integer.parseInt(bits.substring(6, 8), 2)) + "" +
                    (1+Integer.parseInt(bits.substring(8, 10),2) )).replace("4", "6");
    }

    private static String leadingZeros(String input, int length) {
        if(input.length() < length){
            for(int j = input.length(); j<length ; j++) input = "0"+input;
        }
        return input;
    }
}
