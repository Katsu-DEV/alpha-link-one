package com.alpha.alphalink.AlphaCore;

import android.util.Log;

import com.alpha.alphalink.LinkCore.RomManager;

/**
 * PenOG Battle Calculator takes the Input and Output DigiROMs from a battle on a PenOG and calculates which device was the winner.
 */
public class PenOGBattleCalculator {
    private int p1Life = 3, p2Life = 3;
    private String p1ShotPattern, p2ShotPattern, p1HitPattern, p2HitPattern;
    // Each element has two further elements. [x][0] is player one's life points at round x, and likewise for [x][1] and player two.
    private int[][] lifePoints = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
    // True when player one is the winner, false when player two is the winner.
    private boolean winner;

    public PenOGBattleCalculator(String romPenA, String romPenB) {
        String temp;

        //For PenOG, attack is its own packet and will be either weak (0: 1 damage) or strong (1: 2 damage)
        //Hit pattern is the same as Pen20 and DM20 - five bits, 0 (miss) and 1 (hit)
        //life for every mon is 3

        p1ShotPattern = RomManager.getPenOGAttackPattern(romPenA);
        p2ShotPattern = RomManager.getPenOGAttackPattern(romPenB);

        // Obtain Hit Pattern from DigiROM
        //p1HitPattern = RomManager.getPenOGHitPattern(romPenB); //p1's hits are from p2's rom
        p2HitPattern = RomManager.getPenOGHitPattern(romPenA); //p2's hits are from p1's rom
        p1HitPattern = p2HitPattern.replaceAll("0", "x").replaceAll("1", "0").replaceAll("x", "1");

        // hit pattern
        //temp = leadingZeros(Integer.toHexString(RomManager.getPenOGHitPatternInt(romPenA)), 2); //p1's successful hits against p2
        //p1HitPattern = leadingZeros(Integer.toBinaryString(Integer.parseInt(temp.substring(0,1), 16)), 5);
        //p2HitPattern = leadingZeros(Integer.toBinaryString(Integer.parseInt(temp.substring(1), 16)), 5);
        int round = 0;
        do {
            for(int j = 0; j<5; j++) {
                p1Life -= (Integer.parseInt(""+p1HitPattern.toCharArray()[4-j]) * (Integer.parseInt(""+p2ShotPattern.toCharArray()[4-j]) + 1)); //0 is 1 damage, 1 is 2 damage, both are read right to left
                p2Life -= (Integer.parseInt(""+p2HitPattern.toCharArray()[4-j]) * (Integer.parseInt(""+p1ShotPattern.toCharArray()[4-j]) + 1)); //0 is 1 damage, 1 is 2 damage, both are read right to left

                lifePoints[round][0] = p1Life;
                lifePoints[round][1] = p2Life;
                round++;
                print(  "Round "+round+"  | "+"p1 Life: "+p1Life+" | "+"p2 Life: "+p2Life);
                // Check if either player's life points have reached 0
                if(p1Life <= 0) {
                    winner = false;
                    return;
                }
                else if (p2Life <= 0) {
                    winner = true;
                    return;
                }
                // If 5 rounds have been exceeded
                else if (round >= 5 && p2Life != p1Life) {
                    winner = (p2Life<p1Life);
                    return;
                }
            }
        } while (true/*p1Life > 0 && p2Life > 0*/);
    }
    public boolean getWinner() {
        return winner;
    }

    private static String leadingZeros(String input, int length) {
        if(input.length() < length){
            for(int j = input.length(); j<length ; j++) input = "0"+input;
        }
        return input;
    }
    private void print(String t) {
        Log.e("PenOGBC Print", t);
    }
}
