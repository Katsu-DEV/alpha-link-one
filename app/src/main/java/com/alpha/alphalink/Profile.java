package com.alpha.alphalink;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.alpha.alphalink.LinkCore.NodeServer.AllConversations;
import com.alpha.alphalink.LinkCore.NodeServer.ChallengeN;
import com.alpha.alphalink.LinkCore.NodeServer.Conversation;
import com.alpha.alphalink.LinkCore.NodeServer.MQTTInterface;
import com.alpha.alphalink.LinkCore.NodeServer.Message;
import com.alpha.alphalink.LinkCore.NodeServer.MessageIdentifier;
import com.alpha.alphalink.LinkCore.NodeServer.SendMessageResponse;
import com.alpha.alphalink.LinkCore.NodeServer.SlotN;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;
import com.alpha.alphalink.LinkCore.NodeServer.UserNotify;
import com.alpha.alphalink.LinkCore.RomManager;
import com.alpha.alphalink.LinkCore.SettingsStore;
import com.alpha.alphalink.LinkCore.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Profile extends AppCompatActivity implements View.OnClickListener {
    public static Profile k;
    private UserN user;
    private ConstraintLayout profile;
    private ConstraintLayout[] linkChatBoxes, slotCLs, uslcls;
    private TextView[] linkChatNames, linkChatPreviews, linkChatTimestamps, usltxts, uslspcrs,
            challengeUsrs, challengeSubtxt;
    private ImageView[] linkChatIcos, challengeIcos, uslstatus;
    private LinearLayout messageslist;
    private Runnable showAll, expandList, reduceList;
    private Conversation conversation;
    private AllConversations allConversations;
    private SettingsStore settings;
    private Message tempMessage, reportMessage;
    private float x1 = 0, x2 = 0, y1, y2;
    private long t1 = 0, t2;
    private boolean debugLog = true, droppedDown = false, linkChatIsOpen = false, msgChIsExp = false,
            messagesIsOpen = false, hasAllMessages = false, isGlobalChat = false, updGlobalTxt = false,
            globalLinkIsOpen = false, globalLinkTabIsOpen = true, globalChatStart = true, inited = false,
            hasUsersList = false, dropDown = false, linkChatSearchIsOpen = false;
    private int slotCount = 0, MAX_TIME = 72, tempTheme = 0, convoSel = 0;
    private String globalLinkTab = "", localGlobalDmField = "", currentConversant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        k = this;

        user = (UserN) getIntent().getSerializableExtra("User");
        init();

        slotCount = user.getSlots().length;
        populateData();
        animateLoad();

        obeyCommand(getIntent().getStringExtra("command"));
    }

    private void obeyCommand(final String command) {
        switch(command) {
            case "newmessage":
                getAllConversationsFromDatabase();
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!hasAllMessages || !inited) {
                            sleep(250);
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final UserNotify notif = new Gson().fromJson(user.getDmchk(), UserNotify.class);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        try{
                                            int i;
                                            for(i = 0; i< linkChatNames.length; i++) if(linkChatNames[i].getText().toString().equals(notif.getFrom())) break;
                                            anims("openLinkChatWin", null);
                                            convoSel = Integer.parseInt(linkChatBoxes[i].getTag().toString());
                                            Message m = allConversations.getConversations()[convoSel].getMessages().get(0);
                                            currentConversant = user.getName().equals(m.getFrom()) ? m.getTo():m.getFrom();

                                            messageslist = findViewById(R.id.profmessageslist);
                                            isGlobalChat = false;
                                            getConversationWith(currentConversant);
                                            anims("openMessages", null);
                                        }
                                        catch(Exception e) {
                                            obeyCommand(command);
                                        }
                                    }
                                }, 200);
                            }
                        });
                    }
                });
                t.setDaemon(true);
                t.start();
                break;
            default: break;
        }
        base().getInstance().resetCommand();
    }

    private void init() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = ((int) (dm.widthPixels - (50*dm.density))), height = dm.heightPixels;
        getWindow().setLayout(width, height);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.RIGHT;

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        profile = findViewById(R.id.profile);
        slotCLs = new ConstraintLayout[] {
                findViewById(R.id.slotcardone),
                findViewById(R.id.slotcardtwo),
                findViewById(R.id.slotcardthree),
                findViewById(R.id.slotcardfour),
                findViewById(R.id.slotcardfive),
                findViewById(R.id.slotcardsix)
        };
        for(ConstraintLayout cl : slotCLs) cl.setVisibility(View.GONE);

        initialiseLinkChat();
        initialiseGlobalLink();

        settings = loadSettings();
        findViewById(R.id.themeselecgrey).setVisibility(View.VISIBLE);

        switch(user.getStats().getRank().toUpperCase().trim()) {
            case "D":
                findViewById(R.id.themeselecblue).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecpink).setVisibility(View.VISIBLE);
                break;
            case "C":
                findViewById(R.id.themeselecblue).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecpink).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecgreen).setVisibility(View.VISIBLE);
                break;
            case "B":
                findViewById(R.id.themeselecblue).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecpink).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecgreen).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecorange).setVisibility(View.VISIBLE);
                break;
            case "A":
                findViewById(R.id.themeselecblue).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecpink).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecgreen).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecorange).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecred).setVisibility(View.VISIBLE);
                break;
            case "S":
                findViewById(R.id.themeselecblue).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecpink).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecgreen).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecorange).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecred).setVisibility(View.VISIBLE);
                findViewById(R.id.themeselecblack).setVisibility(View.VISIBLE);
                break;
        }

        showAll = new Runnable() {
            @Override
            public void run() {
                for(int j = 0; j<usltxts.length; j++) {
                    final int indtx = j;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String typed = ((EditText) findViewById(R.id.linkchatchopptext)).getText().toString();
                            if(usltxts[indtx].getText().toString().toLowerCase().contains(typed.toLowerCase().trim())){
                                uslcls[indtx].setVisibility(View.VISIBLE);
                                uslspcrs[indtx].setVisibility(View.VISIBLE);
                                //                                                            usltxts[indtx].setVisibility(View.VISIBLE);
                                //                                                            uslspcrs[indtx].setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
        };
        expandList = new Runnable() {
            @Override
            public void run() {
                String ss = ((TextView)findViewById(R.id.linkchatchopptext)).getText().toString();
                for(int j = 0; j<usltxts.length; j++) {
                    if(usltxts[j].getText().toString().toLowerCase().contains(ss.trim().toLowerCase())){
                        final int indtx = j;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String typed = ((EditText) findViewById(R.id.linkchatchopptext)).getText().toString();
                                if(usltxts[indtx].getText().toString().toLowerCase().contains(typed.toLowerCase().trim())){
                                    uslcls[indtx].setVisibility(View.VISIBLE);
                                    uslspcrs[indtx].setVisibility(View.VISIBLE);
                                    //                                                            usltxts[indtx].setVisibility(View.VISIBLE);
                                    //                                                            uslspcrs[indtx].setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                }
            }
        };
        reduceList = new Runnable() {
            @Override
            public void run() {
                String ss = ((TextView)findViewById(R.id.linkchatchopptext)).getText().toString();
                for(int j = 0; j<usltxts.length; j++) {
                    if(!usltxts[j].getText().toString().toLowerCase().contains(ss.trim().toLowerCase())){
                        final int indtx = j;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uslcls[indtx].setVisibility(View.GONE);
                                uslspcrs[indtx].setVisibility(View.GONE);
                            }
                        });
                    }
                }
            }
        };
    }
    private void initialiseLinkChatSearchWin(boolean initChoppText) {
        BaseActivity.getInstance().setRefreshTime(60*1000);
        BaseActivity.getInstance().refreshLeaderboard();
        final UserN[] leaderboard = base().getLeaderboard();
        if(!hasUsersList) {
            hasUsersList = true;
            int i = 0;
            uslcls = new ConstraintLayout[leaderboard.length];
            uslstatus = new ImageView[leaderboard.length];
            usltxts = new TextView[leaderboard.length];
            uslspcrs = new TextView[leaderboard.length];

            TextView[] spcs = new TextView[3];

            // Setup Spacers
            for(int j = 0; j<spcs.length; j++) {
                spcs[j] = new TextView(getApplicationContext());
                WindowManager.LayoutParams spparams = new WindowManager.LayoutParams();
                spparams.width = 32;
                spparams.height = 20;
                spcs[j].setLayoutParams(spparams);
            }

            TextView flspcb = new TextView(getApplicationContext());

            WindowManager.LayoutParams flspcbparams = new WindowManager.LayoutParams();
            flspcbparams.width = 1000;
            flspcbparams.height = 1;
            flspcb.setLayoutParams(flspcbparams);
            flspcb.setBackgroundColor(0xFF383838);
            ((LinearLayout) findViewById(R.id.linkchatuserslistli)).addView(flspcb);

            // Name List
            for (UserN u : base().getLeaderboard()) {
                ConstraintLayout cl = new ConstraintLayout(getApplicationContext());
                cl.setId(View.generateViewId());
                cl.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                uslcls[i] = cl;

                ImageView status = new ImageView(getApplicationContext());
                status.setId(View.generateViewId());
                status.setVisibility(View.VISIBLE);

                WindowManager.LayoutParams stparams = new WindowManager.LayoutParams();
                stparams.width = dpToPx(12);
                stparams.height = dpToPx(12);
                status.setLayoutParams(stparams);

                uslstatus[i] = status;
                if(u.getName().equals(user.getName())) status.setVisibility(View.GONE);

                TextView tv = new TextView(getApplicationContext());
                tv.setText(u.getName());
                tv.setTextSize(15);
                tv.setPadding(10, dpToPx(16), 0, dpToPx(16));
                tv.setTextColor(0xE0383838/*E45E1800*/);
                tv.setId(View.generateViewId());
                usltxts[i] = tv;
                if(u.getName().equals(user.getName())) tv.setVisibility(View.GONE);

                //((LinearLayout) findViewById(R.id.linkchatuserslistli)).addView(tv);
                cl.addView(status);
                cl.addView(tv);
                ((LinearLayout) findViewById(R.id.linkchatuserslistli)).addView(cl);


                // Constraints
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(cl);

                constraintSet.connect(status.getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT, dpToPx(6));
                constraintSet.connect(tv.getId(), ConstraintSet.LEFT, status.getId(), ConstraintSet.RIGHT, dpToPx(6));

                constraintSet.connect(status.getId(), ConstraintSet.TOP, tv.getId(), ConstraintSet.TOP, dpToPx(1.5f));
                constraintSet.connect(status.getId(), ConstraintSet.BOTTOM, tv.getId(), ConstraintSet.BOTTOM);

                constraintSet.applyTo(cl);

                TextView sp = new TextView(getApplicationContext());

                WindowManager.LayoutParams spparams = new WindowManager.LayoutParams();
                spparams.width = 1000;
                spparams.height = 1;
                sp.setLayoutParams(spparams);
                sp.setBackgroundColor(0xFF383838/*B0521800*/);

                ((LinearLayout) findViewById(R.id.linkchatuserslistli)).addView(sp);
                uslspcrs[i] = sp;
                if(u.getName().equals(user.getName())) sp.setVisibility(View.GONE);

                i++;
                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anims("pulse", v);
                        anims("closeLinkChatSearchWin", null);
                        currentConversant = ((TextView) v).getText().toString();
                        Utility.postDelayed(250, new Runnable() {
                            @Override
                            public void run() {
                                messageslist = findViewById(R.id.profmessageslist);
                                isGlobalChat = false;
                                getConversationWith(currentConversant);
                                //makeToast(currentConversant, 1);
                                anims("openMessages", null);
                            }
                        });
                    }
                });
            }

//            final Runnable showAll = new Runnable() {
//                @Override
//                public void run() {
//                    for(int j = 0; j<usltxts.length; j++) {
//                        final int indtx = j;
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                String typed = ((EditText) findViewById(R.id.linkchatchopptext)).getText().toString();
//                                if(usltxts[indtx].getText().toString().toLowerCase().contains(typed.toLowerCase().trim())){
//                                    uslcls[indtx].setVisibility(View.VISIBLE);
//                                    uslspcrs[indtx].setVisibility(View.VISIBLE);
//                                    //                                                            usltxts[indtx].setVisibility(View.VISIBLE);
//                                    //                                                            uslspcrs[indtx].setVisibility(View.VISIBLE);
//                                }
//                            }
//                        });
//                    }
//                }
//            },
//            expandList = new Runnable() {
//                @Override
//                public void run() {
//                    String ss = ((TextView)findViewById(R.id.linkchatchopptext)).getText().toString();
//                    for(int j = 0; j<usltxts.length; j++) {
//                        if(usltxts[j].getText().toString().toLowerCase().contains(ss.trim().toLowerCase())){
//                            final int indtx = j;
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    String typed = ((EditText) findViewById(R.id.linkchatchopptext)).getText().toString();
//                                    if(usltxts[indtx].getText().toString().toLowerCase().contains(typed.toLowerCase().trim())){
//                                        uslcls[indtx].setVisibility(View.VISIBLE);
//                                        uslspcrs[indtx].setVisibility(View.VISIBLE);
//                                        //                                                            usltxts[indtx].setVisibility(View.VISIBLE);
//                                        //                                                            uslspcrs[indtx].setVisibility(View.VISIBLE);
//                                    }
//                                }
//                            });
//                        }
//                    }
//                }
//            };

            if(initChoppText) {
                ((EditText)findViewById(R.id.linkchatchopptext)).addTextChangedListener(new TextWatcher() {
                    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                    @Override public void afterTextChanged(Editable s) {}
                    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(before<count) {
                            if(s.length() != 0) {
                                String ss = s.toString();
                                for(int j = 0; j<usltxts.length; j++) {
                                    if(!usltxts[j].getText().toString().toLowerCase().contains(ss.trim().toLowerCase())){
                                        final int indtx = j;
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                uslcls[indtx].setVisibility(View.GONE);
                                                uslspcrs[indtx].setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        else {
                            if(count == 0) showAll.run();
                            else {
                                expandList.run();
                            }
                        }
                    }
                });
            }
        }

        long ti = Utility.getTime();
        for(int i = 0; i<leaderboard.length; i++) {
            if( (leaderboard[i].getName().equals(user.getName())) || (ti - leaderboard[i].getLastseen() <= (1000*60*15) ) && leaderboard[i].getLastseen() > 0) uslstatus[i].setBackgroundResource(R.drawable.ic_online);
            else uslstatus[i].setBackgroundResource(R.drawable.ic_offline);
        }
    }
    private void initialiseLinkChat() {
        ConstraintLayout lcw = findViewById(R.id.linkchatwin);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = ((int) (dm.widthPixels - (50*dm.density)));

        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) lcw.getLayoutParams();
        lp.width = width;
        lcw.setLayoutParams(lp);
        lcw.setTranslationX(width);
        initialiseMessages();

        ConstraintLayout lcsw = findViewById(R.id.linkchatsearchnewwin);
        DisplayMetrics dm2 = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm2);

        ConstraintLayout.LayoutParams lp2 = (ConstraintLayout.LayoutParams) lcsw.getLayoutParams();
        lp2.width = width;
        lcsw.setLayoutParams(lp2);
        lcsw.setTranslationX(width);
    }
    private void initialiseGlobalLink() {
        ConstraintLayout lcw = findViewById(R.id.profglobalmessageswin);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = ((int) (dm.widthPixels - (50*dm.density)));

        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) lcw.getLayoutParams();
        lp.width = width;
        lcw.setLayoutParams(lp);
        lcw.setTranslationX(width);
    }
    private void initialiseMessages() {
        ConstraintLayout mw = findViewById(R.id.profmessageswin);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = ((int) (dm.widthPixels - (50*dm.density)));

        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) mw.getLayoutParams();
        lp.width = width;
        mw.setLayoutParams(lp);
        mw.setTranslationX(width);

        anims("closeChallengeStats", null);
    }
    private void addLinkChats() {
        int i = 0;
        linkChatBoxes = new ConstraintLayout[allConversations.getConversations().length];
        linkChatNames       = new TextView[linkChatBoxes.length];
        linkChatPreviews = new TextView[linkChatBoxes.length];
        linkChatTimestamps  = new TextView[linkChatBoxes.length];
        linkChatIcos  = new ImageView[linkChatBoxes.length];

        for(Conversation convo : allConversations.getConversations()){
            linkChatBoxes[i] = new ConstraintLayout(this);
            linkChatBoxes[i].setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(70)));
            linkChatBoxes[i].setId(View.generateViewId());
            linkChatBoxes[i].setTag(""+i);


            Message m = convo.getLastMessage();
            String conversant = convo.getConversant(user.getName());
            SlotN s = null;
            try { s = Utility.getUserFromLeaderboard(conversant).getMostBattledPartner(); } catch (Exception e) { }
            int prtnr = (s != null) ? getImageId(getIMGFileName(s.getDigiROM(), s.getCommType())) :
                    R.drawable.ic_ico;

            linkChatIcos[i] = new ImageView(this);
            linkChatIcos[i].setId(View.generateViewId());
            linkChatIcos[i].setTag(""+i);
            linkChatIcos[i].setImageResource(prtnr);
            linkChatIcos[i].setLayoutParams(new WindowManager.LayoutParams(0, 0));
            linkChatIcos[i].getLayoutParams().height = dpToPx(45);
            linkChatIcos[i].getLayoutParams().width = dpToPx(45);
            linkChatIcos[i].setAlpha(0.7f);

            linkChatNames[i] = new TextView(this);
            linkChatNames[i].setId(View.generateViewId());
            linkChatNames[i].setText(conversant);
            linkChatNames[i].setTextSize(18);
            linkChatNames[i].setAlpha(0.8f);

            linkChatPreviews[i] = new TextView(this);
            linkChatPreviews[i].setId(View.generateViewId());
            linkChatPreviews[i].setText(m.getShortWhat());
            linkChatPreviews[i].setTextSize(12);
            linkChatPreviews[i].setAlpha(0.55f);
            linkChatPreviews[i].setSingleLine(true);
            linkChatPreviews[i].setMaxEms(26);


            String time;
            int msgDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(m.getWhen()))),
                    todayDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(/*cal.getTimeInMillis()*/ System.currentTimeMillis())));

            if(msgDate<todayDate)
                time = new SimpleDateFormat("dd MM").format(new Date(m.getWhen()));
            else time = new SimpleDateFormat("HH:mm").format(new Date(m.getWhen()));

            linkChatTimestamps[i] = new TextView(this);
            linkChatTimestamps[i].setId(View.generateViewId());
            linkChatTimestamps[i].setText(time);
            linkChatTimestamps[i].setTextSize(10);
            linkChatTimestamps[i].setAlpha(0.5f);

            linkChatBoxes[i].addView(linkChatIcos[i]);
            linkChatBoxes[i].addView(linkChatNames[i]);
            linkChatBoxes[i].addView(linkChatPreviews[i]);
            linkChatBoxes[i].addView(linkChatTimestamps[i]);

            ((LinearLayout)findViewById(R.id.linkchatlistlist)).addView(linkChatBoxes[i]);


            // Constraints
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(linkChatBoxes[i]);

            constraintSet.connect(linkChatIcos[i].getId(), ConstraintSet.TOP, linkChatBoxes[i].getId(), ConstraintSet.TOP);
            constraintSet.connect(linkChatIcos[i].getId(), ConstraintSet.BOTTOM, linkChatBoxes[i].getId(), ConstraintSet.BOTTOM);
            constraintSet.connect(linkChatIcos[i].getId(), ConstraintSet.LEFT, linkChatBoxes[i].getId(), ConstraintSet.LEFT, dpToPx(8));

            constraintSet.connect(linkChatNames[i].getId(), ConstraintSet.TOP, linkChatIcos[i].getId(), ConstraintSet.TOP, dpToPx(2));
            constraintSet.connect(linkChatNames[i].getId(), ConstraintSet.LEFT, linkChatIcos[i].getId(), ConstraintSet.RIGHT, dpToPx(16));

            constraintSet.connect(linkChatPreviews[i].getId(), ConstraintSet.LEFT, linkChatIcos[i].getId(), ConstraintSet.RIGHT, dpToPx(17));
            constraintSet.connect(linkChatPreviews[i].getId(), ConstraintSet.BOTTOM, linkChatIcos[i].getId(), ConstraintSet.BOTTOM, dpToPx(1));

            constraintSet.connect(linkChatTimestamps[i].getId(), ConstraintSet.TOP, linkChatPreviews[i].getId(), ConstraintSet.TOP);
            constraintSet.connect(linkChatTimestamps[i].getId(), ConstraintSet.BOTTOM, linkChatPreviews[i].getId(), ConstraintSet.BOTTOM);
            constraintSet.connect(linkChatTimestamps[i].getId(), ConstraintSet.END, linkChatBoxes[i].getId(), ConstraintSet.END, dpToPx(4));

            constraintSet.applyTo(linkChatBoxes[i]);

            linkChatBoxes[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    anims("pulse", v);
                    convoSel = Integer.parseInt(v.getTag().toString());
                    Message m = allConversations.getConversations()[convoSel].getMessages().get(0);
                    currentConversant = user.getName().equals(m.getFrom()) ? m.getTo():m.getFrom();

                    messageslist = findViewById(R.id.profmessageslist);
                    isGlobalChat = false;
                    getConversationWith(currentConversant);
                    anims("openMessages", null);
                }
            });
            i++;
        }
        inited = true;
    }

    private void populateData() {
        updateTamerCardBg();
        ((TextView) findViewById(R.id.unm)).setText(user.getName());
        ((TextView) findViewById(R.id.userwins)).setText(""+user.getStats().getWin());
        ((TextView) findViewById(R.id.userloss)).setText(""+user.getStats().getLoss());
        ((TextView) findViewById(R.id.userdraws)).setText(""+user.getStats().getDraw());
        ((TextView) findViewById(R.id.rank)).setText(user.getStats().getRank().toUpperCase());
        ((TextView) findViewById(R.id.alphaid)).setText(user.getAid());
        ((TextView) findViewById(R.id.position)).setText("# " + user.getPosition(base().getLeaderboard()));
        ((TextView) findViewById(R.id.points)).setText(""+user.getStats().getScore());

        String joined = new SimpleDateFormat("dd/MM/yyyy").format(new Date(user.getJoined()));
        ((TextView) findViewById(R.id.joineddate)).setText(joined);

        SlotN partner = user.getMostBattledPartner();

        if (partner != null) {
            ((TextView) findViewById(R.id.partner)).setText(partner.getName().equals("") ? getName(partner.getDigiROM(), partner.getCommType()) : partner.getName());
            ((ImageView) findViewById(R.id.partnermonico) ).setImageResource(getImageId(getIMGFileName(partner.getDigiROM(), partner.getCommType())));
            ((ImageView) findViewById(R.id.partnermonico) ).setVisibility(View.VISIBLE);


        }
        else {
            ((TextView) findViewById(R.id.partner)).setText("N/A");
            ((ImageView) findViewById(R.id.partnermonico) ).setVisibility(View.GONE);

        }

        if(slotCount == 0) {
            findViewById(R.id.emptyslots).setVisibility(View.VISIBLE);
            findViewById(R.id.slotCards).setVisibility(View.GONE);
        }
        else {
            findViewById(R.id.emptyslots).setVisibility(View.GONE);
            findViewById(R.id.slotCards).setVisibility(View.VISIBLE);

            int age, syncTime;
            float rem;
            String alp, colorString;

            if(slotCount > 0){
                ((TextView) findViewById(R.id.sonename)).setText(user.getSlots()[0].getName());
                ((TextView) findViewById(R.id.sonewins)).setText("W: "+user.getSlots()[0].getWins());
                ((TextView) findViewById(R.id.soneloss)).setText("L: "+user.getSlots()[0].getLoss());
                ((TextView) findViewById(R.id.sonedraws)).setText("D: "+user.getSlots()[0].getDraw());

                age = user.getSlots()[0].getAge();
                ((TextView) findViewById(R.id.soneage)).setText(String.valueOf( (int) (age/24) )+"YR");

                syncTime = user.getSlots()[0].getSynchTime();

                rem = 1f - ((float) syncTime / (float) MAX_TIME);
                if(rem < 0.075f) rem = 0.075f;

                findViewById(R.id.sonebar).getLayoutParams().height =  ((int) (findViewById(R.id.sonebar).getLayoutParams().height*rem));

                if(rem+0.2f <= 1f) rem+=0.2f;

                alp = Integer.toHexString((int) (255* (1-rem)));
                if(alp.length() == 1) alp = "0" + alp;

                colorString = "#" + alp + "F33000";
                ((ImageView) findViewById(R.id.sonebar)).setColorFilter(Color.parseColor(colorString));

                ColorMatrix matrix = new ColorMatrix();
                if(user.getSlots()[0].isLocked()) matrix.setSaturation(0);
                else matrix.setSaturation(1f);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                ((ImageView)findViewById(R.id.soneico)).setColorFilter(filter);
                ((ImageView)findViewById(R.id.soneicomirror)).setColorFilter(filter);
            }
            if(slotCount > 1){
                ((TextView) findViewById(R.id.stwoname)).setText(user.getSlots()[1].getName());
                ((TextView) findViewById(R.id.stwowins)).setText("W: "+user.getSlots()[1].getWins());
                ((TextView) findViewById(R.id.stwoloss)).setText("L: "+user.getSlots()[1].getLoss());
                ((TextView) findViewById(R.id.stwodraws)).setText("D: "+user.getSlots()[1].getDraw());

                age = user.getSlots()[1].getAge();

                ((TextView) findViewById(R.id.stwoage)).setText(String.valueOf( (int) (age/24) )+"YR");

                syncTime = user.getSlots()[1].getSynchTime();

                rem = 1f - ((float) syncTime / (float) MAX_TIME);
                if(rem < 0.075f) rem = 0.075f;

                findViewById(R.id.stwobar).getLayoutParams().height =  ((int) (findViewById(R.id.stwobar).getLayoutParams().height*rem));

                if(rem+0.2f <= 1f) rem+=0.2f;

                alp = Integer.toHexString((int) (255* (1-rem)));
                if(alp.length() == 1) alp = "0" + alp;

                colorString = "#" + alp + "F33000";
                ((ImageView) findViewById(R.id.stwobar)).setColorFilter(Color.parseColor(colorString));

                ColorMatrix matrix = new ColorMatrix();
                if(user.getSlots()[1].isLocked()) matrix.setSaturation(0);
                else matrix.setSaturation(1f);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                ((ImageView)findViewById(R.id.stwoico)).setColorFilter(filter);
                ((ImageView)findViewById(R.id.stwoicomirror)).setColorFilter(filter);

            }
            if(slotCount > 2){
                ((TextView) findViewById(R.id.sthreename)).setText(user.getSlots()[2].getName());
                ((TextView) findViewById(R.id.sthreewins)).setText("W: "+user.getSlots()[2].getWins());
                ((TextView) findViewById(R.id.sthreeloss)).setText("L: "+user.getSlots()[2].getLoss());
                ((TextView) findViewById(R.id.sthreedraws)).setText("D: "+user.getSlots()[2].getDraw());

                age = user.getSlots()[2].getAge();
                ((TextView) findViewById(R.id.sthreeage)).setText(String.valueOf( (int) (age/24) )+"YR");

                syncTime = user.getSlots()[2].getSynchTime();

                rem = 1f - ((float) syncTime / (float) MAX_TIME);
                if(rem < 0.075f) rem = 0.075f;

                findViewById(R.id.sthreebar).getLayoutParams().height =  ((int) (findViewById(R.id.sthreebar).getLayoutParams().height*rem));

                if(rem+0.2f <= 1f) rem+=0.2f;

                alp = Integer.toHexString((int) (255* (1-rem)));
                if(alp.length() == 1) alp = "0" + alp;

                colorString = "#" + alp + "F33000";
                ((ImageView) findViewById(R.id.sthreebar)).setColorFilter(Color.parseColor(colorString));

                ColorMatrix matrix = new ColorMatrix();
                if(user.getSlots()[2].isLocked()) matrix.setSaturation(0);
                else matrix.setSaturation(1f);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                ((ImageView)findViewById(R.id.sthreeico)).setColorFilter(filter);
                ((ImageView)findViewById(R.id.sthreeicomirror)).setColorFilter(filter);

            }
            if(slotCount > 3){
                ((TextView) findViewById(R.id.sfourname)).setText(user.getSlots()[3].getName());
                ((TextView) findViewById(R.id.sfourwins)).setText("W: "+user.getSlots()[3].getWins());
                ((TextView) findViewById(R.id.sfourloss)).setText("L: "+user.getSlots()[3].getLoss());
                ((TextView) findViewById(R.id.sfourdraws)).setText("D: "+user.getSlots()[3].getDraw());

                age = user.getSlots()[3].getAge();
                ((TextView) findViewById(R.id.sfourage)).setText(String.valueOf( (int) (age/24) )+"YR");

                syncTime = user.getSlots()[3].getSynchTime();

                rem = 1f - ((float) syncTime / (float) MAX_TIME);
                if(rem < 0.075f) rem = 0.075f;

                findViewById(R.id.sfourbar).getLayoutParams().height =  ((int) (findViewById(R.id.sfourbar).getLayoutParams().height*rem));

                if(rem+0.2f <= 1f) rem+=0.2f;

                alp = Integer.toHexString((int) (255* (1-rem)));
                if(alp.length() == 1) alp = "0" + alp;

                colorString = "#" + alp + "F33000";
                ((ImageView) findViewById(R.id.sfourbar)).setColorFilter(Color.parseColor(colorString));

                ColorMatrix matrix = new ColorMatrix();
                if(user.getSlots()[3].isLocked()) matrix.setSaturation(0);
                else matrix.setSaturation(1f);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                ((ImageView)findViewById(R.id.sfourico)).setColorFilter(filter);
                ((ImageView)findViewById(R.id.sfouricomirror)).setColorFilter(filter);

            }
            if(slotCount > 4){
                ((TextView) findViewById(R.id.sfivename)).setText(user.getSlots()[4].getName());
                ((TextView) findViewById(R.id.sfivewins)).setText("W: "+user.getSlots()[4].getWins());
                ((TextView) findViewById(R.id.sfiveloss)).setText("L: "+user.getSlots()[4].getLoss());
                ((TextView) findViewById(R.id.sfivedraws)).setText("D: "+user.getSlots()[4].getDraw());

                age = user.getSlots()[4].getAge();
                ((TextView) findViewById(R.id.sfiveage)).setText(String.valueOf( (int) (age/24) )+"YR");

                syncTime = user.getSlots()[4].getSynchTime();

                rem = 1f - ((float) syncTime / (float) MAX_TIME);
                if(rem < 0.075f) rem = 0.075f;

                findViewById(R.id.sfivebar).getLayoutParams().height =  ((int) (findViewById(R.id.sfivebar).getLayoutParams().height*rem));

                if(rem+0.2f <= 1f) rem+=0.2f;

                alp = Integer.toHexString((int) (255* (1-rem)));
                if(alp.length() == 1) alp = "0" + alp;

                colorString = "#" + alp + "F33000";
                ((ImageView) findViewById(R.id.sfivebar)).setColorFilter(Color.parseColor(colorString));

                ColorMatrix matrix = new ColorMatrix();
                if(user.getSlots()[4].isLocked()) matrix.setSaturation(0);
                else matrix.setSaturation(1f);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                ((ImageView)findViewById(R.id.sfiveico)).setColorFilter(filter);
                ((ImageView)findViewById(R.id.sfiveicomirror)).setColorFilter(filter);

            }
            if(slotCount > 5){
                ((TextView) findViewById(R.id.ssixname)).setText(user.getSlots()[5].getName());
                ((TextView) findViewById(R.id.ssixwins)).setText("W: "+user.getSlots()[5].getWins());
                ((TextView) findViewById(R.id.ssixloss)).setText("L: "+user.getSlots()[5].getLoss());
                ((TextView) findViewById(R.id.ssixdraws)).setText("D: "+user.getSlots()[5].getDraw());

                age = user.getSlots()[5].getAge();
                ((TextView) findViewById(R.id.ssixage)).setText(String.valueOf( (int) (age/24) )+"YR");

                syncTime = user.getSlots()[5].getSynchTime();

                rem = 1f - ((float) syncTime / (float) MAX_TIME);
                if(rem < 0.075f) rem = 0.075f;

                findViewById(R.id.ssixbar).getLayoutParams().height =  ((int) (findViewById(R.id.ssixbar).getLayoutParams().height*rem));

                if(rem+0.2f <= 1f) rem+=0.2f;

                alp = Integer.toHexString((int) (255* (1-rem)));
                if(alp.length() == 1) alp = "0" + alp;

                colorString = "#" + alp + "F33000";
                ((ImageView) findViewById(R.id.ssixbar)).setColorFilter(Color.parseColor(colorString));

                ColorMatrix matrix = new ColorMatrix();
                if(user.getSlots()[5].isLocked()) matrix.setSaturation(0);
                else matrix.setSaturation(1f);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                ((ImageView)findViewById(R.id.ssixico)).setColorFilter(filter);
                ((ImageView)findViewById(R.id.ssixicomirror)).setColorFilter(filter);

            }
        }
    }
    private void animateLoad() {
        String r;
        int t;
        if(slotCount == 0) {
            findViewById(R.id.emptyslots).animate()
                    .setDuration(1600)
                    .alpha(1.0f)
                    .start();
        }
        if(slotCount > 0){
            r = user.getSlots()[0].getDigiROM();
            t = user.getSlots()[0].getCommType();
            ( (ImageView) findViewById(R.id.soneico) ).setImageResource(getImageId(getIMGFileName(r, t)));
            ( (ImageView) findViewById(R.id.soneicomirror) ).setImageResource(getImageId(getIMGFileName(r, t)));

            int clr = modeColourFromImage(getNameRaw(user.getSlots()[0].getDigiROM(), user.getSlots()[0].getCommType()));
            String clrStr = "#"+RomManager.leadingZeros(Integer.toHexString(clr), 8);
            ((ImageView) findViewById(R.id.sonestreak)).setColorFilter(Color.parseColor(clrStr));
        }
        if(slotCount > 1){
            r = user.getSlots()[1].getDigiROM();
            t = user.getSlots()[1].getCommType();
            ( (ImageView) findViewById(R.id.stwoico) ).setImageResource(getImageId(getIMGFileName(r, t)));
            ( (ImageView) findViewById(R.id.stwoicomirror) ).setImageResource(getImageId(getIMGFileName(r, t)));

            int clr = modeColourFromImage(getNameRaw(user.getSlots()[1].getDigiROM(), user.getSlots()[1].getCommType()));
            String clrStr = "#"+RomManager.leadingZeros(Integer.toHexString(clr), 8);
            ((ImageView) findViewById(R.id.stwostreak)).setColorFilter(Color.parseColor(clrStr));
        }
        if(slotCount > 2){
            r = user.getSlots()[2].getDigiROM();
            t = user.getSlots()[2].getCommType();
            ( (ImageView) findViewById(R.id.sthreeico) ).setImageResource(getImageId(getIMGFileName(r, t)));
            ( (ImageView) findViewById(R.id.sthreeicomirror) ).setImageResource(getImageId(getIMGFileName(r, t)));

            int clr = modeColourFromImage(getNameRaw(user.getSlots()[2].getDigiROM(), user.getSlots()[2].getCommType()));
            String clrStr = "#"+RomManager.leadingZeros(Integer.toHexString(clr), 8);
            ((ImageView) findViewById(R.id.sthreestreak)).setColorFilter(Color.parseColor(clrStr));
        }
        if(slotCount > 3){
            r = user.getSlots()[3].getDigiROM();
            t = user.getSlots()[3].getCommType();
            ( (ImageView) findViewById(R.id.sfourico) ).setImageResource(getImageId(getIMGFileName(r, t)));
            ( (ImageView) findViewById(R.id.sfouricomirror) ).setImageResource(getImageId(getIMGFileName(r, t)));

            int clr = modeColourFromImage(getNameRaw(user.getSlots()[3].getDigiROM(), user.getSlots()[3].getCommType()));
            String clrStr = "#"+RomManager.leadingZeros(Integer.toHexString(clr), 8);
            ((ImageView) findViewById(R.id.sfourstreak)).setColorFilter(Color.parseColor(clrStr));
        }
        if(slotCount > 4){
            r = user.getSlots()[4].getDigiROM();
            t = user.getSlots()[4].getCommType();
            ( (ImageView) findViewById(R.id.sfiveico) ).setImageResource(getImageId(getIMGFileName(r, t)));
            ( (ImageView) findViewById(R.id.sfiveicomirror) ).setImageResource(getImageId(getIMGFileName(r, t)));

            int clr = modeColourFromImage(getNameRaw(user.getSlots()[4].getDigiROM(), user.getSlots()[4].getCommType()));
            String clrStr = "#"+RomManager.leadingZeros(Integer.toHexString(clr), 8);
            ((ImageView) findViewById(R.id.sfivestreak)).setColorFilter(Color.parseColor(clrStr));
        }
        if(slotCount > 5){
            r = user.getSlots()[5].getDigiROM();
            t = user.getSlots()[5].getCommType();
            ( (ImageView) findViewById(R.id.ssixico) ).setImageResource(getImageId(getIMGFileName(r, t)));
            ( (ImageView) findViewById(R.id.ssixicomirror) ).setImageResource(getImageId(getIMGFileName(r, t)));

            int clr = modeColourFromImage(getNameRaw(user.getSlots()[5].getDigiROM(), user.getSlots()[5].getCommType()));
            String clrStr = "#"+RomManager.leadingZeros(Integer.toHexString(clr), 8);
            ((ImageView) findViewById(R.id.ssixstreak)).setColorFilter(Color.parseColor(clrStr));
        }

        profile.setVisibility(View.VISIBLE);
        profile.animate()
                .alpha(0.7f)
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(300)
                .setDuration(180)
                .start();

        findViewById(R.id.dropdowncoverarrow).setVisibility(View.VISIBLE);
        findViewById(R.id.dropdowncoverarrow).animate()
                .alpha(0.6f)
                .setStartDelay(500)
                .setDuration(400)
                .start();

        ConstraintLayout[]
        slotBaseBoxes = {
                ((ConstraintLayout)findViewById(R.id.sonebasebox)),
                ((ConstraintLayout)findViewById(R.id.stwobasebox)),
                ((ConstraintLayout)findViewById(R.id.sthreebasebox)),
                ((ConstraintLayout)findViewById(R.id.sfourbasebox)),
                ((ConstraintLayout)findViewById(R.id.sfivebasebox)),
                ((ConstraintLayout)findViewById(R.id.ssixbasebox))
        },
        slotMonBoxes = {
                ((ConstraintLayout)findViewById(R.id.sonemonbox)),
                ((ConstraintLayout)findViewById(R.id.stwomonbox)),
                ((ConstraintLayout)findViewById(R.id.sthreemonbox)),
                ((ConstraintLayout)findViewById(R.id.sfourmonbox)),
                ((ConstraintLayout)findViewById(R.id.sfivemonbox)),
                ((ConstraintLayout)findViewById(R.id.ssixmonbox))
        };

        for(int i = 0; i<slotCount; i++) {
            slotCLs[i].setVisibility(View.VISIBLE);

            slotMonBoxes[i].animate()
                    .alpha(1.0f)
                    .scaleX(1.0f)
                    .scaleY(1.0f)
                    .setStartDelay(500+(i*200))
                    .setDuration(200)
                    .start();

            slotBaseBoxes[i].setTranslationX( (-1*((i%2 == 0) ? 1:-1 )) *(dpToPx(300)));

            slotBaseBoxes[i].animate()
                    .translationX(0)
                    .setStartDelay(575+(i*200))
                    .setDuration(200)
                    .start();
        }
    }
    private void updateTamerCardBg() {
        int[] themes = new int[] {  R.drawable.card_tamercard_grey, R.drawable.card_tamercard_blue,
                                    R.drawable.card_tamercard_pink, R.drawable.card_tamercard_green,
                                    R.drawable.card_tamercard_orange, R.drawable.card_tamercard_red,
                                    R.drawable.card_tamercard_black };

        findViewById(R.id.profile).setBackgroundResource(themes[settings.getTheme()]);

        int bg = 0;

        switch(settings.getTheme()) {
            case 0:
                bg = R.drawable.tamercard_box_grey;
                break;
            case 1:
                bg = R.drawable.tamercard_box_blue;
                break;
            case 2:
                bg = R.drawable.tamercard_box_pink;
                break;
            case 3:
                bg = R.drawable.tamercard_box_green;
                break;
            case 4:
                bg = R.drawable.tamercard_box_orange;
                break;
            case 5:
                bg = R.drawable.tamercard_box_red;
                break;
            case 6:
                bg = R.drawable.tamercard_box_black;
                break;
            default: break;
        }

        ((TextView) findViewById(R.id.unm)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.userwins)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.userloss)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.userdraws)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.rank)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.alphaid)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.position)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.points)).setBackgroundResource(bg);

        ((TextView) findViewById(R.id.partner)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.partner2)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.joineddate)).setBackgroundResource(bg);
        ((TextView) findViewById(R.id.partner2)).setAlpha(settings.getTheme() == 6 ? 0.2f : 0.4f);
    }

    private void populateConversation() {
        if(isGlobalChat) {
            // Conversation
            ConstraintLayout filler = new ConstraintLayout(this);
            filler.setId(View.generateViewId());
            filler.setLayoutParams(new WindowManager.LayoutParams(0, 0));
            filler.getLayoutParams().height = dpToPx(85);
            messageslist.addView(filler);
        }
        else {
            // Stats
            findViewById(R.id.profmsgchallengetopbg).setPivotY(0);
            findViewById(R.id.profmsgchallengetopbg).animate()
                    .scaleY(0f)
                    .setDuration(1);

            findViewById(R.id.profmessageschallengescorestats).animate()
                    .alpha(0)
                    .setDuration(1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            findViewById(R.id.profmessageschallengescorestats).setVisibility(View.GONE);
                        }});

            Message m;
            try {
                m = allConversations.getConversations()[convoSel].getMessages().get(0);
            }
            catch(Exception e) {
                m = new Message(user.getName(), currentConversant, Utility.getTime(), "");
            }
            String conversant = user.getName().equals(m.getFrom()) ? m.getTo():m.getFrom();

            //ChallengeN chal = base().getAllchallenges().getChallengeWith(conversant);
            ChallengeN chal = base().getAllChallengesN().getChallengeWith(currentConversant);

            if(chal != null) {
                ((TextView)findViewById(R.id.profmessageschallengescoreusername)).setText(user.getName());
                ((TextView)findViewById(R.id.profmessageschallengescoreuserwin)).setText("W: " + chal.getAWins());
                ((TextView)findViewById(R.id.profmessageschallengescoreuserlos)).setText("L: " + chal.getALoss());
                ((TextView)findViewById(R.id.profmessageschallengescoreuserdrw)).setText("D: " + chal.getDraws());

                ((TextView)findViewById(R.id.profmsgtitlebartitle)).setText(currentConversant);
                ((TextView)findViewById(R.id.profmessageschallengescoreoppname)).setText(currentConversant);
                ((TextView)findViewById(R.id.profmessageschallengescoreoppwin)).setText("W: " + chal.getBWins());
                ((TextView)findViewById(R.id.profmessageschallengescoreopplos)).setText("L: " + chal.getBLoss());
                ((TextView)findViewById(R.id.profmessageschallengescoreoppdrw)).setText("D: " + chal.getDraws());
            }
            else {
                ((TextView)findViewById(R.id.profmessageschallengescoreusername)).setText(user.getName());
                ((TextView)findViewById(R.id.profmessageschallengescoreuserwin)).setText("W: " + 0);
                ((TextView)findViewById(R.id.profmessageschallengescoreuserlos)).setText("L: " + 0);
                ((TextView)findViewById(R.id.profmessageschallengescoreuserdrw)).setText("D: " + 0);

                ((TextView)findViewById(R.id.profmsgtitlebartitle)).setText(currentConversant);
                ((TextView)findViewById(R.id.profmessageschallengescoreoppname)).setText(currentConversant);
                ((TextView)findViewById(R.id.profmessageschallengescoreoppwin)).setText("W: " + 0);
                ((TextView)findViewById(R.id.profmessageschallengescoreopplos)).setText("L: " + 0);
                ((TextView)findViewById(R.id.profmessageschallengescoreoppdrw)).setText("D: " + 0);
            }



            // Conversation
            ConstraintLayout filler = new ConstraintLayout(this);
            filler.setId(View.generateViewId());
            filler.setLayoutParams(new WindowManager.LayoutParams(0, 0));
            filler.getLayoutParams().height = dpToPx(144);
            messageslist.addView(filler);
        }

        int tag = 0;
        if(conversation.getMessages() != null) {
            for (Message message : conversation.getShortMessages()) {
                // Message
                ConstraintLayout box = new ConstraintLayout(this);
                box.setId(View.generateViewId());

                MessageIdentifier mi = new MessageIdentifier(message.getFrom(), message.getWhen());
                box.setTag(mi.toJsonString());

                int alignment = 0;

                messageslist.addView(box);


                TextView msg = new TextView(this);
                msg.setId(View.generateViewId());

                if (!message.getFrom().equals(user.getName())) {
                    msg.setBackgroundResource(R.drawable.messageleft);
                    alignment = ConstraintSet.LEFT;
                    if (isGlobalChat) msg.setText(message.getFrom() + ":\n" + message.getWhat());
                    else msg.setText(message.getWhat());
                } else {
                    msg.setBackgroundResource(R.drawable.messageright);
                    alignment = ConstraintSet.RIGHT;
                    msg.setText(message.getWhat());
                }

                msg.setTextColor(Color.parseColor("#FFFFFF"));
                msg.setPadding(dpToPx(17), dpToPx(6), dpToPx(17), dpToPx(6));
                //msg.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                box.addView(msg);

                ViewGroup.MarginLayoutParams params =
                        (ViewGroup.MarginLayoutParams) msg.getLayoutParams();
                params.bottomMargin = (int) (dpToPx(4));
                //params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                msg.setLayoutParams(params);
                msg.setMaxWidth((int) (messageslist.getWidth() * 0.85f));

                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(box);
                constraintSet.connect(msg.getId(), alignment, box.getId(), alignment, 0);
                constraintSet.applyTo(box);

                // Timestamp
                String time;
                int msgDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(message.getWhen()))),
                        todayDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(/*cal.getTimeInMillis()*/ System.currentTimeMillis())));

                if (msgDate < todayDate)
                    time = new SimpleDateFormat("dd MMM HH:mm").format(new Date(message.getWhen()));
                else time = new SimpleDateFormat("HH:mm").format(new Date(message.getWhen()));

                TextView timestamp = new TextView(this);
                timestamp.setText(time);
                timestamp.setTextColor(Color.parseColor("#858585"));
                timestamp.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                timestamp.setTextSize(9.5f);
                messageslist.addView(timestamp);

//                if(Utility.getTime() - message.getWhen() < 300000) timestamp.setVisibility(View.GONE);
//                else
                    timestamp.setVisibility(View.VISIBLE);

                ViewGroup.MarginLayoutParams tsparams =
                        (ViewGroup.MarginLayoutParams) timestamp.getLayoutParams();
                tsparams.topMargin = (int) (dpToPx(2));
                tsparams.bottomMargin = (int) (dpToPx(18));
                timestamp.setLayoutParams(tsparams);

                box.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(final View v, MotionEvent event) {
                        int CLICK_DURATION = 750;

                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                x1 = event.getX();
                                y1 = event.getY();
                                t1 = System.currentTimeMillis();
                                return true;
                            case MotionEvent.ACTION_UP:
                                x2 = event.getX();
                                y2 = event.getY();
                                t2 = System.currentTimeMillis();

                                if (x1 == x2 && y1 == y2 && (t2 - t1) < CLICK_DURATION) {
                                    //Toast.makeText(getApplicationContext(), "Click", Toast.LENGTH_SHORT).show();
                                }
                                else if ((t2 - t1) >= CLICK_DURATION) {
                                    //Toast.makeText(getApplicationContext(), "Long click", Toast.LENGTH_SHORT).show();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            MessageIdentifier mi = new Gson().fromJson(v.getTag().toString(), MessageIdentifier.class);
                                            reportMessage = conversation.getMessageFromWhen(mi.getFrom(), mi.getWhen());
                                            anims("pulse", v);
                                            anims("expand", findViewById(R.id.profreportmessage));
                                            anims("fadein", findViewById(R.id.profbg));
                                        }
                                    });
                                }
                                else if (x1 > x2) {
                                    //Toast.makeText(getApplicationContext(), "Left swipe", Toast.LENGTH_SHORT).show();
                                }
                                else if (x2 > x1) {
                                    //Toast.makeText(getApplicationContext(), "Right swipe", Toast.LENGTH_SHORT).show();
                                }
                                return true;
                        }
                        return false;
                    }
                });
            }
        }

        TextView footer = new TextView(this);
        footer.setFocusable(true);
        footer.setFocusableInTouchMode(true);
        footer.setHeight(1);
        messageslist.addView(footer);
        footer.requestFocus();
    }
    private void cleanMessages() {
        LinearLayout ll = isGlobalChat ?
            ((LinearLayout)findViewById(R.id.profglobalmessageslist)) :
            ((LinearLayout)findViewById(R.id.profmessageslist));

        try {
            ll.removeAllViews();
        }
        catch (Exception e) {}
    }
    private void appendMessage(Message message, boolean isMe) {
        conversation.appendMessage(message);

        // Message
        ConstraintLayout box = new ConstraintLayout(this);
        box.setId(View.generateViewId());
        box.setVisibility(View.GONE);

        MessageIdentifier mi = new MessageIdentifier(message.getFrom(), message.getWhen());
        box.setTag(mi.toJsonString());

        messageslist.addView(box);

        TextView msg = new TextView(this);
        msg.setId(View.generateViewId());
        msg.setText(isGlobalChat && !isMe ? message.getFrom()+":\n"+message.getWhat() : message.getWhat());
        msg.setBackgroundResource(isMe ? R.drawable.messageright : R.drawable.messageleft);
        msg.setTextColor(Color.parseColor("#FFFFFF"));
        msg.setPadding(dpToPx(17) , dpToPx(6), dpToPx(17) , dpToPx(6));
        box.addView(msg);

        ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams) msg.getLayoutParams();
        params.bottomMargin = (int)(dpToPx(2));
        params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        msg.setLayoutParams(params);
        msg.setMaxWidth((int)(messageslist.getWidth()*0.85f));


        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(box);
        if(isMe) constraintSet.connect( msg.getId() , ConstraintSet.RIGHT, box.getId(), ConstraintSet.RIGHT,0);
        else constraintSet.connect( msg.getId() , ConstraintSet.LEFT, box.getId(), ConstraintSet.LEFT,0);
        constraintSet.applyTo(box);

        // Timestamp
        String time;
        int msgDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(message.getWhen()))),
                todayDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(/*cal.getTimeInMillis()*/ System.currentTimeMillis())));

        if(msgDate<todayDate)
            time = new SimpleDateFormat("dd MMM HH:mm").format(new Date(message.getWhen()));
        else time = new SimpleDateFormat("HH:mm").format(new Date(message.getWhen()));

        TextView timestamp = new TextView(this);
        timestamp.setText(time);
        timestamp.setTextColor(Color.parseColor("#656565"));
        timestamp.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        timestamp.setTextSize(9f);
        messageslist.addView(timestamp);

//        if(Utility.getTime() - message.getWhen() < 300000) timestamp.setVisibility(View.GONE);
//        else
            timestamp.setVisibility(View.VISIBLE);

        ViewGroup.MarginLayoutParams tsparams =
                (ViewGroup.MarginLayoutParams) timestamp.getLayoutParams();
        tsparams.bottomMargin = (int)(dpToPx(2));
        tsparams.bottomMargin = (int)(dpToPx(12));
        timestamp.setLayoutParams(tsparams);

        timestamp.setFocusable(true);
        timestamp.setFocusableInTouchMode(true);
        timestamp.requestFocus();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EditText tb = (isGlobalChat) ?
                        ((EditText)findViewById(R.id.profglobalmessagetextfield)) :
                        ((EditText)findViewById(R.id.profmessagetextfield));

                tb.setFocusable(true);
                tb.setFocusableInTouchMode(true);
                tb.requestFocus();
            }
        }, 250);

        box.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View v, MotionEvent event) {
                int CLICK_DURATION = 750;

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        y1 = event.getY();
                        t1 = System.currentTimeMillis();
                        return true;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        y2 = event.getY();
                        t2 = System.currentTimeMillis();

                        if (x1 == x2 && y1 == y2 && (t2 - t1) < CLICK_DURATION) {
                            //Toast.makeText(getApplicationContext(), "Click", Toast.LENGTH_SHORT).show();
                        } else if ((t2 - t1) >= CLICK_DURATION) {
                            //Toast.makeText(getApplicationContext(), "Long click", Toast.LENGTH_SHORT).show();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MessageIdentifier mi = new Gson().fromJson(v.getTag().toString(), MessageIdentifier.class);
                                    reportMessage = conversation.getMessageFromWhen(mi.getFrom(), mi.getWhen());
                                    anims("pulse", v);
                                    anims("expand", findViewById(R.id.profreportmessage));
                                    anims("fadein", findViewById(R.id.profbg));
                                }
                            });
                        } else if (x1 > x2) {
                            //Toast.makeText(getApplicationContext(), "Left swipe", Toast.LENGTH_SHORT).show();
                        } else if (x2 > x1) {
                            //Toast.makeText(getApplicationContext(), "Right swipe", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                }
                return false;
            }
        });

        anims("expand", box);
    }
    private void reportMessage() {
        //makeToast(reportMessage.toJsonString(), 1);
        final Message msg = new Message(user.getName(), "reportMessage", Utility.getTime(), reportMessage.toJsonString());

        String URL = "https://www.alphahub.site/alphalink/api/protected/sendmessage/";

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRe = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final SendMessageResponse resp = (new Gson()).fromJson(response, SendMessageResponse.class);

                        if(resp != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    String  mm = "New Reported Message",
                                            type = "reportedMessage";
                                    UserNotify oppNotify = new UserNotify(user.getName(), resp.getRowid(), mm, type);
                                    notifyUser("reportedMessage", oppNotify);
                                }
                            }, 250);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() {
                return msg.toJsonString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        rQ.add(strRe);
    }
    public void resetLinkChatSearchWinList() {
        LinearLayout lin = (LinearLayout) findViewById(R.id.linkchatuserslistli);
        for (int i = 0; i < uslcls.length; i++) {
            lin.removeView(uslcls[i]);
            lin.removeView(uslspcrs[i]);
        }
        hasUsersList = false;
        if(linkChatSearchIsOpen) {
            initialiseLinkChatSearchWin(false);
            int count = ((EditText) findViewById(R.id.linkchatchopptext)).getText().toString().trim().length();
            if (count == 0) showAll.run();
            else reduceList.run();
        }
    }

    private void getAllConversationsFromDatabase() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String URL = "https://www.alphahub.site/alphalink/api/protected/getallmessages/"+user.getName();

                RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
                JsonObjectRequest obRe = new JsonObjectRequest(Request.Method.GET, URL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                allConversations = (new Gson()).fromJson(response.toString(), AllConversations.class);
                                int ac = allConversations.assembleConversations(user.getName());
                                if(ac != -1) {
                                    hasAllMessages = true;
                                    addLinkChats();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("eRESPr", error.toString());
                            }
                        }
                ){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                        return headers;
                    }
                };

                rQ.add(obRe);
            }
        }, 100);
    }

    private void getGlobalLinkConversation(String tab){
        String URL = "https://www.alphahub.site/alphalink/api/protected/getGlobalLink/"+tab;

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String conversationString = response.toString();
                        conversation = (new Gson()).fromJson(response.toString(), Conversation.class);
                        populateConversation();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Err Resp GGLC", error.toString());
                        conversation = new Conversation();
                        populateConversation();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }
    private void getConversationWith(String other) {
        if(other.contains("globalLink")) {
            getGlobalLinkConversation(globalLinkTab);
            return;
        }

        String URL = "https://www.alphahub.site/alphalink/api/protected/getmessageswith/"+user.getName()+"/"+other;

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String conversationString = response.toString();
                        conversation = (new Gson()).fromJson(response.toString(), Conversation.class);
                        populateConversation();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Err Resp GCW", error.toString());
                        conversation = new Conversation();
                        populateConversation();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }
    private void getLastGlobalLinkMessage() {
        String URL = "https://www.alphahub.site/alphalink/api/protected/getLastGlobalLinkGeneral/";

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Message mess = new Gson().fromJson(response.toString(), Message.class);

                        updateGlobalLinkChatBox(mess);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        log("Global Chat", "Empty");
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }
    private void getMessageFromId(final UserNotify notif) {
        String URL = "https://www.alphahub.site/alphalink/api/protected/getmessagesfromid/"+notif.getRowid();

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Message mess = new Gson().fromJson(response.toString(), Message.class);

                        if(conversation == null) conversation = new Conversation();

                        if(messagesIsOpen || globalLinkIsOpen){
                            if( conversation.hasMessages() && (
                                !user.getName().equals(mess.getFrom()) &&
                                !mess.equals(conversation.getLastMessage())  &&
                                (globalLinkIsOpen || mess.getFrom().equals(conversation.getConversant(user.getName())) ))
                                || (notif.getWhat().contains(user.getName())))
                                    appendMessage(mess, false);
                        }

                        if(mess.getTo().equals("globalLinkGeneral")) updateGlobalLinkChatBox(mess);
                        else if(!mess.getTo().contains("globalLink")) updateLinkChatBox(mess);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        log("Get From ID", "Null");
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }
    private void getMessageFromId(int rowid, final Runnable task) {
        String URL = "https://www.alphahub.site/alphalink/api/protected/getmessagesfromid/"+rowid;

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        tempMessage = new Gson().fromJson(response.toString(), Message.class);
                        if(task != null && task != new Message()) runOnUiThread(task);
                        else Log.e("Something Went", "Wrong");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        log("Get From ID", "Null");
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };
        rQ.add(obRe);
    }

    private void sendMessageTo(String recipient) {
        String what = (isGlobalChat) ?
                ((EditText)findViewById(R.id.profglobalmessagetextfield)).getText().toString() :
                ((EditText)findViewById(R.id.profmessagetextfield)).getText().toString();

        final Message msg = new Message(user.getName(), recipient, Utility.getTime(), what);

        if(recipient.equals("globalLinkGeneral")) updateGlobalLinkChatBox(msg);
        else if(!recipient.contains("globalLink")) updateLinkChatBox(msg);

        String URL = "https://www.alphahub.site/alphalink/api/protected/sendmessage/";

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRe = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final SendMessageResponse resp = (new Gson()).fromJson(response, SendMessageResponse.class);

                        if(resp != null) {
                            appendMessage(msg, true);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    String mm = msg.getShortWhat();
                                    String type = (msg.getTo().equals(globalLinkTab)) ? globalLinkTab : "message";
                                    UserNotify oppNotify = new UserNotify(user.getName(), resp.getRowid(), mm, type);
                                    notifyUser(msg.getTo(), oppNotify);
                                }
                            }, 250);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() {
                return msg.toJsonString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        rQ.add(strRe);
    }
    private void notifyUser(final String name, UserNotify notif) {
        MQTTInterface.publishMessage(name, notif);
    }
    public void onNewMessage(UserNotify dmNotif) {
        if(dmNotif.isGlobalLink()) {
            if(linkChatIsOpen && globalLinkIsOpen && globalLinkTab.equals(dmNotif.getType()) && !dmNotif.getFrom().equals(user.getName())) getMessageFromId(dmNotif);
        }
        else if(messagesIsOpen && !globalLinkIsOpen) getMessageFromId(dmNotif);
        else if(globalLinkIsOpen && dmNotif.getType().equals("message")) {
            updateLinkChatBox(dmNotif);
        }
    }

    private void listenToGlobal(String tab) {
        MQTTInterface.subscribe(tab);
    }
    private void stopListeningToGlobal() {
        MQTTInterface.unSubscribe(globalLinkTab);
    }
    private void resetGlobalListener(final String newTopic) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                stopListeningToGlobal();
                sleep(500);
                listenToGlobal(newTopic);
            }
        });
        t.setDaemon(true);
        t.start();
    }

    private void updateGlobalLinkChatBox(Message mess) {
        ((TextView)findViewById(R.id.globallinkpreview)).setText(mess.getShortWhat());

        int msgDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(mess.getWhen()))),
            todayDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(Utility.getTime())));

        String time = (msgDate<todayDate) ?
                new SimpleDateFormat("dd/MM").format(new Date(mess.getWhen())) :
                new SimpleDateFormat("HH:mm").format(new Date(mess.getWhen()));

        ((TextView)findViewById(R.id.globallinkpreview2)).setText(time);
    }
    private void updateLinkChatBox(final UserNotify notif) {
        getMessageFromId(notif.getRowid(), new Runnable() {
            @Override
            public void run() {
                updateLinkChatBox(tempMessage);
            }
        });
    }
    private void updateLinkChatBox(final Message mess) {
        if(linkChatBoxes == null) return;
        String conversant = (mess.getFrom().equals(user.getName())) ? mess.getTo() : mess.getFrom();
        for(int i = 0; i < linkChatBoxes.length; i++) {
            if(linkChatNames[i].getText().toString().equals(conversant)) {
                final int z = i;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        linkChatPreviews[z].setText(mess.getShortWhat());

                        int msgDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(mess.getWhen()))),
                            todayDate = Integer.parseInt(new SimpleDateFormat("MMdd").format(new Date(Utility.getTime())));

                        String time = (msgDate<todayDate) ?
                            new SimpleDateFormat("dd/MM").format(new Date(mess.getWhen())) :
                            new SimpleDateFormat("HH:mm").format(new Date(mess.getWhen()));

                        linkChatTimestamps[z].setText(time);
                    }
                });
            }
        }
    }

    private void anims(String cmd, final View v) {
        Animation
                pulse = AnimationUtils.loadAnimation(this, R.anim.pulse),
                expand = AnimationUtils.loadAnimation(this, R.anim.expandone),
                close = AnimationUtils.loadAnimation(this, R.anim.closeone),
                expandslow = AnimationUtils.loadAnimation(this, R.anim.expandoneslow),
                closefast = AnimationUtils.loadAnimation(this, R.anim.closefast),
                closedelayed = AnimationUtils.loadAnimation(this, R.anim.closedelayed),
                fade = AnimationUtils.loadAnimation(this, R.anim.fade),
                dropdownopen = AnimationUtils.loadAnimation(this, R.anim.dropdownopen),
                dropdownclose = AnimationUtils.loadAnimation(this, R.anim.dropdownclose),
                fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);

        if(v != null) v.setVisibility(View.VISIBLE);

        switch(cmd) {
            case "pulse":
                v.startAnimation(pulse);
                break;
            case "expand":
                v.startAnimation(expand);
                break;
            case "close":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(close);
                break;
            case "expandslow":
                v.startAnimation(expandslow);
                break;
            case "closefast":
                v.startAnimation(closefast);
                v.setVisibility(View.GONE);
                break;
            case "closeDelayed":
                anims("fade", findViewById(R.id.inbbg));
                closedelayed.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}

                });
                v.startAnimation(closedelayed);
                break;
            case "fadein":
                v.startAnimation(fadein);
                break;
            case "fade":
                fade.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(fade);
                break;
            case "dropDown":
                droppedDown = true;
                findViewById(R.id.dropdowncoverarrow).animate()
                        .setDuration(150)
                        .setStartDelay(100)
                        .scaleX(-0.5f)
                        .start();

                findViewById(R.id.dropDownCover).animate()
                        .setDuration(150)
                        .setStartDelay(170)
                        .translationY(dpToPx(37))
                        .start();

                findViewById(R.id.slotCards).animate()
                        .setDuration(150)
                        .setStartDelay(170)
                        .translationY(dpToPx(37))
                        .start();
                break;
            case "liftUp":
                droppedDown = false;
                findViewById(R.id.dropdowncoverarrow).animate()
                        .setDuration(150)
                        .setStartDelay(100)
                        .scaleX(0.5f)
                        .start();

                findViewById(R.id.dropDownCover).animate()
                        .setDuration(150)
                        .setStartDelay(170)
                        .translationY(0)
                        .start();

                findViewById(R.id.slotCards).animate()
                        .setDuration(150)
                        .setStartDelay(170)
                        .translationY(0)
                        .start();
                break;
            case "openLinkChatWin":
                linkChatIsOpen = true;
                findViewById(R.id.linkchatwin)
                        .animate()
                        .setStartDelay(220)
                        .translationX(0)
                        .setDuration(200)
                        .start();
                break;
            case "closeLinkChatWin":
                linkChatIsOpen = false;
                findViewById(R.id.linkchatwin)
                        .animate()
                        .setStartDelay(100)
                        .translationX(findViewById(R.id.linkchatwin).getWidth())
                        .setDuration(200);
                break;
            case "openMessages":
                messagesIsOpen = true;
                ConstraintLayout mwo = findViewById(R.id.profmessageswin);

                mwo.animate()
                        .setStartDelay(220)
                        .translationX(0)
                        .setDuration(200)
                        .start();
                break;
            case "closeMessages":
                messagesIsOpen = false;

                //makeToast("Closing", 1);
                //updateLinkChatBox(allConversations.getConversationWith(currentConversant).getLastMessage());

                ConstraintLayout mwc = findViewById(R.id.profmessageswin);
                mwc.animate()
                        .setStartDelay(100)
                        .translationX(mwc.getWidth())
                        .setDuration(200);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cleanMessages();
                        if(msgChIsExp) anims("closeChallengeStats", null);
                        ((TextView) findViewById(R.id.profmsgtitlebartitle)).setText("");
                    }
                }, 300);
                break;
            case "expandChallengeStats":
                findViewById(R.id.profmsgchallengetopbg).setPivotY(0);
                findViewById(R.id.profmsgchallengetopbg).animate()
                        .scaleY(1.0f)
                        .setDuration(200);

                msgChIsExp = true;
                findViewById(R.id.profmessageschallengescorestats).animate()
                        .alpha(1)
                        .setDuration(150)
                        .setStartDelay(240)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                findViewById(R.id.profmessageschallengescorestats).setVisibility(View.VISIBLE);
                            }});

                break;
            case "closeChallengeStats":
                msgChIsExp = false;
                findViewById(R.id.profmessageschallengescorestats).setVisibility(View.GONE);

                findViewById(R.id.profmsgchallengetopbg).setPivotY(0);
                findViewById(R.id.profmsgchallengetopbg).animate()
                        .setStartDelay(150)
                        .scaleY(0f)
                        .setDuration(200);
                break;
            case "openGlobalLink":
                globalLinkIsOpen = true;
                ConstraintLayout glo = findViewById(R.id.profglobalmessageswin);

                glo.animate()
                        .setStartDelay(220)
                        .translationX(0)
                        .setDuration(200)
                        .start();

                break;
            case "closeGlobalLink":
                globalLinkIsOpen = false;
                ConstraintLayout glc = findViewById(R.id.profglobalmessageswin);

                glc.animate()
                        .setStartDelay(100)
                        .translationX(glc.getWidth())
                        .setDuration(200);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cleanMessages();
                    }
                }, 300);
                ((TextView) findViewById(R.id.globallinkgeneral)).setTextColor(Color.parseColor("#8B8B8B"));
                ((TextView) findViewById(R.id.globallinkbattle)).setTextColor(Color.parseColor("#CCCCCC"));
                ((TextView) findViewById(R.id.globallinkhelp)).setTextColor(Color.parseColor("#CCCCCC"));
                break;
            case "expandGlobalLinkTabs":
                findViewById(R.id.profglobaltabs).setPivotY(0);
                findViewById(R.id.profglobaltabs).animate()
                        .translationY(0)
                        .setDuration(200);
                globalLinkTabIsOpen = true;
                break;
            case "closeGlobalLinkTabs":
                globalLinkTabIsOpen = false;

                findViewById(R.id.profglobaltabs).setPivotY(0);
                findViewById(R.id.profglobaltabs).animate()
                        .setStartDelay(150)
                        .translationY(-120)
                        .setDuration(140);
                break;
            case "dropdownopen":
                v.setVisibility(View.VISIBLE);
                v.startAnimation(dropdownopen);
                dropDown = true;
                break;
            case "dropdownclose":
                dropdownclose.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);
                    }
                });
                v.startAnimation(dropdownclose);
                dropDown = false;
                break;
            case "openLinkChatSearchWin":
                initialiseLinkChatSearchWin(true);
                linkChatSearchIsOpen = true;
                findViewById(R.id.linkchatsearchnewwin)
                        .animate()
                        .setStartDelay(220)
                        .translationX(0)
                        .setDuration(200)
                        .start();
                break;
            case "closeLinkChatSearchWin":
                linkChatSearchIsOpen = false;
                findViewById(R.id.linkchatsearchnewwin)
                        .animate()
                        .setStartDelay(100)
                        .translationX(findViewById(R.id.linkchatsearchnewwin).getWidth())
                        .setDuration(200);
                new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) { }
                    @Override public void onAnimationRepeat(Animation animation) { }
                    @Override public void onAnimationEnd(Animation animation) {
                        resetLinkChatSearchWinList();
                    }
                };
                break;
            default: break;
        }
    }

    private void closeWindows() {
        if(findViewById(R.id.profbg).getVisibility() == View.VISIBLE) {
            anims("fade", findViewById(R.id.profbg));

            if(findViewById(R.id.profthemechange).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.profthemechange));

            if(findViewById(R.id.profreportmessage).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.profreportmessage));
        }
    }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        closeWindows();
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(linkChatIsOpen && !messagesIsOpen  && !globalLinkIsOpen && !linkChatSearchIsOpen) anims("closeLinkChatWin", null);
            if(linkChatSearchIsOpen) anims("closeLinkChatSearchWin", null);
            if(messagesIsOpen) anims("closeMessages", null);
            if(globalLinkIsOpen) anims("closeGlobalLink", null);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override public void onClick(View v) {
        switch(v.getId()) {
            case R.id.profbg:
                closeWindows();
                break;
            case R.id.profile:
            case R.id.dropdowncoverarrow:
                anims("pulse", v);

                if(droppedDown) anims("liftUp", null);
                else {
                    if(!hasAllMessages) getAllConversationsFromDatabase();
                    getLastGlobalLinkMessage();
                    anims("dropDown", null);
                }
                break;
            case R.id.dropdownlinkchat:
                anims("pulse", v);
                anims("openLinkChatWin", null);
                break;
            case R.id.linkchatlistexit:
                anims("pulse", v);
                anims("closeLinkChatWin", null);
                break;
            case R.id.dropdownthemeselect:
                anims("pulse", v);
                anims("fadein", findViewById(R.id.profbg));
                anims("expand", findViewById(R.id.profthemechange));
                break;

            case R.id.themeselecgrey:
                anims("pulse", v);
                tempTheme = 0;
                ((ImageView)findViewById(R.id.profthemeselecex)).setImageResource(R.drawable.card_tamercard_grey);
                break;
            case R.id.themeselecblue:
                anims("pulse", v);
                tempTheme = 1;
                ((ImageView)findViewById(R.id.profthemeselecex)).setImageResource(R.drawable.card_tamercard_blue);
                break;
            case R.id.themeselecpink:
                anims("pulse", v);
                tempTheme = 2;
                ((ImageView)findViewById(R.id.profthemeselecex)).setImageResource(R.drawable.card_tamercard_pink);
                break;
            case R.id.themeselecgreen:
                anims("pulse", v);
                tempTheme = 3;
                ((ImageView)findViewById(R.id.profthemeselecex)).setImageResource(R.drawable.card_tamercard_green);
                break;
            case R.id.themeselecorange:
                anims("pulse", v);
                tempTheme = 4;
                ((ImageView)findViewById(R.id.profthemeselecex)).setImageResource(R.drawable.card_tamercard_orange);
                break;
            case R.id.themeselecred:
                anims("pulse", v);
                tempTheme = 5;
                ((ImageView)findViewById(R.id.profthemeselecex)).setImageResource(R.drawable.card_tamercard_red);
                break;
            case R.id.themeselecblack:
                anims("pulse", v);
                tempTheme = 6;
                ((ImageView)findViewById(R.id.profthemeselecex)).setImageResource(R.drawable.card_tamercard_black);
                break;
            case R.id.profthemeselect:
                anims("pulse", v);
                user.setTheme(tempTheme);
                settings.setTheme(tempTheme);
                saveSettings(settings.toJsonString());
                user.updateUserInDatabase();
                closeWindows();
                updateTamerCardBg();
                break;
            case R.id.profthemecancel:
                anims("pulse", v);
                tempTheme = user.getTheme();
                closeWindows();
                break;

            case R.id.profmessagesexit:
                anims("pulse", v);
                anims("closeMessages", null);
                break;

            case R.id.profmsgtitlebartitle:
                anims("pulse", v);
                if(msgChIsExp) anims("closeChallengeStats", null);
                else anims("expandChallengeStats", null);
                break;

            case R.id.profmessagessend:
                anims("pulse", v);
                String ms = ((TextView)findViewById(R.id.profmessagetextfield)).getText().toString();

                if(ms.equals("") || ms.equals(" ") || ms.contains("`")) return;
                //sendMessageTo(allConversations.getConversations()[convoSel].getConversant(user.getName()));
                if(!currentConversant.equals(user.getName())) sendMessageTo(currentConversant);
                ((TextView) findViewById(R.id.profmessagetextfield)).setText("");
                break;

            case R.id.globallinkbox:
                anims("pulse", v);
                messageslist = findViewById(R.id.profglobalmessageslist);
                isGlobalChat = true;
                resetGlobalListener("globalLinkGeneral");
                globalLinkTab = "globalLinkGeneral";
                getConversationWith(globalLinkTab);
                anims("openGlobalLink", null);
                break;

            case R.id.globallinkgeneral:
                anims("pulse", v);
                messageslist = findViewById(R.id.profglobalmessageslist);
                isGlobalChat = true;
                resetGlobalListener("globalLinkGeneral");
                globalLinkTab = "globalLinkGeneral";
                getConversationWith(globalLinkTab);

                ((TextView) findViewById(R.id.globallinkgeneral)).setTextColor(Color.parseColor("#8B8B8B"));
                ((TextView) findViewById(R.id.globallinkbattle)).setTextColor(Color.parseColor("#CCCCCC"));
                ((TextView) findViewById(R.id.globallinkhelp)).setTextColor(Color.parseColor("#CCCCCC"));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EditText tb = ((EditText)findViewById(R.id.profglobalmessagetextfield));
                        tb.setFocusable(true);
                        tb.setFocusableInTouchMode(true);
                        tb.requestFocus();
                    }
                }, 250);

                cleanMessages();
                break;

            case R.id.globallinkbattle:
                anims("pulse", v);
                messageslist = findViewById(R.id.profglobalmessageslist);
                isGlobalChat = true;
                resetGlobalListener("globalLinkBattle");
                globalLinkTab = "globalLinkBattle";
                getConversationWith(globalLinkTab);

                ((TextView) findViewById(R.id.globallinkgeneral)).setTextColor(Color.parseColor("#CCCCCC"));
                ((TextView) findViewById(R.id.globallinkbattle)).setTextColor(Color.parseColor("#8B8B8B"));
                ((TextView) findViewById(R.id.globallinkhelp)).setTextColor(Color.parseColor("#CCCCCC"));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EditText tb = ((EditText)findViewById(R.id.profglobalmessagetextfield));
                        tb.setFocusable(true);
                        tb.setFocusableInTouchMode(true);
                        tb.requestFocus();
                    }
                }, 250);

                cleanMessages();
                break;

            case R.id.globallinkhelp:
                anims("pulse", v);
                messageslist = findViewById(R.id.profglobalmessageslist);
                isGlobalChat = true;
                resetGlobalListener("globalLinkHelp");
                globalLinkTab = "globalLinkHelp";
                getConversationWith(globalLinkTab);

                ((TextView) findViewById(R.id.globallinkgeneral)).setTextColor(Color.parseColor("#CCCCCC"));
                ((TextView) findViewById(R.id.globallinkbattle)).setTextColor(Color.parseColor("#CCCCCC"));
                ((TextView) findViewById(R.id.globallinkhelp)).setTextColor(Color.parseColor("#8B8B8B"));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EditText tb = ((EditText)findViewById(R.id.profglobalmessagetextfield));
                        tb.setFocusable(true);
                        tb.setFocusableInTouchMode(true);
                        tb.requestFocus();
                    }
                }, 250);

                cleanMessages();
                break;

            case R.id.profglobalmessagesexit:
                anims("pulse", v);
                anims("closeGlobalLink", null);

                stopListeningToGlobal();
                break;

            case R.id.profglobalmsgtitlebartitle:
                anims("pulse", v);
                if(globalLinkTabIsOpen) anims("closeGlobalLinkTabs", null);
                else anims("expandGlobalLinkTabs", null);
                break;

            case R.id.profglobalmessagessend:
                anims("pulse", v);
                String gms = ((TextView)findViewById(R.id.profglobalmessagetextfield)).getText().toString();

                if(gms.equals("") || gms.equals(" ") || gms.contains("`")) return;
                sendMessageTo(globalLinkTab);
                ((TextView) findViewById(R.id.profglobalmessagetextfield)).setText("");
                break;
            case R.id.profreportmessageselect:
                anims("pulse", v);
                closeWindows();
                reportMessage();
                break;
            case R.id.profreportmessagecancel:
                anims("pulse", v);
                closeWindows();
                break;
            case R.id.proflinkchatcompose:
                anims("pulse", v);
                anims("openLinkChatSearchWin", null);
                break;
            default: break;
        }
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public boolean isMessagesOpen() { return messagesIsOpen; }
    public boolean isLinkChatOpen() { return linkChatIsOpen; }

    private BaseActivity base() { return BaseActivity.getInstance(); }
    public static Profile getInstance(){ return k; }
    public void setUser(UserN user) { this.user = user; }
    public UserN getUser() { return user; }

    private void saveSettings(String text) {
        if(text.equals("") || text == null) text="";
        FileOutputStream fos = null;
        try {
            fos = openFileOutput("settingsstore.txt", MODE_PRIVATE);
            fos.write(text.getBytes());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e("FOS", "FOS == NULL");
        }
    }
    private SettingsStore loadSettings() {
        String ret = "";
        FileInputStream fis = null;

        try {
            fis = openFileInput("settingsstore.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();

            while ( (ret = br.readLine()) !=null ) {
                sb.append(ret).append("\n");
            }
            ret = sb.toString();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fis != null) {
                try {
                    fis.close();

                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        SettingsStore retSets = null;
        if(ret == null || ret.equals("")) retSets = SettingsStore.getEmpty();
        else retSets = new Gson().fromJson(ret, SettingsStore.class);

        return retSets;
    }

    public int pxToDp(final float px) { return (int) (px / ((float) getApplicationContext().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT)); }
    public int dpToPx(final float dp) { return (int) (dp * ((float) getApplicationContext().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT)); }

    private void log(String T, String M) { if(debugLog) Log.e(T, M); }
    private void log(String T, String M, Exception e) { if(debugLog) Log.e(T, M, e); }
    private void makeToast(final String t, final int len) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tv = new TextView(getApplicationContext());
                tv.setGravity(Gravity.CENTER);
                tv.setText(t);
                tv.setPadding(30, 35, 35, 30);
                tv.setTextColor(Color.WHITE);
                tv.setBackgroundResource(R.drawable.toastbg);

                Toast toast = new Toast(getApplicationContext());
                toast.setView(tv);
                toast.setDuration(len);
                toast.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        BaseActivity.getInstance().setRefreshTime(14*60*1000);
        if(linkChatIsOpen) anims("closeLinkChatWin", null);
        if(messagesIsOpen) anims("closeMessages", null);
        if(globalLinkIsOpen) anims("closeGlobalLink", null);
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        BaseActivity.getInstance().setRefreshTime(14*60*1000);
        if(linkChatIsOpen) anims("closeLinkChatWin", null);
        if(messagesIsOpen) anims("closeMessages", null);
        super.onPause();
    }

    @Override
    protected void onResume() {
        BaseActivity.getInstance().setRefreshTime(60*1000);
        super.onResume();
    }

    private int getImageId(String imageName) { return Utility.getImageId(imageName, this); }
    private String getIMGFileName(String rom, int commType) { return Utility.getIMGFileNameFromRom(rom, commType);}
    private String getIMGFileNameFromRom(String rom, int commType) { return Utility.getIMGFileNameFromRom(rom, commType); }
    private String getName(String rom, int commType) { return Utility.getMonName(rom, commType); }
    private String getNameRaw(String rom, int commType) { return Utility.getNameRaw(rom, commType); }
    private int modeColourFromImage(String mon)  {return Utility.modeColourFromImage(mon, this); }
}