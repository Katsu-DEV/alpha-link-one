package com.alpha.alphalink.LinkCore;

public class DatabaseFlags {
    private boolean A,  C, D, E;
    private String B;

    public boolean getA() {
        return A;
    }
    public void setA(boolean a) {
        A = a;
    }

    public String getB() {
        return B;
    }
    public void setB(String b) {
        B = b;
    }

    public boolean getC() {
        return C;
    }
    public void setC(boolean c) {
        C = c;
    }
}
