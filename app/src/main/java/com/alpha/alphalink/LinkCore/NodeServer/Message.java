
package com.alpha.alphalink.LinkCore.NodeServer;

import android.os.Handler;
import android.util.Log;

import com.alpha.alphalink.BaseActivity;
import com.alpha.alphalink.LinkCore.LanguageFilter;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class Message {

    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("when")
    @Expose
    private long when;
    @SerializedName("what")
    @Expose
    private String what;

    public Message() {}

    public Message(String from, String to, long when, String what){
        this.from = from;
        this.to = to;
        this.when = when;
        setWhat(what);
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public long getWhen() {
        return when;
    }

    public void setWhen(long when) {
        this.when = when;
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = LanguageFilter.removeBadChar(LanguageFilter.bobbify(what));
    }

    public String getShortWhat() {
        return (what.length() > 26 ? what.substring(0, 26)+"...": what );
    }

    public boolean equals(Message message) {
        return (message != null) ?
                (this.from.equals(message.getFrom()) &&
                this.to.equals(message.getTo()) &&
                this.what.equals(message.getWhat()) &&
                this.when == message.getWhen())
                : false;
    }

    public String toJsonString() {
        return new Gson().toJson(this);
    }
}
