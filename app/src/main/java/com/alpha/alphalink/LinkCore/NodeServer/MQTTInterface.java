package com.alpha.alphalink.LinkCore.NodeServer;

import android.os.Handler;
import android.util.Log;

import com.alpha.alphalink.BackgroundService;
import com.alpha.alphalink.BaseActivity;
import com.alpha.alphalink.LinkCore.Utility;
import com.alpha.alphalink.Profile;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.HashMap;
import java.util.Map;

public class MQTTInterface {
    //private static BackgroundService bgserv = BackgroundService.getInstance();
    private static MqttClient liveLinkClient;
    private static MqttConnectOptions conOptions;
    private static String broker = "tcp://104.248.224.184:1883"; /*"tcp://192.168.0.27:1883";*/ //"tcp://90.217.181.202:3002"; //"tcp://mqtt.bcraig.dev";
    //private static String broker = "https://www.alphahub.site/livelinkmqtt";
    private static int qos = 1, count = 0;

    public static void connectMqtt(final UserN user, boolean log) {
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            liveLinkClient = new MqttClient(broker, user.getName(), persistence);
            liveLinkClient.generateClientId();
            conOptions = new MqttConnectOptions();
            conOptions.setCleanSession(true);
            conOptions.setKeepAliveInterval(0);
            conOptions.setConnectionTimeout(0);

            liveLinkClient.setCallback(new MqttCallback() {
                public void connectionLost(Throwable cause) {
                    try {
                        if(count++ == 0) BaseActivity.getInstance().makeToast("Something went wrong... [MQ1]\nInternet Connection Lost");
                        Log.e("Something went wrong... [MQ1]", "Internet Connection Lost\nInternet Connection Lost\nInternet Connection Lost");
                        Thread.sleep(60000);
                        MQTTInterface.connectMqtt(user, true);
                    }
                    catch (Exception e) {
                        BaseActivity.getInstance().makeToast("Something went wrong... [MQ0]\n[MQ0]\nInternet Connection Lost");
                        Log.e("ERR", ":"+cause.toString());
//                        Utility.appendToCrashLog("\n [ Internet Connection Lost " + System.currentTimeMillis() + " ]");
//                        try { Thread.sleep(2000); } catch (Exception ee) {}
//                        android.os.Process.killProcess(android.os.Process.myPid());
//                        System.exit(1);
                    }
                }
                public void deliveryComplete(IMqttDeliveryToken token) { Log.e("del", "mq"); }

                public void messageArrived(String topic, MqttMessage mqttBytes) {
                    try{
                        String message = new String(mqttBytes.getPayload());
                        if(message.contains("`!`")) Log.e("MQTT Message Arrived", "Empty");

                        UserNotify notif = new Gson().fromJson(message, UserNotify.class);
                        Log.e("MQTT Message Arrived", "Rec : " + notif.toJsonString());

                        if(notif.isGlobalLink() && Profile.getInstance() != null) Profile.getInstance().onNewMessage(notif);
                        else BackgroundService.getInstance().heardNotification(notif);
                    }
                    catch (Exception e) {
                        logError(e);
                    }
                }
            });

            if(log) print("Connecting to broker: "+ broker);
            liveLinkClient.connect(conOptions);
            if(log) print("Connected");
            count = 0;
        }
        catch(MqttException error) {
            logError(error);
            try {
                if(count++ == 0) BaseActivity.getInstance().makeToast("Something went wrong... [MQ1]\nInternet Connection Lost");
                Log.e("Something went wrong... [MQ1]", "Internet Connection Lost\nInternet Connection Lost\nInternet Connection Lost");
                Thread.sleep(60000);
                MQTTInterface.connectMqtt(user, true);
            }
            catch (Exception e) {
                BaseActivity.getInstance().makeToast("Something went wrong... [MQ0]\n[MQ0]\nInternet Connection Lost");
//                        Utility.appendToCrashLog("\n [ Internet Connection Lost " + System.currentTimeMillis() + " ]");
//                        try { Thread.sleep(2000); } catch (Exception ee) {}
//                        android.os.Process.killProcess(android.os.Process.myPid());
//                        System.exit(1);
            }
        }
    }

    public static void subscribe(String topic) {
        try {
            liveLinkClient.subscribe(topic, qos);
        }
        catch(MqttException error) {
            logError(error);
        }
    }

    public static void unSubscribe(String topic) {
        try {
            liveLinkClient.unsubscribe(topic);
        }
        catch(MqttException error) {
            logError(error);
        }
    }

    public static void publishMessage(String topic, final UserNotify notif) {
        if(liveLinkClient.isConnected()) {
            try {
                MqttMessage mqttBytes = new MqttMessage(notif.toJsonString().getBytes());
                mqttBytes.setQos(qos);
                liveLinkClient.publish(topic, mqttBytes);
                Log.e("MQTTSent", notif.toJsonString());
            }
            catch(MqttException error) {
                logError(error);
                print(error.getMessage());
                error.printStackTrace();
            }
            finally {
                if(!topic.contains("globalLink") && !topic.contains("reportedMessage") && !topic.equals("") ) {
                    //BaseActivity.getInstance().makeToast("TO : [" + topic + "]");
                    RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
                    StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updatexchk/" + topic,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if (response != null) {
                                        Log.e("UpdateUserInxchk", response);
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //BaseActivity.getInstance().makeToast("MQTT ISSUE: ["+error.toString()+"]");
                                    Log.e("UpdateUserInDatabaseErr", ": "+error.getMessage());
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Content-Type", "application/json");
                            return headers;
                        }

                        @Override
                        public byte[] getBody() {
                            return notif.toJsonString().getBytes();
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/json";
                        }
                    };
                    try {
                        Log.e("Body", new String(strRe.getBody()));
                    } catch (AuthFailureError authFailureError) {
                        authFailureError.printStackTrace();
                    }
                    rQ.add(strRe);
                }
            }
        }
        else {
            connectMqtt(BackgroundService.getInstance().getUser(), true);
            subscribe(BackgroundService.getInstance().getUser().getName());

            try {
                MqttMessage mqttBytes = new MqttMessage(notif.toJsonString().getBytes());
                mqttBytes.setQos(qos);
                liveLinkClient.publish(topic, mqttBytes);
                Log.e("MQTTSent", notif.toJsonString());
            }
            catch(MqttException error) {
                logError(error);
                print(error.getMessage());
                error.printStackTrace();
            }
            finally {
                if(!topic.contains("globalLink")) {
                    RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
                    StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updatexchk/" + topic,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if (response != null) {
                                        Log.e("UpdateUserInxchk", response);
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("UpdateUserInDatabaseErr", error.getMessage());
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Content-Type", "application/json");
                            return headers;
                        }

                        @Override
                        public byte[] getBody() {
                            return notif.toJsonString().getBytes();
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/json";
                        }
                    };
                    try {
                        Log.e("Body", new String(strRe.getBody()));
                    } catch (AuthFailureError authFailureError) {
                        authFailureError.printStackTrace();
                    }
                    rQ.add(strRe);
                }
            }
        }

    }

    public static void disconnect() {
        try {
            liveLinkClient.disconnect();
        }
        catch(MqttException error) {
            logError(error);
        }
    }

    public static void keepAlive(final int interval) {
        if(interval < 10) {
            Thread keepAlive = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true) {
                        try {
                            Thread.sleep((int) (180*1000*0.65f));

                            if(liveLinkClient.isConnected()) {
                                MqttMessage mqttBytes = new MqttMessage("!".getBytes());
                                mqttBytes.setQos(qos);
                                try{
                                    liveLinkClient.publish("!", mqttBytes);
                                    Log.e("Keep", "Alive");}
                                catch (Exception e) { Log.e("OHNO", "E"+e.getMessage()+e.getCause()); }
                            }
                            else {
                                connectMqtt(BackgroundService.getInstance().getUser(), true);
                                subscribe(BackgroundService.getInstance().getUser().getName());
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            keepAlive.setDaemon(true);
            keepAlive.start();
        }
        if(interval > 10) {
        Thread keepAlive = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Thread.sleep((int) (interval*1000*0.65f));

                        if(liveLinkClient.isConnected()) {
                            MqttMessage mqttBytes = new MqttMessage("!".getBytes());
                            mqttBytes.setQos(qos);
                            try{
                                liveLinkClient.publish("!", mqttBytes);
                                Log.e("Keep", "Alive");}
                            catch (Exception e) { Log.e("OHNO", "E"+e.getMessage()+e.getCause()); }
                        }
                        else {
                            connectMqtt(BackgroundService.getInstance().getUser(), true);
                            subscribe(BackgroundService.getInstance().getUser().getName());
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        keepAlive.setDaemon(true);
        keepAlive.start();
    }
    }

    // ---------- Other ----------
    private static void print(String m) {
        //BaseActivity.getInstance().makeToast("MQTT PRINT: ["+m+"]");
        Log.e("MQTT", m);}
    private static void logError(MqttException error) {
        Log.e("=======","========");
        Log.e("reason: ", ""+error.getReasonCode());
        Log.e("msg: ", error.getMessage());
        Log.e("loc: ", error.getLocalizedMessage());
        Log.e("cause: ", ""+error.getCause());
        Log.e("exception: ", ""+error);
        error.printStackTrace();
        Log.e("=======","========");
    }
    private static void logError(Exception error) {
        Log.e("=======","========");
        Log.e("msg: ", error.getMessage());
        Log.e("loc: ", error.getLocalizedMessage());
        Log.e("cause: ", ""+error.getCause());
        Log.e("exception: ", ""+error);
        error.printStackTrace();
        Log.e("=======","========");
    }
}
