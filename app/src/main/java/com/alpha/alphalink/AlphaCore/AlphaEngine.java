package com.alpha.alphalink.AlphaCore;

import android.util.Log;

import com.alpha.alphalink.BaseActivity;
import com.alpha.alphalink.LinkCore.RomManager;
import com.alpha.alphalink.LinkCore.Utility;
import com.physicaloid.lib.Physicaloid;

/*
    Core Alpha Functions
    Author: Katsu (Ben)
 */
public class AlphaEngine {
    public Physicaloid ardu;
    private boolean kl = false;
    private int commType = 0, pcktNum, pcktCnt;
    private byte[][] inertCodes;


    public AlphaEngine(Physicaloid ardu, int commType)
    {
        this.ardu = ardu;
        ardu.open();
        ardu.setBaudrate(9600);

        latch();
        setCommType(commType);
        setInertCodes();
    }
    public AlphaEngine(Physicaloid ardu)
    {
        this.ardu = ardu;
        ardu.open();
        ardu.setBaudrate(9600);

        latch();
        setInertCodes();
    }

    public void latch() {
        byte[] latch = (("V1-0000\n")).getBytes();
        write(latch, latch.length);
        while(read() != ']');
    }

    public String getRaw(boolean sendInert) {
        byte[] scn = new byte[1001];
        byte c;
        int i = 0, commType = (this.commType <= 93 && this.commType >= 91) ? 9 : this.commType;
        boolean on = true;

        while (read() != 10);

        if(sendInert) write(inertCodes[commType]);

        Log.e("WHILE", "Enter");
        while(!kl && on) {
            c = read();
            if(c == 'l' || c == 'N') {
                Log.e("Late", "late");
                return "late";
            }
            if(c == 'r'){
                scn[i++] = c;
                do{
                    c = read();
                    if(i>1000) {
                        Log.e("BREAKING", "BREAK");
                        break;
                    }
                    if(c!= 10 && c!= 0) scn[i++] = c;
                    sleep(30);
                }while(c != 10 && !kl);
                on = false;
            }
        }
        Log.e("WHILE", "Left " + (new String(scn)).trim());
        if(i<0) Log.e("C", ""+(char)scn[i-1]);
        Log.e("ON", ""+on);
        Log.e("KL", ""+kl);


        return (new String(scn)).trim();
    }
    public String formatRaw(String dR){
        if(kl) return "-2";
        else if(getRCnt(dR) != pcktNum) return "-1";
        byte[] bytes = dR.getBytes(), bytesOut = new byte[pcktNum*5];
        int i = 0;
        for(int j = 0; j<bytes.length; j++){
            if(bytes[j] == 'r') {
                j+=2;
                for(int k = 0; k<4; k++) bytesOut[i++] = bytes[j++];
                if(i<(pcktNum*5)-1) bytesOut[i++] = '-';
            }
        }
        return new String(bytesOut);
    }
    public String[] formatTwoRaw(String dR){
        if(kl) return new String[]{"-2"};
        else if(getRCnt(dR) != pcktNum) return new String[]{"-1"};

        byte[] bytes = dR.getBytes(),
                romA = new byte[pcktNum*5],
                romB = new byte[pcktNum*5];

        int i = 0;
        for(int j = 0; j<bytes.length; j++){
            if(bytes[j] == 's') {
                j+=2;
                for(int k = 0; k<4; k++) romA[i++] = bytes[j++];
                if(i<(pcktNum*5)-1) romA[i++] = '-';
            }
        }

        i = 0;
        for(int j = 0; j<bytes.length; j++){
            if(bytes[j] == 'r') {
                j+=2;
                for(int k = 0; k<4; k++) romB[i++] = bytes[j++];
                if(i<(pcktNum*5)-1) romB[i++] = '-';
            }
        }
        return new String[] {new String(romA), new String(romB)};
    }
    private int getRCnt(String raw){
        byte[] bytes = raw.getBytes();
        int i = 0;

        for(byte b : bytes) if(b == 'r') i++;
        return i;
    }

    public String getROM() {
        String dR = getRaw(true);
        if(kl) return "-2";

        dR = formatRaw(dR).trim();

        return dR;
    }
    public String getROMNoInert() {
        String dR = getRaw(false);
        if(kl) return "-2";

        dR = formatRaw(dR).trim();

        return dR;
    }
    public void sendInert() {
        Log.e("INERT", ""+new String(inertCodes[(this.commType <= 93 && this.commType >= 91) ? 9 : this.commType]));
        write(inertCodes[(this.commType <= 93 && this.commType >= 91) ? 9 : this.commType]);
    }
    public String sendROM(String dR) {
        print(dR);
        switch(checkROM(dR)){
            case 0:     // Success
                break;
            case -1:    // Rom too short
                return "-3";
            case -2:    // Contains illegal characters
                return "-4";
        }

        dR = convert(dR);
        Log.e("CNVRT",dR);

        if(commType == 9 || commType == 91 || commType == 92 || commType == 93)   dR = "X2-" + dR + "\n";
        else                dR = "V2-" + dR + "\n";

        String dRTemp = "";
        int track = 0;

        Log.e("SendROM", "Start");
        do {
            Log.e("WriteRet", ""+write(dR.getBytes()));
            dRTemp = getRaw(false);
        }while(dRTemp.contains("late"));
        Log.e("SendROM", "End");

        dR = dRTemp;

        if(kl) return "-2";
        dR = formatRaw(dR).trim();
        return dR;
    }
    public String sendV1Rom(String dR) {
        dR = dR + "\n";
        write(dR.getBytes());

        dR = getRaw(false);
        if(kl) return "-2";
        dR = formatRaw(dR).trim();
        return dR;
    }
    public String[] sendROMGetTwoRoms(String dR) {
        print(dR);
        switch(checkROM(dR)){
            case 0:     // Success
                break;
            case -1:    // Rom too short
                return new String[] {"-3"};
            case -2:    // Contains illegal characters
                return new String[] {"-4"};
        }

        dR = convert(dR);

        print(dR);
        if(commType == 9 || commType == 91 || commType == 92 || commType == 93)   dR = "X2-" + dR + "\n";
        else                dR = "V2-" + dR + "\n";
        print(dR);

        write(dR.getBytes());

        dR = getRaw(false);
        if(kl) return new String[] {"-2"};
        return formatTwoRaw(dR);
    }

    public String convert(String dR){
        byte[] pckts, strb = dR.getBytes();
        String s;
        int orderByte;

        switch (commType) {
            case 0:
            case 1:
                //pen20sin
                //pen20tag
                //"@C^F^FE" and order bit
                pckts = new byte[(pcktNum-1) * 5];
                for(int i = pckts.length-1; i>=0; i--) pckts[i] = strb[i];

                // -- Inverting the order bit --
                orderByte = (int) pckts[0];

                // convert to Hex
                if(orderByte >= 48 && orderByte <= 57) orderByte -= 48; // 0-9
                else if(orderByte >= 65 && orderByte <= 70) orderByte -= 55; // 10-15
                // Flip the order bit
                if(orderByte >= 8) orderByte -= 8;
                else if(orderByte < 8) orderByte += 8;
                // Convert back to binary
                if(orderByte >= 0 && orderByte <= 9) orderByte += 48; // 0-9
                else if(orderByte >= 10 && orderByte <= 15) orderByte += 55; // 10-15
                // Inject back to pckts
                pckts[0] = (byte) orderByte;

                s = new String(pckts) + "@C^F^FE";
                return s;

            case 2:
            case 3:
                //dm20sin
                //dm20tag
                //"@0^F^FE" and order bit
                pckts = new byte[(pcktNum-1) * 5];
                for(int i = pckts.length-1; i>=0; i--) pckts[i] = strb[i];

                int ff = Integer.parseInt(""+pckts[10], 16);
                pckts[10] = (byte) Integer.toHexString(Integer.parseInt("0"+ RomManager.leadingZeros(Integer.toBinaryString(ff), 4).substring(1), 2)).toCharArray()[0];


//                // -- Inverting the order bit --
//                orderByte = (int) pckts[10];
//
//                // convert to Hex
//                if(orderByte >= 48 && orderByte <= 57) orderByte -= 48; // 0-9
//                else if(orderByte >= 65 && orderByte <= 70) orderByte -= 55; // 10-15
//                // Flip the order bit
//                if(orderByte >= 8) orderByte -= 8;
//                else if(orderByte < 8) orderByte += 8;
//                // Convert back to binary
//                if(orderByte >= 0 && orderByte <= 9) orderByte += 48; // 0-9
//                else if(orderByte >= 10 && orderByte <= 15) orderByte += 55; // 10-15
//                // Inject back to pckts
//                pckts[10] = (byte) orderByte;


                s = new String(pckts) + "@0^F^FE";
                return s;

            case 4:
                //copy20
                // v2-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx- @0 xxx
                pckts = new byte[(pcktNum*5)];
                for(int i = 0; i<=44; i++) pckts[i] = strb[i];
                pckts[45] = '@'; pckts[46] = '0';
                for(int i = 47; i<pckts.length; i++) pckts[i] = strb[i-1];

                // -- Inverting the order bit --
                orderByte = (int) pckts[10];

                // convert to Hex
                if(orderByte >= 48 && orderByte <= 57) orderByte -= 48; // 0-9
                else if(orderByte >= 65 && orderByte <= 70) orderByte -= 55; // 10-15
                // Flip the order bit
                if(orderByte >= 8) orderByte -= 8;
                else if(orderByte < 8) orderByte += 8;
                // Convert back to binary
                if(orderByte >= 0 && orderByte <= 9) orderByte += 48; // 0-9
                else if(orderByte >= 10 && orderByte <= 15) orderByte += 55; // 10-15
                // Inject back to pckts
                pckts[10] = (byte) orderByte;

                s = new String(pckts);
                return s;
            case 5:
                //jogress20
                //"@A^1^1E"
                pckts = new byte[(pcktNum-1) * 5];
                for(int i = pckts.length-1; i>=0; i--) pckts[i] = strb[i];

                // -- Inverting the order bit --
                orderByte = (int) pckts[0];

                // convert to Hex
                if(orderByte >= 48 && orderByte <= 57) orderByte -= 48; // 0-9
                else if(orderByte >= 65 && orderByte <= 70) orderByte -= 55; // 10-15
                // Flip the order bit
                if(orderByte >= 8) orderByte -= 8;
                else if(orderByte < 8) orderByte += 8;
                // Convert back to binary
                if(orderByte >= 0 && orderByte <= 9) orderByte += 48; // 0-9
                else if(orderByte >= 10 && orderByte <= 15) orderByte += 55; // 10-15
                // Inject back to pckts
                pckts[0] = (byte) orderByte;

                s = new String(pckts) + "@A^1^1E";
                return s;

            case 6:
                //dmx
                //"@8^1^1E"
                pckts = new byte[(pcktNum-1) * 5];
                for(int i = pckts.length-1; i>=0; i--) pckts[i] = strb[i];


                // -- Inverting the order bit --
                orderByte = (int) pckts[0];

                // convert to Hex
                if(orderByte >= 48 && orderByte <= 57) orderByte -= 48; // 0-9
                else if(orderByte >= 65 && orderByte <= 70) orderByte -= 55; // 10-15
                // Flip the order bit
                if(orderByte >= 8) orderByte -= 8;
                else if(orderByte < 8) orderByte += 8;
                // Convert back to binary
                if(orderByte >= 0 && orderByte <= 9) orderByte += 48; // 0-9
                else if(orderByte >= 10 && orderByte <= 15) orderByte += 55; // 10-15
                // Inject back to pckts
                pckts[0] = (byte) orderByte;
                //pckts[0] = 'z';

                s = new String(pckts) + "@8^1^FE";
                return s;

            case 7:
                //dmog
                //v2-xxxx- @C^3 x ^3
                pckts = new byte[(pcktNum*5)+2];

                for(int i = 0; i<=4; i++) pckts[i] = strb[i];
                pckts[5] = '@'; pckts[6] = 'C'; pckts[7] = '^'; pckts[8] = '3';
                pckts[9] = strb[7];
                pckts[10] = '^'; pckts[11] = '3';

                s = new String(pckts);
                return s;

            case 8:
                //penog
                pckts = new byte[(pcktNum*5)+2];

                for(int i = 0; i<=10; i++) pckts[i] = strb[i];
                pckts[11] = '^'; pckts[12] = '1'; pckts[13] = '^'; pckts[14] = 'F';
                pckts[15] = strb[13]; pckts[16] = strb[14];
                pckts[17] = '@'; pckts[18] = 'B' ;
                pckts[19] = strb[16]; pckts[20] = strb[17]; pckts[21] = strb[18];

                s = new String(pckts);
                return s;

            case 9:
            case 91:
            case 92:
            case 93:
                //penx
                //@0^0^09
                pckts = new byte[(pcktNum-1) * 5];
                for(int i = pckts.length-1; i>=0; i--) pckts[i] = strb[i];
                s = new String(pckts) + "@4^1^F9";
                return s;
            case 10:
                //penProg
                // v2-xxxx-xxxx-xxxx-x^1^fx-@5xxx
                // x^1^fx-@5xxx

                pckts = new byte[(pcktNum*5)+2];
                for(int i = 0; i<=15; i++) pckts[i] = strb[i];
                pckts[16] = '^'; pckts[17] = '1';
                pckts[18] = '^'; pckts[19] = 'F';
                pckts[20] = strb[18]; pckts[21] = strb[19];
                pckts[22] = '@'; pckts[23] = '5';
                for(int i = 24; i<pckts.length; i++) pckts[i] = strb[i-3];

                s = new String(pckts);
                return s;
        }
        print("conv: " + dR);
        return dR;
    }

    private int checkROM(String dR) {
        int cnt = 0;
        char[] ddr = dR.toCharArray();

        for(char b : ddr) {
            if (b == '-') cnt++;

            if(b >= 'A' && b <= 'F' || b >= '0' && b <= '9' || b == '-' || b == 'V' || b == 'X' || b == '^' || b == '@' || b == ' ') {}
            else return -2;
        }
        if (cnt != pcktNum-1) return -1;
        print("pcktcnt: " + cnt);

        return 0;
    }

    public void setCommType(int commType){
        this.commType = commType;

        switch (commType){
            case 0:                 // PEN20T
                pcktNum = 10;
                break;
            case 1:                 // PEN20S
                pcktNum = 10;
                break;
            case 2:                 // DM20T
                pcktNum = 10;
                break;
            case 3:                 // DM20S
                pcktNum = 10;
                break;
            case 4:                 // COPY20
                pcktNum = 10;
                break;
            case 5:                 // JOG20
                pcktNum = 3;
                break;
            case 6:                 // DMXB
                pcktNum = 6;
                break;
            case 7:                 // DMOG
                pcktNum = 2;
                break;
            case 8:                 // PENOG
                pcktNum = 4;
                break;
            case 9:                 // PENX
            case 91:                // Mini V1
            case 92:                // Mini V2
            case 93:                // Mini V3
                pcktNum = 4;
                break;
            case 10:                 // PENPROG
                pcktNum = 5;
                break;
            case 99: default: break;
        }
        //write(inertCodes[commType]);
    }
    private void setInertCodes(){
        inertCodes = new byte[][] { new String("V2-820E-003E-000E-000E-000E-003E-000E-000E-000E\n").getBytes(),
                                    new String("V2-800E-003E-000E-000E-000E-000E-000E-000E-000E\n").getBytes(),
                                    new String("V2-0101-0101-020E-008E-05AE-01EE-008E-05AE-01EE\n").getBytes(),
                                    new String("V2-0101-0101-000E-008E-05AE-01EE-000E-000E-000E\n").getBytes(),
                                    new String("V2-0101-0101-037E-000E-000E-003E-000E-000E-000E\n").getBytes(),
                                    new String("V2-196E-003E\n").getBytes(),
                                    new String("V2-000E-007E-041E-141E-004E\n").getBytes(),
                                    new String("V2-FC03\n").getBytes(),
                                    new String("V2-211F-000F-080F\n").getBytes(),
                                    new String("X2-0009-0009-0009\n").getBytes(),
                                    new String("V2-88A1-47A1-BFF1-3BF1\n").getBytes()};
    }
    public int getPcktNum() {
        return pcktNum; }

    private int write(byte[] bfr, int length) {
        print("Writea: "+new String(bfr));
        return ardu.write(bfr, length); }
    private int write(byte[] bfr) {
        if(!(new String(bfr)).contains(("V1-0000"))) Utility.copyTextToClipboard(new String(bfr));
        try {
            Utility.appendToSerialLog("WRITE: " + new String(bfr));
        }
        catch (Exception e) {
            //BaseActivity.getInstance().makeToast("Failed to append to serial log");
        }
        print("Writeb: "+new String(bfr));
        return ardu.write(bfr, bfr.length); }
    private int read(byte[] bfr, int length) {
        return ardu.read(bfr, length); }
    private byte read() {
        byte[] b = new byte[1];
        ardu.read(b, 1);
        try {
            if(b[0] > 32 && b[0] < 126) Utility.appendToSerialLog("READ: " + ((char)b[0]));
        }
        catch (Exception e) {
            //BaseActivity.getInstance().makeToast("Failed to append to serial log");
        }
        //if(b[0] != 0) Log.e("RB", ""+(char)b[0]);
        return b[0];
    }

    public void absorb() {
        byte[] s = new byte[1];
        int cnt = 0;
        //read(s, s.length);

        do {
            read(s, 1);
            if (s[0] == 10) //cnt++// ;
            {
                cnt++;
                read(s, 1);
                if(s[0] == 10) break;
            }
            //if (cnt >= 3) break;
        }while (true);
    }
    public void clear(byte[] bfr){
        for(int i = 0; i< bfr.length; i++)
            if(bfr[i] != 0) bfr[i] = 0;
    }
    public void setkl() {
        kl = true; }
    public void clrkl() {
        kl = false; }
    private void sleep(int millis) {try {
        Thread.sleep(millis);}catch(InterruptedException e){e.printStackTrace();}}
    private void print(String msg) {
        Log.e("MSG", msg); }
}