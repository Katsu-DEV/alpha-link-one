package com.alpha.alphalink;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.alpha.alphalink.AlphaCore.PenXBattleCalculator;
import com.alpha.alphalink.LinkCore.LanguageFilter;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alpha.alphalink.LinkCore.DatabaseFlags;
import com.alpha.alphalink.LinkCore.NodeServer.UserNotify;
import com.alpha.alphalink.LinkCore.RomManager;
import com.alpha.alphalink.LinkCore.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {
    private static MainActivity k;
    private int VERSION = 0;
    private static final String TAG = "GoogleActivity";
    private static final int RC_SIGN_IN = 9001;
    private boolean pressed = false, debugLog = true;
    private UserN user;
    private DatabaseFlags flags;
    private FirebaseUser fbUser;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(BaseActivity.getInstance() != null && BackgroundService.getInstance() != null) startActivity(BaseActivity.getInstance().getIntent());
        else {
            setContentView(R.layout.activity_main);
            k = this;
            testBench();

            title = findViewById(R.id.mainactitle);

            // Button listeners
            findViewById(R.id.signIn).setOnClickListener(this);

            // Configure Google Sign In
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();

            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

            // Initialize Firebase Auth
            mAuth = FirebaseAuth.getInstance();

            ((CheckBox)findViewById(R.id.mainuacheck)).setClickable(false);

            try {
                ((TextView)findViewById(R.id.versnum))
                        .setText("V"+this.getPackageManager().getPackageInfo(getPackageName(), 0).versionName.trim());
            } catch (PackageManager.NameNotFoundException e) {
                ((TextView)findViewById(R.id.versnum)).setVisibility(View.GONE);
                e.printStackTrace();
            }

            getVersionNumber();

            setLogoTranslation();
            anims("boot", null);
        }
    }

    private void setLogoTranslation() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;

        findViewById(R.id.mainactlogo).setTranslationY((height/2)+81);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            }
            catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                log(TAG, "Google sign in failed", e);
                makeToast("Google sign in failed");

            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            //FirebaseUser user = mAuth.getCurrentUser();
                            fbUser = mAuth.getCurrentUser();
                            authComplete();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            makeToast("Sign In Failed");
                        }

                        // [START_EXCLUDE]
                        //hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void checkLive() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference flagRef = db.collection("flags").document("a");

        flagRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        flags = document.toObject(DatabaseFlags.class);
                        if(flags.getA()) {
                            log("Live Check", "Success");
                            signIn();
                        }
                        else {
                            pressed = false;
                            makeToast("Server Down");
                        }
                    }
                    else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }

    private void checkVers() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference flagRef = db.collection("flags").document("a");

        flagRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        flags = document.toObject(DatabaseFlags.class);
                        if(Integer.parseInt(flags.getB()) <= VERSION) {
                            checkLive();
                        }
                        else makeToast("This is an older version of the app. Please update at the website");
                    }
                    else {
                        pressed = false;
                        Log.e(TAG, "No such document");
                    }
                }
                else {
                    pressed = false;
                    Log.e(TAG, "get failed with ", task.getException());
                }
            }
        });
    }

    private void authComplete(){
        if (fbUser != null) getUserFromDatabase();
    }

    private void getUserFromDatabase() {
        String uid = fbUser.getUid();
        String URL = "https://www.alphahub.site/alphalink/api/protected/getUserByUid/"+uid;

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String resp = response.toString();
                        if(resp.contains("'user doesn't exist'")) {
                            createUser();
                        }
                        else {
                            user = new Gson().fromJson(resp, UserN.class);
                            accountFound();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("eRESP", ": "+error.toString());
                        makeToast("Failed to reach server - Please contact an admin");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }

    public void accountFound() {
        if(user.getJoined() == 0){
            anims("expand", findViewById(R.id.mainuawin));
        }
        else {
            Log.e("User", "Found");
            if(user.getBanned() > 10) {
                makeToast("Your account has been temporarily suspended due to inappropriate behaviour or misuse of the app.");
            }
            else {
                user.updateUserInDatabase();
                Intent intent = new Intent(MainActivity.this, BaseActivity.class);
                intent.putExtra("User", user);
                startActivity(intent);
            }
        }
    }

    public void acceptUserAgreement() {
        if( ((CheckBox)findViewById(R.id.mainuapromise4)).isChecked() &&
                ((CheckBox)findViewById(R.id.mainuapromise5)).isChecked() &&
                ((CheckBox)findViewById(R.id.mainuapromise6)).isChecked() &&
                ((CheckBox)findViewById(R.id.mainuapromise7)).isChecked() &&
                ((CheckBox)findViewById(R.id.mainuapromise8)).isChecked() &&
                ((CheckBox)findViewById(R.id.mainuacheck)).isChecked()) {
            anims("close", findViewById(R.id.mainuawin));

            user.setDmchk(UserNotify.getEmpty().toJsonString());
            user.setChalchk(UserNotify.getEmpty().toJsonString());
            user.setJoined(Utility.getTime());
            Log.e("USR", ": "+user.toJsonString());
            user.updateUserInDatabase();

            if(user.getBanned() > 10) {
                makeToast("Your account has been temporarily suspended due to inappropriate behaviour or misuse of the app.");
            }
            else {
                Intent intent = new Intent(MainActivity.this, BaseActivity.class);
                intent.putExtra("User", user);
                startActivity(intent);
            }
        }
        else {
            makeToast("Please accept all parts of the User Agreement");
        }

    }

    private void createUser() {
        Intent intent = new Intent(MainActivity.this, CreateUser.class);
        intent.putExtra("UID", fbUser.getUid());
        startActivity(intent);
    }

    private void authFailed() {
        makeToast("Failed to authenticate Google account \n Please try again");
    }

    private void testBench() {
        log("TESTBENCH", "===================================================");

        String rom = "010A-1317-400E-02EE-09AE-08EE-000E-000E-000E-D0FE";
        log("ROM", RomManager.generateDM20ForcedV1FromBase(rom, "DraCat", true));

        log("T", "===========================================================");
    }

    private void makeToast(final String t) {
        Toast.makeText(this, t, Toast.LENGTH_LONG).show();
    }

    private void getVersionNumber() {
        String vers = "";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            vers = pInfo.versionName;
            vers = vers.replace(".", "");
            VERSION = Integer.parseInt(vers.substring(0, 3));
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void anims(String cmd, final View v) {
        Animation
                pulse = AnimationUtils.loadAnimation(this, R.anim.pulse),
                expand = AnimationUtils.loadAnimation(this, R.anim.expandone),
                close = AnimationUtils.loadAnimation(this, R.anim.closeone),
                expandslow = AnimationUtils.loadAnimation(this, R.anim.expandoneslow),
                closefast = AnimationUtils.loadAnimation(this, R.anim.closefast),
                closedelayed = AnimationUtils.loadAnimation(this, R.anim.closedelayed),
                fade = AnimationUtils.loadAnimation(this, R.anim.fade),
                fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);

        if(v != null) v.setVisibility(View.VISIBLE);

        switch(cmd) {
            case "pulse":
                v.startAnimation(pulse);
                break;
            case "expand":
                v.startAnimation(expand);
                break;
            case "close":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) { v.setVisibility(View.GONE);}});
                v.startAnimation(close);
                break;
            case "expandslow":
                v.startAnimation(expandslow);
                break;
            case "closefast":
                v.startAnimation(closefast);
                v.setVisibility(View.GONE);
                break;
            case "closeDelayed":
                anims("fade", findViewById(R.id.bg));
                closedelayed.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) { v.setVisibility(View.GONE);}});
                v.startAnimation(closedelayed);
                break;
            case "fade":
                fade.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.GONE);}
                });
                v.startAnimation(fade);
                break;
            case "fadein":
                v.startAnimation(fadein);
                break;
            case "boot":
                findViewById(R.id.mainactlogo).animate()
                        .setStartDelay(900)
                        .setDuration(1200)
                        .alpha(1)
                        .start();

                final ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(findViewById(R.id.mainactbg),
                        "backgroundColor",
                        new ArgbEvaluator(),
                        0xFF151515, 0xFFFFFFFF);
                backgroundColorAnimator.setStartDelay(3000);
                backgroundColorAnimator.setDuration(1000);
                backgroundColorAnimator.start();

                final ObjectAnimator backgroundColorAnimator2 = ObjectAnimator.ofObject(findViewById(R.id.mainactlogo),
                        "tint",
                        new ArgbEvaluator(),
                        0xFFCACACA, 0xFFFF1B1B);
                backgroundColorAnimator2.setStartDelay(3000);
                backgroundColorAnimator2.setDuration(1000);
                backgroundColorAnimator2.start();


                findViewById(R.id.mainactlogo).animate()
                        .setStartDelay(3000)
                        .setDuration(900)
                        .translationY(Utility.dpToPx(240, this))
                        .start();

                findViewById(R.id.mainactitle).animate()
                        .setStartDelay(4200)
                        .setDuration(500)
                        .alpha(1.0f)
                        .start();

                findViewById(R.id.signIn).animate()
                        .setStartDelay(4900)
                        .setDuration(500)
                        .alpha(1.0f)
                        .start();

                break;
            default: break;
        }
    }

    public void clearPressed() { pressed = false; }

    private void signInPressed() {
        pressed = true;

        Utility.postDelayed(3000, new Runnable() {
            @Override
            public void run() {
                MainActivity.getInstance().clearPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.signIn:
                anims("pulse", v);
                if(!pressed) {
                    signInPressed();
                    checkVers();
                }
                break;
            case R.id.mainuapromise4:
            case R.id.mainuapromise5:
            case R.id.mainuapromise6:
            case R.id.mainuapromise7:
                if( ((CheckBox)findViewById(R.id.mainuapromise4)).isChecked() &&
                        ((CheckBox)findViewById(R.id.mainuapromise5)).isChecked() &&
                        ((CheckBox)findViewById(R.id.mainuapromise6)).isChecked() &&
                        ((CheckBox)findViewById(R.id.mainuapromise7)).isChecked()) {
                    ((CheckBox)findViewById(R.id.mainuacheck)).setClickable(true);
                    findViewById(R.id.mainuacontrols).setAlpha(0.75f);
                }
                else {
                    ((CheckBox)findViewById(R.id.mainuacheck)).setClickable(false);
                    findViewById(R.id.mainuacontrols).setAlpha(0.25f);
                }
                break;
            case R.id.mainuaaccept:
                anims("pulse", v);
                acceptUserAgreement();
                break;
            default: break;
        }
    }
    public void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void log(String T, String M) {
        if(debugLog) Log.e(T, M);
    }
    private void log(String T, String M, Exception e) {
        if(debugLog) Log.e(T, M, e);
    }

    public static MainActivity getInstance() {
        return k;
    }
    public void setUser(UserN user) { this.user = user; }
    public UserN getUser() { return user; }
}