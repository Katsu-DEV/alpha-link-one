package com.alpha.alphalink.AlphaCore;

import com.alpha.alphalink.LinkCore.RomManager;

/**
 * Pen20 Battle Calculator takes the Input and Output DigiROMs from a battle on a Pen20 and calculates which device was the winner.
 */
public class Pen20BattleCalculator {
    private String[] shotSizePatterns;
    private int p1Shakes, p2Shakes, p1Score, p2Score, p1AtkScore, p2AtkScore, p1ShotW, p1ShotS, p2ShotW, p2ShotS, p1Life = 10, p2Life = 10;
    private String[] p1Mon;
    private String[] p2Mon;
    private String p1ShotPattern, p2ShotPattern, p1HitPattern, p2HitPattern;
    // Each element has two further elements. [x][0] is player one's life points at round x, and likewise for [x][1] and player two.
    private int[][] lifePoints = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
    // True when player one is the winner, false when player two is the winner.
    private boolean winner;



    public Pen20BattleCalculator(String romPenA, String romPenB) {
        initShotSizePatterns();

        String temp;

        // Obtain Shakes and Shot Pattern from DigiROMs
        //Shot pattern is different from DM20 in that, first, we must get attribute (Va, Da, Vi, Free) and level (Child-Ultimate/1-4, and each corresponding shake count (0-15)
        //Second, we look at the corresponding score for that combination of attribute, level, and shake count (equalling 0-14 (0,2,3,4,5,6,77,8,10,11,13,15,17,M1,M2)
        //Third, we get a pattern

        p1Score = RomManager.getPen20Score(romPenA);
        p1Mon = RomManager.getPen20MonFromRom(romPenA);
        p1AtkScore = RomManager.getPen20ScorePattern(p1Score);
        //p1Score = RomManager.getPen20Pattern(p1Mon, p1Shakes);
        p1ShotPattern = shotSizePatterns[p1AtkScore];

        p2Score = RomManager.getPen20Score(romPenB);
        p2Mon = RomManager.getPen20MonFromRom(romPenB);
        p2AtkScore = RomManager.getPen20ScorePattern(p2Score);
        //p2Score = RomManager.getPen20Pattern(p2Mon, p2Shakes);
        p2ShotPattern = shotSizePatterns[p2AtkScore];


        // Obtain Hit Pattern from DigiROM
        temp = leadingZeros(Integer.toHexString(RomManager.getPen20HitPattern(romPenA)), 2);
        p1HitPattern = leadingZeros(Integer.toBinaryString(Integer.parseInt(temp.substring(0,1), 16)), 4);
        p2HitPattern = leadingZeros(Integer.toBinaryString(Integer.parseInt(temp.substring(1), 16)), 4);

        // Obtain ShotSizes
        p1ShotW = RomManager.getPen20ShotW(romPenA);
        p1ShotS = RomManager.getPen20ShotS(romPenA);
        p2ShotW = RomManager.getPen20ShotW(romPenB);;
        p2ShotS = RomManager.getPen20ShotS(romPenB);;

        int round = 0;

        do {
            for(int j = 0; j<4; j++) {
                p1Life -= (Integer.parseInt(""+p1HitPattern.toCharArray()[3-j]) * Integer.parseInt(""+p2ShotPattern.toCharArray()[j]));
                p2Life -= (Integer.parseInt(""+p2HitPattern.toCharArray()[3-j]) * Integer.parseInt(""+p1ShotPattern.toCharArray()[j]));
                lifePoints[round][0] = p1Life;
                lifePoints[round][1] = p2Life;
                round++;

                // Check if either player's life points have reached 0
                if(p1Life <= 0) {
                    winner = false;
                    return;
                }
                else if (p2Life <= 0)  {
                    winner = true;
                    return;
                }
                // If 5 rounds have been exceeded
                else if (round >= 5 && p2Life != p1Life) {
                    winner = (p2Life<p1Life);
                    return;
                }
            }
        } while (true/*p1Life > 0 && p2Life > 0*/);
    }

    public String[] getShotSizePatterns() {
        return shotSizePatterns;
    }
    public int getP1Shakes() {
        return p1Shakes;
    }
    public int getP2Shakes() {
        return p2Shakes;
    }
    public String getP1ShotPattern() {
        return p1ShotPattern;
    }
    public String getP2ShotPattern() {
        return p2ShotPattern;
    }
    public String getP1HitPattern() {
        return p1HitPattern;
    }
    public String getP2HitPattern() {
        return p2HitPattern;
    }
    public int[][] getLifePoints() {
        return lifePoints;
    }
    public boolean getWinner() {
        return winner;
    }
    public String getP1Shot() { return "Player 1 Shots - Weak: " + p1ShotW + ". Strong: " + p1ShotS; }
    public String getP2Shot() { return "Player 2 Shots - Weak: " + p2ShotW + ". Strong: " + p2ShotS; }

    private void initShotSizePatterns() {
        shotSizePatterns = new String[]{
                "1111",
                "1121",
                "1212",
                "3112",
                "2222",
                "3222",
                "2323",
                "2224",
                "3333",
                "3342",
                "4324",
                "3442",
                "4344",
                "4444",
                "4444"};
    }
    private static String leadingZeros(String input, int length) {
        if(input.length() < length){
            for(int j = input.length(); j<length ; j++) input = "0"+input;
        }
        return input;
    }
}
