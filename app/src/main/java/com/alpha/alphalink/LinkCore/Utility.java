package com.alpha.alphalink.LinkCore;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import androidx.palette.graphics.Palette;

import com.alpha.alphalink.BackgroundService;
import com.alpha.alphalink.BaseActivity;
import com.alpha.alphalink.LinkCore.NodeServer.ChallengeN;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;
import com.alpha.alphalink.R;
import com.jakewharton.processphoenix.ProcessPhoenix;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.TimeZone;

public class Utility {
    private static Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

    // ========== GENERAL UTILITY ==========
    public static int getImageId(String imageName, Context context) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }
    public static String getIMGFileNameFromRom(String rom, int commType) {
        String name = "";
        switch(commType) {
            case 0: break;
            case 1:
                name = RomManager.getPen20MonFromRom(rom)[0];
                break;
            case 2: break;
            case 3:
                name = RomManager.getDm20MonFromRom(rom);
                break;
            case 4: break;
            case 5: break;
            case 6: break;
            case 7:
                name = RomManager.getDmogMonFromRom(rom);
                break;
            case 8:
                name = RomManager.getPenOGMonFromRom(rom);
                break;
            case 9:
                name = RomManager.getPenxMonFromRom(rom).replace("_art_", "").replace("_nor_", "").replace("_wil_", "");
                if(name.startsWith("x_")) name = "_"+name;
                break;
            case 91:
                name = RomManager.getMiniMonFromRom(rom, 1);
                break;
            case 92:
                name = RomManager.getMiniMonFromRom(rom, 2);
                break;
            case 93:
                name = RomManager.getMiniMonFromRom(rom,3);
                break;
            case 10: break;
            default: break;
        }

        return "sprite_" + name.trim();
    }
    public static String getIndex(String rom, int commType) {
        switch(commType) {
            case 1: return ""+RomManager.getPen20Index(rom);
            case 3: return ""+RomManager.getDM20Index(rom);
            case 7: return RomManager.getDmogIndex(rom);
            case 8: return RomManager.getPenOGIndex(rom);
            case 9: return RomManager.getPenxIndex(rom);
            case 91:
            case 92:
            case 93:
                return RomManager.getMiniIndex(rom);
            default: break;
        }
        return "";
    }
    public static String getNameRaw(String rom, int commType) {
        String name = "";
        switch(commType) {
            case 0: break;
            case 1:
                name = RomManager.getPen20MonFromRom(rom)[0];
                break;
            case 2: break;
            case 3:
                name = RomManager.getDm20MonFromRom(rom);
                break;
            case 4: break;
            case 5: break;
            case 6: break;
            case 7:
                name = RomManager.getDmogMonFromRom(rom);
                break;
            case 8:
                name = RomManager.getPenOGMonFromRom(rom);
                break;
            case 9:
                name = RomManager.getPenxMonFromRom(rom).replace("_art_", "").replace("_nor_", "").replace("_wil_", "");
                if(name.startsWith("x_")) name = "_"+name;
                break;
            case 91:
                name = RomManager.getMiniMonFromRom(rom, 1);
                break;
            case 92:
                name = RomManager.getMiniMonFromRom(rom, 2);
                break;
            case 93:
                name = RomManager.getMiniMonFromRom(rom,3);
                break;
            case 10: break;
            default: break;
        }

        return name;
    }
    public static String getMonName(String rom, int commType) {
        String index =  RomManager.getIndexGeneral(rom, commType);
        return getMonNameFromIndex(index, commType);
//        String name = "";
//        switch(commType) {
//            case 0: break;
//            case 1:
//                name = RomManager.getPen20MonFromRom(rom)[0];
//                break;
//            case 2: break;
//            case 3:
//                name = RomManager.getDm20MonFromRom(rom);
//                break;
//            case 4: break;
//            case 5: break;
//            case 6: break;
//            case 7:
//                name = RomManager.getDmogMonFromRom(rom);
//                break;
//            case 8: break;
//            case 9:
//                name = RomManager.getPenxMonFromRom(rom);
//                break;
//            case 91:
//                name = RomManager.getMiniMonFromRom(rom, 1);
//                break;
//            case 92:
//                name = RomManager.getMiniMonFromRom(rom, 2);
//                break;
//            case 93:
//                name = RomManager.getMiniMonFromRom(rom,3);
//                break;
//            case 10: break;
//            default: break;
//        }
//
//        if(name.contains("_")) {
//            String s = name.substring(1, 4);
//
//            if(s.equals("blu")) name = name+"";
//            else if(s.equals("gre")) name = name+"";
//            else if(s.equals("tai")) name = name.substring(5)+"mon";
//            else if(s.equals("yam")) name = name.substring(5)+"mon";
//            else if(s.equals("als")) name = "Alter-S "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
//            else if(s.equals("fdm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FDM";
//            else if(s.equals("stn")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon STM";
//            else if(s.equals("plm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon PLM";
//            else if(s.equals("blm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon BLM";
//            else if(s.equals("ftm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FTM";
//            else if(s.equals("rgm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon RGM";
//            else if(s.equals("vcm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon VCM";
//            else if(s.equals("ory")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon ORY";
//            else if(s.equals("bla")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Blanc";
//            else if(s.equals("cie")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Ciel";
//            else if(s.equals("noi")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Noir";
//            else if(s.equals("bsh")) name = "Bushi "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
//            else if(s.equals("hak")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Hakase";
//            else if(s.equals("wil") || s.equals("art") || s.equals("nor")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
//
//            if(name.contains("_x")) name = name.substring(2) + " X";
//
//
//
//            else return "err";
//
//            return name;
//        }
//        else return name.substring(0,1).toUpperCase() + name.substring(1) + "mon";
    }
    public static String getMonNameFromIndex(String index, int commType) {
        String name = "";
        switch(commType) {
            case 0: break;
            case 1:
                name = RomManager.getPen20MonFromIndex(index);
                break;
            case 2: break;
            case 3:
                name = RomManager.getDm20MonFromIndex(index);
                break;
            case 4: break;
            case 5: break;
            case 6: break;
            case 7:
                name = RomManager.getDmogMonFromIndex(index);
                break;
            case 8:
//                makeToast("Index: "+index, BaseActivity.getInstance());
                name = RomManager.getPenOGMonFromIndex(index);
                break;
            case 9:
                name = RomManager.getPenxMonFromIndex(index);
                break;
            case 91:
                name = RomManager.getMiniMonFromIndex(index, 1);
                break;
            case 92:
                name = RomManager.getMiniMonFromIndex(index, 2);
                break;
            case 93:
                name = RomManager.getMiniMonFromIndex(index, 3);
                break;
            case 10: break;
            default: break;
        }

        if(name.contains("_")) {
            String s = name.substring(1, 4);

            if(s.equals("blu")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Blue";
            else if(s.equals("gre")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Green";
            else if(s.equals("tai")) name = name.substring(5)+"mon";
            else if(s.equals("yam")) name = name.substring(5)+"mon";
            else if(s.equals("als")) name = "Alter-S "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
            else if(s.equals("fdm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FDM";
            else if(s.equals("plm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon PLM";
            else if(s.equals("blm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon BLM";
            else if(s.equals("ftm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FTM";
            else if(s.equals("rgm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon RGM";
            else if(s.equals("vcm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon VCM";
            else if(s.equals("ory")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon ORY";
            else if(s.equals("bla")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Blanc";
            else if(s.equals("cie")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Ciel";
            else if(s.equals("noi")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Noir";
            else if(s.equals("bsh")) name = "Bushi "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
            else if(s.equals("hak")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Hakase";
            else if(s.equals("wil") || s.equals("art") || s.equals("nor")) {
                name = name.replace("_art_", "").replace("_nor_", "").replace("_wil_", "");
                if(name.startsWith("x_"))  name = name.substring(2,3).toUpperCase() + name.substring(3) + "mon X";
                // name = name.replace("x_", "") + "mon X";
                // name = name.substring(0,1).toUpperCase() + name.substring(1);
                else name = name.substring(0,1).toUpperCase()+name.substring(1)+"mon";
            }

            if(name.contains("_x_")) name = name.substring(3,4).toUpperCase() + name.substring(4) + "mon X";
            //if(name.contains("x_")) name = name.substring(2) + "mon X";

            //else return "err";

            return name;
        }
        else return name.substring(0,1).toUpperCase() + name.substring(1) + "mon";
    }
    public static int modeColourFromImage(String mon, Context context) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), getImageId("sprite_"+mon, context));
        if(bitmap != null){
            Palette p = Palette.from(bitmap).generate();

            return p.getVibrantColor(0xffCCCCCC);
        }
        else return new Color().parseColor("#FFCCCCCC");
    }

    public static int pxToDp(final float px, Context context) {
        return (int) (px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }
    public static int dpToPx(final float dp, Context context) {
        return (int) (dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static long getTime() { return System.currentTimeMillis(); }

    public static UserN getUserFromLeaderboard(String name) {
        for(UserN u : BaseActivity.getInstance().getLeaderboard()) {
            if(u.getName().equals(name)) return u;
        }
        return null;
    }

    public static void postDelayed(int millis, Runnable runnable) {
        new Handler().postDelayed(runnable, millis);
    }

    public static void copyTextToClipboard(String text) {
        try{
            ClipboardManager clipboard = (ClipboardManager) BaseActivity.getInstance().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("DigiROM", text);
            clipboard.setPrimaryClip(clip);
            //BaseActivity.getInstance().makeToast("Copied: "+text);
        }
        catch(Exception e) {
            Log.e("Utility", "Failed to Copy");
        }
    }

    public static void makeCrash() {
        int[] z = new int[3];
        for(int i = 0; i < 10; i++) z[i] = i;
    }
    public static void catchCrash(String title, Exception e, final Context context) {
        makeToast("Alpha Link has crashed.\nFind the crash log in the Settings menu.", context);
        String log = Log.getStackTraceString(e);
        if(log=="") Log.getStackTraceString(e.getCause());
        if(log=="") Log.getStackTraceString(e.getCause().getCause());
        Utility.appendToCrashLog(title+"\n" + log);

        postDelayed(2000, new Runnable() {
            @Override
            public void run() {
                ProcessPhoenix.triggerRebirth(context);
            }
        });
    }
    public static void catchCrash(String title, Throwable t, final Context context) {
        makeToast("Alpha Link has crashed.\nFind the crash log in the Settings menu.", context);
        String log = Log.getStackTraceString(t);
        if(log=="") Log.getStackTraceString(t.getCause());
        if(log=="") Log.getStackTraceString(t.getCause().getCause());
        Utility.appendToCrashLog(title+"\n" + log);
        postDelayed(2000, new Runnable() {
            @Override
            public void run() {
                ProcessPhoenix.triggerRebirth(context);
            }
        });
    }

    public static void appendToSerialLog(String text) {
        String buffer = loadSerialLog();
        saveSerialLog(buffer/* + "\n"*/ + text);
    }
    public static void resetSerialLog() {
        try {
            String s = loadSerialLog();
            int ind = 0;
            long logT = 0;

            if(!s.startsWith("empty")) {
                ind = s.indexOf("<");
                logT = Long.parseLong(s.substring(ind+1, ind+14));
            }
            if(getTime() - logT > 43200000 || s.startsWith("empty")) {
                saveSerialLog("============ START <" + getTime() + "> ============");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            BaseActivity.getInstance().makeToast("Failed to reset serial log");
            saveSerialLog("============ START <" + getTime() + "> ============");
        }
    }
    public static void copySerialLog() {
        try{
            String text = loadSerialLog();
            ClipboardManager clipboard = (ClipboardManager) BaseActivity.getInstance().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("DigiROM", text);
            clipboard.setPrimaryClip(clip);
            BaseActivity.getInstance().makeToast("Copied Log");
        }
        catch(Exception e) {
            Log.e("Utility", "Failed to Copy Log");
        }
    }
    private static void saveSerialLog(String text) {
        if(text == null || text.equals("")) text="";
        FileOutputStream fos = null;
        try {
            fos = BaseActivity.getInstance().openFileOutput("alpha_link_serial_log.txt", BaseActivity.getInstance().MODE_PRIVATE);
            fos.write(text.getBytes());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fos != null) {
                try {
                    fos.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e("FOS", "FOS == NULL");
        }
    }
    public static String loadSerialLog() {
        String ret = "";
        FileInputStream fis = null;

        try {
            fis = BaseActivity.getInstance().openFileInput("alpha_link_serial_log.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();

            while ( (ret = br.readLine()) != null ) {
                sb.append(ret).append("\n");
            }
            ret = sb.toString();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fis != null) {
                try {
                    fis.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if(ret == null || ret.equals("")) ret = "empty";
        return ret;
    }

    public static void appendToCrashLog(String text) {
        String buffer = loadCrashLog();
        saveCrashLog(buffer + text);
    }
    public static void resetCrashLog() {
        try {
            String s = loadCrashLog();
            int ind = 0;
            long logT = 0;

            if(!s.startsWith("empty")) {
                ind = s.indexOf("<");
                logT = Long.parseLong(s.substring(ind+1, ind+14));
            }

            //Log.e("TIMES", ": "+ getTime() + " " + logT + " " + (getTime() - logT > 43200000) + " " + s.startsWith("empty"));
            if((getTime() - logT > 43200000) || s.startsWith("empty")){
                saveCrashLog("============ START <" + getTime() + "> ============");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            BaseActivity.getInstance().makeToast("Failed to reset crash log");
        }
    }
    public static void copyCrashLog() {
        try{
            String text = loadCrashLog();
            ClipboardManager clipboard = (ClipboardManager) BaseActivity.getInstance().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("DigiROM", text);
            clipboard.setPrimaryClip(clip);
            BaseActivity.getInstance().makeToast("Copied Log");
        }
        catch(Exception e) {
            BaseActivity.getInstance().makeToast("Failed to Copy Log");
        }
    }
    private static void saveCrashLog(String text) {
        if(text == null || text.equals("")) text="";
        FileOutputStream fos = null;
        try {
            fos = BaseActivity.getInstance().openFileOutput("alpha_link_crash_log.txt", BaseActivity.getInstance().MODE_PRIVATE);
            fos.write(text.getBytes());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fos != null) {
                try {
                    fos.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e("FOS", "FOS == NULL");
        }
    }
    public static String loadCrashLog() {
        String ret = "";
        FileInputStream fis = null;

        try {
            fis = BaseActivity.getInstance().openFileInput("alpha_link_crash_log.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();

            while ( (ret = br.readLine()) != null ) {
                sb.append(ret).append("\n");
            }
            ret = sb.toString();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fis != null) {
                try {
                    fis.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if(ret == null || ret.equals("")) ret = "empty";
        return ret;
    }

    public static ChallengeN selectChallengeWith(String opp, ChallengeN[] chals) {
        for(ChallengeN chal : chals) {
            if(chal.getOpp().equals(opp)) return chal;
        }
        return null;
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void makeToast(final String t, Context context) {
        Toast.makeText(context, t, Toast.LENGTH_LONG).show();
    }
}
