package com.alpha.alphalink.LinkCore;

import android.util.Log;

import com.alpha.alphalink.BaseActivity;
import com.alpha.alphalink.Challenges;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RomManager {
    private static HashMap<String, String> map;
    private static HashMap<Integer, Integer> pen20AtkMap;
    private static HashMap<String, String[]> mapp;
    private static List<String[]> pen20Data;

    // ========== DM20 ==========
    public static String generateDM20RomV2FromBase(String digiROM, int presses) {
        char[] rom = digiROM.toCharArray();

        //int ff = Integer.parseInt(""+rom[10], 16);
        //rom[10] = Integer.toHexString(Integer.parseInt("0"+Integer.toBinaryString(ff).substring(1), 2)).toCharArray()[0];

        // Presses
        char[]  prBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[10]+rom[11], 16) ), 8).toCharArray(),
                prArr = leadingZeros(Integer.toBinaryString(presses), 5).toCharArray(),
                prHex;

        for(int i = 0; i<5; i++) prBits[i+1] = prArr[i];
        prHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(prBits), 2)), 2 ).toCharArray();

        rom[10] = prHex[0];
        rom[11] = prHex[1];

        return new String(rom).toUpperCase();
    }
    public static String generateDM20ForcedV1FromBase(String digiROM, String username, boolean win) {
        char[] rom = (digiROM.substring(0, digiROM.length()-5)+"-@0F0E").toCharArray();

        int ff = Integer.parseInt(""+rom[10], 16);
        rom[10] = Integer.toHexString(Integer.parseInt("1"+leadingZeros(Integer.toBinaryString(ff), 4).substring(1), 2)).toCharArray()[0];

        // Name
        char[]  unm = username.toUpperCase().toCharArray(), name;
        if(username.length() > 3) name = new char[] {unm[0], unm[1], unm[2], unm[3]};
        else name = new char[] {'E', 'E', 'E', 'E'};
        int j = 0;
        for (int i = 0; i<username.length(); i++) {
            switch (unm[i]) {
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    break;
                default:
                    if(j < 4) name[j++] = unm[i];
                    else i = username.length() + 1;
            }
        }

        rom[2] = getDM20Char(name[0]).toCharArray()[0];
        rom[3] = getDM20Char(name[0]).toCharArray()[1];
        rom[0] = getDM20Char(name[1]).toCharArray()[0];
        rom[1] = getDM20Char(name[1]).toCharArray()[1];

        rom[7] = getDM20Char(name[2]).toCharArray()[0];
        rom[8] = getDM20Char(name[2]).toCharArray()[1];
        rom[5] = getDM20Char(name[3]).toCharArray()[0];
        rom[6] = getDM20Char(name[3]).toCharArray()[1];

        int presses, power;
        String hp;
        if(win) {
            presses = 0;
            power = 0;
            hp = "F0";
        }
        else {
            presses = 31;
            power = 255;
            hp = "0F";
        }

        Log.e("EE", ":"+rom[10]);
        // Presses
        char[]  prBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[10]+rom[11], 16) ), 8).toCharArray(),
                prArr = leadingZeros(Integer.toBinaryString(presses), 5).toCharArray(),
                prHex;

        Log.e("PRB", new String(prBits));
        Log.e("PRA", new String(prArr));


        for(int i = 0; i<5; i++) prBits[i+1] = prArr[i];
        prHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(prBits), 2)), 2 ).toCharArray();
        Log.e("EE", ":"+prHex[0]);

        // Power
        char[] poHex = leadingZeros(Integer.toHexString(power), 2).toCharArray();

        char[] hpHex = hp.toCharArray();

        rom[10] = prHex[0];
        rom[11] = prHex[1];

        rom[26] = poHex[0];
        rom[27] = poHex[1];

        // HP
        rom[47] = hpHex[0];
        rom[48] = hpHex[1];

        Log.e("Final Rom", new String(rom).toUpperCase());
        return "V1-" + new String(rom).toUpperCase();
    }
    public static String generateDM20RomV2(String index, int presses, int power, int hitpattern, String username) {
        char[] rom = new String("0101-0101-000E-008E-05AE-01EE-000E-000E-000E-@000E").toCharArray();

        // Name
        char[]  unm = username.toUpperCase().toCharArray(), name;
        if(username.length() > 3) name = new char[] {unm[0], unm[1], unm[3], unm[4]};
        else name = new char[] {'E', 'E', 'E', 'E'};
        int j = 0;
        for (int i = 0; i<username.length(); i++) {
            switch (unm[i]) {
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    break;
                default:
                    if(j < 4) name[j++] = unm[i];
                    else i = username.length() + 1;
            }
        }

        rom[2] = getDM20Char(name[0]).toCharArray()[0];
        rom[3] = getDM20Char(name[0]).toCharArray()[1];
        rom[0] = getDM20Char(name[1]).toCharArray()[0];
        rom[1] = getDM20Char(name[1]).toCharArray()[1];

        rom[7] = getDM20Char(name[2]).toCharArray()[0];
        rom[8] = getDM20Char(name[2]).toCharArray()[1];
        rom[5] = getDM20Char(name[3]).toCharArray()[0];
        rom[6] = getDM20Char(name[3]).toCharArray()[1];


        // Presses
        char[]  prBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[10]+rom[11], 16) ), 8).toCharArray(),
                prArr = leadingZeros(Integer.toBinaryString(presses), 5).toCharArray(),
                prHex;

        for(int i = 0; i<5; i++) prBits[i+1] = prArr[i];
        prHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(prBits), 2)), 2 ).toCharArray();

        // Index/Attribute
        char[]  inAttrBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[15]+rom[16]+rom[17], 16) ), 12).toCharArray(),
                inBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(index)), 10).toCharArray(),
                attrBits = getDm20Attribute(index).toCharArray(),
                inAttrHex;

        for(int i = 0; i<10; i++) inAttrBits[i] = inBits[i];
        for(int i = 0; i<2; i++) inAttrBits[i+10] = attrBits[i];
        inAttrHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(inAttrBits), 2)), 3 ).toCharArray();

        // Power
        char[] poHex = leadingZeros(Integer.toHexString(power), 2).toCharArray();

        // HitPattern
        char[] hitHex = leadingZeros(Integer.toHexString(hitpattern), 2).toCharArray();

        rom[10] = prHex[0];
        rom[11] = prHex[1];

        rom[15] = inAttrHex[0];
        rom[16] = inAttrHex[1];
        rom[17] = inAttrHex[2];

        rom[26] = poHex[0];
        rom[27] = poHex[1];

        rom[47] = hitHex[0];
        rom[48] = hitHex[1];

        return new String(rom).toUpperCase();
    }
    public static String generateDM20RomV2(String index, int presses, int power, String username) {
        Log.e("===== :Gen Rom", " =====");
        Log.e("Index", ""+index);
        Log.e("Presses", ""+presses);
        Log.e("Power", ""+power);
        Log.e("===== :=======", " =====");

        char[] rom = new String("0101-0101-800E-008E-05AE-01EE-000E-000E-000E-000E").toCharArray();

        // Name
        char[]  unm = username.toUpperCase().toCharArray(), name;
        if(username.length() > 3) name = new char[] {unm[0], unm[1], unm[2], unm[3]};
        else name = new char[] {'E', 'E', 'E', 'E'};
        int j = 0;
        for (int i = 0; i<username.length(); i++) {
            switch (unm[i]) {
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    break;
                default:
                    if(j < 4) name[j++] = unm[i];
                    else i = username.length() + 1;
            }
        }

        rom[2] = getDM20Char(name[0]).toCharArray()[0];
        rom[3] = getDM20Char(name[0]).toCharArray()[1];
        rom[0] = getDM20Char(name[1]).toCharArray()[0];
        rom[1] = getDM20Char(name[1]).toCharArray()[1];

        rom[7] = getDM20Char(name[2]).toCharArray()[0];
        rom[8] = getDM20Char(name[2]).toCharArray()[1];
        rom[5] = getDM20Char(name[3]).toCharArray()[0];
        rom[6] = getDM20Char(name[3]).toCharArray()[1];


        // Presses
        char[]  prBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[10]+rom[11], 16) ), 8).toCharArray(),
                prArr = leadingZeros(Integer.toBinaryString(presses), 5).toCharArray(),
                prHex;

        for(int i = 0; i<5; i++) prBits[i+1] = prArr[i];
        prHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(prBits), 2)), 2 ).toCharArray();

        // Index/Attribute
        char[]  inAttrBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[15]+rom[16]+rom[17], 16) ), 12).toCharArray(),
                inBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(index)), 10).toCharArray(),
                attrBits = getDm20Attribute(index).toCharArray(),
                inAttrHex;

        for(int i = 0; i<10; i++) inAttrBits[i] = inBits[i];
        for(int i = 0; i<2; i++) inAttrBits[i+10] = attrBits[i];
        inAttrHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(inAttrBits), 2)), 3 ).toCharArray();

        // Power
        char[] poHex = leadingZeros(Integer.toHexString(power), 2).toCharArray();


        rom[10] = prHex[0];
        rom[11] = prHex[1];

        rom[15] = inAttrHex[0];
        rom[16] = inAttrHex[1];
        rom[17] = inAttrHex[2];

        rom[26] = poHex[0];
        rom[27] = poHex[1];

        return new String(rom).toUpperCase();
    }
    public static String generateDM20ForcedV1(String index, String username, boolean win) {
        char[] rom = new String("0101-0101-800E-008E-05AE-01EE-000E-000E-000E-@0F0E").toCharArray();

        // Name
        char[]  unm = username.toUpperCase().toCharArray(), name;
        if(username.length() > 3) name = new char[] {unm[0], unm[1], unm[2], unm[3]};
        else name = new char[] {'E', 'E', 'E', 'E'};
        int j = 0;
        for (int i = 0; i<username.length(); i++) {
            switch (unm[i]) {
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    break;
                default:
                    if(j < 4) name[j++] = unm[i];
                    else i = username.length() + 1;
            }
        }

        rom[2] = getDM20Char(name[0]).toCharArray()[0];
        rom[3] = getDM20Char(name[0]).toCharArray()[1];
        rom[0] = getDM20Char(name[1]).toCharArray()[0];
        rom[1] = getDM20Char(name[1]).toCharArray()[1];

        rom[7] = getDM20Char(name[2]).toCharArray()[0];
        rom[8] = getDM20Char(name[2]).toCharArray()[1];
        rom[5] = getDM20Char(name[3]).toCharArray()[0];
        rom[6] = getDM20Char(name[3]).toCharArray()[1];

        int presses, power;
        String hp;
        if(win) {
            presses = 0;
            power = 0;
            hp = "F0";
        } else {
            presses = 31;
            power = 255;
            hp = "0F";
        }

        // Presses
        char[]  prBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[10]+rom[11], 16) ), 8).toCharArray(),
                prArr = leadingZeros(Integer.toBinaryString(presses), 5).toCharArray(),
                prHex;

        for(int i = 0; i<5; i++) prBits[i+1] = prArr[i];
        prHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(prBits), 2)), 2 ).toCharArray();

        // Index/Attribute
        char[]  inAttrBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[15]+rom[16]+rom[17], 16) ), 12).toCharArray(),
                inBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(index)), 10).toCharArray(),
                attrBits = getDm20Attribute(index).toCharArray(),
                inAttrHex;

        for(int i = 0; i<10; i++) inAttrBits[i] = inBits[i];
        for(int i = 0; i<2; i++) inAttrBits[i+10] = attrBits[i];
        inAttrHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(inAttrBits), 2)), 3 ).toCharArray();

        // Power
        char[] poHex = leadingZeros(Integer.toHexString(power), 2).toCharArray();

        char[] hpHex = hp.toCharArray();

        rom[10] = prHex[0];
        rom[11] = prHex[1];

        rom[15] = inAttrHex[0];
        rom[16] = inAttrHex[1];
        rom[17] = inAttrHex[2];

        rom[26] = poHex[0];
        rom[27] = poHex[1];

        // HP
        rom[47] = hpHex[0];
        rom[48] = hpHex[1];

        return "V1-" + new String(rom).toUpperCase();
    }

    public static String getDM20Index(String rom) {
        int index = (Integer.parseInt(rom.substring(15, 18), 16))/4;
        if(index>=512) index -= 512;
        if(index>=256) index -= 256;

        return ""+index;
    }
    public static int getDM20Power(String rom) {
        int power = (Integer.parseInt(rom.substring(26, 28), 16));
        return power;
    }
    public static int getDM20Presses(String rom) {
        int presses = (Integer.parseInt(rom.substring(10, 12), 16));
        String bits = (Integer.toBinaryString(presses));
/*        if(bits.length() < 8){
            for(int i = bits.length(); i<8 ; i++) bits = "0"+bits;
        }*/
        bits = leadingZeros(bits, 8);

        bits = bits.substring(2, 6);
        presses = Integer.parseInt(bits, 2);
        return presses;
    }
    public static int getDM20HitPattern(String rom) {
        int hp = (Integer.parseInt(rom.substring(46, 48), 16));
        return hp;
    }
    private static String getDM20Char(char c) {
        switch(c) {
            case 'A':
                return "01";
            case 'B':
                return "02";
            case 'C':
                return "03";
            case 'D':
                return "04";
            case 'E':
                return "05";
            case 'F':
                return "06";
            case 'G':
                return "07";
            case 'H':
                return "08";
            case 'I':
                return "09";
            case 'J':
                return "0A";
            case 'K':
                return "0B";
            case 'L':
                return "0C";
            case 'M':
                return "0D";
            case 'N':
                return "0E";
            case 'O':
                return "0F";
            case 'P':
                return "10";
            case 'Q':
                return "11";
            case 'R':
                return "12";
            case 'S':
                return "13";
            case 'T':
                return "14";
            case 'U':
                return "15";
            case 'V':
                return "16";
            case 'W':
                return "17";
            case 'X':
                return "18";
            case 'Y':
                return "19";
            case 'Z':
                return "1A";
            default:
                return "01";
        }
    }

    public static String getDm20MonFromRom(String rom) {
        dm20MapInit();

        int index = (Integer.parseInt(rom.substring(15, 18), 16))/4;
        if(index>=512) index -= 512;
        if(index>=256) index -=256;
        //if(index>=128) index -= 128;
        //if(index>=64) index -= 64;

        if(mapp.containsKey(String.valueOf(index))) return mapp.get(String.valueOf(index))[0];
        else return "err";
//        map = new HashMap<String, String>();
//        dm20IndexInit();
//
//        int index = (Integer.parseInt(rom.substring(15, 18), 16))/4;
//        if(index>=512) index -= 512;
//        if(index>=256) index -=256;
//        //if(index>=128) index -= 128;
//        //if(index>=64) index -= 64;
//
//        if(map.containsKey(String.valueOf(index))) return map.get(String.valueOf(index));
//        else return "err";
    }
    public static String getDm20MonFromIndex(String index) {
        dm20MapInit();

        if(mapp.containsKey(String.valueOf(index))) return mapp.get(String.valueOf(index))[0];
        else return "err";
//        map = new HashMap<String, String>();
//        dm20IndexInit();
//
//        if(map.containsKey(String.valueOf(index))) return map.get(String.valueOf(index));
//        else return "err";
    }
    public static String getDm20Attribute(String index) {
        // return 2 bits
        dm20MapInit();

        if(mapp.containsKey(String.valueOf(index))) return mapp.get(String.valueOf(index))[2];
        else return "err";
//        // return 2 bits
//        dm20AttributeInit();
//
//        if(map.containsKey(String.valueOf(index))) return map.get(String.valueOf(index));
//        else return "err";
    }
    public static String getDm20Level(String index) {
        // return 2 bits
        dm20MapInit();

        if(mapp.containsKey(String.valueOf(index))) return mapp.get(String.valueOf(index))[1];
        else return "err";
    }
    public static String getDm20BasePower(String index) {
        // return 2 bits
        dm20MapInit();

        if(mapp.containsKey(String.valueOf(index))) return mapp.get(String.valueOf(index))[4];
        else return "err";
    }
    private static void dm20MapInit() {
        mapp = new HashMap<String, String[]>();

        mapp.put("0", new String[] {"bota", "Baby I", "11", ",koro", "0"});
        mapp.put("1", new String[] {"koro", "Baby II", "11", ",agu,beta,_tai_agu", "0"});
        mapp.put("2", new String[] {"agu", "Child", "00", ",grey,tyranno,devi,mera,nume", "18"});
        mapp.put("3", new String[] {"beta", "Child", "10", ",devi,mera,airdra,seadra,nume", "10"});
        mapp.put("4", new String[] {"grey", "Adult", "00", ",metalgrey", "75"});
        mapp.put("5", new String[] {"tyranno", "Adult", "01", ",mame", "70"});
        mapp.put("6", new String[] {"devi", "Adult", "10", ",metalgrey", "65"});
        mapp.put("7", new String[] {"mera", "Adult", "01", ",mame", "60"});
        mapp.put("8", new String[] {"airdra", "Adult", "00", ",metalgrey", "55"});
        mapp.put("9", new String[] {"seadra", "Adult", "01", ",mame", "50"});
        mapp.put("10", new String[] {"nume", "Adult", "10", ",monzae", "40"});
        mapp.put("11", new String[] {"metalgrey", "Perfect", "10", ",blitzgrey", "126"});
        mapp.put("12", new String[] {"mame", "Perfect", "01", ",banchomame", "118"});
        mapp.put("13", new String[] {"monzae", "Perfect", "00", ",", "107"});
        mapp.put("14", new String[] {"blitzgrey", "Ultimate", "10", ",_als_omega", "188"});
        mapp.put("15", new String[] {"banchomame", "Ultimate", "01", ",", "176"});
        mapp.put("16", new String[] {"puni", "Baby I", "11", ",tuno", "0"});
        mapp.put("17", new String[] {"tuno", "Baby II", "11", ",gabu,elec,_yam_gabu", "0"});
        mapp.put("18", new String[] {"gabu", "Child", "01", ",kabuteri,garuru,ange,yukidaru,veggie", "18"});
        mapp.put("19", new String[] {"elec", "Child", "01", ",ange,yukidaru,birdra,wha,veggie", "10"});
        mapp.put("20", new String[] {"kabuteri", "Adult", "00", ",skullgrey", "75"});
        mapp.put("21", new String[] {"garuru", "Adult", "00", ",metalmame", "70"});
        mapp.put("22", new String[] {"ange", "Adult", "00", ",skullgrey", "65"});
        mapp.put("23", new String[] {"yukidaru", "Adult", "00", ",metalmame", "60"});
        mapp.put("24", new String[] {"birdra", "Adult", "00", ",skullgrey", "55"});
        mapp.put("25", new String[] {"wha", "Adult", "00", ",metalmame", "50"});
        mapp.put("26", new String[] {"veggie", "Adult", "10", ",vade", "40"});
        mapp.put("27", new String[] {"skullgrey", "Perfect", "10", ",skullmam", "126"});
        mapp.put("28", new String[] {"metalmame", "Perfect", "01", ",cresgaruru", "118"});
        mapp.put("29", new String[] {"vade", "Perfect", "10", ",", "107"});
        mapp.put("30", new String[] {"skullmam", "Ultimate", "00", ",", "169"});
        mapp.put("31", new String[] {"cresgaruru", "Ultimate", "01", ",_als_omega", "188"});
        mapp.put("32", new String[] {"poyo", "Baby I", "11", ",toko", "0"});
        mapp.put("33", new String[] {"toko", "Baby II", "11", ",pata,kune,", "0"});
        mapp.put("34", new String[] {"pata", "Child", "01", ",uni,kentaru,ogre,bake,suka", "18"});
        mapp.put("35", new String[] {"kune", "Child", "10", ",ogre,bake,shell,drimoge,suka", "10"});
        mapp.put("36", new String[] {"uni", "Adult", "00", ",andro", "75"});
        mapp.put("37", new String[] {"kentaru", "Adult", "01", ",giro", "70"});
        mapp.put("38", new String[] {"ogre", "Adult", "10", ",andro", "65"});
        mapp.put("39", new String[] {"bake", "Adult", "10", ",giro", "60"});
        mapp.put("40", new String[] {"shell", "Adult", "01", ",andro", "55"});
        mapp.put("41", new String[] {"drimoge", "Adult", "01", ",giro", "50"});
        mapp.put("42", new String[] {"suka", "Adult", "10", ",ete", "40"});
        mapp.put("43", new String[] {"andro", "Perfect", "00", ",hiandro", "126"});
        mapp.put("44", new String[] {"giro", "Perfect", "00", ",", "118"});
        mapp.put("45", new String[] {"ete", "Perfect", "10", ",kingete", "107"});
        mapp.put("46", new String[] {"hiandro", "Ultimate", "00", ",", "176"});
        mapp.put("47", new String[] {"kingete", "Ultimate", "10", ",", "169"});
        mapp.put("48", new String[] {"yura", "Baby I", "11", ",tane", "0"});
        mapp.put("49", new String[] {"tane", "Baby II", "11", ",biyo,pal,", "0"});
        mapp.put("50", new String[] {"biyo", "Child", "00", ",monochro,cockatri,leo,kuwaga,nani", "18"});
        mapp.put("51", new String[] {"pal", "Child", "01", ",leo,kuwaga,coela,mojya,nani", "10"});
        mapp.put("52", new String[] {"monochro", "Adult", "01", ",megadra", "75"});
        mapp.put("53", new String[] {"cockatri", "Adult", "01", ",piccolo", "70"});
        mapp.put("54", new String[] {"leo", "Adult", "00", ",megadra", "65"});
        mapp.put("55", new String[] {"kuwaga", "Adult", "10", ",piccolo", "60"});
        mapp.put("56", new String[] {"coela", "Adult", "01", ",megadra", "55"});
        mapp.put("57", new String[] {"mojya", "Adult", "00", ",piccolo", "50"});
        mapp.put("58", new String[] {"nani", "Adult", "10", ",digitama", "40"});
        mapp.put("59", new String[] {"megadra", "Perfect", "10", ",aegisdra", "126"});
        mapp.put("60", new String[] {"piccolo", "Perfect", "01", ",", "118"});
        mapp.put("61", new String[] {"digitama", "Perfect", "01", ",tita", "107"});
        mapp.put("62", new String[] {"aegisdra", "Ultimate", "00", ",rusttyranno", "188"});
        mapp.put("63", new String[] {"tita", "Ultimate", "10", ",", "176"});
        mapp.put("64", new String[] {"zuru", "Baby I", "11", ",pagu", "0"});
        mapp.put("65", new String[] {"pagu", "Baby II", "11", ",gazi,giza,", "0"});
        mapp.put("66", new String[] {"gazi", "Child", "10", ",darktyranno,cyclo,devidra,tusk,rare", "18"});
        mapp.put("67", new String[] {"giza", "Child", "10", ",devidra,tusk,fly,delta,rare", "10"});
        mapp.put("68", new String[] {"darktyranno", "Adult", "10", ",metaltyranno", "75"});
        mapp.put("69", new String[] {"cyclo", "Adult", "10", ",nano", "70"});
        mapp.put("70", new String[] {"devidra", "Adult", "10", ",metaltyranno", "65"});
        mapp.put("71", new String[] {"tusk", "Adult", "10", ",nano", "60"});
        mapp.put("72", new String[] {"fly", "Adult", "10", ",metaltyranno", "55"});
        mapp.put("73", new String[] {"delta", "Adult", "10", ",nano", "50"});
        mapp.put("74", new String[] {"rare", "Adult", "10", ",extyranno", "40"});
        mapp.put("75", new String[] {"metaltyranno", "Perfect", "10", ",mugendra", "126"});
        mapp.put("76", new String[] {"nano", "Perfect", "10", ",", "118"});
        mapp.put("77", new String[] {"extyranno", "Perfect", "00", ",pinochi", "107"});
        mapp.put("78", new String[] {"mugendra", "Ultimate", "10", ",rusttyranno", "188"});
        mapp.put("79", new String[] {"pinochi", "Ultimate", "10", ",", "176"});
        mapp.put("80", new String[] {"saku", "Baby I", "11", ",sakutto", "0"});
        mapp.put("81", new String[] {"sakutto", "Baby II", "11", ",zuba,hack,", "0"});
        mapp.put("82", new String[] {"zuba", "Child", "00", ",zubaeager", "34"});
        mapp.put("83", new String[] {"zubaeager", "Adult", "00", ",dura", "90"});
        mapp.put("84", new String[] {"dura", "Perfect", "00", ",duranda", "155"});
        mapp.put("85", new String[] {"duranda", "Ultimate", "00", ",", "210"});
        mapp.put("86", new String[] {"hack", "Child", "01", ",baohack", "34"});
        mapp.put("87", new String[] {"baohack", "Adult", "01", ",saviorhack", "90"});
        mapp.put("88", new String[] {"saviorhack", "Perfect", "01", ",jes", "155"});
        mapp.put("89", new String[] {"jes", "Ultimate", "01", ",", "210"});
        mapp.put("90", new String[] {"petit", "Baby I", "11", ",babyd", "0"});
        mapp.put("91", new String[] {"babyd", "Baby II", "11", ",draco", "0"});
        mapp.put("92", new String[] {"draco", "Child", "01", ",_blu_coredra,_gre_coredra,", "34"});
        mapp.put("93", new String[] {"_blu_coredra", "Adult", "00", ",wingdra", "80"});
        mapp.put("94", new String[] {"wingdra", "Perfect", "00", ",slayerdra,_tai_wargrey,", "143"});
        mapp.put("95", new String[] {"slayerdra", "Ultimate", "00", ",exa", "199"});
        mapp.put("96", new String[] {"_gre_coredra", "Adult", "10", ",groundra", "80"});
        mapp.put("97", new String[] {"groundra", "Perfect", "10", ",breakdra,_yam_metalgaruru,", "143"});
        mapp.put("98", new String[] {"breakdra", "Ultimate", "10", ",exa", "199"});
        mapp.put("99", new String[] {"pitch", "Baby I", "11", ",puka", "0"});
        mapp.put("100", new String[] {"puka", "Baby II", "11", ",corona,luna,", "0"});
        mapp.put("101", new String[] {"corona", "Child", "00", ",fira", "27"});
        mapp.put("102", new String[] {"fira", "Adult", "00", ",flare", "80"});
        mapp.put("103", new String[] {"flare", "Perfect", "00", ",apollo", "135"});
        mapp.put("104", new String[] {"apollo", "Ultimate", "00", ",gracenova", "199"});
        mapp.put("105", new String[] {"luna", "Child", "01", ",lekis", "27"});
        mapp.put("106", new String[] {"lekis", "Adult", "01", ",cresce", "80"});
        mapp.put("107", new String[] {"cresce", "Perfect", "01", ",diana", "135"});
        mapp.put("108", new String[] {"diana", "Ultimate", "01", ",gracenova", "199"});
        mapp.put("109", new String[] {"_tai_agu", "Child", "00", ",_tai_grey", "25"});
        mapp.put("110", new String[] {"_tai_grey", "Adult", "00", ",_tai_metalgrey", "83"});
        mapp.put("111", new String[] {"_tai_metalgrey", "Perfect", "00", ",_tai_wargrey", "135"});
        mapp.put("112", new String[] {"_tai_wargrey", "Ultimate", "00", ",omega", "199"});
        mapp.put("113", new String[] {"_yam_gabu", "Child", "01", ",_yam_garuru", "25"});
        mapp.put("114", new String[] {"_yam_garuru", "Adult", "00", ",_yam_weregaruru", "77"});
        mapp.put("115", new String[] {"_yam_weregaruru", "Perfect", "00", ",_yam_metalgaruru", "135"});
        mapp.put("116", new String[] {"_yam_metalgaruru", "Ultimate", "01", ",omega", "199"});
        mapp.put("117", new String[] {"dodo", "Baby I", "11", ",dori", "0"});
        mapp.put("118", new String[] {"dori", "Baby II", "11", ",doru", "0"});
        mapp.put("119", new String[] {"doru", "Child", "01", ",doruga", "27"});
        mapp.put("120", new String[] {"doruga", "Adult", "01", ",dorugure", "90"});
        mapp.put("121", new String[] {"dorugure", "Perfect", "01", ",alpha", "143"});
        mapp.put("122", new String[] {"alpha", "Ultimate", "00", ",", "210"});
        mapp.put("123", new String[] {"yukimibota", "Baby I", "11", ",nyaro", "0"});
        mapp.put("124", new String[] {"nyaro", "Baby II", "11", ",plot", "0"});
        mapp.put("125", new String[] {"plot", "Child", "00", ",meicoo", "27"});
        mapp.put("126", new String[] {"meicoo", "Adult", "11", ",meicrack", "80"});
        mapp.put("127", new String[] {"meicrack", "Perfect", "00", ",rasiel", "143"});
        mapp.put("128", new String[] {"rasiel", "Ultimate", "00", ",", "210"});
        mapp.put("129", new String[] {"_als_omega", "Super Ultimate", "10", ",", "238"});
        mapp.put("130", new String[] {"rusttyranno", "Super Ultimate", "10", ",", "238"});
        mapp.put("131", new String[] {"exa", "Super Ultimate", "01", ",", "238"});
        mapp.put("132", new String[] {"gracenova", "Super Ultimate", "00", ",", "238"});
        mapp.put("133", new String[] {"omega", "Super Ultimate", "00", ",", "238"});
        mapp.put("134", new String[] {"_fdm_luce", "Perfect", "10", ",", "0"});
        mapp.put("135", new String[] {"beelzebu", "Ultimate", "10", ",", "0"});
        mapp.put("136", new String[] {"_rgm_belphe", "Ultimate", "10", ",", "0"});
        mapp.put("137", new String[] {"lilith", "Ultimate", "10", ",", "0"});
        mapp.put("138", new String[] {"de", "Ultimate", "10", ",", "0"});
        mapp.put("139", new String[] {"barba", "Ultimate", "10", ",", "0"});
        mapp.put("140", new String[] {"levia", "Ultimate", "10", ",", "0"});
        mapp.put("141", new String[] {"belialvamde", "Ultimate", "10", ",", "0"});
        mapp.put("142", new String[] {"death", "Ultimate", "10", ",", "0"});
        mapp.put("143", new String[] {"murmukus", "Ultimate", "10", ",", "0"});
        mapp.put("144", new String[] {"_plm_imperialdra", "Super Ultimate", "11", ",", "0"});
        mapp.put("145", new String[] {"ulforcevdra", "Ultimate", "00", ",", "0"});
        mapp.put("146", new String[] {"sleip", "Ultimate", "00", ",", "0"});
        mapp.put("147", new String[] {"duft", "Ultimate", "01", ",", "0"});
        mapp.put("148", new String[] {"magna", "Ultimate", "11", ",", "0"});
        mapp.put("149", new String[] {"lordknight", "Ultimate", "10", ",", "0"});
        mapp.put("150", new String[] {"cranium", "Ultimate", "00", ",", "0"});
        mapp.put("151", new String[] {"dynas", "Ultimate", "01", ",", "0"});
        mapp.put("152", new String[] {"gankoo", "Ultimate", "01", ",", "0"});
        mapp.put("153", new String[] {"duke", "Ultimate", "10", ",", "0"});
    }
    private static void dm20IndexInit() {
        map = new HashMap<String, String>();

        map.put("0", "bota");
        map.put("1", "koro");
        map.put("2", "agu");
        map.put("3", "beta");
        map.put("4", "grey");
        map.put("5", "tyranno");
        map.put("6", "devi");
        map.put("7", "mera");
        map.put("8", "airdra");
        map.put("9", "seadra");
        map.put("10", "nume");
        map.put("11", "metalgrey");
        map.put("12", "mame");
        map.put("13", "monzae");
        map.put("14", "blitzgrey");
        map.put("15", "banchomame");
        map.put("16", "puni");
        map.put("17", "tuno");
        map.put("18", "gabu");
        map.put("19", "elec");
        map.put("20", "kabuteri");
        map.put("21", "garuru");
        map.put("22", "ange");
        map.put("23", "yukidaru");
        map.put("24", "birdra");
        map.put("25", "wha");
        map.put("26", "veggie");
        map.put("27", "skullgrey");
        map.put("28", "metalmame");
        map.put("29", "vade");
        map.put("30", "skullmam");
        map.put("31", "cresgaruru");
        map.put("32", "poyo");
        map.put("33", "toko");
        map.put("34", "pata");
        map.put("35", "kune");
        map.put("36", "uni");
        map.put("37", "kentaru");
        map.put("38", "ogre");
        map.put("39", "bake");
        map.put("40", "shell");
        map.put("41", "drimoge");
        map.put("42", "suka");
        map.put("43", "andro");
        map.put("44", "giro");
        map.put("45", "ete");
        map.put("46", "hiandro");
        map.put("47", "kingete");
        map.put("48", "yura");
        map.put("49", "tane");
        map.put("50", "biyo");
        map.put("51", "pal");
        map.put("52", "monochro");
        map.put("53", "cockatri");
        map.put("54", "leo");
        map.put("55", "kuwaga");
        map.put("56", "coela");
        map.put("57", "mojya");
        map.put("58", "nani");
        map.put("59", "megadra");
        map.put("60", "piccolo");
        map.put("61", "digitama");
        map.put("62", "aegisdra");
        map.put("63", "tita");
        map.put("64", "zuru");
        map.put("65", "pagu");
        map.put("66", "gazi");
        map.put("67", "giza");
        map.put("68", "darktyranno");
        map.put("69", "cyclo");
        map.put("70", "devidra");
        map.put("71", "tusk");
        map.put("72", "fly");
        map.put("73", "delta");
        map.put("74", "rare");
        map.put("75", "metaltyranno");
        map.put("76", "nano");
        map.put("77", "extyranno");
        map.put("78", "mugendra");
        map.put("79", "pinochi");
        map.put("80", "saku");
        map.put("81", "sakutto");
        map.put("82", "zuba");
        map.put("83", "zubaeager");
        map.put("84", "dura");
        map.put("85", "duranda");
        map.put("86", "hack");
        map.put("87", "baohack");
        map.put("88", "saviorhack");
        map.put("89", "jes");
        map.put("90", "petit");
        map.put("91", "babyd");
        map.put("92", "draco");
        map.put("93", "_blu_coredra");
        map.put("94", "wingdra");
        map.put("95", "slayerdra");
        map.put("96", "_gre_coredra");
        map.put("97", "groundra");
        map.put("98", "breakdra");
        map.put("99", "pitch");
        map.put("100", "puka");
        map.put("101", "corona");
        map.put("102", "fira");
        map.put("103", "flare");
        map.put("104", "apollo");
        map.put("105", "luna");
        map.put("106", "lekis");
        map.put("107", "cresce");
        map.put("108", "diana");
        map.put("109", "_tai_agu");
        map.put("110", "_tai_grey");
        map.put("111", "_tai_metalgrey");
        map.put("112", "_tai_wargrey");
        map.put("113", "_yam_gabu");
        map.put("114", "_yam_garuru");
        map.put("115", "_yam_weregaruru");
        map.put("116", "_yam_metalgaruru");
        map.put("117", "dodo");
        map.put("118", "dori");
        map.put("119", "doru");
        map.put("120", "doruga");
        map.put("121", "dorugure");
        map.put("122", "alpha");
        map.put("123", "yukimibota");
        map.put("124", "nyaro");
        map.put("125", "plot");
        map.put("126", "meicoo");
        map.put("127", "meicrack");
        map.put("128", "rasiel");
        map.put("129", "_als_omega");
        map.put("130", "rusttyranno");
        map.put("131", "exa");
        map.put("132", "gracenova");
        map.put("133", "omega");
        map.put("134", "_fdm_luce");
        map.put("135", "beelzebu");
        map.put("136", "_rgm_belphe");
        map.put("137", "lilith");
        map.put("138", "de");
        map.put("139", "barba");
        map.put("140", "levia");
        map.put("141", "belialvamde");
        map.put("142", "death");
        map.put("143", "murmukus");
        map.put("144", "_plm_imperialdra");
        map.put("145", "ulforcevdra");
        map.put("146", "sleip");
        map.put("147", "duft");
        map.put("148", "magna");
        map.put("149", "lordknight");
        map.put("150", "cranium");
        map.put("151", "dynas");
        map.put("152", "gankoo");
        map.put("153", "duke");
    }
    private static void dm20AttributeInit() {
        map = new HashMap<String, String>();

        map.put("0", "11");
        map.put("1", "11");
        map.put("2", "00");
        map.put("3", "10");
        map.put("4", "00");
        map.put("5", "01");
        map.put("6", "10");
        map.put("7", "01");
        map.put("8", "00");
        map.put("9", "01");
        map.put("10", "10");
        map.put("11", "10");
        map.put("12", "01");
        map.put("13", "00");
        map.put("14", "10");
        map.put("15", "01");
        map.put("16", "11");
        map.put("17", "11");
        map.put("18", "01");
        map.put("19", "01");
        map.put("20", "00");
        map.put("21", "00");
        map.put("22", "00");
        map.put("23", "00");
        map.put("24", "00");
        map.put("25", "00");
        map.put("26", "10");
        map.put("27", "10");
        map.put("28", "01");
        map.put("29", "10");
        map.put("30", "00");
        map.put("31", "01");
        map.put("32", "11");
        map.put("33", "11");
        map.put("34", "01");
        map.put("35", "10");
        map.put("36", "00");
        map.put("37", "01");
        map.put("38", "10");
        map.put("39", "10");
        map.put("40", "01");
        map.put("41", "01");
        map.put("42", "10");
        map.put("43", "00");
        map.put("44", "00");
        map.put("45", "10");
        map.put("46", "00");
        map.put("47", "10");
        map.put("48", "11");
        map.put("49", "11");
        map.put("50", "00");
        map.put("51", "01");
        map.put("52", "01");
        map.put("53", "01");
        map.put("54", "00");
        map.put("55", "10");
        map.put("56", "01");
        map.put("57", "00");
        map.put("58", "10");
        map.put("59", "10");
        map.put("60", "01");
        map.put("61", "01");
        map.put("62", "00");
        map.put("63", "10");
        map.put("64", "11");
        map.put("65", "11");
        map.put("66", "10");
        map.put("67", "10");
        map.put("68", "10");
        map.put("69", "10");
        map.put("70", "10");
        map.put("71", "10");
        map.put("72", "10");
        map.put("73", "10");
        map.put("74", "10");
        map.put("75", "10");
        map.put("76", "10");
        map.put("77", "00");
        map.put("78", "10");
        map.put("79", "10");
        map.put("80", "11");
        map.put("81", "11");
        map.put("82", "00");
        map.put("83", "00");
        map.put("84", "00");
        map.put("85", "00");
        map.put("86", "01");
        map.put("87", "01");
        map.put("88", "01");
        map.put("89", "01");
        map.put("90", "11");
        map.put("91", "11");
        map.put("92", "01");
        map.put("93", "00");
        map.put("94", "00");
        map.put("95", "00");
        map.put("96", "10");
        map.put("97", "10");
        map.put("98", "10");
        map.put("99", "11");
        map.put("100", "11");
        map.put("101", "00");
        map.put("102", "00");
        map.put("103", "00");
        map.put("104", "00");
        map.put("105", "01");
        map.put("106", "01");
        map.put("107", "01");
        map.put("108", "01");
        map.put("109", "00");
        map.put("110", "00");
        map.put("111", "00");
        map.put("112", "00");
        map.put("113", "01");
        map.put("114", "00");
        map.put("115", "00");
        map.put("116", "01");
        map.put("117", "11");
        map.put("118", "11");
        map.put("119", "01");
        map.put("120", "01");
        map.put("121", "01");
        map.put("122", "00");
        map.put("123", "11");
        map.put("124", "11");
        map.put("125", "00");
        map.put("126", "11");
        map.put("127", "00");
        map.put("128", "00");
        map.put("129", "10");
        map.put("130", "10");
        map.put("131", "01");
        map.put("132", "00");
        map.put("133", "00");
        map.put("134", "10");
        map.put("135", "10");
        map.put("136", "10");
        map.put("137", "10");
        map.put("138", "10");
        map.put("139", "10");
        map.put("140", "10");
        map.put("141", "10");
        map.put("142", "10");
        map.put("143", "10");
        map.put("144", "11");
        map.put("145", "00");
        map.put("146", "00");
        map.put("147", "01");
        map.put("148", "11");
        map.put("149", "10");
        map.put("150", "00");
        map.put("151", "01");
        map.put("152", "01");
        map.put("153", "10");
    }
    public static boolean checkEvoDM20(String index, String mon) {
        dm20MapInit();
        if(mapp.containsKey(index)) return (mapp.get(index)[3].contains(","+mon) || mapp.get(index)[0].equals(mon));
        else return false;
//        dm20EvoTreeInit();
//        return map.get(a).contains(b) || a.equals(b);
    }
    private static void dm20EvoTreeInit() {
        map = new HashMap<String, String>();

        map.put("bota", "koro");
        map.put("koro", "agu,beta,_tai_agu");
        map.put("agu", "grey,tyranno,devi.mera,nume");
        map.put("beta", "devi,mera,airdra,seadra,nume");
        map.put("grey", "metalgrey");
        map.put("tyranno", "mame");
        map.put("devi", "metalgrey");
        map.put("mera", "mame");
        map.put("airdra", "metalgrey");
        map.put("seadra", "mame");
        map.put("nume", "monzae");
        map.put("metalgrey", "blitzgrey");
        map.put("mame", "banchomame");
        map.put("blitzgrey", "_als_omega");
        map.put("puni", "tuno");
        map.put("tuno", "gabu,elec,_yam_gabu");
        map.put("gabu", "kabuteri,garuru,ange,yukidaru,veggie");
        map.put("elec", "ange,yukidaru,birdra,wha,veggie");
        map.put("kabuteri", "skullgrey");
        map.put("garuru", "metalmame");
        map.put("ange", "skullgrey");
        map.put("yukidaru", "metalmame");
        map.put("birdra", "skullgrey");
        map.put("wha", "metalmame");
        map.put("veggie", "vade");
        map.put("skullgrey", "skullmam");
        map.put("metalmame", "cresgaruru");
        map.put("cresgaruru", "_als_omega");
        map.put("poyo", "toko");
        map.put("toko", "pata,kune");
        map.put("pata", "uni,kentaru,ogre,bake,suka");
        map.put("kune", "ogre,bake,shell,drimoge,suka");
        map.put("uni", "andro");
        map.put("kentaru", "giro");
        map.put("ogre", "andro");
        map.put("bake", "giro");
        map.put("shell", "andro");
        map.put("drimoge", "giro");
        map.put("suka", "ete");
        map.put("andro", "hiandro");
        map.put("ete", "kingete");
        map.put("yura", "tane");
        map.put("tane", "biyo,pal");
        map.put("biyo", "monochro,cockatri,leo,kuwaga,nani");
        map.put("pal", "leo,kuwaga,coela,mojya,nani");
        map.put("monochro", "megadra");
        map.put("cockatri", "piccolo");
        map.put("leo", "megadra");
        map.put("kuwaga", "piccolo");
        map.put("coela", "megadra");
        map.put("mojya", "piccolo");
        map.put("nani", "digitama");
        map.put("megadra", "aegisdra");
        map.put("digitama", "tita");
        map.put("aegisdra", "rusttyranno");
        map.put("zuru", "pagu");
        map.put("pagu", "gazi,giza");
        map.put("gazi", "darktyranno,cyclo,devidra,tusk,rare");
        map.put("giza", "devidra,tusk,fly,delta,rare");
        map.put("darktyranno", "metaltyranno");
        map.put("cyclo", "nano");
        map.put("devidra", "metaltyranno");
        map.put("tusk", "nano");
        map.put("fly", "metaltyranno");
        map.put("delta", "nano");
        map.put("rare", "extyranno");
        map.put("metaltyranno", "mugendra");
        map.put("extyranno", "pinochi");
        map.put("mugendra", "rusttyranno");
        map.put("saku", "sakutto");
        map.put("sakutto", "zuba,hack");
        map.put("zuba", "zubaeager");
        map.put("zubaeager", "dura");
        map.put("dura", "duranda");
        map.put("hack", "baohack");
        map.put("baohack", "saviorhack");
        map.put("saviorhack", "jes");
        map.put("petit", "babyd");
        map.put("babyd", "draco");
        map.put("draco", "_blu_coredra,_gre_coredra");
        map.put("_blu_coredra", "wingdra");
        map.put("wingdra", "slayerdra,_tai_wargrey");
        map.put("slayerdra", "exa");
        map.put("_gre_coredra", "groundra");
        map.put("groundra", "breakdra,_yam_metalgaruru");
        map.put("breakdra", "exa");
        map.put("pitch", "puka");
        map.put("puka", "corona,luna");
        map.put("corona", "fira");
        map.put("fira", "flare");
        map.put("flare", "apollo");
        map.put("apollo", "gracenova");
        map.put("luna", "lekis");
        map.put("lekis", "cresce");
        map.put("cresce", "diana");
        map.put("diana", "gracenova");
        map.put("_tai_agu", "_tai_grey");
        map.put("_tai_grey", "_tai_metalgrey");
        map.put("_tai_metalgrey", "_tai_wargrey");
        map.put("_tai_wargrey", "omega");
        map.put("_yam_gabu", "_yam_garuru");
        map.put("_yam_garuru", "_yam_weregaruru");
        map.put("_yam_weregaruru", "_yam_metalgaruru");
        map.put("_yam_metalgaruru", "omega");
        map.put("dodo", "dori");
        map.put("dori", "doru");
        map.put("doru", "doruga");
        map.put("doruga", "dorugure");
        map.put("dorugure", "alpha");
        map.put("yukimibota", "nyaro");
        map.put("nyaro", "plot");
        map.put("plot", "meicoo");
        map.put("meicoo", "meicrack");
        map.put("meicrack", "rasiel");
    }


    // ========== DMOG ==========
    public static String getDmogIndex(String rom) {
        return (rom.substring(3, 4)) + (rom.substring(7, 8));
    }
    public static String getDmogMonFromRom(String rom) {
        map = new HashMap<String, String>();
        dmogIndexInit();
        String index = (rom.substring(3, 4)) + (rom.substring(7, 8));

        if(map.containsKey(index)) return map.get(index);
        else return "err";
    }
    public static String getDmogMonFromIndex(String index) {
        map = new HashMap<String, String>();
        dmogIndexInit();

        if(map.containsKey(index)) return map.get(index);
        else return "err";
    }
    public static String getDmogLevel(String index) {
        // return 2 bits
        dmogMapInit();

        if(mapp.containsKey(String.valueOf(index))) return mapp.get(String.valueOf(index))[1];
        else return "err";
    }
    public static String getDmogAttribute(String index) {
        // return 2 bits
        dmogMapInit();

        if(mapp.containsKey(String.valueOf(index))) return mapp.get(String.valueOf(index))[2];
        else return "err";
    }
    public static boolean getDmogWinner(String rom){
        Log.e("WINNER", rom.substring(rom.length()-1));
        if(rom.substring(rom.length()-1).equals("1")){
            return true;
        }else {
            return false;
        }
    }
    public static String getDmogEffort(String rom) {
        return rom.substring(2,3);
    }

    private static void dmogIndexInit() {
        map = new HashMap<String, String>();

        map.put("30", "agu");
        map.put("40", "beta");
        map.put("50", "grey");
        map.put("60", "tyranno");
        map.put("70", "devi");
        map.put("80", "mera");
        map.put("90", "airdra");
        map.put("A0", "seadra");
        map.put("B0", "nume");
        map.put("C0", "metalgrey");
        map.put("D0", "mame");
        map.put("E0", "monzae");
        map.put("31", "gabu");
        map.put("41", "elec");
        map.put("51", "kabuteri");
        map.put("61", "garuru");
        map.put("71", "ange");
        map.put("81", "yukidaru");
        map.put("91", "birdra");
        map.put("A1", "wha");
        map.put("B1", "veggie");
        map.put("C1", "skullgrey");
        map.put("D1", "metalmame");
        map.put("E1", "vade");
        map.put("32", "pata");
        map.put("42", "kune");
        map.put("52", "uni");
        map.put("62", "kentaru");
        map.put("72", "ogre");
        map.put("82", "bake");
        map.put("92", "shell");
        map.put("A2", "drimoge");
        map.put("B2", "suka");
        map.put("C2", "andro");
        map.put("D2", "giro");
        map.put("E2", "ete");
        map.put("33", "biyo");
        map.put("43", "pal");
        map.put("53", "monochro");
        map.put("63", "cockatri");
        map.put("73", "leo");
        map.put("83", "kuwaga");
        map.put("93", "coela");
        map.put("A3", "mojya");
        map.put("B3", "nani");
        map.put("C3", "megadra");
        map.put("D3", "piccolo");
        map.put("E3", "digitama");
        map.put("34", "gazi");
        map.put("44", "giza");
        map.put("54", "darktyranno");
        map.put("64", "cyclo");
        map.put("74", "devidra");
        map.put("84", "tusk");
        map.put("94", "fly");
        map.put("A4", "delta");
        map.put("B4", "rare");
        map.put("C4", "metaltyranno");
        map.put("D4", "nano");
        map.put("E4", "extyranno");
        //v6 mons, Monochromon, Tortamon, Gekomon, and ShoguneGekomon unconfirmed
        //Version in serial is 6 (7)
        map.put("36", "tento");
        map.put("46", "otama");
        map.put("56", "kabuteri");
        map.put("66", "star");
        map.put("76", "torto"); //haven't confirmed slot yet
        map.put("86", "kuwaga");
        map.put("96", "monochro");
        map.put("A6", "shellnume");
        map.put("B6", "geko"); //haven't confirmed slot yet
        map.put("C6", "megakabuteri");
        map.put("D6", "tekka");
        map.put("E6", "shougungeko");
    }
    private static void dmogMapInit() {
        mapp = new HashMap<>();

        mapp.put("30", new String[] {"agu", "Child", "Vaccine", ",grey,tyranno,devi,mera,nume"});
        mapp.put("40", new String[] {"beta", "Child", "Virus", ",devi,mera,airdra,seadra,nume"});
        mapp.put("50", new String[] {"grey", "Adult", "Vaccine", ",metalgrey"});
        mapp.put("60", new String[] {"tyranno", "Adult", "Data", ",mame"});
        mapp.put("70", new String[] {"devi", "Adult", "Virus", ",metalgrey"});
        mapp.put("80", new String[] {"mera", "Adult", "Data", ",mame"});
        mapp.put("90", new String[] {"airdra", "Adult", "Vaccine", ",metalgrey"});
        mapp.put("A0", new String[] {"seadra", "Adult", "Data", ",mame"});
        mapp.put("B0", new String[] {"nume", "Adult", "Virus", ",monzae"});
        mapp.put("C0", new String[] {"metalgrey", "Perfect", "Virus", ""});
        mapp.put("D0", new String[] {"mame", "Perfect", "Data", ""});
        mapp.put("E0", new String[] {"monzae", "Perfect", "Vaccine", ""});
        mapp.put("31", new String[] {"gabu", "Child", "Vaccine", ",kabuteri,garuru,ange,yukidaru,veggie"});
        mapp.put("41", new String[] {"elec", "Child", "Virus", ",ange,yukidaru,birdra,wha,veggie"});
        mapp.put("51", new String[] {"kabuteri", "Adult", "Vaccine", ",skullgrey,megakabuteri"});
        mapp.put("61", new String[] {"garuru", "Adult", "Data", ",metalmame"});
        mapp.put("71", new String[] {"ange", "Adult", "Virus", ",skullgrey"});
        mapp.put("81", new String[] {"yukidaru", "Adult", "Data", ",metalmame"});
        mapp.put("91", new String[] {"birdra", "Adult", "Vaccine", ",skullgrey"});
        mapp.put("A1", new String[] {"wha", "Adult", "Data", ",metalmame"});
        mapp.put("B1", new String[] {"veggie", "Adult", "Virus", ",vade"});
        mapp.put("C1", new String[] {"skullgrey", "Perfect", "Virus", ""});
        mapp.put("D1", new String[] {"metalmame", "Perfect", "Data", ""});
        mapp.put("E1", new String[] {"vade", "Perfect", "Vaccine", ""});
        mapp.put("32", new String[] {"pata", "Child", "Vaccine", ",uni,kentaru,ogre,bake,suka"});
        mapp.put("42", new String[] {"kune", "Child", "Virus", ",ogre,bake,shell,drimoge,suka"});
        mapp.put("52", new String[] {"uni", "Adult", "Vaccine", ",andro"});
        mapp.put("62", new String[] {"kentaru", "Adult", "Data", ",giro"});
        mapp.put("72", new String[] {"ogre", "Adult", "Virus", ",andro"});
        mapp.put("82", new String[] {"bake", "Adult", "Data", ",giro"});
        mapp.put("92", new String[] {"shell", "Adult", "Vaccine", ",andro"});
        mapp.put("A2", new String[] {"drimoge", "Adult", "Data", ",giro"});
        mapp.put("B2", new String[] {"suka", "Adult", "Virus", ",ete"});
        mapp.put("C2", new String[] {"andro", "Perfect", "Virus", ""});
        mapp.put("D2", new String[] {"giro", "Perfect", "Data", ""});
        mapp.put("E2", new String[] {"ete", "Perfect", "Vaccine", ""});
        mapp.put("33", new String[] {"biyo", "Child", "Vaccine", ",monochro,cockatri,leo,kuwaga,nani"});
        mapp.put("43", new String[] {"pal", "Child", "Virus", ",leo,kuwaga,coela,mojya,nani"});
        mapp.put("53", new String[] {"monochro", "Adult", "Vaccine", ",megadra,megakabuteri"});
        mapp.put("63", new String[] {"cockatri", "Adult", "Data", ",piccolo"});
        mapp.put("73", new String[] {"leo", "Adult", "Virus", ",megadra"});
        mapp.put("83", new String[] {"kuwaga", "Adult", "Data", ",piccolo,tekka"});
        mapp.put("93", new String[] {"coela", "Adult", "Vaccine", ",megadra"});
        mapp.put("A3", new String[] {"mojya", "Adult", "Data", ",piccolo"});
        mapp.put("B3", new String[] {"nani", "Adult", "Virus", ",digitama"});
        mapp.put("C3", new String[] {"megadra", "Perfect", "Virus", ""});
        mapp.put("D3", new String[] {"piccolo", "Perfect", "Data", ""});
        mapp.put("E3", new String[] {"digitama", "Perfect", "Vaccine", ""});
        mapp.put("34", new String[] {"gazi", "Child", "Vaccine", ",darktyranno,cyclo,devidra,tusk,rare"});
        mapp.put("44", new String[] {"giza", "Child", "Virus", ",devidra,tusk,fly,delta,rare"});
        mapp.put("54", new String[] {"darktyranno", "Adult", "Vaccine", ",metaltyranno"});
        mapp.put("64", new String[] {"cyclo", "Adult", "Data", ",nano"});
        mapp.put("74", new String[] {"devidra", "Adult", "Virus", ",metaltyranno"});
        mapp.put("84", new String[] {"tusk", "Adult", "Data", ",nano"});
        mapp.put("94", new String[] {"fly", "Adult", "Vaccine", ",metaltyranno"});
        mapp.put("A4", new String[] {"delta", "Adult", "Data", ",nano"});
        mapp.put("B4", new String[] {"rare", "Adult", "Virus", ",extyranno"});
        mapp.put("C4", new String[] {"metaltyranno", "Perfect", "Virus", ""});
        mapp.put("D4", new String[] {"nano", "Perfect", "Data", ""});
        mapp.put("E4", new String[] {"extyranno", "Perfect", "Vaccine", ""});
        mapp.put("36", new String[] {"tento", "Child", "Vaccine", ",kabuteri,star,torto,kuwaga,geko"});
        mapp.put("46", new String[] {"otama", "Child", "Virus", ",torto,kuwaga,monochro,shellnume,geko"});
        mapp.put("56", new String[] {"kabuteri", "Adult", "Vaccine", ",tekka"});
        mapp.put("66", new String[] {"star", "Adult", "Data", ",megakabuteri"});
        mapp.put("76", new String[] {"torto", "Adult", "Vaccine", ",tekka"});
        mapp.put("86", new String[] {"kuwaga", "Adult", "Virus", ",shogungeko"});
        mapp.put("96", new String[] {"monochro", "Adult", "Data", ",megakabuteri"});
        mapp.put("A6", new String[] {"shellnume", "Adult", "Virus", ",tekka"});
        mapp.put("B6", new String[] {"geko", "Adult", "Virus", ",shougungeko"});
        mapp.put("C6", new String[] {"megakabuteri", "Perfect", "Data", ""});
        mapp.put("D6", new String[] {"tekka", "Perfect", "Virus", ""});
        mapp.put("E6", new String[] {"shougungeko", "Perfect", "Virus", ""});
    }
    public static boolean checkEvoDmog(String index, String mon) {
        dmogMapInit();
        if(mapp.containsKey(index)) return (mapp.get(index)[3].contains(","+mon) || mapp.get(index)[0].equals(mon));
        else return false;
    }
    private static void dmogEvoTreeInit() {
        map = new HashMap<String, String>();

        map.put("bota", "koro");
        map.put("koro", "agu,beta");
        map.put("agu", "grey,tyranno,devi.mera,nume");
        map.put("beta", "devi,mera,airdra,seadra,nume");
        map.put("grey", "metalgrey");
        map.put("tyranno", "mame");
        map.put("devi", "metalgrey");
        map.put("mera", "mame");
        map.put("airdra", "metalgrey");
        map.put("seadra", "mame");
        map.put("nume", "monzae");
        map.put("puni", "tuno");
        map.put("tuno", "gabu,elec");
        map.put("gabu", "kabuteri,garuru,ange,yukidaru,veggie");
        map.put("elec", "ange,yukidaru,birdra,wha,veggie");
        map.put("kabuteri", "skullgrey,megakabuteri");
        map.put("garuru", "metalmame");
        map.put("ange", "skullgrey");
        map.put("yukidaru", "metalmame");
        map.put("birdra", "skullgrey");
        map.put("wha", "metalmame");
        map.put("veggie", "vade");
        map.put("poyo", "toko");
        map.put("toko", "pata,kune");
        map.put("pata", "uni,kentaru,ogre,bake,suka");
        map.put("kune", "ogre,bake,shell,drimoge,suka");
        map.put("uni", "andro");
        map.put("kentaru", "giro");
        map.put("ogre", "andro");
        map.put("bake", "giro");
        map.put("shell", "andro");
        map.put("drimoge", "giro");
        map.put("suka", "ete");
        map.put("yura", "tane");
        map.put("tane", "biyo,pal");
        map.put("biyo", "monochro,cockatri,leo,kuwaga,nani");
        map.put("pal", "leo,kuwaga,coela,mojya,nani");
        map.put("monochro", "megadra, megakabuteri");
        map.put("cockatri", "piccolo");
        map.put("leo", "megadra");
        map.put("kuwaga", "piccolo, tekka");
        map.put("coela", "megadra");
        map.put("mojya", "piccolo");
        map.put("nani", "digitama");
        map.put("zuru", "pagu");
        map.put("pagu", "gazi,giza");
        map.put("gazi", "darktyranno,cyclo,devidra,tusk,rare");
        map.put("giza", "devidra,tusk,fly,delta,rare");
        map.put("darktyranno", "metaltyranno");
        map.put("cyclo", "nano");
        map.put("devidra", "metaltyranno");
        map.put("tusk", "nano");
        map.put("fly", "metaltyranno");
        map.put("delta", "nano");
        map.put("rare", "extyranno");
        //v6
        map.put("pabu", "moti");
        map.put("moti", "tento,otama");
        map.put("tento", "kabuteri,star,torto,kuwaga,geko");
        map.put("otama", "torto,kuwaga,monochro,shellnume,geko");
        map.put("star", "tekka");
        map.put("torto", "megakabuteri");
        map.put("shellnume", "tekka");
        map.put("geko", "shogungeko");
    }
    public static String generateDmogRomV2(int index, int effort) {
        char[] rom = new String("FC03-FD02").toCharArray();
        String in = leadingZeros(Integer.toHexString(index), 2),
               ef = Integer.toHexString(effort),
               slot = in.substring(0,1),
               ver = in.substring(1),
               invSlot = leadingZeros(Integer.toBinaryString(Integer.parseInt(slot, 16)), 4),
               invVer = leadingZeros(Integer.toBinaryString(Integer.parseInt(ver, 16)), 4),
               invEf = leadingZeros(Integer.toBinaryString(Integer.parseInt(ef, 16)), 4);

        invSlot = invSlot.replaceAll("0", "A");
        invSlot = invSlot.replaceAll("1", "0");
        invSlot = invSlot.replaceAll("A", "1");
        invSlot = Integer.toHexString(Integer.parseInt(invSlot, 2));

        invVer = invVer.replaceAll("0", "A");
        invVer = invVer.replaceAll("1", "0");
        invVer = invVer.replaceAll("A", "1");
        invVer = Integer.toHexString(Integer.parseInt(invVer, 2));

        invEf = invEf.replaceAll("0", "A");
        invEf = invEf.replaceAll("1", "0");
        invEf = invEf.replaceAll("A", "1");
        invEf = Integer.toHexString(Integer.parseInt(invEf, 2));

        rom[0] = invEf.toCharArray()[0];
        rom[1] = invSlot.toCharArray()[0];
        rom[2] = ef.toCharArray()[0];
        rom[3] = slot.toCharArray()[0];

        rom[5] = invVer.toCharArray()[0];
        rom[6] = 'E';
        rom[7] = ver.toCharArray()[0];
        rom[8] = '1';

        return new String(rom).toUpperCase();
    }
    public static String generateDmogForcedV1(int index, boolean win) {
        char[] rom = new String("FC03-FD02").toCharArray();
        String in = leadingZeros(Integer.toHexString(index), 2),
                slot = in.substring(0,1),
                ver = in.substring(1),
                invSlot = leadingZeros(Integer.toBinaryString(Integer.parseInt(slot, 16)), 4),
                invVer = leadingZeros(Integer.toBinaryString(Integer.parseInt(ver, 16)), 4);
        char wi, invWi;

        if(win) {
            wi = '2';
            invWi = 'D';
        }
        else {
            wi = '1';
            invWi = 'E';
        }

        invSlot = invSlot.replaceAll("0", "A");
        invSlot = invSlot.replaceAll("1", "0");
        invSlot = invSlot.replaceAll("A", "1");
        invSlot = Integer.toHexString(Integer.parseInt(invSlot, 2));

        invVer = invVer.replaceAll("0", "A");
        invVer = invVer.replaceAll("1", "0");
        invVer = invVer.replaceAll("A", "1");
        invVer = Integer.toHexString(Integer.parseInt(invVer, 2));

        rom[0] = 'F';
        rom[1] = invSlot.toCharArray()[0];
        rom[2] = '0';
        rom[3] = slot.toCharArray()[0];

        rom[5] = invVer.toCharArray()[0];
        rom[6] = invWi;
        rom[7] = ver.toCharArray()[0];
        rom[8] = wi;

        return "V1-" + new String(rom).toUpperCase();
    }


    // ========== PEN20 ==========
    public static String generatePen20RomV2FromBase(String digiROM, int shakes) {
        char[] rom = digiROM.toCharArray();

        int ff = Integer.parseInt(""+rom[0], 16);
        rom[0] = Integer.toHexString(Integer.parseInt("0"+Integer.toBinaryString(ff).substring(1), 2)).toCharArray()[0];

        // Shakes
        shakes = (shakes>15) ? 15:shakes;
        int attack = getPen20Pattern(getPen20MonFromIndexFull(Integer.parseInt(getPen20Index(digiROM))), shakes);
        char[]  shBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[0]+rom[1], 16) ), 8).toCharArray(),
                shArr = leadingZeros(Integer.toBinaryString(attack), 5).toCharArray(),
                shHex;

        for(int i = 0; i<5; i++) shBits[i+1] = shArr[i];
        shHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(shBits), 2)), 2 ).toCharArray();

        rom[0] = shHex[0];
        rom[1] = shHex[1];

        return new String(rom).toUpperCase();
    }
    public static String generatePen20ForcedV1FromBase(String digiROM, boolean win) {
        char[] rom = new String(digiROM.substring(0, digiROM.length()-5)+"-@CFFE").toCharArray();

        int ff = Integer.parseInt(""+rom[0], 16);
        rom[0] = Integer.toHexString(Integer.parseInt("1"+Integer.toBinaryString(ff).substring(1), 2)).toCharArray()[0];

        int shakes, power;
        String hp, index = getPen20Index(digiROM);
        if(win) {
            shakes = 0;
            power = 0;
            hp = "F0";
        }
        else {
            shakes = 15;
            power = 255;
            hp = "0F";
        }

        // Shakes
        int attack = getPen20Pattern(getPen20MonFromIndexFull(Integer.parseInt(index)), shakes);
        char[]  shBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[0]+rom[1], 16) ), 8).toCharArray(),
                shArr = leadingZeros(Integer.toBinaryString(attack), 5).toCharArray(),
                shHex;

        for(int i = 0; i<5; i++) shBits[i+1] = shArr[i];
        shHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(shBits), 2)), 2 ).toCharArray();

        // Power
        char[] poHex = leadingZeros(Integer.toHexString(power), 2).toCharArray();

        char[] hpHex = hp.toCharArray();

        rom[0] = shHex[0];
        rom[1] = shHex[1];

        rom[21] = poHex[0];
        rom[22] = poHex[1];

        // HP
        rom[47] = hpHex[0];
        rom[48] = hpHex[1];

        return "V1-" + new String(rom).toUpperCase();
    }

    public static String generatePen20RomV2(String index, int shakes, int power, int hitpattern) {
        char[] rom = new String("3C7E-2EDE-02FE-042E-226E-000E-000E-000E-000E-CFFE").toCharArray();

        //Pen20 does not use name

        // Shakes
        char[]  prBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[1]+rom[2], 16) ), 8).toCharArray(),
                prArr = leadingZeros(Integer.toBinaryString(shakes), 5).toCharArray(),
                prHex;

        for(int i = 0; i<5; i++) prBits[i+1] = prArr[i];
        prHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(prBits), 2)), 2 ).toCharArray();

        // Index/Attribute
        char[]  inAttrBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[5]+rom[6]+rom[7], 16) ), 12).toCharArray(),
                inBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(index)), 10).toCharArray(),
                attrBits = getPen20Attribute(index).toCharArray(),
                inAttrHex;

        for(int i = 0; i<10; i++) inAttrBits[i] = inBits[i];
        for(int i = 0; i<2; i++) inAttrBits[i+10] = attrBits[i];
        inAttrHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(inAttrBits), 2)), 3 ).toCharArray();

        // Power
        char[] poHex = leadingZeros(Integer.toHexString(power), 2).toCharArray();

        // HitPattern
        char[] hitHex = leadingZeros(Integer.toHexString(hitpattern), 2).toCharArray();

        rom[1] = prHex[0];
        rom[2] = prHex[1];

        rom[5] = inAttrHex[0];
        rom[6] = inAttrHex[1];
        rom[7] = inAttrHex[2];

        rom[21] = poHex[0];
        rom[22] = poHex[1];

        rom[47] = hitHex[0];
        rom[48] = hitHex[1];

        return new String(rom).toUpperCase();
    }
    public static String generatePen20RomV2(String index, int shakes, int power) {
        char[] rom = new String("3C7E-2EDE-02FE-042E-226E-000E-000E-000E-000E-CFFE").toCharArray();

        shakes = (shakes>15) ? 15:shakes;

        // Shakes
        int attack = getPen20Pattern(getPen20MonFromIndexFull(Integer.parseInt(index)), shakes);
        char[]  shBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[0]+rom[1], 16) ), 8).toCharArray(),
                shArr = leadingZeros(Integer.toBinaryString(attack), 5).toCharArray(),
                shHex;


        for(int i = 0; i<5; i++) shBits[i+1] = shArr[i];
        shHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(shBits), 2)), 2 ).toCharArray();

        // Index/Attribute
        char[]  inAttrBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[5]+rom[6]+rom[7], 16) ), 12).toCharArray(),
                inBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(index)), 10).toCharArray(),
                attrBits = getPen20Attribute(index).toCharArray(),
                inAttrHex;

        for(int i = 0; i<10; i++) inAttrBits[i] = inBits[i];
        for(int i = 0; i<2; i++) inAttrBits[i+10] = attrBits[i];
        inAttrHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(inAttrBits), 2)), 3 ).toCharArray();

        // Power
        char[] poHex = leadingZeros(Integer.toHexString(power), 2).toCharArray();


        rom[0] = shHex[0];
        rom[1] = shHex[1];

        rom[5] = inAttrHex[0];
        rom[6] = inAttrHex[1];
        rom[7] = inAttrHex[2];

        rom[21] = poHex[0];
        rom[22] = poHex[1];

        return new String(rom).toUpperCase();
    }
    public static String generatePen20ForcedV1(String index, boolean win) {
        char[] rom = new String("3C7E-2EDE-02FE-042E-226E-000E-000E-000E-000E-@CFFE").toCharArray();

        // Pen20 does not have a name

        // String[] mon = getPen

        int shakes, power;
        String hp;
        if(win) {
            shakes = 0;
            power = 0;
            hp = "F0";
        } else {
            shakes = 15;
            power = 255;
            hp = "0F";
        }

        // Shakes
        int attack = getPen20Pattern(getPen20MonFromIndexFull(Integer.parseInt(index)), shakes);
        char[]  shBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[0]+rom[1], 16) ), 8).toCharArray(),
                shArr = leadingZeros(Integer.toBinaryString(attack), 5).toCharArray(),
                shHex;


        for(int i = 0; i<5; i++) shBits[i+1] = shArr[i];
        shHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(shBits), 2)), 2 ).toCharArray();



        // Index/Attribute
        char[]  inAttrBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[5]+rom[6]+rom[7], 16) ), 12).toCharArray(),
                inBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(index)), 10).toCharArray(),
                attrBits = getPen20Attribute(index).toCharArray(),
                inAttrHex;

        for(int i = 0; i<10; i++) inAttrBits[i] = inBits[i];
        for(int i = 0; i<2; i++) inAttrBits[i+10] = attrBits[i];
        inAttrHex = leadingZeros( Integer.toHexString(Integer.parseInt( new String(inAttrBits), 2)), 3 ).toCharArray();

        // Power
        char[] poHex = leadingZeros(Integer.toHexString(power), 2).toCharArray();

        char[] hpHex = hp.toCharArray();

        rom[0] = shHex[0];
        rom[1] = shHex[1];

        rom[5] = inAttrHex[0];
        rom[6] = inAttrHex[1];
        rom[7] = inAttrHex[2];

        rom[21] = poHex[0];
        rom[22] = poHex[1];

        // HP
        rom[47] = hpHex[0];
        rom[48] = hpHex[1];

        return "V1-" + new String(rom).toUpperCase();
    }

    public static String getPen20Attribute(String index) {
        // return 2 bits
        Pen20AttributeInit();

        if (map.containsKey(String.valueOf(index))) return map.get(String.valueOf(index));
        else return "err";
    }
    public static Integer getPen20ScorePattern(Integer index) {
        initPen20AttackMap();

        if (pen20AtkMap.containsKey(index)) return pen20AtkMap.get(Integer.valueOf(index));
        else return null;
    }
    private static void initPen20AttackMap() {
        pen20AtkMap = new HashMap<Integer, Integer>();
        //attack, score
        pen20AtkMap.put(0, 0);
        pen20AtkMap.put(2, 1); //skips attack of 1 for some reason?
        pen20AtkMap.put(3, 2);
        pen20AtkMap.put(4, 3);
        pen20AtkMap.put(5, 4);
        pen20AtkMap.put(6, 5);
        pen20AtkMap.put(7, 6);
        pen20AtkMap.put(8, 7);
        pen20AtkMap.put(9, 8);
        pen20AtkMap.put(10, 9);
        pen20AtkMap.put(11, 10);
        pen20AtkMap.put(12, 11);
        pen20AtkMap.put(13, 12);
        pen20AtkMap.put(14, 13);
        pen20AtkMap.put(15, 14);
    }
    private static void Pen20AttributeInit() {
        map = new HashMap<String, String>();

        map.put("0","11" );
        map.put("1","11" );
        map.put("2","11" );
        map.put("3","00" );
        map.put("4","01" );
        map.put("5","10" );
        map.put("6","00" );
        map.put("7","00" );
        map.put("8","01" );
        map.put("9","01" );
        map.put("10","10" );
        map.put("11","10" );
        map.put("12","00" );
        map.put("13","00" );
        map.put("14","00" );
        map.put("15","01" );
        map.put("16","01" );
        map.put("17","10" );
        map.put("18","10" );
        map.put("19","00" );
        map.put("20","00" );
        map.put("21","01" );
        map.put("22","00" );
        map.put("23","10" );
        map.put("24","11" );
        map.put("25","11" );
        map.put("26","00" );
        map.put("27","01" );
        map.put("28","10" );
        map.put("29","00" );
        map.put("30","00" );
        map.put("31","01" );
        map.put("32","01" );
        map.put("33","10" );
        map.put("34","10" );
        map.put("35","01" );
        map.put("36","00" );
        map.put("37","00" );
        map.put("38","01" );
        map.put("39","01" );
        map.put("40","10" );
        map.put("41","10" );
        map.put("42","01" );
        map.put("43","00" );
        map.put("44","01" );
        map.put("45","10" );
        map.put("46","01" );
        map.put("47","11" );
        map.put("48","11" );
        map.put("49","00" );
        map.put("50","01" );
        map.put("51","10" );
        map.put("52","00" );
        map.put("53","00" );
        map.put("54","01" );
        map.put("55","01" );
        map.put("56","10" );
        map.put("57","10" );
        map.put("58","10" );
        map.put("59","00" );
        map.put("60","00" );
        map.put("61","01" );
        map.put("62","01" );
        map.put("63","10" );
        map.put("64","10" );
        map.put("65","10" );
        map.put("66","00" );
        map.put("67","01" );
        map.put("68","10" );
        map.put("69","10" );
        map.put("70","11" );
        map.put("71","11" );
        map.put("72","00" );
        map.put("73","01" );
        map.put("74","10" );
        map.put("75","01" );
        map.put("76","00" );
        map.put("77","00" );
        map.put("78","01" );
        map.put("79","01" );
        map.put("80","10" );
        map.put("81","10" );
        map.put("82","00" );
        map.put("83","00" );
        map.put("84","01" );
        map.put("85","01" );
        map.put("86","10" );
        map.put("87","10" );
        map.put("88","01" );
        map.put("89","00" );
        map.put("90","01" );
        map.put("91","10" );
        map.put("92","01" );
        map.put("93","11" );
        map.put("94","11" );
        map.put("95","00" );
        map.put("96","01" );
        map.put("97","10" );
        map.put("98","00" );
        map.put("99","00" );
        map.put("100","01" );
        map.put("101","01" );
        map.put("102","10" );
        map.put("103","10" );
        map.put("104","01" );
        map.put("105","00" );
        map.put("106","00" );
        map.put("107","01" );
        map.put("108","01" );
        map.put("109","10" );
        map.put("110","10" );
        map.put("111","00" );
        map.put("112","00" );
        map.put("113","01" );
        map.put("114","10" );
        map.put("115","10" );
        map.put("116","11" );
        map.put("117","11" );
        map.put("118","00" );
        map.put("119","01" );
        map.put("120","00" );
        map.put("121","00" );
        map.put("122","01" );
        map.put("123","00" );
        map.put("124","00" );
        map.put("125","01" );
        map.put("126","00" );
        map.put("127","00" );
        map.put("128","00" );
        map.put("129","10" );
        map.put("130","10" );
        map.put("131","10" );
        map.put("132","11" );
        map.put("133","11" );
        map.put("134","11" );
        map.put("135","10" );
        map.put("136","11" );
        map.put("137","11" );
        map.put("138","10" );
        map.put("139","10" );
        map.put("140","11" );
        map.put("141","11" );
        map.put("142","10" );
        map.put("143","00" );
        map.put("144","10" );
        map.put("145","00" );
        map.put("146","11" );
        map.put("147","11" );
        map.put("148","10" );
        map.put("149","11" );
        map.put("150","00" );
        map.put("151","10" );
        map.put("152","01" );
        map.put("153","11" );
        map.put("154","11" );
        map.put("155","00" );
        map.put("156","01" );
        map.put("157","00" );
        map.put("158","01" );
        map.put("159","10" );
        map.put("160","00" );
        map.put("161","01" );
        map.put("162","10" );
        map.put("163","00" );
        map.put("164","01" );
        map.put("165","01" );
        map.put("166","00" );
        map.put("167","11" );
        map.put("168","11" );
        map.put("169","01" );
        map.put("170","00" );
        map.put("171","00" );
        map.put("172","10" );
        map.put("173","10" );
        map.put("174","00" );
        map.put("175","10" );
        map.put("176","00" );
        map.put("177","10" );
        map.put("178","10" );
        map.put("179","11" );
        map.put("180","11" );
        map.put("181","00" );
        map.put("182","00" );
        map.put("183","00" );
        map.put("184","00" );
        map.put("185","11" );
        map.put("186","11" );
        map.put("187","01" );
        map.put("188","01" );
        map.put("189","10" );
        map.put("190","00" );
        map.put("191","11" );
        map.put("192","11" );
        map.put("193","01" );
        map.put("194","01" );
        map.put("195","01" );
        map.put("196","01" );
        map.put("197","10" );
        map.put("198","10" );
        map.put("199","10" );
        map.put("200","10" );
        map.put("201","11" );
        map.put("202","11" );
        map.put("203","11" );
        map.put("204","11" );
        map.put("205","00" );
        map.put("206","11" );
        map.put("207","11" );
        map.put("208","11" );
        map.put("209","11" );
        map.put("210","11" );
        map.put("211","00" );
        map.put("212","00" );
        map.put("213","00" );
        map.put("214","00" );
        map.put("215","11" );
        map.put("216","01" );
        map.put("217","01" );
        map.put("218","01" );
        map.put("219","01" );
        map.put("220","11" );
        map.put("221","11" );
        map.put("222","01" );
        map.put("223","01" );
        map.put("224","01" );
        map.put("225","01" );
        map.put("226","00" );
        map.put("227","00" );
        map.put("228","10" );
        map.put("229","11" );
        map.put("230","11" );
        map.put("231","11" );
        map.put("232","10" );
        map.put("233","10" );
        map.put("234","10" );
        map.put("235","00" );
        map.put("236","01" );
        map.put("237","00" );
        map.put("238","00" );
        map.put("239","01" );
        map.put("240","11" );
        map.put("241","11" );
        map.put("242","01" );
        map.put("243","00" );
        map.put("244","01" );
        map.put("245","10" );
        map.put("246","00" );
        map.put("247","00" );
        map.put("248","10" );
        map.put("249","11" );
        map.put("250","11" );
        map.put("251","11" );
        map.put("252","11" );
        map.put("253","11" );
        map.put("254","00" );
        map.put("255","00" );
        map.put("256","00" );
        map.put("257","10" );
        map.put("258","10" );
        map.put("259","10" );
        map.put("260","10" );
        map.put("261","10" );
        map.put("262","10" );
        map.put("263","10" );
        map.put("264","00" );
    }
    public static String getPen20Index(String rom) {
        int index = (Integer.parseInt(rom.substring(5, 8), 16)) / 4;

        return "" + index;
    }
    public static int getPen20Power(String rom) {
        int power = (Integer.parseInt(rom.substring(21, 23), 16));
        return power;
    }
    public static int getPen20Score(String rom) {
        int shakes = (Integer.parseInt(rom.substring(0, 2), 16));
        String bits = (Integer.toBinaryString(shakes));

        bits = leadingZeros(bits, 8);

        bits = bits.substring(2, 6);
        shakes = Integer.parseInt(bits, 2);
        return shakes;
    }
    public static int getPen20Shakes(String rom) {
        int shakes = (Integer.parseInt(rom.substring(0, 2), 16));
        String bits = (Integer.toBinaryString(shakes));

        bits = leadingZeros(bits, 8);

        bits = bits.substring(2, 6);
        shakes = Integer.parseInt(bits, 2);
        return shakes;
    }

    public static int getPen20ShotW(String rom) {
        int shotW = (Integer.parseInt(rom.substring(11, 13), 16));
        String bits = (Integer.toBinaryString(shotW));

        bits = leadingZeros(bits, 8);

        shotW = Integer.parseInt(bits, 2);
        return shotW;
    }
    public static int getPen20ShotS(String rom) {
        int shotS = (Integer.parseInt(rom.substring(16, 18), 16));
        String bits = (Integer.toBinaryString(shotS));

        bits = leadingZeros(bits, 8);

        shotS = Integer.parseInt(bits, 2);
        return shotS;
    }

    public static int getPen20HitPattern(String rom) {
        int hp = (Integer.parseInt(rom.substring(46, 48), 16));
        return hp;
    }
    public static int getPen20Pattern(String[] data, int shakes) {
        int score = 0;
        String level = data[1];
        String attr = data[2];


        //A Digimon's score is depending on a Digimon's attribute and level, as well as shake
        //After reading the attribute and level, look to see what the corresponding 'score' is when matching with the number of shakes
        if (shakes == 0)
            return score;
        else {
            switch (level) {
                case "Child":
                    score = Pen20ChildScores(shakes);
                    break;
                case "Adult":
                    if (attr == "Vaccine" || attr == "Free")
                        score = Pen20VaFreeAdultScores(shakes);
                    if (attr == "Data")
                        score = Pen20DaAdultScores(shakes);
                    if (attr == "Virus")
                        score = Pen20ViAdultScores(shakes);
                    break;
                case "Perfect":
                    if (attr == "Vaccine")
                        score = Pen20VaPerfectScores(shakes);
                    if (attr == "Data" || attr == "Free")
                        score = Pen20DaFrPerfectScores(shakes);
                    if (attr == "Virus")
                        score = Pen20ViPerfectScores(shakes);
                    break;
                case "Ultimate":
                case "SuperUltimate":
                    if (attr == "Vaccine")
                        score = Pen20VaUltimateScores(shakes);
                    if (attr == "Data")
                        score = Pen20DaUltimateScores(shakes);
                    if (attr == "Virus" || attr == "Free")
                        score = Pen20ViFrUltimateScores(shakes);
                    break;
                default:
                    score = 0;
            }
        }

        return score;
    }

    private static Integer Pen20ChildScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                4,
                4,
                3,
                3,
                12,
                12,
                13,
                13,
                2,
                2,
                6,
                6,
                8,
                8,
                14
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20VaFreeAdultScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                7,
                7,
                8,
                8,
                14,
                14,
                9,
                9,
                2,
                2,
                1,
                1,
                3,
                3,
                12
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20DaAdultScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                3,
                3,
                1,
                1,
                4,
                4,
                9,
                9,
                14,
                14,
                9,
                9,
                12,
                12,
                1
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20ViAdultScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                4,
                4,
                7,
                7,
                10,
                10,
                1,
                1,
                3,
                3,
                9,
                9,
                10,
                10,
                14
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20VaPerfectScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                1,
                1,
                4,
                4,
                13,
                13,
                12,
                12,
                6,
                1,
                11,
                11,
                2,
                2,
                12
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20DaFrPerfectScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                3,
                3,
                2,
                2,
                7,
                7,
                9,
                9,
                5,
                5,
                13,
                13,
                10,
                10,
                11
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20ViPerfectScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                4,
                4,
                7,
                7,
                1,
                1,
                8,
                8,
                12,
                12,
                8,
                8,
                2,
                2,
                13
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20VaUltimateScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                3,
                3,
                1,
                1,
                2,
                2,
                13,
                13,
                12,
                12,
                2,
                2,
                9,
                9,
                11
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20DaUltimateScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                6,
                6,
                1,
                1,
                9,
                9,
                8,
                8,
                12,
                12,
                2,
                2,
                13,
                13,
                10
        };
        return pen20Scores[shakes];
    }
    private static Integer Pen20ViFrUltimateScores(int shakes) {
        int[] pen20Scores = new int[]{
                0,
                4,
                4,
                7,
                7,
                4,
                4,
                12,
                12,
                9,
                9,
                3,
                3,
                11,
                11,
                14
        };
        return pen20Scores[shakes];
    }

    public static String[] getPen20MonFromRom(String rom) {
        pen20Data = new ArrayList<>();
        pen20IndexInit();

        //get index of mon from rom
        int index = (Integer.parseInt(rom.substring(5, 8), 16)) / 4;

        if (index > pen20Data.size())
            return null;
        else {
            return pen20Data.get(index);
        }
    }
    public static String getPen20MonFromIndex(String index) {
        pen20Data = new ArrayList<>();
        pen20IndexInit();

        //get index of mon from rom

        if (Integer.parseInt(index) > pen20Data.size())
            return "err";
        else {
            String s = pen20Data.get(Integer.parseInt(index))[0];
            return (s != null) ? s: "";
        }
    }
    public static String[] getPen20MonFromIndexFull(int index) {
        pen20Data = new ArrayList<>();
        pen20IndexInit();

        if (index > pen20Data.size())
            return null;
        else {
            return pen20Data.get(index);
        }

    }
    public static String getPen20Level(String index) {
        pen20MapInit();
        if(mapp.containsKey(index)) return mapp.get(index)[1];
        else return "err";
    }
    private static void pen20IndexInit() {
        //List<String[]>
        pen20Data = new ArrayList<>();

        pen20Data.add( new String[]{"err", "Child", "Data"});
        pen20Data.add(new String[]{"bubb", "BabyI", "Free"});
        pen20Data.add(new String[]{"mochi", "BabyII", "Free"});
        pen20Data.add(new String[]{"tento", "Child", "Vaccine"});
        pen20Data.add(new String[]{"gottsu", "Child", "Data"});
        pen20Data.add(new String[]{"otama", "Child", "Virus"});
        pen20Data.add(new String[]{"kabuteri", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"torta", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"monochro", "Adult", "Data"});
        pen20Data.add(new String[]{"star", "Adult", "Data"});
        pen20Data.add(new String[]{"geko", "Adult", "Virus"});
        pen20Data.add(new String[]{"kuwaga", "Adult", "Virus"});
        pen20Data.add(new String[]{"tail", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"atlurkabuteri", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"jyaga", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"tricera", "Perfect", "Data"});
        pen20Data.add(new String[]{"piccolo", "Perfect", "Data"});
        pen20Data.add(new String[]{"tonosamageko", "Perfect", "Virus"});
        pen20Data.add(new String[]{"okuwa", "Perfect", "Virus"});
        pen20Data.add(new String[]{"angewo", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"heraklekabuteri", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"saberleo", "Ultimate", "Data"});
        pen20Data.add(new String[]{"holydra", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"metalete", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"pitch", "BabyI", "Free"});
        pen20Data.add(new String[]{"puka", "BabyII", "Free"});
        pen20Data.add(new String[]{"goma", "Child", "Vaccine"});
        pen20Data.add(new String[]{"gani", "Child", "Data"});
        pen20Data.add(new String[]{"shako", "Child", "Virus"});
        pen20Data.add(new String[]{"ruka", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"ikkaku", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"seadra", "Adult", "Data"});
        pen20Data.add(new String[]{"coela", "Adult", "Data"});
        pen20Data.add(new String[]{"oct", "Adult", "Virus"});
        pen20Data.add(new String[]{"geso", "Adult", "Virus"});
        pen20Data.add(new String[]{"ebidra", "Adult", "Data"});
        pen20Data.add(new String[]{"wha", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"zudo", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"megaseadra", "Perfect", "Data"});
        pen20Data.add(new String[]{"anomalocari", "Perfect", "Data"});
        pen20Data.add(new String[]{"dago", "Perfect", "Virus"});
        pen20Data.add(new String[]{"marindevi", "Perfect", "Virus"});
        pen20Data.add(new String[]{"hangyo", "Perfect", "Data"});
        pen20Data.add(new String[]{"marinange", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"metalseadra", "Ultimate", "Data"});
        pen20Data.add(new String[]{"puku", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"plesio", "Ultimate", "Data"});
        pen20Data.add(new String[]{"moku", "BabyI", "Free"});
        pen20Data.add(new String[]{"petimera", "BabyII", "Free"});
        pen20Data.add(new String[]{"baku", "Child", "Vaccine"});
        pen20Data.add(new String[]{"cand", "Child", "Data"});
        pen20Data.add(new String[]{"picodevi", "Child", "Virus"});
        pen20Data.add(new String[]{"hanu", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"garuru", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"mera", "Adult", "Data"});
        pen20Data.add(new String[]{"wizar", "Adult", "Data"});
        pen20Data.add(new String[]{"devi", "Adult", "Virus"});
        pen20Data.add(new String[]{"bake", "Adult", "Virus"});
        pen20Data.add(new String[]{"dokugu", "Adult", "Virus"});
        pen20Data.add(new String[]{"mam", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"weregaruru", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"deathmera", "Perfect", "Data"});
        pen20Data.add(new String[]{"pump", "Perfect", "Data"});
        pen20Data.add(new String[]{"vamde", "Perfect", "Virus"});
        pen20Data.add(new String[]{"fanto", "Perfect", "Virus"});
        pen20Data.add(new String[]{"ladydevi", "Perfect", "Virus"});
        pen20Data.add(new String[]{"skullmam", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"bolt", "Ultimate", "Data"});
        pen20Data.add(new String[]{"de", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"pie", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"nyoki", "BabyI", "Free"});
        pen20Data.add(new String[]{"pyoco", "BabyII", "Free"});
        pen20Data.add(new String[]{"piyo", "Child", "Vaccine"});
        pen20Data.add(new String[]{"flora", "Child", "Data"});
        pen20Data.add(new String[]{"mush", "Child", "Virus"});
        pen20Data.add(new String[]{"pal", "Child", "Data"});
        pen20Data.add(new String[]{"vdra", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"birdra", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"toge", "Adult", "Data"});
        pen20Data.add(new String[]{"kiwi", "Adult", "Data"});
        pen20Data.add(new String[]{"wood", "Adult", "Virus"});
        pen20Data.add(new String[]{"redveggie", "Adult", "Virus"});
        pen20Data.add(new String[]{"aerovdra", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"garuda", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"blosso", "Perfect", "Data"});
        pen20Data.add(new String[]{"delu", "Perfect", "Data"});
        pen20Data.add(new String[]{"jyurei", "Perfect", "Virus"});
        pen20Data.add(new String[]{"gerbe", "Perfect", "Virus"});
        pen20Data.add(new String[]{"lili", "Perfect", "Data"});
        pen20Data.add(new String[]{"houou", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"griffo", "Ultimate", "Data"});
        pen20Data.add(new String[]{"pinochi", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"rose", "Ultimate", "Data"});
        pen20Data.add(new String[]{"choro", "BabyI", "Free"});
        pen20Data.add(new String[]{"capri", "BabyII", "Free"});
        pen20Data.add(new String[]{"toyagu", "Child", "Vaccine"});
        pen20Data.add(new String[]{"kokuwa", "Child", "Data"});
        pen20Data.add(new String[]{"haguru", "Child", "Virus"});
        pen20Data.add(new String[]{"grey", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"revol", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"tank", "Adult", "Data"});
        pen20Data.add(new String[]{"clock", "Adult", "Data"});
        pen20Data.add(new String[]{"guardro", "Adult", "Virus"});
        pen20Data.add(new String[]{"mechanori", "Adult", "Virus"});
        pen20Data.add(new String[]{"thunderball", "Adult", "Data"});
        pen20Data.add(new String[]{"metalgrey", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"andro", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"knight", "Perfect", "Data"});
        pen20Data.add(new String[]{"bigmame", "Perfect", "Data"});
        pen20Data.add(new String[]{"megadra", "Perfect", "Virus"});
        pen20Data.add(new String[]{"warumonzae", "Perfect", "Virus"});
        pen20Data.add(new String[]{"cyberdra", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"wargrey", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"metalgaruru", "Ultimate", "Data"});
        pen20Data.add(new String[]{"mugendra", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"venomvamde", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"yukimibota", "BabyI", "Free"});
        pen20Data.add(new String[]{"nyaro", "BabyII", "Free"});
        pen20Data.add(new String[]{"agu", "Child", "Vaccine"});
        pen20Data.add(new String[]{"gabu", "Child", "Data"});
        pen20Data.add(new String[]{"plot", "Child", "Vaccine"});
        pen20Data.add(new String[]{"leo", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"iga", "Adult", "Data"});
        pen20Data.add(new String[]{"ange", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"asura", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"metalmame", "Perfect", "Data"});
        pen20Data.add(new String[]{"holyange", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"seraphi", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"ofani", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"phasco", "Child", "Virus"});
        pen20Data.add(new String[]{"imp", "Child", "Virus"});
        pen20Data.add(new String[]{"porcupa", "Adult", "Virus"});
        pen20Data.add(new String[]{"madleo", "Adult", "Free"});
        pen20Data.add(new String[]{"shadra", "Adult", "Free"});
        pen20Data.add(new String[]{"troop", "Adult", "Free"});
        pen20Data.add(new String[]{"asta", "Perfect", "Virus"});
        pen20Data.add(new String[]{"baal", "Perfect", "Free"});
        pen20Data.add(new String[]{"darkknight", "Perfect", "Free"});
        pen20Data.add(new String[]{"_rgm_belphe", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"beelzebu", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"bota", "BabyI", "Free"});
        pen20Data.add(new String[]{"koro", "BabyII", "Free"});
        pen20Data.add(new String[]{"guil", "Child", "Virus"});
        pen20Data.add(new String[]{"_bsh_agu", "Child", "Vaccine"});
        pen20Data.add(new String[]{"grow", "Adult", "Virus"});
        pen20Data.add(new String[]{"reppa", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"arresterdra", "Adult", "Free"});
        pen20Data.add(new String[]{"manbo", "Adult", "Free"});
        pen20Data.add(new String[]{"megalogrow", "Perfect", "Virus"});
        pen20Data.add(new String[]{"beowolf", "Perfect", "Free"});
        pen20Data.add(new String[]{"rizegrey", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"duke", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"duft", "Ultimate", "Data"});
        pen20Data.add(new String[]{"saku", "BabyI", "Free"});
        pen20Data.add(new String[]{"sakutto", "BabyII", "Free"});
        pen20Data.add(new String[]{"zuba", "Child", "Vaccine"});
        pen20Data.add(new String[]{"hack", "Child", "Data"});
        pen20Data.add(new String[]{"zubaeager", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"baohack", "Adult", "Data"});
        pen20Data.add(new String[]{"target", "Adult", "Virus"});
        pen20Data.add(new String[]{"dura", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"saviorhack", "Perfect", "Data"});
        pen20Data.add(new String[]{"ete", "Perfect", "Virus"});
        pen20Data.add(new String[]{"duranda", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"jes", "Ultimate", "Data"});
        pen20Data.add(new String[]{"jesx", "SuperUltimate", "Data"});
        pen20Data.add(new String[]{"bancholeo", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"petit", "BabyI", "Free"});
        pen20Data.add(new String[]{"babyd", "BabyII", "Free"});
        pen20Data.add(new String[]{"draco", "Child", "Data"});
        pen20Data.add(new String[]{"_hak_agu", "Child", "Vaccine"});
        pen20Data.add(new String[]{"_blu_coredra", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"_gre_coredra", "Adult", "Virus"});
        pen20Data.add(new String[]{"devidra", "Adult", "Virus"});
        pen20Data.add(new String[]{"wingdra", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"groundra", "Perfect", "Virus"});
        pen20Data.add(new String[]{"slayerdra", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"breakdra", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"darkdra", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"zeri", "BabyI", "Free"});
        pen20Data.add(new String[]{"gummy", "BabyII", "Free"});
        pen20Data.add(new String[]{"terrier", "Child", "Vaccine"});
        pen20Data.add(new String[]{"galgo", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"rapid", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"saintgalgo", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"coco", "BabyI", "Free"});
        pen20Data.add(new String[]{"choco", "BabyII", "Free"});
        pen20Data.add(new String[]{"lop", "Child", "Data"});
        pen20Data.add(new String[]{"turuie", "Adult", "Data"});
        pen20Data.add(new String[]{"andira", "Perfect", "Virus"});
        pen20Data.add(new String[]{"cherubi", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"cotsuco", "BabyI", "Free"});
        pen20Data.add(new String[]{"kakkin", "BabyII", "Free"});
        pen20Data.add(new String[]{"ludo", "Child", "Data"});
        pen20Data.add(new String[]{"tialudo", "Adult", "Data"});
        pen20Data.add(new String[]{"raijiludo", "Perfect", "Data"});
        pen20Data.add(new String[]{"bryweludra", "Ultimate", "Data"});
        pen20Data.add(new String[]{"vorvo", "Child", "Virus"});
        pen20Data.add(new String[]{"lavorvo", "Adult", "Virus"});
        pen20Data.add(new String[]{"lavogarita", "Perfect", "Virus"});
        pen20Data.add(new String[]{"volcanicdra", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"chico", "BabyI", "Free"});
        pen20Data.add(new String[]{"chibi", "BabyII", "Free"});
        pen20Data.add(new String[]{"v", "child", "Free"});
        pen20Data.add(new String[]{"xv", "Adult", "Free"});
        pen20Data.add(new String[]{"ulforcevdra", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"meicoo", "Adult", "Free"});
        pen20Data.add(new String[]{"_vcm_meicrack", "Perfect", "Free"});
        pen20Data.add(new String[]{"raguel", "Ultimate", "Free"});
        pen20Data.add(new String[]{"fufu", "BabyI", "Free"});
        pen20Data.add(new String[]{"kyokyo", "BabyII", "Free"});
        pen20Data.add(new String[]{"ryuda", "Child", "Vaccine"});
        pen20Data.add(new String[]{"ginryu", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"hisyaryu", "Perfect", "Vaccine"});
        pen20Data.add(new String[]{"ouryu", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"bud", "BabyII", "Free"});
        pen20Data.add(new String[]{"lala", "Child", "Data"});
        pen20Data.add(new String[]{"sunflow", "Adult", "Data"});
        pen20Data.add(new String[]{"lila", "Perfect", "Data"});
        pen20Data.add(new String[]{"lotus", "Ultimate", "Data"});
        pen20Data.add(new String[]{"dodo", "BabyI", "Free"});
        pen20Data.add(new String[]{"dori", "BabyII", "Free"});
        pen20Data.add(new String[]{"doru", "Child", "Data"});
        pen20Data.add(new String[]{"doruga", "Adult", "Data"});
        pen20Data.add(new String[]{"dorugure", "Perfect", "Data"});
        pen20Data.add(new String[]{"dorugora", "Ultimate", "Data"});
        pen20Data.add(new String[]{"alpha", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"omega", "SuperUltimate", "Vaccine"});
        pen20Data.add(new String[]{"ogudo", "SuperUltimate", "Virus"});
        pen20Data.add(new String[]{"ordine", "SuperUltimate", "Free"});
        pen20Data.add(new String[]{"paildra", "Perfect", "Free"});
        pen20Data.add(new String[]{"_ftm_imperialdra", "Ultimate", "Free"});
        pen20Data.add(new String[]{"voltobauta", "SuperUltimate", "Virus"});
        pen20Data.add(new String[]{"_blm_beelzebu", "SuperUltimate", "Virus"});
        pen20Data.add(new String[]{"ragnalord", "SuperUltimate", "Virus"});
        pen20Data.add(new String[]{"_ory_alpha", "SuperUltimate", "Vaccine"});
        pen20Data.add(new String[]{"rafflesi", "SuperUltimate", "Data"});
        pen20Data.add(new String[]{"maste", "SuperUltimate", "Vaccine"});
        pen20Data.add(new String[]{"chaos", "SuperUltimate", "Vaccine"});
        pen20Data.add(new String[]{"exa", "SuperUltimate", "Data"});
        pen20Data.add(new String[]{"_plm_imperialdra", "SuperUltimate", "Free"});
        pen20Data.add(new String[]{"magna", "Armor", "Free"});
        pen20Data.add(new String[]{"dynas", "Ultimate", "Data"});
        pen20Data.add(new String[]{"cranium", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"gankoo", "Ultimate", "Data"});
        pen20Data.add(new String[]{"lordknight", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"sleip", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"_fdm_ofani", "Ultimate", "Vaccine"});
        pen20Data.add(new String[]{"beelstar", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"sting", "Adult", "Free"});
        pen20Data.add(new String[]{"susanoo", "SuperUltimate", "Free"});
        pen20Data.add(new String[]{"apocaly", "SuperUltimate", "Free"});
        pen20Data.add(new String[]{"n.e.o", "???", "Free"});
        pen20Data.add(new String[]{"heriss", "Child", "Free"});
        pen20Data.add(new String[]{"bluco", "Child", "Vaccine"});
        pen20Data.add(new String[]{"sisterciel", "Adult", "Vaccine"});
        pen20Data.add(new String[]{"sisterblanc", "Child", "Vaccine"});
        pen20Data.add(new String[]{"sisternoir", "Adult", "Virus"});
        pen20Data.add(new String[]{"lilith", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"levia", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"barba", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"_fdm_luce", "Ultimate", "Virus"});
        pen20Data.add(new String[]{"_als_omega", "SuperUltimate", "Virus"});
        pen20Data.add(new String[]{"rusttyranno", "SuperUltimate", "Virus"});
        pen20Data.add(new String[]{"gracenova", "SuperUltimate", "Vaccine"});


    }
    private static void pen20MapInit() {
        mapp = new HashMap<>();
        mapp.put("1", new String[] {"bubb", "Baby I", "Free", ",mochi"});
        mapp.put("2", new String[] {"mochi", "Baby II", "Free", ",tento,gottsu,otama"});
        mapp.put("3", new String[] {"tento", "Child", "Vaccine", ",kabuteri,tail,kuwaga,geko"});
        mapp.put("4", new String[] {"gottsu", "Child", "Data", ",monochro,star,torta,geko"});
        mapp.put("5", new String[] {"otama", "Child", "Virus", ",geko,kuwaga,tail,star"});
        mapp.put("6", new String[] {"kabuteri", "Adult", "Vaccine", ",atlurkabuteri,jyaga,piccolo"});
        mapp.put("7", new String[] {"torta", "Adult", "Vaccine", ",jyaga,piccolo,atlurkabuteri"});
        mapp.put("8", new String[] {"monochro", "Adult", "Data", ",tricera,piccolo,angewo,tonosamageko"});
        mapp.put("9", new String[] {"star", "Adult", "Data", ",tricera,piccolo,angewo,tonosamageko"});
        mapp.put("10", new String[] {"geko", "Adult", "Virus", ",tonosamageko,piccolo"});
        mapp.put("11", new String[] {"kuwaga", "Adult", "Virus", ",okuwa,tonosamageko,piccolo"});
        mapp.put("12", new String[] {"tail", "Adult", "Vaccine", ",angewo,jyaga,piccolo,angewo,holyange,metalmame"});
        mapp.put("13", new String[] {"atlurkabuteri", "Perfect", "Vaccine", ",heraklekabuteri"});
        mapp.put("14", new String[] {"jyaga", "Perfect", "Vaccine", ",holydra"});
        mapp.put("15", new String[] {"tricera", "Perfect", "Data", ",saberleo"});
        mapp.put("16", new String[] {"piccolo", "Perfect", "Data", ",saberleo"});
        mapp.put("17", new String[] {"tonosamageko", "Perfect", "Virus", ",metalete"});
        mapp.put("18", new String[] {"okuwa", "Perfect", "Virus", ",heraklekabuteri,metalete"});
        mapp.put("19", new String[] {"angewo", "Perfect", "Vaccine", ",holydra,ofani,maste"});
        mapp.put("20", new String[] {"heraklekabuteri", "Ultimate", "Vaccine", ""});
        mapp.put("21", new String[] {"saberleo", "Ultimate", "Data", ""});
        mapp.put("22", new String[] {"holydra", "Ultimate", "Vaccine", ""});
        mapp.put("23", new String[] {"metalete", "Ultimate", "Virus", ""});
        mapp.put("24", new String[] {"pitch", "Baby I", "Free", ",puka"});
        mapp.put("25", new String[] {"puka", "Baby II", "Free", ",goma,gani,shako"});
        mapp.put("26", new String[] {"goma", "Child", "Vaccine", ",ikkaku,ruka,coela,geso"});
        mapp.put("27", new String[] {"gani", "Child", "Data", ",seadra,ebidra,ruka,oct"});
        mapp.put("28", new String[] {"shako", "Child", "Virus", ",oct,geso,ikkaku,seadra"});
        mapp.put("29", new String[] {"ruka", "Adult", "Vaccine", ",zudo,wha,hangyo"});
        mapp.put("30", new String[] {"ikkaku", "Adult", "Vaccine", ",zudo,wha,anomalocari"});
        mapp.put("31", new String[] {"seadra", "Adult", "Data", ",megaseadra,hangyo,wha,dago"});
        mapp.put("32", new String[] {"coela", "Adult", "Data", ",megaseadra,anomalocari,wha,dago"});
        mapp.put("33", new String[] {"oct", "Adult", "Virus", ",marindevi,dago,anomalocari"});
        mapp.put("34", new String[] {"geso", "Adult", "Virus", ",marindevi,dago,hangyo"});
        mapp.put("35", new String[] {"ebidra", "Adult", "Data", ",megaseadra,hangyo,wha,dago"});
        mapp.put("36", new String[] {"wha", "Perfect", "Vaccine", ",plesio"});
        mapp.put("37", new String[] {"zudo", "Perfect", "Vaccine", ",marinange"});
        mapp.put("38", new String[] {"megaseadra", "Perfect", "Data", ",marinange,metalseadra"});
        mapp.put("39", new String[] {"anomalocari", "Perfect", "Data", ",metalseadra"});
        mapp.put("40", new String[] {"dago", "Perfect", "Virus", ",puku"});
        mapp.put("41", new String[] {"marindevi", "Perfect", "Virus", ",puku"});
        mapp.put("42", new String[] {"hangyo", "Perfect", "Data", ",plesio"});
        mapp.put("43", new String[] {"marinange", "Ultimate", "Vaccine", ""});
        mapp.put("44", new String[] {"metalseadra", "Ultimate", "Data", ""});
        mapp.put("45", new String[] {"puku", "Ultimate", "Virus", ""});
        mapp.put("46", new String[] {"plesio", "Ultimate", "Data", ""});
        mapp.put("47", new String[] {"moku", "Baby I", "Free", ",petimera"});
        mapp.put("48", new String[] {"petimera", "Baby II", "Free", ",baku,cand,picodevi,vorvo"});
        mapp.put("49", new String[] {"baku", "Child", "Vaccine", ",hanu,garuru,mera,wizar"});
        mapp.put("50", new String[] {"cand", "Child", "Data", ",mera,wizar,hanu,devi"});
        mapp.put("51", new String[] {"picodevi", "Child", "Virus", ",devi,bake,dokugu,wizar"});
        mapp.put("52", new String[] {"hanu", "Adult", "Vaccine", ",mam,weregaruru,pump"});
        mapp.put("53", new String[] {"garuru", "Adult", "Vaccine", ",weregaruru,mam,weregaruru,pump,angewo,metalmame"});
        mapp.put("54", new String[] {"mera", "Adult", "Data", ",deathmera,pump,weregaruru,fanto"});
        mapp.put("55", new String[] {"wizar", "Adult", "Data", ",deathmera,pump,weregaruru,fanto"});
        mapp.put("56", new String[] {"devi", "Adult", "Virus", ",vamde,ladydevi,pump"});
        mapp.put("57", new String[] {"bake", "Adult", "Virus", ",fanto,pump,ladydevi"});
        mapp.put("58", new String[] {"dokugu", "Adult", "Virus", ",ladydevi,vamde,pump"});
        mapp.put("59", new String[] {"mam", "Perfect", "Vaccine", ",skullmam"});
        mapp.put("60", new String[] {"weregaruru", "Perfect", "Vaccine", ",skullmam,metalgaruru"});
        mapp.put("61", new String[] {"deathmera", "Perfect", "Data", ",bolt,skullmam"});
        mapp.put("62", new String[] {"pump", "Perfect", "Data", ",bolt"});
        mapp.put("63", new String[] {"vamde", "Perfect", "Virus", ",pie,voltobauta"});
        mapp.put("64", new String[] {"fanto", "Perfect", "Virus", ",pie"});
        mapp.put("65", new String[] {"ladydevi", "Perfect", "Virus", ",de,maste"});
        mapp.put("66", new String[] {"skullmam", "Ultimate", "Vaccine", ""});
        mapp.put("67", new String[] {"bolt", "Ultimate", "Data", ""});
        mapp.put("68", new String[] {"de", "Ultimate", "Virus", ",ogudo"});
        mapp.put("69", new String[] {"pie", "Ultimate", "Virus", ",voltobauta"});
        mapp.put("70", new String[] {"nyoki", "Baby I", "Free", ",pyoco,bud"});
        mapp.put("71", new String[] {"pyoco", "Baby II", "Free", ",piyo,flora,pal,mush"});
        mapp.put("72", new String[] {"piyo", "Child", "Vaccine", ",birdra,vdra,toge,wood"});
        mapp.put("73", new String[] {"flora", "Child", "Data", ",kiwi,toge,vdra,redveggie"});
        mapp.put("74", new String[] {"mush", "Child", "Virus", ",wood,redveggie,vdra,birdra"});
        mapp.put("75", new String[] {"pal", "Child", "Data", ",toge,kiwi,redveggie,birdra"});
        mapp.put("76", new String[] {"vdra", "Adult", "Vaccine", ",aerovdra,garuda,delu"});
        mapp.put("77", new String[] {"birdra", "Adult", "Vaccine", ",garuda,aerovdra,delu"});
        mapp.put("78", new String[] {"toge", "Adult", "Data", ",lili,delu,garuda,gerbe"});
        mapp.put("79", new String[] {"kiwi", "Adult", "Data", ",blosso,delu,garuda,gerbe"});
        mapp.put("80", new String[] {"wood", "Adult", "Virus", ",jyurei,gerbe,delu"});
        mapp.put("81", new String[] {"redveggie", "Adult", "Virus", ",jyurei,gerbe,delu"});
        mapp.put("82", new String[] {"aerovdra", "Perfect", "Vaccine", ",ulforcevdra,houou,ulforcevdra"});
        mapp.put("83", new String[] {"garuda", "Perfect", "Vaccine", ",houou"});
        mapp.put("84", new String[] {"blosso", "Perfect", "Data", ",griffo"});
        mapp.put("85", new String[] {"delu", "Perfect", "Data", ",griffo,houou"});
        mapp.put("86", new String[] {"jyurei", "Perfect", "Virus", ",pinochi"});
        mapp.put("87", new String[] {"gerbe", "Perfect", "Virus", ",pinochi"});
        mapp.put("88", new String[] {"lili", "Perfect", "Data", ",rose"});
        mapp.put("89", new String[] {"houou", "Ultimate", "Vaccine", ""});
        mapp.put("90", new String[] {"griffo", "Ultimate", "Data", ""});
        mapp.put("91", new String[] {"pinochi", "Ultimate", "Virus", ""});
        mapp.put("92", new String[] {"rose", "Ultimate", "Data", ",rafflesi"});
        mapp.put("93", new String[] {"choro", "Baby I", "Free", ",capri"});
        mapp.put("94", new String[] {"capri", "Baby II", "Free", ",toyagu,kokuwa,haguru,phasco,imp"});
        mapp.put("95", new String[] {"toyagu", "Child", "Vaccine", ",grey,revol,clock,thunderball"});
        mapp.put("96", new String[] {"kokuwa", "Child", "Data", ",clock,thunderball,revol,guardro"});
        mapp.put("97", new String[] {"haguru", "Child", "Virus", ",guardro,mechanori,grey,tank"});
        mapp.put("98", new String[] {"grey", "Adult", "Vaccine", ",metalgrey,cyberdra,bigmame,asura,metalmame"});
        mapp.put("99", new String[] {"revol", "Adult", "Vaccine", ",metalgrey,andro,bigmame"});
        mapp.put("100", new String[] {"tank", "Adult", "Data", ",knight,bigmame,andro,warumonzae"});
        mapp.put("101", new String[] {"clock", "Adult", "Data", ",knight,bigmame,andro,warumonzae"});
        mapp.put("102", new String[] {"guardro", "Adult", "Virus", ",megadra,warumonzae,bigmame"});
        mapp.put("103", new String[] {"mechanori", "Adult", "Virus", ",megadra,warumonzae,bigmame"});
        mapp.put("104", new String[] {"thunderball", "Adult", "Data", ",knight,cyberdra,bigmame,warumonzae"});
        mapp.put("105", new String[] {"metalgrey", "Perfect", "Vaccine", ",wargrey"});
        mapp.put("106", new String[] {"andro", "Perfect", "Vaccine", ",mugendra"});
        mapp.put("107", new String[] {"knight", "Perfect", "Data", ",wargrey,metalgaruru"});
        mapp.put("108", new String[] {"bigmame", "Perfect", "Data", ",metalgaruru"});
        mapp.put("109", new String[] {"megadra", "Perfect", "Virus", ",mugendra,darkdra"});
        mapp.put("110", new String[] {"warumonzae", "Perfect", "Virus", ",venomvamde"});
        mapp.put("111", new String[] {"cyberdra", "Perfect", "Vaccine", ",wargrey"});
        mapp.put("112", new String[] {"wargrey", "Ultimate", "Vaccine", ",omega"});
        mapp.put("113", new String[] {"metalgaruru", "Ultimate", "Data", ",omega"});
        mapp.put("114", new String[] {"mugendra", "Ultimate", "Virus", ""});
        mapp.put("115", new String[] {"venomvamde", "Ultimate", "Virus", ""});
        mapp.put("116", new String[] {"yukimibota", "Baby I", "Free", ",nyaro"});
        mapp.put("117", new String[] {"nyaro", "Baby II", "Free", ",agu,gabu,plot"});
        mapp.put("118", new String[] {"agu", "Child", "Vaccine", ",grey,leo,garuru,ange"});
        mapp.put("119", new String[] {"gabu", "Child", "Data", ",garuru,iga,leo,tail"});
        mapp.put("120", new String[] {"plot", "Child", "Vaccine", ",tail,ange,grey,iga,meicoo"});
        mapp.put("121", new String[] {"leo", "Adult", "Vaccine", ",metalgrey,asura,metalmame"});
        mapp.put("122", new String[] {"iga", "Adult", "Data", ",weregaruru,metalmame,asura"});
        mapp.put("123", new String[] {"ange", "Adult", "Vaccine", ",holyange,angewo,metalmame"});
        mapp.put("124", new String[] {"asura", "Perfect", "Vaccine", ",wargrey"});
        mapp.put("125", new String[] {"metalmame", "Perfect", "Data", ",metalgaruru"});
        mapp.put("126", new String[] {"holyange", "Perfect", "Vaccine", ",seraphi"});
        mapp.put("127", new String[] {"seraphi", "Ultimate", "Vaccine", ""});
        mapp.put("128", new String[] {"ofani", "Ultimate", "Vaccine", ""});
        mapp.put("129", new String[] {"phasco", "Child", "Virus", ",porcupa,shadra,troop"});
        mapp.put("130", new String[] {"imp", "Child", "Virus", ",madleo,shadra,troop"});
        mapp.put("131", new String[] {"porcupa", "Adult", "Virus", ",asta,baal,darkknight"});
        mapp.put("132", new String[] {"madleo", "Adult", "Free", ",baal,asta"});
        mapp.put("133", new String[] {"shadra", "Adult", "Free", ",darkknight,asta"});
        mapp.put("134", new String[] {"troop", "Adult", "Free", ",darkknight"});
        mapp.put("135", new String[] {"asta", "Perfect", "Virus", ",_rgm_belphe"});
        mapp.put("136", new String[] {"baal", "Perfect", "Free", ",beelzebu"});
        mapp.put("137", new String[] {"darkknight", "Perfect", "Free", ",_rgm_belphe,beelzebu"});
        mapp.put("138", new String[] {"_rgm_belphe", "Ultimate", "Virus", ",ogudo"});
        mapp.put("139", new String[] {"beelzebu", "Ultimate", "Virus", ",_blm_beelzebu"});
        mapp.put("140", new String[] {"bota", "Baby I", "Free", ",koro"});
        mapp.put("141", new String[] {"koro", "Baby II", "Free", ",guil,_bsh_agu"});
        mapp.put("142", new String[] {"guil", "Child", "Virus", ",grow,arresterdra,manbo"});
        mapp.put("143", new String[] {"_bsh_agu", "Child", "Vaccine", ",reppa,arresterdra,manbo"});
        mapp.put("144", new String[] {"grow", "Adult", "Virus", ",megalogrow,rizegrey,beowolf"});
        mapp.put("145", new String[] {"reppa", "Adult", "Vaccine", ",beowolf,rizegrey,megalogrow"});
        mapp.put("146", new String[] {"arresterdra", "Adult", "Free", ",rizegrey,megalogrow"});
        mapp.put("147", new String[] {"manbo", "Adult", "Free", ",rizegrey"});
        mapp.put("148", new String[] {"megalogrow", "Perfect", "Virus", ",duke"});
        mapp.put("149", new String[] {"beowolf", "Perfect", "Free", ",duft"});
        mapp.put("150", new String[] {"rizegrey", "Perfect", "Vaccine", ",duke,duft"});
        mapp.put("151", new String[] {"duke", "Ultimate", "Virus", ""});
        mapp.put("152", new String[] {"duft", "Ultimate", "Data", ""});
        mapp.put("153", new String[] {"saku", "Baby I", "Free", ",sakutto"});
        mapp.put("154", new String[] {"sakutto", "Baby II", "Free", ",zuba,hack"});
        mapp.put("155", new String[] {"zuba", "Child", "Vaccine", ",zubaeager,target"});
        mapp.put("156", new String[] {"hack", "Child", "Data", ",baohack,target"});
        mapp.put("157", new String[] {"zubaeager", "Adult", "Vaccine", ",dura,ete,saviorhack"});
        mapp.put("158", new String[] {"baohack", "Adult", "Data", ",saviorhack,ete,dura"});
        mapp.put("159", new String[] {"target", "Adult", "Virus", ",ete,dura,saviorhack"});
        mapp.put("160", new String[] {"dura", "Perfect", "Vaccine", ",duranda"});
        mapp.put("161", new String[] {"saviorhack", "Perfect", "Data", ",jes"});
        mapp.put("162", new String[] {"ete", "Perfect", "Virus", ",bancholeo"});
        mapp.put("163", new String[] {"duranda", "Ultimate", "Vaccine", ",ragnalord"});
        mapp.put("164", new String[] {"jes", "Ultimate", "Data", ",_x_jes"});
        mapp.put("165", new String[] {"_x_jes", "Super Ultimate", "Data", ""});
        mapp.put("166", new String[] {"bancholeo", "Ultimate", "Vaccine", ",chaos"});
        mapp.put("167", new String[] {"petit", "Baby I", "Free", ",babyd"});
        mapp.put("168", new String[] {"babyd", "Baby II", "Free", ",draco,_hak_agu"});
        mapp.put("169", new String[] {"draco", "Child", "Data", ",_blu_coredra,_gre_coredra"});
        mapp.put("170", new String[] {"_hak_agu", "Child", "Vaccine", ",devidra,_gre_coredra"});
        mapp.put("171", new String[] {"_blu_coredra", "Adult", "Vaccine", ",wingdra,groundra,megadra"});
        mapp.put("172", new String[] {"_gre_coredra", "Adult", "Virus", ",groundra,wingdra,megadra"});
        mapp.put("173", new String[] {"devidra", "Adult", "Virus", ",megadra,wingdra,groundra"});
        mapp.put("174", new String[] {"wingdra", "Perfect", "Vaccine", ",slayerdra"});
        mapp.put("175", new String[] {"groundra", "Perfect", "Virus", ",breakdra"});
        mapp.put("176", new String[] {"slayerdra", "Ultimate", "Vaccine", ",exa"});
        mapp.put("177", new String[] {"breakdra", "Ultimate", "Virus", ",exa"});
        mapp.put("178", new String[] {"darkdra", "Ultimate", "Virus", ",chaos"});
        mapp.put("179", new String[] {"zeri", "Baby I", "Free", ",gummy"});
        mapp.put("180", new String[] {"gummy", "Baby II", "Free", ",terrier"});
        mapp.put("181", new String[] {"terrier", "Child", "Vaccine", ",galgo"});
        mapp.put("182", new String[] {"galgo", "Adult", "Vaccine", ",rapid"});
        mapp.put("183", new String[] {"rapid", "Perfect", "Vaccine", ",saintgalgo"});
        mapp.put("184", new String[] {"saintgalgo", "Ultimate", "Vaccine", ""});
        mapp.put("185", new String[] {"coco", "Baby I", "Free", ",choco"});
        mapp.put("186", new String[] {"choco", "Baby II", "Free", ",lop"});
        mapp.put("187", new String[] {"lop", "Child", "Data", ",turuie"});
        mapp.put("188", new String[] {"turuie", "Adult", "Data", ",andira"});
        mapp.put("189", new String[] {"andira", "Perfect", "Virus", ",cherubi"});
        mapp.put("190", new String[] {"cherubi", "Ultimate", "Vaccine", ""});
        mapp.put("191", new String[] {"cotsuco", "Baby I", "Free", ",kakkin"});
        mapp.put("192", new String[] {"kakkin", "Baby II", "Free", ",ludo"});
        mapp.put("193", new String[] {"ludo", "Child", "Data", ",tialudo"});
        mapp.put("194", new String[] {"tialudo", "Adult", "Data", ",raijiludo"});
        mapp.put("195", new String[] {"raijiludo", "Perfect", "Data", ",bryweludra"});
        mapp.put("196", new String[] {"bryweludra", "Ultimate", "Data", ",ragnalord"});
        mapp.put("197", new String[] {"vorvo", "Child", "Virus", ",lavorvo"});
        mapp.put("198", new String[] {"lavorvo", "Adult", "Virus", ",lavogarita"});
        mapp.put("199", new String[] {"lavogarita", "Perfect", "Virus", ",volcanicdra"});
        mapp.put("200", new String[] {"volcanicdra", "Ultimate", "Virus", ""});
        mapp.put("201", new String[] {"chico", "Baby I", "Free", ",chibi"});
        mapp.put("202", new String[] {"chibi", "Baby II", "Free", ",v"});
        mapp.put("203", new String[] {"v", "Child", "Free", ",xv,vdra"});
        mapp.put("204", new String[] {"xv", "Adult", "Free", ",paildra"});
        mapp.put("205", new String[] {"ulforcevdra", "Ultimate", "Vaccine", ""});
        mapp.put("206", new String[] {"meicoo", "Adult", "Free", ",_vcm_meicrack"});
        mapp.put("207", new String[] {"_vcm_meicrack", "Perfect", "Free", ",raguel"});
        mapp.put("208", new String[] {"raguel", "Ultimate", "Free", ",ordine"});
        mapp.put("209", new String[] {"fufu", "Baby I", "Free", ",kyokyo"});
        mapp.put("210", new String[] {"kyokyo", "Baby II", "Free", ",ryuda"});
        mapp.put("211", new String[] {"ryuda", "Child", "Vaccine", ",ginryu"});
        mapp.put("212", new String[] {"ginryu", "Adult", "Vaccine", ",hisyaryu"});
        mapp.put("213", new String[] {"hisyaryu", "Perfect", "Vaccine", ",ouryu"});
        mapp.put("214", new String[] {"ouryu", "Ultimate", "Vaccine", ",_ory_alpha"});
        mapp.put("215", new String[] {"bud", "Baby II", "Free", ",lala"});
        mapp.put("216", new String[] {"lala", "Child", "Data", ",sunflow"});
        mapp.put("217", new String[] {"sunflow", "Adult", "Data", ",lila"});
        mapp.put("218", new String[] {"lila", "Perfect", "Data", ",lotus"});
        mapp.put("219", new String[] {"lotus", "Ultimate", "Data", ",rafflesi"});
        mapp.put("220", new String[] {"dodo", "Baby I", "Free", ",dori"});
        mapp.put("221", new String[] {"dori", "Baby II", "Free", ",doru"});
        mapp.put("222", new String[] {"doru", "Child", "Data", ",doruga"});
        mapp.put("223", new String[] {"doruga", "Adult", "Data", ",dorugure"});
        mapp.put("224", new String[] {"dorugure", "Perfect", "Data", ",dorugora,alpha"});
        mapp.put("225", new String[] {"dorugora", "Ultimate", "Data", ""});
        mapp.put("226", new String[] {"alpha", "Ultimate", "Vaccine", ",_ory_alpha"});
        mapp.put("227", new String[] {"omega", "Super Ultimate", "Vaccine", ",_plm_imperialdra"});
        mapp.put("228", new String[] {"ogudo", "Super Ultimate", "Virus", ""});
        mapp.put("229", new String[] {"ordine", "Super Ultimate", "Free", ""});
        mapp.put("230", new String[] {"paildra", "Perfect", "Free", ",_ftm_imperialdra"});
        mapp.put("231", new String[] {"_ftm_imperialdra", "Ultimate", "Free", ",_plm_imperialdra"});
        mapp.put("232", new String[] {"voltobauta", "Super Ultimate", "Virus", ""});
        mapp.put("233", new String[] {"_blm_beelzebu", "Super Ultimate", "Virus", ""});
        mapp.put("234", new String[] {"ragnalord", "Super Ultimate", "Virus", ""});
        mapp.put("235", new String[] {"_ory_alpha", "Super Ultimate", "Vaccine", ""});
        mapp.put("236", new String[] {"rafflesi", "Super Ultimate", "Data", ""});
        mapp.put("237", new String[] {"maste", "Super Ultimate", "Vaccine", ""});
        mapp.put("238", new String[] {"chaos", "Super Ultimate", "Vaccine", ""});
        mapp.put("239", new String[] {"exa", "Super Ultimate", "Data", ""});
        mapp.put("240", new String[] {"_plm_imperialdra", "Super Ultimate", "Free", ""});
        mapp.put("241", new String[] {"magna", "Armor", "Free", ""});
        mapp.put("242", new String[] {"dynas", "Ultimate", "Data", ""});
        mapp.put("243", new String[] {"cranium", "Ultimate", "Vaccine", ""});
        mapp.put("244", new String[] {"gankoo", "Ultimate", "Data", ""});
        mapp.put("245", new String[] {"lordknight", "Ultimate", "Virus", ""});
        mapp.put("246", new String[] {"sleip", "Ultimate", "Vaccine", ""});
        mapp.put("247", new String[] {"_fdm_ofani", "Ultimate", "Vaccine", ",ordine"});
        mapp.put("248", new String[] {"beelstar", "Ultimate", "Virus", ",_blm_beelzebu"});
        mapp.put("249", new String[] {"sting", "Adult", "Free", ",paildra"});
        mapp.put("250", new String[] {"susanoo", "Super Ultimate", "Free", ""});
        mapp.put("251", new String[] {"apocaly", "Super Ultimate", "Free", ""});
        mapp.put("252", new String[] {"n.e.o", "???", "Free", ""});
        mapp.put("253", new String[] {"heriss", "Child", "Free", ""});
        mapp.put("254", new String[] {"bluco", "Child", "Vaccine", ""});
        mapp.put("255", new String[] {"sisterciel", "Adult", "Vaccine", ""});
        mapp.put("256", new String[] {"sisterblanc", "Child", "Vaccine", ""});
        mapp.put("257", new String[] {"sisternoir", "Adult", "Virus", ""});
        mapp.put("258", new String[] {"lilith", "Ultimate", "Virus", ""});
        mapp.put("259", new String[] {"levia", "Ultimate", "Virus", ""});
        mapp.put("260", new String[] {"barba", "Ultimate", "Virus", ""});
        mapp.put("261", new String[] {"_fdm_luce", "Ultimate", "Virus", ""});
        mapp.put("262", new String[] {"_als_omega", "Super Ultimate", "Virus", ""});
        mapp.put("263", new String[] {"rusttyranno", "Super Ultimate", "Virus", ""});
        mapp.put("264", new String[] {"gracenova", "Super Ultimate", "Vaccine", ""});
    }
    public static boolean checkEvoPen20(String index, String mon) {
        pen20MapInit();
        if(mapp.containsKey(index)) return (mapp.get(index)[3].contains(","+mon) || mapp.get(index)[0].equals(mon));
        else return false;
/*        pen20EvoTreeInit();
        if(map.containsKey(a)) return map.get(a).contains(b);
        else return false;*/
    }
    private static void pen20EvoTreeInit() {
        map = new HashMap<String, String>();

        map.put("tento", "kabuteri,tail,kuwaga,geko");
        map.put("gottsu", "monochro,star,torta,geko");
        map.put("otama", "geko,kuwaga,tail,star");
        map.put("goma", "ikkaku,ruka,coela,geso");
        map.put("gani", "seadra,ebidra,ruka,oct");
        map.put("shako", "oct,geso,ikkaku,seadra");
        map.put("baku", "hanu,garuru,mera,wizar");
        map.put("cand", "mera,wizar,hanu,devi");
        map.put("picodevi", "devi,bake,dokugu,wizar");
        map.put("piyo", "birdra,vdra,toge,wood");
        map.put("flora", "kiwi,toge,vdra,redveggie");
        map.put("mush", "wood,redveggie,vdra,birdra");
        map.put("pal", "toge,kiwi,redveggie,birdra");
        map.put("toyagu", "grey,revol,clock,thunderball");
        map.put("kokuwa", "clock,thunderball,revol,guardro");
        map.put("haguru", "guardro,mechanori,grey,tank");
        map.put("agu", "grey,leo,garuru,ange");
        map.put("gabu", "garuru,iga,leo,tail");
        map.put("plot", "tail,ange,grey,iga,meicoo");
        map.put("phasco", "porcupa,shadra,troop");
        map.put("imp", "madleo,shadra,troop");
        map.put("guil", "grow,arresterdra,manbo");
        map.put("_bsh_agu", "reppa,arresterdra,manbo");
        map.put("zuba", "zubaeager,target");
        map.put("hack", "baohack,target");
        map.put("draco", "_blu_coredra,_gre_coredra");
        map.put("_hak_agu", "devidra,_gre_coredra");
        map.put("terrier", "galgo");
        map.put("lop", "turuie");
        map.put("ludo", "tialudo");
        map.put("vorvo", "lavorvo");
        map.put("v", "xv,vdra");
        map.put("ryuda", "ginryu");
        map.put("lala", "sunflow");
        map.put("doru", "doruga");
        map.put("kabuteri", "atlurkabuteri,jyaga,piccolo");
        map.put("torta", "jyaga,piccolo,atlurkabuteri");
        map.put("monochro", "tricera,piccolo,angewo,tonosamageko");
        map.put("star", "tricera,piccolo,angewo,tonosamageko");
        map.put("geko", "tonosamageko,piccolo");
        map.put("kuwaga", "okuwa,tonosamageko,piccolo");
        map.put("tail", "angewo,jyaga,piccolo,angewo,holyange,metalmame");
        map.put("ruka", "zudo,wha,hangyo");
        map.put("ikkaku", "zudo,wha,anomalocari");
        map.put("seadra", "megaseadra,hangyo,wha,dago");
        map.put("coela", "megaseadra,anomalocari,wha,dago");
        map.put("oct", "marindevi,dago,anomalocari");
        map.put("geso", "marindevi,dago,hangyo");
        map.put("ebidra", "megaseadra,hangyo,wha,dago");
        map.put("hanu", "mam,weregaruru,pump");
        map.put("garuru", "weregaruru,mam,weregaruru,pump,angewo,metalmame");
        map.put("mera", "deathmera,pump,weregaruru,fanto");
        map.put("wizar", "deathmera,pump,weregaruru,fanto");
        map.put("devi", "vamde,ladydevi,pump");
        map.put("bake", "fanto,pump,ladydevi");
        map.put("dokugu", "ladydevi,vamde,pump");
        map.put("vdra", "aerovdra,garuda,delu");
        map.put("birdra", "garuda,aerovdra,delu");
        map.put("toge", "lili,delu,garuda,gerbe");
        map.put("kiwi", "blosso,delu,garuda,gerbe");
        map.put("wood", "jyurei,gerbe,delu");
        map.put("redveggie", "jyurei,gerbe,delu");
        map.put("grey", "metalgrey,cyberdra,bigmame,asura,metalmame");
        map.put("revol", "metalgrey,andro,bigmame");
        map.put("tank", "knight,bigmame,andro,waruzae");
        map.put("clock", "knight,bigmame,andro,waruzae");
        map.put("guardro", "megadra,waruzae,bigmame");
        map.put("mechanori", "megadra,waruzae,bigmame");
        map.put("thunderball", "knight,cyberdra,bigmame,waruzae");
        map.put("leo", "metalgrey,asura,metalmame");
        map.put("iga", "weregaruru,metalmame,asura");
        map.put("ange", "holyange,angewo,metalmame");
        map.put("porcupa", "asta,baal,darkknight");
        map.put("madleo", "baal,asta");
        map.put("shadra", "darkknight,asta");
        map.put("troop", "darkknight");
        map.put("grow", "megalogrow,rizegrey,beowolf");
        map.put("reppa", "beowolf,rizegrey,megalogrow");
        map.put("arresterdra", "rizegrey,megalogrow");
        map.put("manbo", "rizegrey");
        map.put("zubaeager", "dura,ete,saviorhack");
        map.put("baohack", "saviorhack,ete,dura");
        map.put("target", "ete,dura,saviorhack");
        map.put("_blu_coredra", "wingdra,groundra,megadra");
        map.put("_gre_coredra", "groundra,wingdra,megadra");
        map.put("devidra", "megadra,wingdra,groundra");
        map.put("galgo", "rapid");
        map.put("turuie", "andira");
        map.put("tialudo", "raijiludo");
        map.put("lavorvo", "lavogarita");
        map.put("xv", "paildra");
        map.put("meicoo", "_vcm_meicrack");
        map.put("ginryu", "hisyaryu");
        map.put("sunflow", "lila");
        map.put("doruga", "dorugure");
        map.put("sting", "paildra");
        map.put("atlurkabuteri", "heraklekabuteri");
        map.put("jyaga", "holydra");
        map.put("tricera", "saberleo");
        map.put("piccolo", "saberleo");
        map.put("tonosamageko", "metalete");
        map.put("okuwa", "heraklekabuteri,metalete");
        map.put("angewo", "holydra,ofani,maste");
        map.put("wha", "plesio");
        map.put("zudo", "marinange");
        map.put("megaseadra", "marinange,metalseadra");
        map.put("anomalocari", "metalseadra");
        map.put("dago", "puku");
        map.put("marindevi", "puku");
        map.put("hangyo", "plesio");
        map.put("mam", "skullmam");
        map.put("weregaruru", "skullmam,metalgaruru");
        map.put("deathmera", "bolt,skullmam");
        map.put("pump", "bolt");
        map.put("vamde", "pie,voltobauta");
        map.put("fanto", "pie");
        map.put("ladydevi", "de,maste");
        map.put("aerovdra", "ulforcevdra,houou,ulforcevdra");
        map.put("garuda", "houou");
        map.put("blosso", "griffo");
        map.put("delu", "griffo,houou");
        map.put("jyurei", "pinochi");
        map.put("gerbe", "pinochi");
        map.put("lili", "rose");
        map.put("metalgrey", "wargrey");
        map.put("andro", "mugendra");
        map.put("knight", "wargrey,metalgaruru");
        map.put("bigmame", "metalgaruru");
        map.put("megadra", "mugendra,darkdra");
        map.put("waruzae", "venomvamde");
        map.put("cyberdra", "wargrey");
        map.put("asura", "wargrey");
        map.put("metalmame", "metalgaruru");
        map.put("holyange", "seraphi");
        map.put("asta", "_rgm_belphe");
        map.put("baal", "beelzebu");
        map.put("darkknight", "_rgm_belphe,beelzebu");
        map.put("megalogrow", "duke");
        map.put("beowolf", "duft");
        map.put("rizegrey", "duke,duft");
        map.put("dura", "duranda");
        map.put("saviorhack", "jes");
        map.put("ete", "bancholeo");
        map.put("wingdra", "slayerdra");
        map.put("groundra", "breakdra");
        map.put("rapid", "saintgalgo");
        map.put("andira", "cherubi");
        map.put("raijiludo", "bryweludra");
        map.put("lavogarita", "volcanicdra");
        map.put("_vcm_meicrack", "raguel");
        map.put("hisyaryu", "ouryu");
        map.put("lila", "lotus");
        map.put("dorugure", "dorugora,alpha");
        map.put("paildra", "_ftm_imperialdra");
        map.put("de", "ogudo");
        map.put("pie", "voltobauta");
        map.put("rose", "rafflesi");
        map.put("wargrey", "omega");
        map.put("metalgaruru", "omega");
        map.put("_rgm_belphe", "ogudo");
        map.put("beelzebu", "_blm_beelzebu");
        map.put("duranda", "ragnalord");
        map.put("jes", "jesx");
        map.put("bancholeo", "chaos");
        map.put("slayerdra", "exa");
        map.put("breakdra", "exa");
        map.put("darkdra", "chaos");
        map.put("bryweludra", "ragnalord");
        map.put("raguel", "ordine");
        map.put("ouryu", "_ory_alpha");
        map.put("lotus", "rafflesi");
        map.put("alpha", "_ory_alpha");
        map.put("_ftm_imperialdra", "_plm_imperialdra");
        map.put("_fdm_ofani", "ordine");
        map.put("beelstar", "_blm_beelzebu");
        map.put("omega", "_plm_imperialdra");
    }


    // ========== PENOG ==========
    public static String generatePenOGRomV2FromBase(String digiROM, int shakes) {
        char[] rom = digiROM.toCharArray();

        shakes = (shakes > 15) ? 15 : shakes;
        String index = getPenOGIndex(digiROM),
        slot = index.substring(1,3),
        shot = index.substring(3,5);

        // Attack
        int attack = getPenOGPattern(slot, shakes); // Shakes - use Pen20 Patterns
        String atkPattern = penOGShotSizePatterns()[attack];
        Log.e("ATK Pattern", atkPattern);
        String effort = leadingZeros(Long.toBinaryString(Long.parseLong(""+rom[5]+rom[6], 16)), 8).substring(0, 7);
        Log.e("Effort", effort);
        char[] efrtAtkHex = leadingZeros(Long.toHexString(Long.parseLong(effort+atkPattern, 2)), 3).toCharArray();
        Log.e("Effort+Attack Hex", ":"+new String(efrtAtkHex));

        char[] shotHex = shot.toCharArray();

        rom[5] = efrtAtkHex[0];
        rom[6] = efrtAtkHex[1];
        rom[7] = efrtAtkHex[2];

        //rom[16] = shotHex[0];
        //rom[17] = shotHex[1];

        return new String(rom).toUpperCase();
    }
    public static String generatePenOGForcedV1FromBase(String digiROM, boolean win) {
        char[] rom = (digiROM.substring(0, digiROM.length()-5)+"-@BABF").toCharArray();

        int shakes = win ? 0: 15;
        String  hp = win ? "00" : "1F",
                index = getPenOGIndex(digiROM);

//        atkHex = win ? new char[] {'0', '0'} : new char[] {'1', 'F'};
//        Log.e("C", new String(atkHex));

        String  slot = index.substring(1,3),
                shot = index.substring(3,5);

        // Attack
        int attack = getPenOGPattern(slot, shakes); // Shakes - use Pen20 Patterns
        String atkPattern = penOGShotSizePatterns()[attack];
        String effort = leadingZeros(Long.toBinaryString(Long.parseLong(""+rom[5]+rom[6], 16)), 8).substring(0, 7);
        char[] efrtAtkHex = leadingZeros(Long.toHexString(Long.parseLong(effort+atkPattern, 2)), 3).toCharArray();
        Log.e("EAHex", ":"+new String(efrtAtkHex));

        // Shot
        //char[] shotHex = shot.toCharArray();

        // Hit Pattern
        String  cou = leadingZeros(Long.toBinaryString(Long.parseLong(""+rom[10]+rom[11], 16)), 8).substring(0, 7),
                hpBin = leadingZeros(Long.toBinaryString(Long.parseLong(hp, 16)), 8).substring(3, 8);
        Log.e("COU", cou);
        Log.e("HpBin", hpBin);
        char[]  hpHex = leadingZeros(Long.toHexString(Long.parseLong(cou+hpBin, 2)), 3).toCharArray();
        Log.e("HpHex", ":"+new String(hpHex));

        rom[5] = efrtAtkHex[0];
        rom[6] = efrtAtkHex[1];
        rom[7] = efrtAtkHex[2];

        rom[10] = hpHex[0];
        rom[11] = hpHex[1];
        rom[12] = hpHex[2];

        //rom[17] = shotHex[0];
        //rom[18] = shotHex[1];

        Log.e("Final Rom", new String(rom).toUpperCase());
        return "V1-" + new String(rom).toUpperCase();
    }

//    public static String generatePenOGRomV2FromBase(String digiROM, int shakes) {
//        shakes = (shakes > 15) ? 15 : shakes;
//        char[] rom = digiROM.toCharArray();
//
//        String index = getPenOGIndex(digiROM);
//        //PenOG does not have name
//
//        String version = index.substring(0,1);
//        String slot = index.substring(1,3);
//        String shot = index.substring(3,5);
//        String[] chIndex = new String[3];
//        chIndex[0] = version;
//        chIndex[1] = slot;
//        chIndex[2] = shot;
//
//        //Version
//        char[] verHex = new BigInteger(000 + version).toString(16).toCharArray();
//
//        //Slot
//        char[] slotHex = new BigInteger(000 + slot).toString(10).toCharArray();
//
//        // Attack
//        int attack = getPenOGPattern(slot, shakes); // Shakes - use Pen20 Patterns
//        Log.e("V2 A", ":"+attack);
//
//        String atkPattern = penOGShotSizePatterns()[attack];
//        Log.e("V2 B", atkPattern);
//
//        char[] atkHex = leadingZeros(new BigInteger(atkPattern).toString(16), 2).toCharArray();
//        Log.e("V2 C", ": "+new String(atkHex));
//
//        //Shot
//        char[] shotHex = new BigInteger(shot).toString(10).toCharArray();
//
//        rom[0] = verHex[0];
//
//        rom[1] = slotHex[0];
//        rom[2] = slotHex[1];
//
//        rom[6] = atkHex[0];
//        rom[7] = atkHex[1];
//
//        rom[16] = shotHex[0];
//        rom[17] = shotHex[1];
//
//        return new String(rom).toUpperCase();
//    }
//    public static String generatePenOGForcedV1FromBase(String digiROM, boolean win) {
//        Log.e("Input", digiROM);
//        char[] rom = (digiROM.substring(0, digiROM.length()-5)+"-@BABF").toCharArray();
//
//        int shakes;
//        String hp, index = getPenOGIndex(digiROM);
//        if(win) {
//            shakes = 0;
//            hp = "F0";
//        }
//        else {
//            shakes = 15;
//            hp = "1F";
//        }
//
//        String version = index.substring(0,1);
//        String slot = index.substring(1,3);
//        String shot = index.substring(3,5);
//        String[] chIndex = new String[3];
//        chIndex[0] = version;
//        chIndex[1] = slot;
//        chIndex[2] = shot;
//
//        //Version
//        char[] verHex = new BigInteger(000 + version).toString(16).toCharArray();
//
//        //Slot
//        char[] slotHex = new BigInteger(000 + slot).toString(10).toCharArray();
//
//
//        // Attack
//        int attack = getPenOGPattern(slot, shakes); // Shakes - use Pen20 Patterns
//        Log.e("A", ":"+attack);
//
//        String atkPattern = penOGShotSizePatterns()[attack];
//        Log.e("B", atkPattern);
//
//        char[] atkHex = leadingZeros(new BigInteger(atkPattern).toString(16), 2).toCharArray();
//
//
//        atkHex = win ? new char[] {'0', '0'} : new char[] {'1', 'F'};
//        Log.e("C", new String(atkHex));
//
//
//        //Shot
//        char[] shotHex = new BigInteger(shot).toString(10).toCharArray();
//
//        char[] hpHex = hp.toCharArray();
//
//        rom[0] = verHex[0];
//
//        rom[1] = slotHex[0];
//        rom[2] = slotHex[1];
//
//        rom[6] = atkHex[0];
//        rom[7] = atkHex[1];
//
//        rom[11] = hpHex[0];
//        rom[12] = hpHex[1];
//
//        rom[17] = shotHex[0];
//        rom[18] = shotHex[1];
//
//        Log.e("Final Rom", new String(rom).toUpperCase());
//        return "V1-" + new String(rom).toUpperCase();
//    }

/*    public static String generatePenOGRomV2(String index, int shakes, int hitpattern) {
        char[] rom = new String("211F-000F-080F-EABF").toCharArray();

        //PenOG does not have name

        String version = index.substring(0,1);
        String slot = index.substring(1,3);
        String shot = index.substring(3,5);
        String[] chIndex = new String[3];
        chIndex[0] = version;
        chIndex[1] = slot;
        chIndex[2] = shot;

        //Version
        char[] verHex = new BigInteger(000 + version).toString(16).toCharArray();

        //Slot
        char[] slotHex = new BigInteger(000 + slot).toString(10).toCharArray();

        //Effort
        //Effort on PenOG is split out into two bits; Effort 2 and Effort 1, which when concatenated, make the effort value (eg E2=0, E1=4 means Effort=40)
        //char[] efHex = leadingZeros(Integer.toHexString(effort), 2).toCharArray();
        //String ef2 = new BigInteger(000 + slot).toString(2);

        // Attack
        int attack = getPenOGPattern(slot, shakes); // Shakes - use Pen20 Patterns
        penOGShotSizePatterns();
        String atkPattern = shotSizePatterns[attack];
        char[] atkHex = new BigInteger(atkPattern).toString(16).toCharArray();


        //Shot
        char[] shotHex = new BigInteger(shot).toString(10).toCharArray();

        // HitPattern
        char[] hitHex = leadingZeros(Integer.toHexString(hitpattern), 2).toCharArray();

        rom[0] = verHex[0];

        rom[1] = slotHex[0];
        rom[2] = slotHex[1];

        rom[6] = atkHex[0];
        rom[7] = atkHex[1];

        rom[11] = hitHex[0];
        rom[12] = hitHex[1];

        rom[16] = shotHex[0];
        rom[17] = shotHex[1];


        return new String(rom).toUpperCase();
    }
    public static String generatePenOGRomV2(String index, int shakes) {
        char[] rom = new String("211F-000F-080F-EABF").toCharArray();

        //PenOG does not have name

        String version = index.substring(0,1);
        String slot = index.substring(1,3);
        String shot = index.substring(3,5);
        String[] chIndex = new String[3];
        chIndex[0] = version;
        chIndex[1] = slot;
        chIndex[2] = shot;

        //Version
        char[] verHex = new BigInteger(000 + version).toString(16).toCharArray();

        //Slot
        char[] slotHex = new BigInteger(000 + slot).toString(10).toCharArray();

        //Effort
        //Effort on PenOG is split out into two bits; Effort 2 and Effort 1, which when concatenated, make the effort value (eg E2=0, E1=4 means Effort=40)
        //char[] efHex = leadingZeros(Integer.toHexString(effort), 2).toCharArray();
        //String ef2 = new BigInteger(000 + slot).toString(2);

        // Attack
        int attack = getPenOGPattern(slot, shakes); // Shakes - use Pen20 Patterns
        penOGShotSizePatterns();
        String atkPattern = shotSizePatterns[attack];
        char[] atkHex = new BigInteger(atkPattern).toString(16).toCharArray();


        //Shot
        char[] shotHex = new BigInteger(shot).toString(10).toCharArray();

        rom[0] = verHex[0];

        rom[1] = slotHex[0];
        rom[2] = slotHex[1];

        rom[6] = atkHex[0];
        rom[7] = atkHex[1];

        rom[16] = shotHex[0];
        rom[17] = shotHex[1];

        return new String(rom).toUpperCase();
    }
    public static String generatePenOGForcedV1(String index, boolean win) {
        char[] rom = new String("211F-000F-080F-EABF").toCharArray();

        int shakes, power;
        String hp;
        if(win) {
            shakes = 0;
            power = 0;
            hp = "F0";
        } else {
            shakes = 31;
            power = 255;
            hp = "0F";
        }

        //PenOG does not have name

        String version = index.substring(0,1);
        String slot = index.substring(1,3);
        String shot = index.substring(3,5);
        String[] chIndex = new String[3];
        chIndex[0] = version;
        chIndex[1] = slot;
        chIndex[2] = shot;

        //Version
        char[] verHex = new BigInteger(000 + version).toString(16).toCharArray();

        //Slot
        char[] slotHex = new BigInteger(000 + slot).toString(10).toCharArray();

        //Effort
        //Effort on PenOG is split out into two bits; Effort 2 and Effort 1, which when concatenated, make the effort value (eg E2=0, E1=4 means Effort=40)
        //char[] efHex = leadingZeros(Integer.toHexString(effort), 2).toCharArray();
        //String ef2 = new BigInteger(000 + slot).toString(2);

        // Attack
        int attack = getPenOGPattern(slot, shakes); // Shakes - use Pen20 Patterns
        penOGShotSizePatterns();
        String atkPattern = shotSizePatterns[attack];
        char[] atkHex = new BigInteger(atkPattern).toString(16).toCharArray();


        //Shot
        char[] shotHex = new BigInteger(shot).toString(10).toCharArray();

        char[] hpHex = hp.toCharArray();

        rom[0] = verHex[0];

        rom[1] = slotHex[0];
        rom[2] = slotHex[1];

        rom[6] = atkHex[0];
        rom[7] = atkHex[1];

        rom[11] = hpHex[0];
        rom[12] = hpHex[1];

        rom[16] = shotHex[0];
        rom[17] = shotHex[1];

        return "V1-" + new String(rom).toUpperCase();
    }*/

    public static String getPenOGIndex(String rom) {
        String version = rom.substring(0, 1);
        String slot = rom.substring(1, 3);
        String shot = rom.substring(16, 18);

        return "" + version + slot + shot;
    }
//    public static String getPenOGMonFromRom(String rom) {
//        map = new HashMap<String, String>();
//        penOGIndexInit();
//
//        String version = rom.substring(0, 1);
//        String slot = rom.substring(1, 3);
//        String shot = rom.substring(16, 18);
//
//        String index = "" + version + slot + shot;
//
//        if(map.containsKey(index)) return map.get(index);
//        else if (map.containsKey("" + version + slot + "00")) return map.get("" + version + slot + "00"); //as a few shots are missing, we may need to check for shot = 00
//        else return "err";
//    }
//    public static String getPenOGMonFromIndex(String index) {
//        map = new HashMap<String, String>();
//        penOGIndexInit();
//
//        //as a few shots are missing, we may need to check for shot = 00
//        String version = index.substring(0,1);
//        String slot = index.substring(1,3);
//
//        if(map.containsKey(index)) return map.get(index);
//        else if (map.containsKey("" + version + slot + "00")) return map.get("" + version + slot + "00");
//        else return "err";
//
//    }
    public static String getPenOGHitPattern(String rom) {
        int hp = (Integer.parseInt(rom.substring(11, 13), 16));
        String bits = (Integer.toBinaryString(hp));
        bits = leadingZeros(bits, 8);

        bits = bits.substring(3, 8);
        return bits;
    }
    public static int getPenOGHitPatternInt(String rom) {
        int hp = (Integer.parseInt(rom.substring(11, 13), 16));
        return hp;
    }
    public static String getPenOGAttackPattern(String rom) {
        int atk = (Integer.parseInt(rom.substring(6, 8), 16));
        String bits = (Integer.toBinaryString(atk));
        bits = leadingZeros(bits, 8);

        bits = bits.substring(3, 8);
        return bits;
    }
    public static Integer getPenOGEffort1(String rom) {
        int atk = (Integer.parseInt(rom.substring(6, 7), 16));
        String bits = (Integer.toBinaryString(atk));
        bits = leadingZeros(bits, 3);
        //atk = (Integer.parseInt(bits, 10));

        return (Integer.parseInt(bits, 2));
    }
    public static Integer getPenOGEffort2(String rom) {
        int atk = (Integer.parseInt(rom.substring(5, 6), 16));
        String bits = (Integer.toBinaryString(atk));
        bits = leadingZeros(bits, 4);
        //atk = (Integer.parseInt(bits, 10));
        return (Integer.parseInt(bits, 2));
    }
    public static int getPenOGPattern(String slot , int shakes) {
        int score = 0;

        //Use Pen20 patterns for PenOG
        //A Digimon's score is depending on a Digimon's attribute and level, as well as shake
        //After reading the attribute and level, look to see what the corresponding 'score' is when matching with the number of shakes
        if (shakes == 0)
            return score;
        else {
            if (slot == "03" || slot == "04" || slot == "05") {
                score = Pen20ChildScores(shakes);
            }
            else if (slot == "06" || slot == "07") {
                score = Pen20VaFreeAdultScores(shakes); //no free on PenOG
            }
            else if (slot == "08" || slot == "09") {
                score = Pen20DaAdultScores(shakes);
            }
            else if (slot == "0A" || slot == "0B") {
                score = Pen20ViAdultScores(shakes);
            }
            else if (slot == "0D" || slot == "0C") {
                score = Pen20VaPerfectScores(shakes);
            }
            else if (slot == "0E" || slot == "0F") {
                score = Pen20DaFrPerfectScores(shakes);
            }
            else if (slot == "10" || slot == "11") {
                score = Pen20ViPerfectScores(shakes);
            }
            else if (slot == "12") {
                score = Pen20VaUltimateScores(shakes);
            }
            else if (slot == "13") {
                score = Pen20DaUltimateScores(shakes);
            }
            else if (slot == "14") {
                score = Pen20ViFrUltimateScores(shakes);
            }
            else {
                score = Pen20ChildScores(shakes);
            }
        }

        return score;
    }
    private static String[] penOGShotSizePatterns() {
        return new String[]{
                "00000",
                "00100",
                "01010",
                "10010",
                "10100",
                "10010",
                "11001",
                "01011",
                "10110",
                "10110",
                "10101",
                "11110",
                "10111",
                "11111",
                "11111"};
    }

    private static void penOGMapInit() {
        mapp = new HashMap<String, String[]>();
        mapp.put("00281", new String[] {"mochi", "Baby II", "11", ",tento,gottsu,otama"});
        mapp.put("00382", new String[] {"tento", "Child", "00", ",kabuteri,torta,tail,kuwaga,geko"});
        mapp.put("00400", new String[] {"gottsu", "Child", "01", ",torat,tail,monochro,star,geko"});
        mapp.put("00584", new String[] {"otama", "Child", "10", ",torta,tail,star,kuwaga,geko"});
        mapp.put("006E1", new String[] {"kabuteri", "Adult", "00", ",atlurkabuteri,jyaga,angewo,piccolo"});
        mapp.put("00700", new String[] {"tail", "Adult", "00", ",atlurkabuteri,angewo,piccolo"});
        mapp.put("00786", new String[] {"torta", "Adult", "00", ",atlurkabuteri,jyaga,piccolo"});
        mapp.put("00800", new String[] {"monochro", "Adult", "01", ",tricera,piccolo,jyaga,angewo,tonosamageko"});
        mapp.put("00900", new String[] {"star", "Adult", "01", ",tricera,piccolo,jyaga,angewo,tonosamageko"});
        mapp.put("00AE2", new String[] {"kuwaga", "Adult", "10", ",okuwa,tonosamageko,piccolo"});
        mapp.put("00B00", new String[] {"geko", "Adult", "10", ",okuwa,tonosamageko,piccolo"});
        mapp.put("00C00", new String[] {"alturkabuteri", "Perfect", "00", ",heraklekabuteri,holydra"});
        mapp.put("00D00", new String[] {"angewo", "Perfect", "00", ""});
        mapp.put("00D8C", new String[] {"jyaga", "Perfect", "00", ""});
        mapp.put("00E00", new String[] {"tricera", "Perfect", "01", ",saberleo"});
        mapp.put("00FE6", new String[] {"piccolo", "Perfect", "01", ",saberleo"});
        mapp.put("01085", new String[] {"ookuwa", "Perfect", "10", ",heraklekabuteri,holydra,metalete"});
        mapp.put("01100", new String[] {"tonosamageko", "Perfect", "10", ",metalete"});
        mapp.put("01289", new String[] {"heraklekabuteri", "Ultimate", "00", ""});
        mapp.put("01200", new String[] {"holydra", "Ultimate", "01", ""});
        mapp.put("01390", new String[] {"saberleo", "Ultimate", "01", ""});
        mapp.put("01491", new String[] {"metalete", "Ultimate", "10", ""});
        mapp.put("10192", new String[] {"pitch", "Baby I", "11", ",puka"});
        mapp.put("10281", new String[] {"puka", "Baby II", "11", ",goma,gani,shako"});
        mapp.put("10394", new String[] {"goma", "Child", "00", ",ikkaku,ruka,coela,ebidra,oct"});
        mapp.put("10496", new String[] {"gani", "Child", "01", ",ruka,seadra,coela,ebidra,geso"});
        mapp.put("10593", new String[] {"shako", "Child", "10", ",ikkaku,seadra,geso,oct"});
        mapp.put("10697", new String[] {"ikkaku", "Adult", "00", ",zudo,wha,anomalocari,hangyo"});
        mapp.put("107EA", new String[] {"ruka", "Adult", "00", ",zudo,wha,anomalocari,hangyo"});
        mapp.put("108E3", new String[] {"seadra", "Adult", "01", ",megaseadra,anomalocari,hangyo,wha"});
        mapp.put("1099A", new String[] {"ebidra", "Adult", "01", ",megaseadra,anomalocari,wha,dago"});
        mapp.put("109E3", new String[] {"coela", "Adult", "01", ",megaseadra,anomalocari,wha,dago"});
        mapp.put("10A00", new String[] {"geso", "Adult", "10", ",marindevi,dago,anomalocari,hangyo"});
        mapp.put("10B99", new String[] {"oct", "Adult", "10", ",marindevi,dago,anomalocari,hangyo"});
        mapp.put("10C9C", new String[] {"zudo", "Perfect", "00", ",marinange"});
        mapp.put("10DE8", new String[] {"wha", "Perfect", "00", ""});
        mapp.put("10E00", new String[] {"megaseadra", "Perfect", "01", ",metalseadra,plesio,marinange"});
        mapp.put("10F9A", new String[] {"anomalocari", "Perfect", "01", ",metalseadra"});
        mapp.put("10FA1", new String[] {"hangyo", "Perfect", "01", ",plesio"});
        mapp.put("1109E", new String[] {"marindevi", "Perfect", "10", ",puku"});
        mapp.put("111A1", new String[] {"dago", "Perfect", "10", ",puku"});
        mapp.put("11200", new String[] {"marinange", "Ultimate", "00", ""});
        mapp.put("11389", new String[] {"metalseadra", "Ultimate", "01", ""});
        mapp.put("113D7", new String[] {"plesio", "Ultimate", "01", ""});
        mapp.put("114A8", new String[] {"puku", "Ultimate", "10", ""});
        mapp.put("20100", new String[] {"moku", "Baby I", "11", ",petimera"});
        mapp.put("202A4", new String[] {"petimera", "Baby II", "11", ",baku,cand,picodevi"});
        mapp.put("20300", new String[] {"baku", "Child", "00", ",hanu,garuru,mera,wizar"});
        mapp.put("20400", new String[] {"cand", "Child", "01", ",hanu,mera,wizar,devi"});
        mapp.put("20500", new String[] {"picodevi", "Child", "10", ",mera,wizar,devi,bake,dokugu"});
        mapp.put("20600", new String[] {"hanu", "Adult", "00", ",mam,weregaruru,pump"});
        mapp.put("207E1", new String[] {"garuru", "Adult", "00", ",mam,weregaruru,pump"});
        mapp.put("20800", new String[] {"mera", "Adult", "01", ",deathmera,pump,weregaruru,fanto"});
        mapp.put("20900", new String[] {"wizar", "Adult", "01", ",deathmera,pump,weregaruru,fanto"});
        mapp.put("20A00", new String[] {"devi", "Adult", "10", ",vamde,ladydevi,fanto,pump"});
        mapp.put("20BA9", new String[] {"bake", "Adult", "10", ",vamde,fanto,pump"});
        mapp.put("20BA2", new String[] {"dokugu", "Adult", "10", ",ladydevi,fanto,pump"});
        mapp.put("20C00", new String[] {"mam", "Perfect", "00", ",skullmam"});
        mapp.put("20D00", new String[] {"weregaruru", "Perfect", "00", ""});
        mapp.put("20E00", new String[] {"deathmera", "Perfect", "01", ",sullmam,bolt"});
        mapp.put("20FAC", new String[] {"pump", "Perfect", "01", ",bolt"});
        //mapp.put("210AA", new String[] {"vamde", "Perfect", "10", ",pie"});
        mapp.put("210AA", new String[] {"ladydevi", "Perfect", "10", ",de"});
        mapp.put("211AB", new String[] {"fanto", "Perfect", "10", ",pie,de"});
        mapp.put("21200", new String[] {"skullmam", "Ultimate", "00", ""});
        mapp.put("21300", new String[] {"bolt", "Ultimate", "01", ""});
        mapp.put("214B4", new String[] {"pie", "Ultimate", "10", ""});
        mapp.put("2149C", new String[] {"de", "Ultimate", "10", ""});
        mapp.put("301B6", new String[] {"nyoki", "Baby I", "11", ",pyoco"});
        mapp.put("302B7", new String[] {"pyoco", "Baby II", "11", ",piyo,pal,flora"});
        mapp.put("303DF", new String[] {"piyo", "Child", "00", ",vdra,birdra,toge,wood"});
        mapp.put("30400", new String[] {"pal", "Child", "01", ",toge,kiwi,wood,redveggie"});
        mapp.put("30484", new String[] {"flora", "Child", "01", ",toge,kiwi,wood,redveggie"});
        mapp.put("305C4", new String[] {"mush", "Child", "10", ",vdra,birdra,wood,redveggie"});
        mapp.put("306C5", new String[] {"vdra", "Adult", "00", ",aerovdra,garuda,delu"});
        mapp.put("307BA", new String[] {"birdra", "Adult", "00", ",aerovdra,garuda,delu"});
        mapp.put("308BD", new String[] {"toge", "Adult", "01", ",blosso,lili,delu,garuda"});
        mapp.put("309BF", new String[] {"kiwi", "Adult", "01", ",blosso,lili,delu,garuda"});
        mapp.put("30ABC", new String[] {"wood", "Adult", "10", ",jyurei,gerbe,delu"});
        mapp.put("30BA6", new String[] {"redveggie", "Adult", "10", ",jyurei,gerbe,delu"});
        mapp.put("30CD1", new String[] {"aerovdra", "Perfect", "00", ",houou"});
        mapp.put("30D00", new String[] {"garuda", "Perfect", "00", ""});
        mapp.put("30E00", new String[] {"blosso", "Perfect", "01", ",griffo"});
        mapp.put("30E89", new String[] {"lili", "Perfect", "01", ",rose"});
        mapp.put("30FB0", new String[] {"delu", "Perfect", "01", ",houou,griffo,rose"});
        mapp.put("31000", new String[] {"jyurei", "Perfect", "10", ",pinocchi"});
        mapp.put("311E4", new String[] {"gerbe", "Perfect", "10", ",pinocchi"});
        mapp.put("312C3", new String[] {"houou", "Ultimate", "00", ""});
        mapp.put("313A0", new String[] {"rose", "Ultimate", "01", ""});
        mapp.put("313D7", new String[] {"griffo", "Ultimate", "01", ""});
        mapp.put("314C6", new String[] {"pinocchi", "Ultimate", "10", ""});
        mapp.put("40100", new String[] {"choro", "Baby I", "11", ",capri"});
        mapp.put("40200", new String[] {"capri", "Baby II", "11", ",toyagu,kokuwa,haguru"});
        mapp.put("403DF", new String[] {"toyagu", "Child", "00", ",grey,revol,tank,thunderball,clock"});
        mapp.put("4049A", new String[] {"kokuwa", "Child", "01", ",revol,tank,thunderball,clock,guardro"});
        mapp.put("405CC", new String[] {"haguru", "Child", "10", ",grey,tank,thunderball,guardro,mechanori"});
        mapp.put("406E1", new String[] {"grey", "Adult", "00", ",metalgrey,andro,cyberdra,bigmame"});
        mapp.put("407CA", new String[] {"revol", "Adult", "00", ",metalgrey,andro,cyberdra,bigmame"});
        mapp.put("408E9", new String[] {"tank", "Adult", "01", ",knight,bigmame,andro,warumonzae"});
        mapp.put("408B2", new String[] {"thunderball", "Adult", "01", ",knight,bigmame,cyberdra,warumonzae"});
        mapp.put("409D2", new String[] {"clock", "Adult", "01", ",knight,bi,andro,cyberdra,warumonzae"});
        mapp.put("40AD6", new String[] {"guardro", "Adult", "10", ",megadra,warumonzae,bigmame"});
        mapp.put("40BD7", new String[] {"mechanori", "Adult", "10", ",megadra,warumonzae,bigmame"});
        mapp.put("40CE5", new String[] {"metalgrey", "Perfect", "00", ",wargrey"});
        mapp.put("40D00", new String[] {"andro", "Perfect", "00", ""});
        mapp.put("40D90", new String[] {"cyberdra", "Perfect", "00", ""});
        mapp.put("40E00", new String[] {"knight", "Perfect", "01", ",wargrey,metalgaruru"});
        mapp.put("40FDC", new String[] {"bigmame", "Perfect", "01", ",metalgaruru"});
        mapp.put("41000", new String[] {"megadra", "Perfect", "10", ",mugendra,venomvamde"});
        mapp.put("411D9", new String[] {"warumonzae", "Perfect", "10", ",mugendra,venomvamde"});
        mapp.put("412C1", new String[] {"wargrey", "Ultimate", "00", ""});
        mapp.put("4139C", new String[] {"metalgaruru", "Ultimate", "01", ""});
        mapp.put("41489", new String[] {"mugendra", "Ultimate", "10", ""});
        mapp.put("414AE", new String[] {"venomvamde", "Ultimate", "10", ""});
        mapp.put("50100", new String[] {"yukimibota", "Baby I", "11", ",nyaro"});
        mapp.put("50200", new String[] {"nyaro", "Baby II", "11", ",ago,gabu,plot"});
        mapp.put("50300", new String[] {"agu", "Child", "00", ",grey,leo,garuru,ange"});
        mapp.put("50400", new String[] {"gabu", "Child", "01", ",leo,garuru,iga,tail"});
        mapp.put("50500", new String[] {"plot", "Child", "10", ",grey,iga,ange,tail"});
        mapp.put("50600", new String[] {"grey", "Adult", "00", ",metalgrey,asura,metalmame"});
        mapp.put("50700", new String[] {"leo", "Adult", "00", ",metalgrey,asura,metalmame"});
        mapp.put("50800", new String[] {"garuru", "Adult", "01", ",weregaruru,metalmame,asura,angewo"});
        mapp.put("50900", new String[] {"iga", "Adult", "01", ",weregaruru,metalmame,asura,angewo"});
        mapp.put("50A00", new String[] {"ange", "Adult", "10", ",holyange,angewo,metalmame"});
        mapp.put("50B00", new String[] {"tail", "Adult", "10", ",holyange,angewo,metalmame"});
        mapp.put("50C00", new String[] {"metalgrey", "Perfect", "00", ",wargrey"});
        mapp.put("50DE2", new String[] {"asura", "Perfect", "00", ",wargrey"});
        mapp.put("50E00", new String[] {"weregaruru", "Perfect", "01", ",metalgaruru"});
        mapp.put("50F00", new String[] {"metalmame", "Perfect", "01", ",metalgaruru"});
        mapp.put("51000", new String[] {"holyange", "Perfect", "10", ",omega"});
        mapp.put("51100", new String[] {"angewo", "Perfect", "10", ",omega"});
        mapp.put("51200", new String[] {"wargrey", "Ultimate", "00", ",omega"});
        mapp.put("51300", new String[] {"metalgaruru", "Ultimate", "01", ",omega"});
        mapp.put("514CB", new String[] {"omega", "Ultimate", "10", ""});

    }
    private static void penOGIndexInit() {
        map = new HashMap<String, String>();
        map.put("00281","Mochimon");
        map.put("00382","Tentomon");
        map.put("00400","Gottsumon");
        map.put("00584","Otamamon");
        map.put("006E1","Kabuterimon");
        map.put("00700","Tailmon");
        map.put("00786","Tortamon");
        map.put("00800","Monochromon");
        map.put("00900","Starmon");
        map.put("00AE2","Kuwagamon");
        map.put("00B00","Gekomon");
        map.put("00C00","AlturKabuterimon (Blue)");
        map.put("00D00","Angewomon");
        map.put("00D8C","Jyagamon");
        map.put("00E00","Triceramon");
        map.put("00FE6","Piccolomon");
        map.put("01085","Ookuwamon");
        map.put("01100","TonosamaGekomon");
        map.put("01289","HerakleKabuterimon");
        map.put("01200","Holydramon");
        map.put("01390","SaberLeomon");
        map.put("01491","MetalEtemon");
        map.put("10192","Pitchmon");
        map.put("10281","Pukamon");
        map.put("10394","Gomamon");
        map.put("10496","Ganimon");
        map.put("10593","Shakomon");
        map.put("10697","Ikkakumon");
        map.put("107EA","Rukamon");
        map.put("108E3","Seadramon");
        map.put("1099A","Ebidramon");
        map.put("109E3","Coelamon");
        map.put("10A00","Gesomon");
        map.put("10B99","Octmon");
        map.put("10C9C","Zudomon");
        map.put("10DE8","Whamon (Perfect)");
        map.put("10E00","MegaSeadramon");
        map.put("10F9A","Anomalocarimon");
        map.put("10FA1","Hangyomon");
        map.put("1109E","MarinDevimon");
        map.put("111A1","Dagomon");
        map.put("11200","MarinAngemon");
        map.put("11389","MetalSeadramon");
        map.put("113D7","Plesiomon");
        map.put("114A8","Pukumon");
        map.put("20100","Mokumon");
        map.put("202A4","PetiMeramon");
        map.put("20300","Bakumon");
        map.put("20400","Candmon");
        map.put("20500","PicoDevimon");
        map.put("20600","Hanumon");
        map.put("207E1","Garurumon");
        map.put("20800","Meramon");
        map.put("20900","Wizarmon");
        map.put("20A00","Devimon");
        map.put("20BA9","Bakemon");
        map.put("20BA2","Dokugumon");
        map.put("20C00","Mammon");
        map.put("20D00","WereGarurumon");
        map.put("20E00","DeathMeramon");
        map.put("20FAC","Pumpmon");
        map.put("210AA","Vamdemon");
        //map.put("210AA","LadyDevimon"); Lady Devimon and Vamdemon have the same version, index, and shot
        map.put("211AB","Fantomon");
        map.put("21200","SkullMammon");
        map.put("21300","Boltmon");
        map.put("214B4","Piemon");
        map.put("2149C","Demon");
        map.put("301B6","Nyokimon");
        map.put("302B7","Pyocomon");
        map.put("303DF","Piyomon");
        map.put("30400","Palmon");
        map.put("30484","Floramon");
        map.put("305C4","Mushmon");
        map.put("306C5","V-dramon");
        map.put("307BA","Birdramon");
        map.put("308BD","Togemon");
        map.put("309BF","Kiwimon");
        map.put("30ABC","Woodmon");
        map.put("30BA6","redveggiemon");
        map.put("30CD1","AeroV-dramon");
        map.put("30D00","Garudamon");
        map.put("30E00","Blossomon");
        map.put("30E89","Lilimon");
        map.put("30FB0","Delumon");
        map.put("31000","Jyureimon");
        map.put("311E4","Gerbemon");
        map.put("312C3","Hououmon");
        map.put("313A0","Rosemon");
        map.put("313D7","Griffomon");
        map.put("314C6","Pinocchimon");
        map.put("40100","Choromon");
        map.put("40200","Caprimon");
        map.put("403DF","ToyAgumon");
        map.put("4049A","Kokuwamon");
        map.put("405CC","Hagurumon");
        map.put("406E1","Greymon");
        map.put("407CA","Revolmon");
        map.put("40800","Tankmon");
        map.put("408B2","Thunderballmon");
        map.put("409D2","Clockmon");
        map.put("40AD6","Guardromon");
        map.put("40BD7","Mechanorimon");
        map.put("40CE5","MetalGreymon");
        map.put("40D00","Andromon");
        map.put("40D90","Cyberdramon");
        map.put("40E00","Knightmon");
        map.put("40FDC","BigMamemon");
        map.put("41000","Megadramon");
        map.put("411D9","WaruMonzaemon");
        map.put("412C1","WarGreymon");
        map.put("4139C","MetalGarurumon");
        map.put("41489","Mugendramon");
        map.put("414AE","VenomVamdemon");
        map.put("50100","YukimiBotamon");
        map.put("50200","Nyaromon");
        map.put("50300","Agumon");
        map.put("50400","Gabumon");
        map.put("50500","Plotmon");
        map.put("50600","Greymon");
        map.put("50700","Leomon");
        map.put("50800","Garurumon");
        map.put("50900","Igamon");
        map.put("50A00","Angemon");
        map.put("50B00","Tailmon");
        map.put("50C00","MetalGreymon");
        map.put("50DE2","Asuramon");
        map.put("50E00","WereGarurumon");
        map.put("50F00","MetalMamemon");
        map.put("51000","HolyAngemon");
        map.put("51100","Angewomon");
        map.put("51200","WarGreymon");
        map.put("51300","MetalGarurumon");
        map.put("514CB","Omegamon");
    }
    private static void penOGAttributeInit() {
        map = new HashMap<String, String>();

        map.put("00180","11" );
        map.put("00281","11" );
        map.put("00382","00" );
        map.put("00400","01" );
        map.put("00584","10" );
        map.put("006E1","00" );
        map.put("00700","00" );
        map.put("00786","00" );
        map.put("00800","01" );
        map.put("00900","01" );
        map.put("00AE2","10" );
        map.put("00B00","10" );
        map.put("00C00","00" );
        map.put("00D00","00" );
        map.put("00D8C","00" );
        map.put("00E00","01" );
        map.put("00FE6","01" );
        map.put("01085","10" );
        map.put("01100","10" );
        map.put("01289","00" );
        map.put("01200","01" );
        map.put("01390","01" );
        map.put("01491","10" );
        map.put("10192","11" );
        map.put("10281","11" );
        map.put("10394","00" );
        map.put("10496","01" );
        map.put("10593","10" );
        map.put("10697","00" );
        map.put("107EA","00" );
        map.put("108E3","01" );
        map.put("1099A","01" );
        map.put("109E3","01" );
        map.put("10A00","10" );
        map.put("10B99","10" );
        map.put("10C9C","00" );
        map.put("10DE8","00" );
        map.put("10E00","01" );
        map.put("10F9A","01" );
        map.put("10FA1","01" );
        map.put("1109E","10" );
        map.put("111A1","10" );
        map.put("11200","00" );
        map.put("11389","01" );
        map.put("113D7","01" );
        map.put("114A8","10" );
        map.put("20100","11" );
        map.put("202A4","11" );
        map.put("20300","00" );
        map.put("20400","01" );
        map.put("20500","10" );
        map.put("20600","00" );
        map.put("207E1","00" );
        map.put("20800","01" );
        map.put("20900","01" );
        map.put("20A00","10" );
        map.put("20BA9","10" );
        map.put("20BA2","10" );
        map.put("20C00","00" );
        map.put("20D00","00" );
        map.put("20E00","01" );
        map.put("20FAC","01" );
        //map.put("210AA","10" ); Lady Devimon and Vamdemon have the same version, index, and shot
        map.put("210AA","10" );
        map.put("211AB","10" );
        map.put("21200","00" );
        map.put("21300","01" );
        map.put("214B4","10" );
        map.put("2149C","10" );
        map.put("301B6","11" );
        map.put("302B7","11" );
        map.put("303DF","00" );
        map.put("30400","01" );
        map.put("30484","01" );
        map.put("305C4","10" );
        map.put("306C5","00" );
        map.put("307BA","00" );
        map.put("308BD","01" );
        map.put("309BF","01" );
        map.put("30ABC","10" );
        map.put("30BA6","10" );
        map.put("30CD1","00" );
        map.put("30D00","00" );
        map.put("30E00","01" );
        map.put("30E89","01" );
        map.put("30FB0","01" );
        map.put("31000","10" );
        map.put("311E4","10" );
        map.put("312C3","00" );
        map.put("313A0","01" );
        map.put("313D7","01" );
        map.put("314C6","10" );
        map.put("40100","11" );
        map.put("40200","11" );
        map.put("403DF","00" );
        map.put("4049A","01" );
        map.put("405CC","10" );
        map.put("406E1","00" );
        map.put("407CA","00" );
        map.put("40800","01" );
        map.put("408B2","01" );
        map.put("409D2","01" );
        map.put("40AD6","10" );
        map.put("40BD7","10" );
        map.put("40CE5","00" );
        map.put("40D00","00" );
        map.put("40D90","00" );
        map.put("40E00","01" );
        map.put("40FDC","01" );
        map.put("41000","10" );
        map.put("411D9","10" );
        map.put("412C1","00" );
        map.put("4139C","01" );
        map.put("41489","10" );
        map.put("414AE","10" );
        map.put("50100","11" );
        map.put("50200","11" );
        map.put("50300","00" );
        map.put("50400","01" );
        map.put("50500","10" );
        map.put("50600","00" );
        map.put("50700","00" );
        map.put("50800","01" );
        map.put("50900","01" );
        map.put("50A00","10" );
        map.put("50B00","10" );
        map.put("50C00","00" );
        map.put("50DE2","00" );
        map.put("50E00","01" );
        map.put("50F00","01" );
        map.put("51000","10" );
        map.put("51100","10" );
        map.put("51200","00" );
        map.put("51300","01" );
        map.put("514CB","10" );
    }
    public static boolean checkEvoPenOG(String index, String mon) {
        penOGMapInit();

        if(mapp.containsKey(index)) return (mapp.get(index)[3].contains(","+mon) || mapp.get(index)[0].equals(mon));
        else if(mapp.containsKey(index.substring(0,3)+"00")) return (mapp.get(index.substring(0,3)+"00")[3].contains(","+mon) || mapp.get(index.substring(0,3)+"00")[0].equals(mon));
        //if(mapp.containsKey(index)) return (mapp.get(index)[3].contains(","+mon) || mapp.get(index)[0].equals(mon));
        else return false;
    }
    public static String getPenOGMonFromIndex(String index) {
        penOGMapInit();
        if(mapp.containsKey(index)) return mapp.get(index)[0];
        else if(mapp.containsKey(index.substring(0,3)+"00")) return mapp.get(index.substring(0,3)+"00")[0];
        else return "err";
    }
    public static String getPenOGMonFromRom(String rom) {
        penOGMapInit();
        String index = getPenOGIndex(rom);
        if(mapp.containsKey(index)) return mapp.get(index)[0];
        else if(mapp.containsKey(index.substring(0,3)+"00")) return mapp.get(index.substring(0,3)+"00")[0];
        else return "err";
    }
    public static String getPenOGLevel(String index) {
        penOGMapInit();
        if(mapp.containsKey(index)) return mapp.get(index)[1];
        else if(mapp.containsKey(index.substring(0,3)+"00")) return mapp.get(index.substring(0,3)+"00")[1];
        else return "err";
    }
    public static String getPenOGAttribute(String index) {
        penOGMapInit();
        if(mapp.containsKey(index)) return mapp.get(index)[2];
        else if(mapp.containsKey(index.substring(0,3)+"00")) return mapp.get(index.substring(0,3)+"00")[2];
        else return "err";
    }


    // ========== PEN-X ==========
    private static void penxMapInit() {
        mapp = new HashMap<String, String[]>();
        mapp.put("0502", new String[] {"_art_doru", "Child", "Data", ",_art_doruga,_nor_doruga,_wil_doruga,_art_deathxdoruga,_nor_deathxdoruga,_wil_deathxdoruga,_art_raptordra,_nor_raptordra,_wil_raptordra,_art_x_seadra,_nor_x_seadra,_wil_x_seadra,_art_x_grey,_nor_x_grey,_wil_x_grey,_art_x_kuwaga,_nor_x_kuwaga,_wil_x_kuwaga"});
        mapp.put("0800", new String[] {"_art_x_agu", "Child", "Vaccine", ",_art_doruga,_nor_doruga,_wil_doruga,_art_deathxdoruga,_nor_deathxdoruga,_wil_deathxdoruga,_art_raptordra,_nor_raptordra,_wil_raptordra,_art_x_seadra,_nor_x_seadra,_wil_x_seadra,_art_x_grey,_nor_x_grey,_wil_x_grey,_art_x_kuwaga,_nor_x_kuwaga,_wil_x_kuwaga"});
        mapp.put("0301", new String[] {"_art_doru", "Child", "Data", ",_art_doruga,_nor_doruga,_wil_doruga,_art_deathxdoruga,_nor_deathxdoruga,_wil_deathxdoruga,_art_raptordra,_nor_raptordra,_wil_raptordra,_art_x_seadra,_nor_x_seadra,_wil_x_seadra,_art_x_grey,_nor_x_grey,_wil_x_grey,_art_x_kuwaga,_nor_x_kuwaga,_wil_x_kuwaga"});
        mapp.put("0401", new String[] {"_nor_doru", "Child", "Data", ",_art_doruga,_nor_doruga,_wil_doruga,_art_deathxdoruga,_nor_deathxdoruga,_wil_deathxdoruga,_art_raptordra,_nor_raptordra,_wil_raptordra,_art_x_seadra,_nor_x_seadra,_wil_x_seadra,_art_x_grey,_nor_x_grey,_wil_x_grey,_art_x_kuwaga,_nor_x_kuwaga,_wil_x_kuwaga"});
        mapp.put("0501", new String[] {"_wil_doru", "Child", "Data", ",_art_doruga,_nor_doruga,_wil_doruga,_art_deathxdoruga,_nor_deathxdoruga,_wil_deathxdoruga,_art_raptordra,_nor_raptordra,_wil_raptordra,_art_x_seadra,_nor_x_seadra,_wil_x_seadra,_art_x_grey,_nor_x_grey,_wil_x_grey,_art_x_kuwaga,_nor_x_kuwaga,_wil_x_kuwaga"});
        mapp.put("0602", new String[] {"_art_x_agu", "Child", "Vaccine", ",_art_doruga,_nor_doruga,_wil_doruga,_art_deathxdoruga,_nor_deathxdoruga,_wil_deathxdoruga,_art_raptordra,_nor_raptordra,_wil_raptordra,_art_x_seadra,_nor_x_seadra,_wil_x_seadra,_art_x_grey,_nor_x_grey,_wil_x_grey,_art_x_kuwaga,_nor_x_kuwaga,_wil_x_kuwaga"});
        mapp.put("0702", new String[] {"_nor_x_agu", "Child", "Vaccine", ",_art_doruga,_nor_doruga,_wil_doruga,_art_deathxdoruga,_nor_deathxdoruga,_wil_deathxdoruga,_art_raptordra,_nor_raptordra,_wil_raptordra,_art_x_seadra,_nor_x_seadra,_wil_x_seadra,_art_x_grey,_nor_x_grey,_wil_x_grey,_art_x_kuwaga,_nor_x_kuwaga,_wil_x_kuwaga"});
        mapp.put("0802", new String[] {"_wil_x_agu", "Child", "Vaccine", ",_art_doruga,_nor_doruga,_wil_doruga,_art_deathxdoruga,_nor_deathxdoruga,_wil_deathxdoruga,_art_raptordra,_nor_raptordra,_wil_raptordra,_art_x_seadra,_nor_x_seadra,_wil_x_seadra,_art_x_grey,_nor_x_grey,_wil_x_grey,_art_x_kuwaga,_nor_x_kuwaga,_wil_x_kuwaga"});
        mapp.put("0901", new String[] {"_art_doruga", "Adult", "Data", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("0A01", new String[] {"_nor_doruga", "Adult", "Data", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("0B01", new String[] {"_wil_doruga", "Adult", "Data", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("0C02", new String[] {"_art_raptordra", "Adult", "Vaccine", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_grade,_nor_grade,_wil_grade,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("0D02", new String[] {"_nor_raptordra", "Adult", "Vaccine", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_grade,_nor_grade,_wil_grade,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("0E02", new String[] {"_wil_raptordra", "Adult", "Vaccine", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_grade,_nor_grade,_wil_grade,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("0F01", new String[] {"_art_x_seadra", "Adult", "Data", ",_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1001", new String[] {"_nor_x_seadra", "Adult", "Data", ",_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1101", new String[] {"_wil_x_seadra", "Adult", "Data", ",_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1202", new String[] {"_art_x_grey", "Adult", "Vaccine", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1302", new String[] {"_nor_x_grey", "Adult", "Vaccine", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1402", new String[] {"_wil_x_grey", "Adult", "Vaccine", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1500", new String[] {"_art_x_kuwaga", "Adult", "Virus", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1600", new String[] {"_nor_x_kuwaga", "Adult", "Virus", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1700", new String[] {"_wil_x_kuwaga", "Adult", "Virus", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1801", new String[] {"_art_dorugure", "Perfect", "Data", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("1901", new String[] {"_nor_dorugure", "Perfect", "Data", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("1A01", new String[] {"_wil_dorugure", "Perfect", "Data", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("1B02", new String[] {"_art_grade", "Perfect", "Vaccine", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_alpha,_nor_alpha,_wil_alpha,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("1C02", new String[] {"_nor_grade", "Perfect", "Vaccine", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_alpha,_nor_alpha,_wil_alpha,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("1D02", new String[] {"_wil_grade", "Perfect", "Vaccine", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_alpha,_nor_alpha,_wil_alpha,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("1E01", new String[] {"_art_x_megaseadra", "Perfect", "Data", ",_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_x_wargrey,_nor_x_wargrey,_wil_x_wargrey,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("1F01", new String[] {"_nor_x_megaseadra", "Perfect", "Data", ",_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_x_wargrey,_nor_x_wargrey,_wil_x_wargrey,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("2001", new String[] {"_wil_x_megaseadra", "Perfect", "Data", ",_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_x_wargrey,_nor_x_wargrey,_wil_x_wargrey,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("2102", new String[] {"_art_x_metalgrey", "Perfect", "Vaccine", ",_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_x_wargrey,_nor_x_wargrey,_wil_x_wargrey,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("2202", new String[] {"_nor_x_metalgrey", "Perfect", "Vaccine", ",_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_x_wargrey,_nor_x_wargrey,_wil_x_wargrey,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("2302", new String[] {"_wil_x_metalgrey", "Perfect", "Vaccine", ",_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_x_wargrey,_nor_x_wargrey,_wil_x_wargrey,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("2400", new String[] {"_art_x_okuwa", "Perfect", "Virus", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("2500", new String[] {"_nor_x_okuwa", "Perfect", "Virus", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("2600", new String[] {"_wil_x_okuwa", "Perfect", "Virus", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga"});
        mapp.put("2701", new String[] {"_art_dorugora", "Ultimate", "Data", ""});
        mapp.put("2801", new String[] {"_nor_dorugora", "Ultimate", "Data", ""});
        mapp.put("2901", new String[] {"_wil_dorugora", "Ultimate", "Data", ""});
        mapp.put("2A02", new String[] {"_art_alpha", "Ultimate", "Vaccine", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("2B02", new String[] {"_nor_alpha", "Ultimate", "Vaccine", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("2C02", new String[] {"_wil_alpha", "Ultimate", "Vaccine", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("2D01", new String[] {"_art_gigaseadra", "Ultimate", "Data", ""});
        mapp.put("2E01", new String[] {"_nor_gigaseadra", "Ultimate", "Data", ""});
        mapp.put("2F01", new String[] {"_wil_gigaseadra", "Ultimate", "Data", ""});
        mapp.put("3002", new String[] {"_art_x_wargrey", "Ultimate", "Vaccine", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("3102", new String[] {"_nor_x_wargrey", "Ultimate", "Vaccine", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("3202", new String[] {"_wil_x_wargrey", "Ultimate", "Vaccine", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("3300", new String[] {"_art_grandiskuwaga", "Ultimate", "Virus", ""});
        mapp.put("3400", new String[] {"_nor_grandiskuwaga", "Ultimate", "Virus", ""});
        mapp.put("3500", new String[] {"_wil_grandiskuwaga", "Ultimate", "Virus", ""});
        mapp.put("3602", new String[] {"_art_x_omega", "Ultimate", "Vaccine", ""});
        mapp.put("3702", new String[] {"_nor_x_omega", "Ultimate", "Vaccine", ""});
        mapp.put("3802", new String[] {"_wil_x_omega", "Ultimate", "Vaccine", ""});
        mapp.put("0C00", new String[] {"_art_deathxdoruga", "Adult", "Virus", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_deathxdorugure,_nor_deathxdorugure,_wil_deathxdorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("0D00", new String[] {"_nor_deathxdoruga", "Adult", "Virus", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_deathxdorugure,_nor_deathxdorugure,_wil_deathxdorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("0E00", new String[] {"_wil_deathxdoruga", "Adult", "Virus", ",_art_dorugure,_nor_dorugure,_wil_dorugure,_art_deathxdorugure,_nor_deathxdorugure,_wil_deathxdorugure,_art_x_megaseadra,_nor_x_megaseadra,_wil_x_megaseadra,_art_x_metalgrey,_nor_x_metalgrey,_wil_x_metalgrey,_art_x_okuwa,_nor_x_okuwa,_wil_x_okuwa"});
        mapp.put("1B00", new String[] {"_art_deathxdorugure", "Perfect", "Virus", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_deathxdorugora,_nor_deathxdorugora,_wil_deathxdorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga,_art_gaiou,_nor_gaiou,_wil_gaiou"});
        mapp.put("1C00", new String[] {"_nor_deathxdorugure", "Perfect", "Virus", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_deathxdorugora,_nor_deathxdorugora,_wil_deathxdorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga,_art_gaiou,_nor_gaiou,_wil_gaiou"});
        mapp.put("1D00", new String[] {"_wil_deathxdorugure", "Perfect", "Virus", ",_art_dorugora,_nor_dorugora,_wil_dorugora,_art_deathxdorugora,_nor_deathxdorugora,_wil_deathxdorugora,_art_gigaseadra,_nor_gigaseadra,_wil_gigaseadra,_art_grandiskuwaga,_nor_grandiskuwaga,_wil_grandiskuwaga,_art_gaiou,_nor_gaiou,_wil_gaiou"});
        mapp.put("2A00", new String[] {"_art_deathxdorugora", "Ultimate", "Virus", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("2B00", new String[] {"_nor_deathxdorugora", "Ultimate", "Virus", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("2C00", new String[] {"_wil_deathxdorugora", "Ultimate", "Virus", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("3000", new String[] {"_art_gaiou", "Ultimate", "Virus", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("3100", new String[] {"_nor_gaiou", "Ultimate", "Virus", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("3200", new String[] {"_wil_gaiou", "Ultimate", "Virus", ",_art_x_omega,_nor_x_omega,_wil_x_omega"});
        mapp.put("0312", new String[] {"_art_ryuda", "Child", "Vaccine", ",_art_ginryu,_nor_ginryu,_wil_ginryu,_art_tobucat,_nor_tobucat,_wil_tobucat,_art_x_allo,_nor_x_allo,_wil_x_allo,_art_x_grow,_nor_x_grow,_wil_x_grow,_art_x_monochro,_nor_x_monochro,_wil_x_monochro"});
        mapp.put("0412", new String[] {"_nor_ryuda", "Child", "Vaccine", ",_art_ginryu,_nor_ginryu,_wil_ginryu,_art_tobucat,_nor_tobucat,_wil_tobucat,_art_x_allo,_nor_x_allo,_wil_x_allo,_art_x_grow,_nor_x_grow,_wil_x_grow,_art_x_monochro,_nor_x_monochro,_wil_x_monochro"});
        mapp.put("0512", new String[] {"_wil_ryuda", "Child", "Vaccine", ",_art_ginryu,_nor_ginryu,_wil_ginryu,_art_tobucat,_nor_tobucat,_wil_tobucat,_art_x_allo,_nor_x_allo,_wil_x_allo,_art_x_grow,_nor_x_grow,_wil_x_grow,_art_x_monochro,_nor_x_monochro,_wil_x_monochro"});
        mapp.put("0610", new String[] {"_art_x_guil", "Child", "Virus", ",_art_ginryu,_nor_ginryu,_wil_ginryu,_art_tobucat,_nor_tobucat,_wil_tobucat,_art_x_allo,_nor_x_allo,_wil_x_allo,_art_x_grow,_nor_x_grow,_wil_x_grow,_art_x_monochro,_nor_x_monochro,_wil_x_monochro"});
        mapp.put("0710", new String[] {"_nor_x_guil", "Child", "Virus", ",_art_ginryu,_nor_ginryu,_wil_ginryu,_art_tobucat,_nor_tobucat,_wil_tobucat,_art_x_allo,_nor_x_allo,_wil_x_allo,_art_x_grow,_nor_x_grow,_wil_x_grow,_art_x_monochro,_nor_x_monochro,_wil_x_monochro"});
        mapp.put("0810", new String[] {"_wil_x_guil", "Child", "Virus", ",_art_ginryu,_nor_ginryu,_wil_ginryu,_art_tobucat,_nor_tobucat,_wil_tobucat,_art_x_allo,_nor_x_allo,_wil_x_allo,_art_x_grow,_nor_x_grow,_wil_x_grow,_art_x_monochro,_nor_x_monochro,_wil_x_monochro"});
        mapp.put("0912", new String[] {"_art_ginryu", "Adult", "Vaccine", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("0A12", new String[] {"_nor_ginryu", "Adult", "Vaccine", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("0B12", new String[] {"_wil_ginryu", "Adult", "Vaccine", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("0C11", new String[] {"_art_tobucat", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("0D11", new String[] {"_nor_tobucat", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("0E11", new String[] {"_wil_tobucat", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("0F11", new String[] {"_art_x_allo", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1011", new String[] {"_nor_x_allo", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1111", new String[] {"_wil_x_allo", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1210", new String[] {"_art_x_grow", "Adult", "Virus", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1310", new String[] {"_nor_x_grow", "Adult", "Virus", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1410", new String[] {"_wil_x_grow", "Adult", "Virus", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1511", new String[] {"_art_x_monochro", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1611", new String[] {"_nor_x_monochro", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1711", new String[] {"_wil_x_monochro", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_skullbaluchi,_nor_skullbaluchi,_wil_skullbaluchi,_art_mametyra,_nor_mametyra,_wil_mametyra,_art_x_megalogrow,_nor_x_megalogrow,_wil_x_megalogrow,_art_x_tricera,_nor_x_tricera,_wil_x_tricera"});
        mapp.put("1812", new String[] {"_art_hisyaryu", "Perfect", "Vaccine", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("1912", new String[] {"_nor_hisyaryu", "Perfect", "Vaccine", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("1A12", new String[] {"_wil_hisyaryu", "Perfect", "Vaccine", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("1B11", new String[] {"_art_skullbaluchi", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("1C11", new String[] {"_nor_skullbaluchi", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("1D11", new String[] {"_wil_skullbaluchi", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("1E11", new String[] {"_art_mametyra", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("1F11", new String[] {"_nor_mametyra", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("2011", new String[] {"_wil_mametyra", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("2110", new String[] {"_art_x_megalogrow", "Perfect", "Virus", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("2210", new String[] {"_nor_x_megalogrow", "Perfect", "Virus", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("2310", new String[] {"_wil_x_megalogrow", "Perfect", "Virus", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("2411", new String[] {"_art_x_tricera", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("2511", new String[] {"_nor_x_tricera", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("2611", new String[] {"_wil_x_tricera", "Perfect", "Data", ",_art_ouryu,_nor_ouryu,_wil_ouryu,_art_dinotige,_nor_dinotige,_wil_dinotige,_art_x_ulforcevdra,_nor_x_ulforcevdra,_wil_x_ulforcevdra,_art_dinorex,_nor_dinorex,_wil_dinorex,_art_medievalduke,_nor_medievalduke,_wil_medievalduke,_art_ultimatebrachi,_nor_ultimatebrachi,_wil_ultimatebrachi"});
        mapp.put("2712", new String[] {"_art_ouryu", "Ultimate", "Vaccine", ""});
        mapp.put("2812", new String[] {"_nor_ouryu", "Ultimate", "Vaccine", ""});
        mapp.put("2912", new String[] {"_wil_ouryu", "Ultimate", "Vaccine", ""});
        mapp.put("2A11", new String[] {"_art_dinotige", "Ultimate", "Data", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("2B11", new String[] {"_nor_dinotige", "Ultimate", "Data", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("2C11", new String[] {"_wil_dinotige", "Ultimate", "Data", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("2A12", new String[] {"_art_x_ulforcevdra", "Ultimate", "Vaccine", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("2B12", new String[] {"_nor_x_ulforcevdra", "Ultimate", "Vaccine", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("2C12", new String[] {"_wil_x_ulforcevdra", "Ultimate", "Vaccine", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("2D11", new String[] {"_art_dinorex", "Ultimate", "Data", ""});
        mapp.put("2E11", new String[] {"_nor_dinorex", "Ultimate", "Data", ""});
        mapp.put("2F11", new String[] {"_wil_dinorex", "Ultimate", "Data", ""});
        mapp.put("3011", new String[] {"_art_medievalduke", "Ultimate", "Data", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("3111", new String[] {"_nor_medievalduke", "Ultimate", "Data", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("3211", new String[] {"_wil_medievalduke", "Ultimate", "Data", ",_art_x_duke,_nor_x_duke,_wil_x_duke"});
        mapp.put("3311", new String[] {"_art_ultimatebrachi", "Ultimate", "Data", ""});
        mapp.put("3411", new String[] {"_nor_ultimatebrachi", "Ultimate", "Data", ""});
        mapp.put("3511", new String[] {"_wil_ultimatebrachi", "Ultimate", "Data", ""});
        mapp.put("3610", new String[] {"_art_x_duke", "Ultimate", "Virus", ""});
        mapp.put("3710", new String[] {"_nor_x_duke", "Ultimate", "Virus", ""});
        mapp.put("3810", new String[] {"_wil_x_duke", "Ultimate", "Virus", ""});
        mapp.put("0321", new String[] {"_art_doru", "Child", "Data", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0421", new String[] {"_nor_doru", "Child", "Data", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0521", new String[] {"_wil_doru", "Child", "Data", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0622", new String[] {"_art_ryuda", "Child", "Vaccine", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0722", new String[] {"_nor_ryuda", "Child", "Vaccine", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0822", new String[] {"_wil_ryuda", "Child", "Vaccine", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0920", new String[] {"_art_funbee", "Child", "Virus", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0A20", new String[] {"_nor_funbee", "Child", "Virus", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0B20", new String[] {"_wil_funbee", "Child", "Virus", ",_art_raptordra,_nor_raptordra,_wil_raptordra,_art_ginryu,_nor_ginryu,_wil_ginryu,_art_wasp,_nor_wasp,_wil_wasp,_art_omeka,_nor_omeka,_wil_omeka"});
        mapp.put("0C22", new String[] {"_art_raptordra", "Adult", "Vaccine", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("0D22", new String[] {"_nor_raptordra", "Adult", "Vaccine", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("0E22", new String[] {"_wil_raptordra", "Adult", "Vaccine", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("0F22", new String[] {"_art_ginryu", "Adult", "Vaccine", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1022", new String[] {"_nor_ginryu", "Adult", "Vaccine", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1122", new String[] {"_wil_ginryu", "Adult", "Vaccine", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1220", new String[] {"_art_wasp", "Adult", "Virus", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1320", new String[] {"_nor_wasp", "Adult", "Virus", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1420", new String[] {"_wil_wasp", "Adult", "Virus", ",_art_grade,_nor_grade,_wil_grade,_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1521", new String[] {"_art_omeka", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1621", new String[] {"_nor_omeka", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1721", new String[] {"_wil_omeka", "Adult", "Data", ",_art_hisyaryu,_nor_hisyaryu,_wil_hisyaryu,_art_cannonbee,_nor_cannonbee,_wil_cannonbee,_art_metalfanto,_nor_metalfanto,_wil_metalfanto"});
        mapp.put("1822", new String[] {"_art_grade", "Perfect", "Vaccine", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("1922", new String[] {"_nor_grade", "Perfect", "Vaccine", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("1A22", new String[] {"_wil_grade", "Perfect", "Vaccine", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("1B22", new String[] {"_art_hisyaryu", "Perfect", "Vaccine", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("1C22", new String[] {"_nor_hisyaryu", "Perfect", "Vaccine", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("1D22", new String[] {"_wil_hisyaryu", "Perfect", "Vaccine", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("1E20", new String[] {"_art_cannonbee", "Perfect", "Virus", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("1F20", new String[] {"_nor_cannonbee", "Perfect", "Virus", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("2020", new String[] {"_wil_cannonbee", "Perfect", "Virus", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("2121", new String[] {"_art_metalfanto", "Perfect", "Data", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("2221", new String[] {"_nor_metalfanto", "Perfect", "Data", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("2321", new String[] {"_wil_metalfanto", "Perfect", "Data", ",_art_alpha,_nor_alpha,_wil_alpha,_art_ouryu,_nor_ouryu,_wil_ouryu,_art_metalpirani,_nor_metalpirani,_wil_metalpirani"});
        mapp.put("2422", new String[] {"_art_alpha", "Ultimate", "Vaccine", ",_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_ory_alpha ,_nor_ory_alpha ,_wil_ory_alpha "});
        mapp.put("2522", new String[] {"_nor_alpha", "Ultimate", "Vaccine", ",_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_ory_alpha ,_nor_ory_alpha ,_wil_ory_alpha "});
        mapp.put("2622", new String[] {"_wil_alpha", "Ultimate", "Vaccine", ",_art_tigervespa,_nor_tigervespa,_wil_tigervespa,_art_ory_alpha ,_nor_ory_alpha ,_wil_ory_alpha "});
        mapp.put("2722", new String[] {"_art_ouryu", "Ultimate", "Vaccine", ""});
        mapp.put("2822", new String[] {"_nor_ouryu", "Ultimate", "Vaccine", ""});
        mapp.put("2922", new String[] {"_wil_ouryu", "Ultimate", "Vaccine", ""});
        mapp.put("2A20", new String[] {"_art_tigervespa", "Ultimate", "Virus", ""});
        mapp.put("2B20", new String[] {"_nor_tigervespa", "Ultimate", "Virus", ""});
        mapp.put("2C20", new String[] {"_wil_tigervespa", "Ultimate", "Virus", ""});
        mapp.put("2D20", new String[] {"_art_metalpirani", "Ultimate", "Virus", ""});
        mapp.put("2E20", new String[] {"_nor_metalpirani", "Ultimate", "Virus", ""});
        mapp.put("2F20", new String[] {"_wil_metalpirani", "Ultimate", "Virus", ""});
        mapp.put("3022", new String[] {"_art_ory_alpha ", "Ultimate", "Vaccine", ""});
        mapp.put("3122", new String[] {"_nor_ory_alpha ", "Ultimate", "Vaccine", ""});
        mapp.put("3222", new String[] {"_wil_ory_alpha ", "Ultimate", "Vaccine", ""});
    }
    public static String getPenxIndex(String rom) {
        // [slot(2)][version(1)][Attr(1)]
        String  index = leadingZeros(Integer.toHexString(Integer.parseInt(leadingZeros(Integer.toBinaryString(Integer.parseInt(rom.substring(1,3), 16)), 8).substring(2), 2)), 2).toUpperCase(),
                version = rom.substring(0,1),
                attr = Integer.toHexString(Integer.parseInt(leadingZeros(Integer.toBinaryString(Integer.parseInt(rom.substring(10,11), 16)), 4).substring(0,2), 2));
        return index+version+attr;
    } //
    public static String getPenxMonFromRom(String rom) {
        penxMapInit();
        String index = getPenxIndex(rom);

//        if(mapp.containsKey(index)) {
//            Log.e("From ROM GOT", mapp.get(index)[0]);
//        }
//        else{
//            Log.e("From ROM Err", ": "+index);
//        }

        if(mapp.containsKey(index)) return mapp.get(index)[0];
        else return "err";
    }
    public static String getPenxMonFromIndex(String index) {
        penxMapInit();

//        if(mapp.containsKey(index)) {
//            Log.e("From Index GOT", mapp.get(index)[0]);
//        }
//        else{
//            Log.e("From Index Err", ": "+index);
//        }

        if(mapp.containsKey(index)) return mapp.get(index)[0];
        else return "err";
    }
    public static String getPenxLevelFromIndex(String index) {
        penxMapInit();

        if(mapp.containsKey(index)) return mapp.get(index)[1];
        else return "err";
    }
    public static String getPenxPower(String rom) {
        return rom.substring(6,8);
    } //
    public static String getPenxAttribute(String index) {
        penxMapInit();

        if(mapp.containsKey(index)) return mapp.get(index)[2];
        else return "err";
    }
    public static boolean checkEvoPenx(String index, String mon) {
        penxMapInit();

        if(mapp.containsKey(index)) return (mapp.get(index)[3].contains(","+mon) || mapp.get(index)[0].equals(mon));
        else return false;
    }

    /*

    private static void penxIndexInit() {
        map = new HashMap<String, String>();

        map.put("0301", "art_doru");
        map.put("0401", "nor_doru");
        map.put("0501", "wil_doru");
        map.put("0602", "art_x_agu");
        map.put("0702", "nor_x_agu");
        map.put("0802", "wil_x_agu");
        map.put("0901", "art_doruga");
        map.put("0A01", "nor_doruga");
        map.put("0B01", "wil_doruga");
        map.put("0C02", "art_raptordra");
        map.put("0D02", "nor_raptordra");
        map.put("0E02", "wil_raptordra");
        map.put("0F01", "art_x_seadra");
        map.put("1001", "nor_x_seadra");
        map.put("1101", "wil_x_seadra");
        map.put("1202", "art_x_grey");
        map.put("1302", "nor_x_grey");
        map.put("1402", "wil_x_grey");
        map.put("1500", "art_x_kuwaga");
        map.put("1600", "nor_x_kuwaga");
        map.put("1700", "wil_x_kuwaga");
        map.put("1801", "art_dorugure");
        map.put("1901", "nor_dorugure");
        map.put("1A01", "wil_dorugure");
        map.put("1B02", "art_grade");
        map.put("1C02", "nor_grade");
        map.put("1D02", "wil_grade");
        map.put("1E01", "art_x_megaseadra");
        map.put("1F01", "nor_x_megaseadra");
        map.put("2001", "wil_x_megaseadra");
        map.put("2102", "art_x_metalgrey");
        map.put("2202", "nor_x_metalgrey");
        map.put("2302", "wil_x_metalgrey");
        map.put("2400", "art_x_okuwa");
        map.put("2500", "nor_x_okuwa");
        map.put("2600", "wil_x_okuwa");
        map.put("2701", "art_dorugora");
        map.put("2801", "nor_dorugora");
        map.put("2901", "wil_dorugora");
        map.put("2A02", "art_alpha");
        map.put("2B02", "nor_alpha");
        map.put("2C02", "wil_alpha");
        map.put("2D01", "art_gigaseadra");
        map.put("2E01", "nor_gigaseadra");
        map.put("2F01", "wil_gigaseadra");
        map.put("3002", "art_x_wargrey");
        map.put("3102", "nor_x_wargrey");
        map.put("3202", "wil_x_wargrey");
        map.put("3300", "art_grandiskuwaga");
        map.put("3400", "nor_grandiskuwaga");
        map.put("3500", "wil_grandiskuwaga");
        map.put("3602", "art_x_omega");
        map.put("3702", "nor_x_omega");
        map.put("3802", "wil_x_omega");
        map.put("0C00", "art_deathxdoruga");
        map.put("0D00", "nor_deathxdoruga");
        map.put("0E00", "wil_deathxdoruga");
        map.put("1B00", "art_deathxdorugure");
        map.put("1C00", "nor_deathxdorugure");
        map.put("1D00", "wil_deathxdorugure");
        map.put("2A00", "art_deathxdorugora");
        map.put("2B00", "nor_deathxdorugora");
        map.put("2C00", "wil_deathxdorugora");
        map.put("3000", "art_gaiou");
        map.put("3100", "nor_gaiou");
        map.put("3200", "wil_gaiou");
        map.put("0312", "art_ryuda");
        map.put("0412", "nor_ryuda");
        map.put("0512", "wil_ryuda");
        map.put("0610", "art_x_guil");
        map.put("0710", "nor_x_guil");
        map.put("0810", "wil_x_guil");
        map.put("0912", "art_ginryu");
        map.put("0A12", "nor_ginryu");
        map.put("0B12", "wil_ginryu");
        map.put("0C11", "art_tobucat");
        map.put("0D11", "nor_tobucat");
        map.put("0E11", "wil_tobucat");
        map.put("0F11", "art_x_allo");
        map.put("1011", "nor_x_allo");
        map.put("1111", "wil_x_allo");
        map.put("1210", "art_x_grow");
        map.put("1310", "nor_x_grow");
        map.put("1410", "wil_x_grow");
        map.put("1511", "art_x_monochro");
        map.put("1611", "nor_x_monochro");
        map.put("1711", "wil_x_monochro");
        map.put("1812", "art_hisyaryu");
        map.put("1912", "nor_hisyaryu");
        map.put("1A12", "wil_hisyaryu");
        map.put("1B11", "art_skullbaluchi");
        map.put("1C11", "nor_skullbaluchi");
        map.put("1D11", "wil_skullbaluchi");
        map.put("1E11", "art_mametyra");
        map.put("1F11", "nor_mametyra");
        map.put("2011", "wil_mametyra");
        map.put("2110", "art_x_megalogrow");
        map.put("2210", "nor_x_megalogrow");
        map.put("2310", "wil_x_megalogrow");
        map.put("2411", "art_x_tricera");
        map.put("2511", "nor_x_tricera");
        map.put("2611", "wil_x_tricera");
        map.put("2712", "art_ouryu");
        map.put("2812", "nor_ouryu");
        map.put("2912", "wil_ouryu");
        map.put("2A11", "art_dinotige");
        map.put("2B11", "nor_dinotige");
        map.put("2C11", "wil_dinotige");
        map.put("2A12", "art_x_ulforcevdra");
        map.put("2B12", "nor_x_ulforcevdra");
        map.put("2C12", "wil_x_ulforcevdra");
        map.put("2D11", "art_dinorex");
        map.put("2E11", "nor_dinorex");
        map.put("2F11", "wil_dinorex");
        map.put("3011", "art_medievalduke");
        map.put("3111", "nor_medievalduke");
        map.put("3211", "wil_medievalduke");
        map.put("3311", "art_ultimatebrachi");
        map.put("3411", "nor_ultimatebrachi");
        map.put("3511", "wil_ultimatebrachi");
        map.put("3610", "art_x_duke");
        map.put("3710", "nor_x_duke");
        map.put("3810", "wil_x_duke");
        map.put("0321", "art_doru");
        map.put("0421", "nor_doru");
        map.put("0521", "wil_doru");
        map.put("0622", "art_ryuda");
        map.put("0722", "nor_ryuda");
        map.put("0822", "wil_ryuda");
        map.put("0920", "art_funbee");
        map.put("0A20", "nor_funbee");
        map.put("0B20", "wil_funbee");
        map.put("0C22", "art_raptordra");
        map.put("0D22", "nor_raptordra");
        map.put("0E22", "wil_raptordra");
        map.put("0F22", "art_ginryu");
        map.put("1022", "nor_ginryu");
        map.put("1122", "wil_ginryu");
        map.put("1220", "art_wasp");
        map.put("1320", "nor_wasp");
        map.put("1420", "wil_wasp");
        map.put("1521", "art_omeka");
        map.put("1621", "nor_omeka");
        map.put("1721", "wil_omeka");
        map.put("1822", "art_grade");
        map.put("1922", "nor_grade");
        map.put("1A22", "wil_grade");
        map.put("1B22", "art_hisyaryu");
        map.put("1C22", "nor_hisyaryu");
        map.put("1D22", "wil_hisyaryu");
        map.put("1E20", "art_cannonbee");
        map.put("1F20", "nor_cannonbee");
        map.put("2020", "wil_cannonbee");
        map.put("2121", "art_metalfanto");
        map.put("2221", "nor_metalfanto");
        map.put("2321", "wil_metalfanto");
        map.put("2422", "art_alpha");
        map.put("2522", "nor_alpha");
        map.put("2622", "wil_alpha");
        map.put("2722", "art_ouryu");
        map.put("2822", "nor_ouryu");
        map.put("2922", "wil_ouryu");
        map.put("2A20", "art_tigervespa");
        map.put("2B20", "nor_tigervespa");
        map.put("2C20", "wil_tigervespa");
        map.put("2D20", "art_metalpirani");
        map.put("2E20", "nor_metalpirani");
        map.put("2F20", "wil_metalpirani");
        map.put("3022", "art_ory_alpha ");
        map.put("3122", "nor_ory_alpha ");
        map.put("3222", "wil_ory_alpha ");
    }
    private static void penxEvoTreeInit() {
        map = new HashMap<String, String>();

        map.put("art_alpha", "art_x_omega,nor_x_omega,wil_x_omega,art_tigervespa,nor_tigervespa,wil_tigervespa,art_ory_alpha ,nor_ory_alpha ,wil_ory_alpha ");
        map.put("art_cannonbee", "art_alpha,nor_alpha,wil_alpha,art_ouryu,nor_ouryu,wil_ouryu,art_tigervespa,nor_tigervespa,wil_tigervespa,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("art_deathxdoruga", "art_dorugure,nor_dorugure,wil_dorugure,art_deathxdorugure,nor_deathxdorugure,wil_deathxdorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("art_deathxdorugora", "art_x_omega,nor_x_omega,wil_x_omega");
        map.put("art_deathxdorugure", "art_dorugora,nor_dorugora,wil_dorugora,art_deathxdorugora,nor_deathxdorugora,wil_deathxdorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga,art_gaiou,nor_gaiou,wil_gaiou");
        map.put("art_dinorex", "");
        map.put("art_dinotige", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("art_doru", "art_doruga,nor_doruga,wil_doruga,art_deathxdoruga,nor_deathxdoruga,wil_deathxdoruga,art_raptordra,nor_raptordra,wil_raptordra,art_x_seadra,nor_x_seadra,wil_x_seadra,art_x_grey,nor_x_grey,wil_x_grey,art_x_kuwaga,nor_x_kuwaga,wil_x_kuwaga,art_ginryu,nor_ginryu,wil_ginryu,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("art_doruga", "art_dorugure,nor_dorugure,wil_dorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("art_dorugora", "");
        map.put("art_dorugure", "art_dorugora,nor_dorugora,wil_dorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("art_funbee", "art_raptordra,nor_raptordra,wil_raptordra,art_ginryu,nor_ginryu,wil_ginryu,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("art_gaiou", "art_x_omega,nor_x_omega,wil_x_omega");
        map.put("art_gigaseadra", "");
        map.put("art_ginryu", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera,art_grade,nor_grade,wil_grade,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("art_grade", "art_dorugora,nor_dorugora,wil_dorugora,art_alpha,nor_alpha,wil_alpha,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga,art_ouryu,nor_ouryu,wil_ouryu,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("art_grandiskuwaga", "");
        map.put("art_hisyaryu", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi,art_alpha,nor_alpha,wil_alpha,art_tigervespa,nor_tigervespa,wil_tigervespa,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("art_mametyra", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("art_medievalduke", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("art_metalfanto", "art_alpha,nor_alpha,wil_alpha,art_ouryu,nor_ouryu,wil_ouryu,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("art_metalpirani", "");
        map.put("art_omeka", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("art_ory_alpha ", "");
        map.put("art_ouryu", "");
        map.put("art_raptordra", "art_dorugure,nor_dorugure,wil_dorugure,art_grade,nor_grade,wil_grade,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa,art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("art_ryuda", "art_ginryu,nor_ginryu,wil_ginryu,art_tobucat,nor_tobucat,wil_tobucat,art_x_allo,nor_x_allo,wil_x_allo,art_x_grow,nor_x_grow,wil_x_grow,art_x_monochro,nor_x_monochro,wil_x_monochro,art_raptordra,nor_raptordra,wil_raptordra,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("art_skullbaluchi", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("art_tigervespa", "");
        map.put("art_tobucat", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("art_ultimatebrachi", "");
        map.put("art_wasp", "art_grade,nor_grade,wil_grade,art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("art_x_agu", "art_doruga,nor_doruga,wil_doruga,art_deathxdoruga,nor_deathxdoruga,wil_deathxdoruga,art_raptordra,nor_raptordra,wil_raptordra,art_x_seadra,nor_x_seadra,wil_x_seadra,art_x_grey,nor_x_grey,wil_x_grey,art_x_kuwaga,nor_x_kuwaga,wil_x_kuwaga");
        map.put("art_x_allo", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("art_x_duke", "");
        map.put("art_x_grey", "art_dorugure,nor_dorugure,wil_dorugure,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("art_x_grow", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("art_x_guil", "art_ginryu,nor_ginryu,wil_ginryu,art_tobucat,nor_tobucat,wil_tobucat,art_x_allo,nor_x_allo,wil_x_allo,art_x_grow,nor_x_grow,wil_x_grow,art_x_monochro,nor_x_monochro,wil_x_monochro");
        map.put("art_x_kuwaga", "art_dorugure,nor_dorugure,wil_dorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("art_x_megalogrow", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("art_x_megaseadra", "art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_x_wargrey,nor_x_wargrey,wil_x_wargrey,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("art_x_metalgrey", "art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_x_wargrey,nor_x_wargrey,wil_x_wargrey,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("art_x_monochro", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("art_x_okuwa", "art_dorugora,nor_dorugora,wil_dorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("art_x_omega", "");
        map.put("art_x_seadra", "art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("art_x_tricera", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("art_x_ulforcevdra", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("art_x_wargrey", "art_x_omega,nor_x_omega,wil_x_omega");
        map.put("nor_alpha", "art_x_omega,nor_x_omega,wil_x_omega,art_tigervespa,nor_tigervespa,wil_tigervespa,art_ory_alpha ,nor_ory_alpha ,wil_ory_alpha ");
        map.put("nor_cannonbee", "art_alpha,nor_alpha,wil_alpha,art_ouryu,nor_ouryu,wil_ouryu,art_tigervespa,nor_tigervespa,wil_tigervespa,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("nor_deathxdoruga", "art_dorugure,nor_dorugure,wil_dorugure,art_deathxdorugure,nor_deathxdorugure,wil_deathxdorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("nor_deathxdorugora", "art_x_omega,nor_x_omega,wil_x_omega");
        map.put("nor_deathxdorugure", "art_dorugora,nor_dorugora,wil_dorugora,art_deathxdorugora,nor_deathxdorugora,wil_deathxdorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga,art_gaiou,nor_gaiou,wil_gaiou");
        map.put("nor_dinorex", "");
        map.put("nor_dinotige", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("nor_doru", "art_doruga,nor_doruga,wil_doruga,art_deathxdoruga,nor_deathxdoruga,wil_deathxdoruga,art_raptordra,nor_raptordra,wil_raptordra,art_x_seadra,nor_x_seadra,wil_x_seadra,art_x_grey,nor_x_grey,wil_x_grey,art_x_kuwaga,nor_x_kuwaga,wil_x_kuwaga,art_ginryu,nor_ginryu,wil_ginryu,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("nor_doruga", "art_dorugure,nor_dorugure,wil_dorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("nor_dorugora", "");
        map.put("nor_dorugure", "art_dorugora,nor_dorugora,wil_dorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("nor_funbee", "art_raptordra,nor_raptordra,wil_raptordra,art_ginryu,nor_ginryu,wil_ginryu,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("nor_gaiou", "art_x_omega,nor_x_omega,wil_x_omega");
        map.put("nor_gigaseadra", "");
        map.put("nor_ginryu", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera,art_grade,nor_grade,wil_grade,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("nor_grade", "art_dorugora,nor_dorugora,wil_dorugora,art_alpha,nor_alpha,wil_alpha,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga,art_ouryu,nor_ouryu,wil_ouryu,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("nor_grandiskuwaga", "");
        map.put("nor_hisyaryu", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi,art_alpha,nor_alpha,wil_alpha,art_tigervespa,nor_tigervespa,wil_tigervespa,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("nor_mametyra", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("nor_medievalduke", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("nor_metalfanto", "art_alpha,nor_alpha,wil_alpha,art_ouryu,nor_ouryu,wil_ouryu,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("nor_metalpirani", "");
        map.put("nor_omeka", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("nor_ory_alpha ", "");
        map.put("nor_ouryu", "");
        map.put("nor_raptordra", "art_dorugure,nor_dorugure,wil_dorugure,art_grade,nor_grade,wil_grade,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa,art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("nor_ryuda", "art_ginryu,nor_ginryu,wil_ginryu,art_tobucat,nor_tobucat,wil_tobucat,art_x_allo,nor_x_allo,wil_x_allo,art_x_grow,nor_x_grow,wil_x_grow,art_x_monochro,nor_x_monochro,wil_x_monochro,art_raptordra,nor_raptordra,wil_raptordra,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("nor_skullbaluchi", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("nor_tigervespa", "");
        map.put("nor_tobucat", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("nor_ultimatebrachi", "");
        map.put("nor_wasp", "art_grade,nor_grade,wil_grade,art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("nor_x_agu", "art_doruga,nor_doruga,wil_doruga,art_deathxdoruga,nor_deathxdoruga,wil_deathxdoruga,art_raptordra,nor_raptordra,wil_raptordra,art_x_seadra,nor_x_seadra,wil_x_seadra,art_x_grey,nor_x_grey,wil_x_grey,art_x_kuwaga,nor_x_kuwaga,wil_x_kuwaga");
        map.put("nor_x_allo", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("nor_x_duke", "");
        map.put("nor_x_grey", "art_dorugure,nor_dorugure,wil_dorugure,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("nor_x_grow", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("nor_x_guil", "art_ginryu,nor_ginryu,wil_ginryu,art_tobucat,nor_tobucat,wil_tobucat,art_x_allo,nor_x_allo,wil_x_allo,art_x_grow,nor_x_grow,wil_x_grow,art_x_monochro,nor_x_monochro,wil_x_monochro");
        map.put("nor_x_kuwaga", "art_dorugure,nor_dorugure,wil_dorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("nor_x_megalogrow", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("nor_x_megaseadra", "art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_x_wargrey,nor_x_wargrey,wil_x_wargrey,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("nor_x_metalgrey", "art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_x_wargrey,nor_x_wargrey,wil_x_wargrey,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("nor_x_monochro", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("nor_x_okuwa", "art_dorugora,nor_dorugora,wil_dorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("nor_x_omega", "");
        map.put("nor_x_seadra", "art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("nor_x_tricera", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("nor_x_ulforcevdra", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("nor_x_wargrey", "art_x_omega,nor_x_omega,wil_x_omega");
        map.put("wil_alpha", "art_x_omega,nor_x_omega,wil_x_omega,art_tigervespa,nor_tigervespa,wil_tigervespa,art_ory_alpha ,nor_ory_alpha ,wil_ory_alpha ");
        map.put("wil_cannonbee", "art_alpha,nor_alpha,wil_alpha,art_ouryu,nor_ouryu,wil_ouryu,art_tigervespa,nor_tigervespa,wil_tigervespa,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("wil_deathxdoruga", "art_dorugure,nor_dorugure,wil_dorugure,art_deathxdorugure,nor_deathxdorugure,wil_deathxdorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("wil_deathxdorugora", "art_x_omega,nor_x_omega,wil_x_omega");
        map.put("wil_deathxdorugure", "art_dorugora,nor_dorugora,wil_dorugora,art_deathxdorugora,nor_deathxdorugora,wil_deathxdorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga,art_gaiou,nor_gaiou,wil_gaiou");
        map.put("wil_dinorex", "");
        map.put("wil_dinotige", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("wil_doru", "art_doruga,nor_doruga,wil_doruga,art_deathxdoruga,nor_deathxdoruga,wil_deathxdoruga,art_raptordra,nor_raptordra,wil_raptordra,art_x_seadra,nor_x_seadra,wil_x_seadra,art_x_grey,nor_x_grey,wil_x_grey,art_x_kuwaga,nor_x_kuwaga,wil_x_kuwaga,art_ginryu,nor_ginryu,wil_ginryu,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("wil_doruga", "art_dorugure,nor_dorugure,wil_dorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("wil_dorugora", "");
        map.put("wil_dorugure", "art_dorugora,nor_dorugora,wil_dorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("wil_funbee", "art_raptordra,nor_raptordra,wil_raptordra,art_ginryu,nor_ginryu,wil_ginryu,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("wil_gaiou", "art_x_omega,nor_x_omega,wil_x_omega");
        map.put("wil_gigaseadra", "");
        map.put("wil_ginryu", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera,art_grade,nor_grade,wil_grade,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("wil_grade", "art_dorugora,nor_dorugora,wil_dorugora,art_alpha,nor_alpha,wil_alpha,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga,art_ouryu,nor_ouryu,wil_ouryu,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("wil_grandiskuwaga", "");
        map.put("wil_hisyaryu", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi,art_alpha,nor_alpha,wil_alpha,art_tigervespa,nor_tigervespa,wil_tigervespa,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("wil_mametyra", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("wil_medievalduke", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("wil_metalfanto", "art_alpha,nor_alpha,wil_alpha,art_ouryu,nor_ouryu,wil_ouryu,art_metalpirani,nor_metalpirani,wil_metalpirani");
        map.put("wil_metalpirani", "");
        map.put("wil_omeka", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("wil_ory_alpha ", "");
        map.put("wil_ouryu", "");
        map.put("wil_raptordra", "art_dorugure,nor_dorugure,wil_dorugure,art_grade,nor_grade,wil_grade,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa,art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("wil_ryuda", "art_ginryu,nor_ginryu,wil_ginryu,art_tobucat,nor_tobucat,wil_tobucat,art_x_allo,nor_x_allo,wil_x_allo,art_x_grow,nor_x_grow,wil_x_grow,art_x_monochro,nor_x_monochro,wil_x_monochro,art_raptordra,nor_raptordra,wil_raptordra,art_wasp,nor_wasp,wil_wasp,art_omeka,nor_omeka,wil_omeka");
        map.put("wil_skullbaluchi", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("wil_tigervespa", "");
        map.put("wil_tobucat", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("wil_ultimatebrachi", "");
        map.put("wil_wasp", "art_grade,nor_grade,wil_grade,art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_cannonbee,nor_cannonbee,wil_cannonbee,art_metalfanto,nor_metalfanto,wil_metalfanto");
        map.put("wil_x_agu", "art_doruga,nor_doruga,wil_doruga,art_deathxdoruga,nor_deathxdoruga,wil_deathxdoruga,art_raptordra,nor_raptordra,wil_raptordra,art_x_seadra,nor_x_seadra,wil_x_seadra,art_x_grey,nor_x_grey,wil_x_grey,art_x_kuwaga,nor_x_kuwaga,wil_x_kuwaga");
        map.put("wil_x_allo", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("wil_x_duke", "");
        map.put("wil_x_grey", "art_dorugure,nor_dorugure,wil_dorugure,art_x_metalgrey,nor_x_metalgrey,wil_x_metalgrey,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("wil_x_grow", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("wil_x_guil", "art_ginryu,nor_ginryu,wil_ginryu,art_tobucat,nor_tobucat,wil_tobucat,art_x_allo,nor_x_allo,wil_x_allo,art_x_grow,nor_x_grow,wil_x_grow,art_x_monochro,nor_x_monochro,wil_x_monochro");
        map.put("wil_x_kuwaga", "art_dorugure,nor_dorugure,wil_dorugure,art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("wil_x_megalogrow", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("wil_x_megaseadra", "art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_x_wargrey,nor_x_wargrey,wil_x_wargrey,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("wil_x_metalgrey", "art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_x_wargrey,nor_x_wargrey,wil_x_wargrey,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("wil_x_monochro", "art_hisyaryu,nor_hisyaryu,wil_hisyaryu,art_skullbaluchi,nor_skullbaluchi,wil_skullbaluchi,art_mametyra,nor_mametyra,wil_mametyra,art_x_megalogrow,nor_x_megalogrow,wil_x_megalogrow,art_x_tricera,nor_x_tricera,wil_x_tricera");
        map.put("wil_x_okuwa", "art_dorugora,nor_dorugora,wil_dorugora,art_gigaseadra,nor_gigaseadra,wil_gigaseadra,art_grandiskuwaga,nor_grandiskuwaga,wil_grandiskuwaga");
        map.put("wil_x_omega", "");
        map.put("wil_x_seadra", "art_x_megaseadra,nor_x_megaseadra,wil_x_megaseadra,art_x_okuwa,nor_x_okuwa,wil_x_okuwa");
        map.put("wil_x_tricera", "art_ouryu,nor_ouryu,wil_ouryu,art_dinotige,nor_dinotige,wil_dinotige,art_x_ulforcevdra,nor_x_ulforcevdra,wil_x_ulforcevdra,art_dinorex,nor_dinorex,wil_dinorex,art_medievalduke,nor_medievalduke,wil_medievalduke,art_ultimatebrachi,nor_ultimatebrachi,wil_ultimatebrachi");
        map.put("wil_x_ulforcevdra", "art_x_duke,nor_x_duke,wil_x_duke");
        map.put("wil_x_wargrey", "art_x_omega,nor_x_omega,wil_x_omega");
    }

        public static String getPenxIndex(String rom) {
        // [slot(2)][version(1)][Attr(1)]
        String  index = leadingZeros(Integer.toHexString(Integer.parseInt(leadingZeros(Integer.toBinaryString(Integer.parseInt(rom.substring(1,3), 16)), 8).substring(2), 2)), 2).toUpperCase(),
                version = rom.substring(0,1),
                attr = Integer.toHexString(Integer.parseInt(leadingZeros(Integer.toBinaryString(Integer.parseInt(rom.substring(10,11), 16)), 4).substring(0,2), 2));
        return index+version+attr;
    } //
    public static String getPenxMonFromRom(String rom) {
        map = new HashMap<String, String>();
        penxIndexInit();
        String index = getPenxIndex(rom);

        if(map.containsKey(index)) return map.get(index);
        else return "err";
    } //
    public static String getPenxMonFromIndex(String index) {
        map = new HashMap<String, String>();
        penxIndexInit();

        if(map.containsKey(index)) return map.get(index);
        else return "err";
    }
    public static String getPenxPower(String rom) {
        return rom.substring(6,8);
    } //
    public static String getPenxAttribute(String index) {
        // return 2 bits

        return leadingZeros(Integer.toBinaryString(Integer.parseInt(index.substring(3,4))), 2);
    } //

     */

    public static String getPenxShotPatternFromPresses(int presses) {
        if(presses > 24) return "66666";
        else {
            String[] shotSizes =
                    new String[] {  "11111",
                            "22111",
                            "23231",
                            "33222",
                            "22222",
                            "22333",
                            "32333",
                            "31261",
                            "22361",
                            "26313",
                            "62361",
                            "33333",
                            "33333",
                            "62222",
                            "33336",
                            "26666",
                            "66222",
                            "23313",
                            "23623",
                            "66363",
                            "63636",
                            "36666",
                            "63666",
                            "66636",
                            "66666"};
            return shotSizes[presses];
        }
    }
    public static String generatePenxRomV2FromBase(String digiROM, int presses) {
        char[] rom = new String(digiROM/*"0159-4379-0009-C009"*/).toCharArray();
        if(rom[0] == '2') rom[0] = '0';

        // Presses
        String  patt = getPenxShotPatternFromPresses(presses).replace("6", "4"),
                pattBits =  leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(0,1))-1), 2) +
                            leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(1,2))-1), 2) +
                            leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(2,3))-1), 2) +
                            leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(3,4))-1), 2) +
                            leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(4,5))-1), 2);
        pattBits = leadingZeros(Integer.toHexString(Integer.parseInt(pattBits, 2)), 3).toUpperCase();

        String  attributeBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[10], 16)), 4),
                patternBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(pattBits.substring(0,1), 16)), 4),
                combBits = Integer.toHexString(Integer.parseInt(attributeBits.substring(0,2)+patternBits.substring(2,4), 2)).toUpperCase();

        rom[10] = combBits.toCharArray()[0];
        rom[11] = pattBits.toCharArray()[1];
        rom[12] = pattBits.toCharArray()[2];

        return new String(rom);
    }
    public static String generatePenxForcedV1FromBase(String digiROM, boolean win) {
        char[] rom = new String(digiROM/*"0159-4379-0009-C009"*/).toCharArray(), hp;
        if(rom[0] == '2') rom[0] = '1';
        int presses = 0;

        if(win) {
            presses = 0;
            hp = "00".toCharArray();
        }
        else {
            presses = 31;
            hp = "1F".toCharArray();
        }


        // Presses
        String  patt = getPenxShotPatternFromPresses(presses).replaceAll("6", "4"),
                pattBits =  leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(0,1))-1), 2) +
                        leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(1,2))-1), 2) +
                        leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(2,3))-1), 2) +
                        leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(3,4))-1), 2) +
                        leadingZeros(Integer.toBinaryString(Integer.parseInt(patt.substring(4,5))-1), 2);
        pattBits = leadingZeros(Integer.toHexString(Integer.parseInt(pattBits, 2)), 3).toUpperCase();

        String  attributeBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(""+rom[10], 16)), 4),
                patternBits = leadingZeros(Integer.toBinaryString(Integer.parseInt(pattBits.substring(0,1), 16)), 4),
                combBits = Integer.toHexString(Integer.parseInt(attributeBits.substring(0,2)+patternBits.substring(2,4), 2)).toUpperCase();

        rom[10] = combBits.toCharArray()[0];
        rom[11] = pattBits.toCharArray()[1];
        rom[12] = pattBits.toCharArray()[2];

        // 2099-4089-8009-91F9

        rom[16] = hp[0];
        rom[17] = hp[1];

        String romm = new String(rom);
        return "X1-"+romm.substring(0,15)+"@4"+romm.substring(16);
        //return "X1-"+new String(rom);
    }

    // ========== Mini ==========
    private static void miniV1MapInit() {
        mapp = new HashMap<String, String[]>();
        mapp.put("01", new String[] {"koro", "Baby II", "Free", ",agu,gabu,beta"});
        mapp.put("02", new String[] {"agu", "Child", "Vaccine", ",grey,garuru,seadra,nume"});
        mapp.put("03", new String[] {"gabu", "Child", "Data", ",garuru,seadra,nume"});
        mapp.put("04", new String[] {"beta", "Child", "Virus", ",garuru,seadra,nume"});
        mapp.put("05", new String[] {"grey", "Adult", "Vaccine", ",metalgrey,tricera"});
        mapp.put("06", new String[] {"garuru", "Adult", "Vaccine", ",tricera"});
        mapp.put("07", new String[] {"seadra", "Adult", "Data", ",tricera"});
        mapp.put("08", new String[] {"nume", "Adult", "Virus", ",metalgrey"});
        mapp.put("09", new String[] {"metalgrey", "Perfect", "Vaccine", ",wargrey"});
        mapp.put("0A", new String[] {"tricera", "Perfect", "Data", ""});
        mapp.put("0B", new String[] {"wargrey", "Ultimate", "Vaccine", ",omega"});
        mapp.put("0C", new String[] {"omega", "Super Ultimate", "Vaccine", ""});
    }
    private static void miniV2MapInit() {
        mapp = new HashMap<String, String[]>();
        mapp.put("01", new String[] {"mochi", "Baby II", "Free", ",tento,elec,kune"});
        mapp.put("02", new String[] {"tento", "Child", "Vaccine", ",kabuteri,cental,tyranno,suka"});
        mapp.put("03", new String[] {"elec", "Child", "Data", ",cental,tyranno,suka"});
        mapp.put("04", new String[] {"kune", "Child", "Virus", ",cental,tyranno,suka"});
        mapp.put("05", new String[] {"kabuteri", "Adult", "Vaccine", ",alturkabuteri,warumonzae"});
        mapp.put("06", new String[] {"cental", "Adult", "Data", ",warumonzae"});
        mapp.put("07", new String[] {"tyranno", "Adult", "Data", ",warumonzae"});
        mapp.put("08", new String[] {"suka", "Adult", "Virus", ",alturkabuteri"});
        mapp.put("09", new String[] {"alturkabuteri", "Perfect", "Vaccine", ",heraklekabuteri"});
        mapp.put("0A", new String[] {"warumonzae", "Perfect", "Virus", ""});
        mapp.put("0B", new String[] {"heraklekabuteri", "Ultimate", "Vaccine", ",tyrantkabuteri"});
        mapp.put("0C", new String[] {"tyrantkabuteri", "Super Ultimate", "Virus", ""});
    }
    private static void miniV3MapInit() {
        mapp = new HashMap<String, String[]>();
        mapp.put("01", new String[] {"toko", "Baby II", "Free", ",pata,picodevi,gani"});
        mapp.put("02", new String[] {"pata", "Child", "Vaccine", ",ange,devi,leo,nani"});
        mapp.put("03", new String[] {"picodevi", "Child", "Virus", ",devi,shell,coela,nani"});
        mapp.put("04", new String[] {"gani", "Child", "Data", ",leo,shell,coela,nani"});
        mapp.put("05", new String[] {"ange", "Adult", "Vaccine", ",coela,nani"});
        mapp.put("06", new String[] {"devi", "Adult", "Virus", ",vamde"});
        mapp.put("07", new String[] {"leo", "Adult", "Vaccine", ",ete"});
        mapp.put("08", new String[] {"shell", "Adult", "Data", ""});
        mapp.put("09", new String[] {"coela", "Adult", "Data", ""});
        mapp.put("0A", new String[] {"nani", "Adult", "Virus", ",ete"});
        mapp.put("0B", new String[] {"holyange", "Perfect", "Vaccine", ",holydra"});
        mapp.put("0C", new String[] {"vamde", "Perfect", "Virus", ",pie"});
        mapp.put("0D", new String[] {"ete", "Perfect", "Virus", ",bancholeo"});
        mapp.put("0E", new String[] {"holydra", "Ultimate", "Vaccine", ",_stm_luce"});
        mapp.put("0F", new String[] {"pie", "Ultimate", "Virus", ",_stm_luce"});
        mapp.put("10", new String[] {"bancholeo", "Ultimate", "Vaccine", ""});
        mapp.put("11", new String[] {"_stm_luce", "Super Ultimate", "Virus", ""});
    }

    public static String getMiniIndex(String rom) {
        return leadingZeros(Integer.toHexString(Integer.parseInt(leadingZeros(Integer.toBinaryString(Integer.parseInt(rom.substring(1,3), 16)), 8).substring(2), 2)), 2).toUpperCase();
    }
    public static String getMiniMonFromRom(String rom, int ver) {
        switch(ver) {
            case 1:
                miniV1MapInit();
                break;
            case 2:
                miniV2MapInit();
                break;
            case 3:
                miniV3MapInit();
                break;
            default: break;
        }
        String index = getMiniIndex(rom);

//        if(mapp.containsKey(index)) {
//            Log.e("From ROM GOT", mapp.get(index)[0]);
//        }
//        else{
//            Log.e("From ROM Err", ": "+index);
//        }

        if(mapp.containsKey(index)) return mapp.get(index)[0];
        else return "err";
    }
    public static String getMiniMonFromIndex(String index, int ver) {
        switch(ver) {
            case 1:
                miniV1MapInit();
                break;
            case 2:
                miniV2MapInit();
                break;
            case 3:
                miniV3MapInit();
                break;
            default: break;
        }

//        if(mapp.containsKey(index)) {
//            Log.e("From Index GOT", mapp.get(index)[0]);
//        }
//        else{
//            Log.e("From Index Err", ": "+index);
//        }

        if(mapp.containsKey(index)) return mapp.get(index)[0];
        else return "err";
    }
    public static String getMiniLevelFromIndex(String index, int ver) {
        switch(ver) {
            case 1:
                miniV1MapInit();
                break;
            case 2:
                miniV2MapInit();
                break;
            case 3:
                miniV3MapInit();
                break;
            default: break;
        }

        if(mapp.containsKey(index)) return mapp.get(index)[1];
        else return "err";
    }
    public static String getMiniPower(String rom) {
        return rom.substring(6,8);
    } //
    public static String getMiniAttribute(String index, int ver) {
        switch(ver) {
            case 1:
                miniV1MapInit();
                break;
            case 2:
                miniV2MapInit();
                break;
            case 3:
                miniV3MapInit();
                break;
            default: break;
        }

        if(mapp.containsKey(index)) return mapp.get(index)[2];
        else return "err";
    }
    public static boolean checkEvoMini(String index, String mon, int ver) {
        String s ="";
        switch(ver) {
            case 91:
                miniV1MapInit();
                s = "1";
                break;
            case 92:
                miniV2MapInit();
                s = "2";
                break;
            case 93:
                miniV3MapInit();
                s = "3";
                break;
            default: break;
        }

        Log.e("===== Rom Manager Mini Check Evo ====", "[ "+ ver + " | "+ mapp.get(index)[3].contains(","+mon) + " | " + mapp.get(index)[0].equals(mon) + " ]");
        if(mapp.containsKey(index)) return (mapp.get(index)[3].contains(","+mon) || mapp.get(index)[0].equals(mon));
        else return false;
    }

    // ========== GENERAL UTILITY ==========
    public static String getIndexGeneral(String digiROM, int commType) {
        switch(commType) {
            case 1: return RomManager.getPen20Index(digiROM);
            case 3: return RomManager.getDM20Index(digiROM);
            case 7: return RomManager.getDmogIndex(digiROM);
            case 8: return getPenOGIndex(digiROM);
            case 9: return RomManager.getPenxIndex(digiROM);
            case 91:
            case 92:
            case 93:
                return RomManager.getMiniIndex(digiROM);
            default: return "err";
        }
    }
    public static String getLevelGeneral(String digiROM, int commType) {
        switch(commType) {
            case 1: return getPen20Level(getPen20Index(digiROM));
            case 3: return getDm20Level(getDM20Index(digiROM));
            case 7: return getDmogLevel(getDmogIndex(digiROM));
            case 8: return getPenOGLevel(getPenOGIndex(digiROM));
            case 9: return getPenxLevelFromIndex(getPenxIndex(digiROM));
            case 91: return getMiniLevelFromIndex(getMiniIndex(digiROM), 1);
            case 92: return getMiniLevelFromIndex(getMiniIndex(digiROM), 2);
            case 93: return getMiniLevelFromIndex(getMiniIndex(digiROM), 3);
            default: return "err";
        }
    }
    public static String generateForcedV1FromBaseGeneral(String digiROM, int commType, boolean win) {
        switch(commType) {
            case 1: return RomManager.generatePen20ForcedV1FromBase(digiROM, win);
            case 3: return RomManager.generateDM20ForcedV1FromBase(digiROM, Challenges.getInstance().currentChallenge().getOpp(), win);
            case 7: return RomManager.generateDmogForcedV1(Integer.parseInt(getDmogIndex(digiROM), 16), win);
            case 8: return generatePenOGForcedV1FromBase(digiROM, win);
            case 9:
            case 91:
            case 92:
            case 93:
                return RomManager.generatePenxForcedV1FromBase(digiROM, win);
            default: return "err";
        }
    }

    public static int levelVal(String level) {
        switch(level) {
            case "Baby I": return 0;
            case "Baby II": return 1;
            case "Child": return 2;
            case "Adult": return 3;
            case "Perfect": return 4;
            case "Ultimate": return 5;
            case "Super Ultimate": return 6;
            default: return -1;
        }
    }
    public static boolean checkCommTypesCompatibility(int typeA, int typeB) {
        //Log.e("RMNGR CCTC", "| "+typeA+" | "+typeB+" |");
        switch(typeA) {
            case 1: return (typeB == 1);
            case 3: return (typeB == 3);
            case 6: return (typeB == 6);
            case 7: return (typeB == 7);
            case 8: return (typeB == 8);
            case 9: return (typeB == 9 || typeB == 91 || typeB == 92 || typeB == 93);
            case 91: return (typeB == 9 || typeB == 91 || typeB == 92 || typeB == 93);
            case 92: return (typeB == 9 || typeB == 91 || typeB == 92 || typeB == 93);
            case 93: return (typeB == 9 || typeB == 91 || typeB == 92 || typeB == 93);
            case 10: return (typeB == 10);
            default: return false;
        }
    }

    // ========== OTHER ==========
    public static String leadingZeros(String input, int length) {
        if(input.length() < length){
            for(int j = input.length(); j<length ; j++) input = "0"+input;
        }
        return input;
    }
}

/*

mapp.put("0", new String[] {"bota", "Baby I", "11", ",koro", "0"});
mapp.put("1", new String[] {"koro", "Baby II", "11", ",agu,beta,_tai_agu", "0"});
mapp.put("2", new String[] {"agu", "Child", "00", ",grey,tyranno,devi,mera,nume", "18"});
mapp.put("3", new String[] {"beta", "Child", "10", ",devi,mera,airdra,seadra,nume", "10"});
mapp.put("4", new String[] {"grey", "Adult", "00", ",metalgrey", "75"});
mapp.put("5", new String[] {"tyranno", "Adult", "01", ",mame", "70"});
mapp.put("6", new String[] {"devi", "Adult", "10", ",metalgrey", "65"});
mapp.put("7", new String[] {"mera", "Adult", "01", ",mame", "60"});
mapp.put("8", new String[] {"airdra", "Adult", "00", ",metalgrey", "55"});
mapp.put("9", new String[] {"seadra", "Adult", "01", ",mame", "50"});
mapp.put("10", new String[] {"nume", "Adult", "10", ",monzae", "40"});
mapp.put("11", new String[] {"metalgrey", "Perfect", "10", ",blitzgrey", "126"});
mapp.put("12", new String[] {"mame", "Perfect", "01", ",banchomame", "118"});
mapp.put("13", new String[] {"monzae", "Perfect", "00", ",", "107"});
mapp.put("14", new String[] {"blitzgrey", "Ultimate", "10", ",_als_omega", "188"});
mapp.put("15", new String[] {"banchomame", "Ultimate", "01", ",", "176"});
mapp.put("16", new String[] {"puni", "Baby I", "11", ",tuno", "0"});
mapp.put("17", new String[] {"tuno", "Baby II", "11", ",gabu,elec,_yam_gabu", "0"});
mapp.put("18", new String[] {"gabu", "Child", "01", ",kabuteri,garuru,ange,yukidaru,veggie", "18"});
mapp.put("19", new String[] {"elec", "Child", "01", ",ange,yukidaru,birdra,wha,veggie", "10"});
mapp.put("20", new String[] {"kabuteri", "Adult", "00", ",skullgrey", "75"});
mapp.put("21", new String[] {"garuru", "Adult", "00", ",metalmame", "70"});
mapp.put("22", new String[] {"ange", "Adult", "00", ",skullgrey", "65"});
mapp.put("23", new String[] {"yukidaru", "Adult", "00", ",metalmame", "60"});
mapp.put("24", new String[] {"birdra", "Adult", "00", ",skullgrey", "55"});
mapp.put("25", new String[] {"wha", "Adult", "00", ",metalmame", "50"});
mapp.put("26", new String[] {"veggie", "Adult", "10", ",vade", "40"});
mapp.put("27", new String[] {"skullgrey", "Perfect", "10", ",skullmam", "126"});
mapp.put("28", new String[] {"metalmame", "Perfect", "01", ",cresgaruru", "118"});
mapp.put("29", new String[] {"vade", "Perfect", "10", ",", "107"});
mapp.put("30", new String[] {"skullmam", "Ultimate", "00", ",", "169"});
mapp.put("31", new String[] {"cresgaruru", "Ultimate", "01", ",_als_omega", "188"});
mapp.put("32", new String[] {"poyo", "Baby I", "11", ",toko", "0"});
mapp.put("33", new String[] {"toko", "Baby II", "11", ",pata,kune,", "0"});
mapp.put("34", new String[] {"pata", "Child", "01", ",uni,kentaru,ogre,bake,suka", "18"});
mapp.put("35", new String[] {"kune", "Child", "10", ",ogre,bake,shell,drimoge,suka", "10"});
mapp.put("36", new String[] {"uni", "Adult", "00", ",andro", "75"});
mapp.put("37", new String[] {"kentaru", "Adult", "01", ",giro", "70"});
mapp.put("38", new String[] {"ogre", "Adult", "10", ",andro", "65"});
mapp.put("39", new String[] {"bake", "Adult", "10", ",giro", "60"});
mapp.put("40", new String[] {"shell", "Adult", "01", ",andro", "55"});
mapp.put("41", new String[] {"drimoge", "Adult", "01", ",giro", "50"});
mapp.put("42", new String[] {"suka", "Adult", "10", ",ete", "40"});
mapp.put("43", new String[] {"andro", "Perfect", "00", ",hiandro", "126"});
mapp.put("44", new String[] {"giro", "Perfect", "00", ",", "118"});
mapp.put("45", new String[] {"ete", "Perfect", "10", ",kingete", "107"});
mapp.put("46", new String[] {"hiandro", "Ultimate", "00", ",", "176"});
mapp.put("47", new String[] {"kingete", "Ultimate", "10", ",", "169"});
mapp.put("48", new String[] {"yura", "Baby I", "11", ",tane", "0"});
mapp.put("49", new String[] {"tane", "Baby II", "11", ",biyo,pal,", "0"});
mapp.put("50", new String[] {"biyo", "Child", "00", ",monochro,cockatri,leo,kuwaga,nani", "18"});
mapp.put("51", new String[] {"pal", "Child", "01", ",leo,kuwaga,coela,mojya,nani", "10"});
mapp.put("52", new String[] {"monochro", "Adult", "01", ",megadra", "75"});
mapp.put("53", new String[] {"cockatri", "Adult", "01", ",piccolo", "70"});
mapp.put("54", new String[] {"leo", "Adult", "00", ",megadra", "65"});
mapp.put("55", new String[] {"kuwaga", "Adult", "10", ",piccolo", "60"});
mapp.put("56", new String[] {"coela", "Adult", "01", ",megadra", "55"});
mapp.put("57", new String[] {"mojya", "Adult", "00", ",piccolo", "50"});
mapp.put("58", new String[] {"nani", "Adult", "10", ",digitama", "40"});
mapp.put("59", new String[] {"megadra", "Perfect", "10", ",aegisdra", "126"});
mapp.put("60", new String[] {"piccolo", "Perfect", "01", ",", "118"});
mapp.put("61", new String[] {"digitama", "Perfect", "01", ",tita", "107"});
mapp.put("62", new String[] {"aegisdra", "Ultimate", "00", ",rusttyranno", "188"});
mapp.put("63", new String[] {"tita", "Ultimate", "10", ",", "176"});
mapp.put("64", new String[] {"zuru", "Baby I", "11", ",pagu", "0"});
mapp.put("65", new String[] {"pagu", "Baby II", "11", ",gazi,giza,", "0"});
mapp.put("66", new String[] {"gazi", "Child", "10", ",darktyranno,cyclo,devidra,tusk,rare", "18"});
mapp.put("67", new String[] {"giza", "Child", "10", ",devidra,tusk,fly,delta,rare", "10"});
mapp.put("68", new String[] {"darktyranno", "Adult", "10", ",metaltyranno", "75"});
mapp.put("69", new String[] {"cyclo", "Adult", "10", ",nano", "70"});
mapp.put("70", new String[] {"devidra", "Adult", "10", ",metaltyranno", "65"});
mapp.put("71", new String[] {"tusk", "Adult", "10", ",nano", "60"});
mapp.put("72", new String[] {"fly", "Adult", "10", ",metaltyranno", "55"});
mapp.put("73", new String[] {"delta", "Adult", "10", ",nano", "50"});
mapp.put("74", new String[] {"rare", "Adult", "10", ",extyranno", "40"});
mapp.put("75", new String[] {"metaltyranno", "Perfect", "10", ",mugendra", "126"});
mapp.put("76", new String[] {"nano", "Perfect", "10", ",", "118"});
mapp.put("77", new String[] {"extyranno", "Perfect", "00", ",pinochi", "107"});
mapp.put("78", new String[] {"mugendra", "Ultimate", "10", ",rusttyranno", "188"});
mapp.put("79", new String[] {"pinochi", "Ultimate", "10", ",", "176"});
mapp.put("80", new String[] {"saku", "Baby I", "11", ",sakutto", "0"});
mapp.put("81", new String[] {"sakutto", "Baby II", "11", ",zuba,hack,", "0"});
mapp.put("82", new String[] {"zuba", "Child", "00", ",zubaeager", "34"});
mapp.put("83", new String[] {"zubaeager", "Adult", "00", ",dura", "90"});
mapp.put("84", new String[] {"dura", "Perfect", "00", ",duranda", "155"});
mapp.put("85", new String[] {"duranda", "Ultimate", "00", ",", "210"});
mapp.put("86", new String[] {"hack", "Child", "01", ",baohack", "34"});
mapp.put("87", new String[] {"baohack", "Adult", "01", ",saviorhack", "90"});
mapp.put("88", new String[] {"saviorhack", "Perfect", "01", ",jes", "155"});
mapp.put("89", new String[] {"jes", "Ultimate", "01", ",", "210"});
mapp.put("90", new String[] {"petit", "Baby I", "11", ",babyd", "0"});
mapp.put("91", new String[] {"babyd", "Baby II", "11", ",draco", "0"});
mapp.put("92", new String[] {"draco", "Child", "01", ",_blu_coredra,_gre_coredra,", "34"});
mapp.put("93", new String[] {"_blu_coredra", "Adult", "00", ",wingdra", "80"});
mapp.put("94", new String[] {"wingdra", "Perfect", "00", ",slayerdra,_tai_wargrey,", "143"});
mapp.put("95", new String[] {"slayerdra", "Ultimate", "00", ",exa", "199"});
mapp.put("96", new String[] {"_gre_coredra", "Adult", "10", ",groundra", "80"});
mapp.put("97", new String[] {"groundra", "Perfect", "10", ",breakdra,_yam_metalgaruru,", "143"});
mapp.put("98", new String[] {"breakdra", "Ultimate", "10", ",exa", "199"});
mapp.put("99", new String[] {"pitch", "Baby I", "11", ",puka", "0"});
mapp.put("100", new String[] {"puka", "Baby II", "11", ",corona,luna,", "0"});
mapp.put("101", new String[] {"corona", "Child", "00", ",fira", "27"});
mapp.put("102", new String[] {"fira", "Adult", "00", ",flare", "80"});
mapp.put("103", new String[] {"flare", "Perfect", "00", ",apollo", "135"});
mapp.put("104", new String[] {"apollo", "Ultimate", "00", ",gracenova", "199"});
mapp.put("105", new String[] {"luna", "Child", "01", ",lekis", "27"});
mapp.put("106", new String[] {"lekis", "Adult", "01", ",cresce", "80"});
mapp.put("107", new String[] {"cresce", "Perfect", "01", ",diana", "135"});
mapp.put("108", new String[] {"diana", "Ultimate", "01", ",gracenova", "199"});
mapp.put("109", new String[] {"_tai_agu", "Child", "00", ",_tai_grey", "25"});
mapp.put("110", new String[] {"_tai_grey", "Adult", "00", ",_tai_metalgrey", "83"});
mapp.put("111", new String[] {"_tai_metalgrey", "Perfect", "00", ",_tai_wargrey", "135"});
mapp.put("112", new String[] {"_tai_wargrey", "Ultimate", "00", ",omega", "199"});
mapp.put("113", new String[] {"_yam_gabu", "Child", "01", ",_yam_garuru", "25"});
mapp.put("114", new String[] {"_yam_garuru", "Adult", "00", ",_yam_weregaruru", "77"});
mapp.put("115", new String[] {"_yam_weregaruru", "Perfect", "00", ",_yam_metalgaruru", "135"});
mapp.put("116", new String[] {"_yam_metalgaruru", "Ultimate", "01", ",omega", "199"});
mapp.put("117", new String[] {"dodo", "Baby I", "11", ",dori", "0"});
mapp.put("118", new String[] {"dori", "Baby II", "11", ",doru", "0"});
mapp.put("119", new String[] {"doru", "Child", "01", ",doruga", "27"});
mapp.put("120", new String[] {"doruga", "Adult", "01", ",dorugure", "90"});
mapp.put("121", new String[] {"dorugure", "Perfect", "01", ",alpha", "143"});
mapp.put("122", new String[] {"alpha", "Ultimate", "00", ",", "210"});
mapp.put("123", new String[] {"yukimibota", "Baby I", "11", ",nyaro", "0"});
mapp.put("124", new String[] {"nyaro", "Baby II", "11", ",plot", "0"});
mapp.put("125", new String[] {"plot", "Child", "00", ",meicoo", "27"});
mapp.put("126", new String[] {"meicoo", "Adult", "11", ",meicrack", "80"});
mapp.put("127", new String[] {"meicrack", "Perfect", "00", ",rasiel", "143"});
mapp.put("128", new String[] {"rasiel", "Ultimate", "00", ",", "210"});
mapp.put("129", new String[] {"_als_omega", "Super Ultimate", "10", ",", "238"});
mapp.put("130", new String[] {"rusttyranno", "Super Ultimate", "10", ",", "238"});
mapp.put("131", new String[] {"exa", "Super Ultimate", "01", ",", "238"});
mapp.put("132", new String[] {"gracenova", "Super Ultimate", "00", ",", "238"});
mapp.put("133", new String[] {"omega", "Super Ultimate", "00", ",", "238"});
mapp.put("134", new String[] {"_fdm_luce", "Perfect", "10", ",", "0"});
mapp.put("135", new String[] {"beelzebu", "Ultimate", "10", ",", "0"});
mapp.put("136", new String[] {"_rgm_belphe", "Ultimate", "10", ",", "0"});
mapp.put("137", new String[] {"lilith", "Ultimate", "10", ",", "0"});
mapp.put("138", new String[] {"de", "Ultimate", "10", ",", "0"});
mapp.put("139", new String[] {"barba", "Ultimate", "10", ",", "0"});
mapp.put("140", new String[] {"levia", "Ultimate", "10", ",", "0"});
mapp.put("141", new String[] {"belialvamde", "Ultimate", "10", ",", "0"});
mapp.put("142", new String[] {"death", "Ultimate", "10", ",", "0"});
mapp.put("143", new String[] {"murmukus", "Ultimate", "10", ",", "0"});
mapp.put("144", new String[] {"_plm_imperialdra", "Super Ultimate", "11", ",", "0"});
mapp.put("145", new String[] {"ulforcevdra", "Ultimate", "00", ",", "0"});
mapp.put("146", new String[] {"sleip", "Ultimate", "00", ",", "0"});
mapp.put("147", new String[] {"duft", "Ultimate", "01", ",", "0"});
mapp.put("148", new String[] {"magna", "Ultimate", "11", ",", "0"});
mapp.put("149", new String[] {"lordknight", "Ultimate", "10", ",", "0"});
mapp.put("150", new String[] {"cranium", "Ultimate", "00", ",", "0"});
mapp.put("151", new String[] {"dynas", "Ultimate", "01", ",", "0"});
mapp.put("152", new String[] {"gankoo", "Ultimate", "01", ",", "0"});
mapp.put("153", new String[] {"duke", "Ultimate", "10", ",", "0"});



 */