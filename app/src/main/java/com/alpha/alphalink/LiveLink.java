//package com.alpha.alphalink;
//
//import android.animation.Animator;
//import android.animation.AnimatorListenerAdapter;
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.Rect;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.KeyEvent;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.AdapterView;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.constraintlayout.widget.ConstraintLayout;
//import androidx.constraintlayout.widget.ConstraintSet;
//
//import com.alpha.alphalink.AlphaCore.AlphaEngine;
//import com.alpha.alphalink.AlphaCore.DM20BattleCalculator;
//import com.alpha.alphalink.AlphaCore.Pen20BattleCalculator;
//import com.alpha.alphalink.LinkCore.ChallengeManager;
//import com.alpha.alphalink.LinkCore.Link;
//import com.alpha.alphalink.LinkCore.NodeServer.UserN;
//import com.alpha.alphalink.LinkCore.RomManager;
//import com.alpha.alphalink.LinkCore.User;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.firestore.DocumentReference;
//import com.google.firebase.firestore.DocumentSnapshot;
//import com.google.firebase.firestore.EventListener;
//import com.google.firebase.firestore.FirebaseFirestore;
//import com.google.firebase.firestore.FirebaseFirestoreException;
//import com.google.gson.Gson;
//import com.physicaloid.lib.Physicaloid;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.Date;
//import java.util.List;
//
//import static android.view.WindowManager.LayoutParams;
//
//public class LiveLink extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
//    static LiveLink k;
//    private Context th = this;
//    private User user;
//    private TextView title;
//    private ConstraintLayout[] messages, linkBoxes, linkBoxBgs;
//    private ImageView[] msgIcos, linkBoxMonIcos;
//    private TextView[] unms, txt, usltxts, uslspcrs, linkBoxOriginators, linkBoxRanks, linkBoxLevels,
//            linkBoxPositions, linkBoxScores, linkBoxCommTypes;
//    private View prevSel;
//    private int commType, state, slotSel, msgSel, slotCount, backCnt = 0, chrgcnt = 0;
//    private int[] open = {9, 9};
//    private String rom, opp, index, ncOppName;
//    private boolean srch = false, thrd = false, retry = true, ringsSpin = false, ringsSpinTwo,
//                    hasLinksList = false, dropDown = false, pressed = false, del = false,
//                    debugLog = true;
//    private boolean[] filters;
//    private List<String> friendsList;
//    private Physicaloid ardu;
//    private AlphaEngine alpha;
//    private Thread findThread, scanThread;
//    private DM20BattleCalculator dm20BCalc;
//    private Pen20BattleCalculator pen20BCalc;
//    private Animation expOne, expTwo, expThree;
//    private Link[] linksList;
//    private EditText lilisrchbar;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_livelink);
//        //init();
//    }
//
////    private void init() {
////        k = this;
////
////        findViewById(R.id.livelinkbg).setBackgroundColor(getResources().getColor(R.color.colorBg));
////
////        DisplayMetrics dm = new DisplayMetrics();
////        getWindowManager().getDefaultDisplay().getMetrics(dm);
////        int width = ((int) (dm.widthPixels - (50*dm.density))), height = dm.heightPixels;
////        getWindow().setLayout(width, height);
////        LayoutParams params = getWindow().getAttributes();
////        params.gravity = Gravity.RIGHT;
////        getWindow().setFlags(LayoutParams.FLAG_NOT_TOUCH_MODAL,
////                LayoutParams.FLAG_NOT_TOUCH_MODAL);
////        getWindow().setFlags(LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
////                LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
////
////        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.liliSwipeContainer);
////        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
////            @Override
////            public void onRefresh() {
////                pullToRefresh.setRefreshing(false);
////                makeToast("Updating List...", 1);
////            }
////        });
////
////        user = (User) getIntent().getSerializableExtra("User");
////        msgs = (Challenge[]) getIntent().getSerializableExtra("msgs");
////        linksList = (Link[]) getIntent().getSerializableExtra("linksList");
////        getUsersListFromString((String) getIntent().getSerializableExtra("usersListString"));
////
////        initialiseLinksSearch();
////
////        initialiseLinks();
////    }
//    private void genFakeLinksList() {
//        linksList = new Link[] {
//            new Link("Katsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352311203"),
//            new Link("Kbtsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352312203"),
//            new Link("Kctsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352313203"),
//            new Link("Kdtsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352314203"),
//            new Link("Ketsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352311203"),
//            new Link("Kftsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352312203"),
//            new Link("Kgtsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352313203"),
//            new Link("Khtsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352314203"),
//            new Link("Kitsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352311203"),
//            new Link("Kjtsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352312203"),
//            new Link("Kktsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352313203"),
//            new Link("Kltsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352314203"),
//            new Link("Kmtsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352311203"),
//            new Link("Kntsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352312203"),
//            new Link("Kotsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352313203"),
//            new Link("Kptsu", "0000-0000-801E-008E-05AE-012E-000E-000E-000E-E87E", "1", "9", "3", "1602352314203")
//        };
//    }
////    private void getFriends() {
////        friendsList = new ArrayList<String>();
////        if(msgs != null) {
////            orderChallenges();
////            if(msgs.length>0){
////                log("msgs", ""+msgs.length);
////
////
////            } else {
////                log("msgs", "0");
////            }
////            for(Challenge ch : msgs) {
////                if(ch.getA().equals(user.gUnm())){
////                    friendsList.add(ch.getB());
////                } else if(ch.getB().equals(user.gUnm())){
////                    friendsList.add(ch.getA());
////                }
////            }
////        } else log("frndsmsgs", "null");
////    }
////    private void orderChallenges() {
////        log("order", "order");
////
////        int active = 0;
////
////        if(msgs.length == 2) {
////            if((!isActiveRequiresAttention(msgs[0]) &&
////                 isActiveRequiresAttention(msgs[1])) &&
////                 (msgs[0].gENum() < msgs[1].gENum())) {
////                Challenge a = msgs[0];
////                msgs[0] = msgs[1];
////                msgs[1] = a;
////            }
////        }
////        else {
////            for (int i = msgs.length - 1; i>1; i--) {
////                for(int j = 0; j<i; j++) {
////                    log("Ordering", user.gUnm() + " vs " + ( (user.gUnm().equals(msgs[j].getA())) ? msgs[j].getB():user.gUnm() ) +": "+ msgs[j].gENum());
////
////                    if(msgs[j].gENum() < msgs[j+1].gENum()) {
////                        Challenge c = msgs[j];
////                        msgs[j] = msgs[j+1];
////                        msgs[j+1] = c;
////                    }
////                }
////            }
////
////            Challenge[] aF = new Challenge[msgs.length];
////
////            log("==", "===");
////            int i = 0;
////            for(Challenge ac : msgs) {
////                if(isActiveRequiresAttention(ac)) {
////                    aF[i++] = ac;
////                    log("Active", ac.docId());
////                }
////            }
////            for(Challenge ac : msgs) {
////                if(!isActiveRequiresAttention(ac)) {
////                    aF[i++] = ac;
////                    log("!Active", ac.docId());
////                }
////            }
////
////            msgs = aF;
////            log("==", "===");
////        }
////    }
////    private void initialiseViews() {
////        title = findViewById(R.id.Title);
////
////        messages = new ConstraintLayout[] {
////            findViewById(R.id.m1),
////            findViewById(R.id.m2),
////            findViewById(R.id.m3),
////            findViewById(R.id.m4),
////            findViewById(R.id.m5),
////            findViewById(R.id.m6),
////            findViewById(R.id.m7),
////            findViewById(R.id.m8),
////            findViewById(R.id.m9),
////            findViewById(R.id.m10),
////        };
////
////        for(int i = 0; i< messages.length; i++){
////            if (msgs!= null && i<msgs.length) anims("expand", messages[i]);
////            else messages[i].setVisibility(View.GONE);
////        }
////
//////        populateChallenges();
////
////        initialiseWindows();
////    }--
//    private void initialiseWindows() {
//        findViewById(R.id.inbbg).setVisibility(View.GONE);
//        findViewById(R.id.inbchallenge).setVisibility(View.GONE);
//        findViewById(R.id.inbstats).setVisibility(View.VISIBLE);
//        findViewById(R.id.inbchallengecard).setVisibility(View.VISIBLE);
//        findViewById(R.id.inbinfotextpl).setVisibility(View.GONE);
//        findViewById(R.id.inbchallengecancelearly).setVisibility(View.GONE);
//        findViewById(R.id.msgcommtypeid).setVisibility(View.GONE);
//        findViewById(R.id.inbchallengecontrols).setVisibility(View.VISIBLE);
//
//
//        findViewById(R.id.inbchallengewin).setVisibility(View.GONE);
//        findViewById(R.id.inbmon).setVisibility(View.VISIBLE);
//
//        findViewById(R.id.inbsndcenterico).setVisibility(View.GONE);
//
//        findViewById(R.id.inbmonlist).setVisibility(View.VISIBLE);
//        findViewById(R.id.inbsndlwrcntrls).setVisibility(View.VISIBLE);
//
//        findViewById(R.id.inbscanvpet).setVisibility(View.GONE);
//        findViewById(R.id.inbcbig).setVisibility(View.INVISIBLE);
//        findViewById(R.id.inbcsml).setVisibility(View.INVISIBLE);
//        findViewById(R.id.inbinfocenterico).setVisibility(View.INVISIBLE);
//        ((ImageView)findViewById(R.id.inbinfocenterico)).setScaleY(1f);
//        ((ImageView)findViewById(R.id.inbinfocenterico)).setScaleX(1f);
//
//        findViewById(R.id.inbcnctardu).setVisibility(View.GONE);
//
//        findViewById(R.id.inbpresswin).setVisibility(View.VISIBLE);
//        findViewById(R.id.inbncmon).setVisibility(View.GONE);
//        findViewById(R.id.inbnccenterico).setVisibility(View.GONE);
//    }
//    private void initialiseMessages() {
//        ConstraintLayout mw = findViewById(R.id.messageswin);
//        DisplayMetrics dm = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(dm);
//        int width = ((int) (dm.widthPixels - (50*dm.density)));
//
//        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) mw.getLayoutParams();
//        lp.width = width;
//        mw.setLayoutParams(lp);
//        mw.setTranslationX(width);
//    }
////    private void initialiseSlots() {
////        slotCount = 0;
////        if(user.getR1() != null && !user.getR1().equals("")) {
////            slots[slotCount] = new Slot(user.getR1());
////            slotCount++;
////        }
////        if(user.getR2() != null && !user.getR2().equals("")) {
////            slots[slotCount] = new Slot(user.getR2());
////            slotCount++;
////        }
////        if(user.getR3() != null && !user.getR3().equals("")) {
////            slots[slotCount] = new Slot(user.getR3());
////            slotCount++;
////        }
////        if(user.getR4() != null && !user.getR4().equals("")) {
////            slots[slotCount] = new Slot(user.getR4());
////            slotCount++;
////        }
////        if(user.getR5() != null && !user.getR5().equals("")) {
////            slots[slotCount] = new Slot(user.getR5());
////            slotCount++;
////        }
////        if(user.getR6() != null && !user.getR6().equals("")) {
////            slots[slotCount] = new Slot(user.getR6());
////            slotCount++;
////        }
////    }
////    private void initialiseLinks() {
////        log("li", "+" + hasLinksList);
////
////        int i = 0, len = linksList.length;
////        linkBoxes = new ConstraintLayout[len];
////        linkBoxMonIcos = new ImageView[len];
////        linkBoxBgs = new ConstraintLayout[len];
////        linkBoxOriginators = new TextView[len];
////        linkBoxRanks = new TextView[len];
////        linkBoxLevels = new TextView[len];
////        linkBoxPositions = new TextView[len];
////        linkBoxScores = new TextView[len];
////        linkBoxCommTypes = new TextView[len];
////
////
////        // Setup filter
////        filters = new boolean[3];
////        for(int j = 0; j<filters.length; j++) filters[j] = true;
////        final Runnable
////                emptyList = new Runnable() {
////            @Override
////            public void run() {
////                        // Filter out users according to filter bools
////                        for(int j = 0; j<linkBoxOriginators.length; j++) {
////                            boolean remove = true;
////                            //for(Link l : linksList) {
////                            // commtype filter
////                                                    /*if(u.getName().equals(linkBoxOriginators[j].getText().toString())) {
////                                                        int[] cts = {   new Slot(u.getSlots().getS1()).getCommType(),
////                                                                new Slot(u.getSlots().getS2()).getCommType(),
////                                                                new Slot(u.getSlots().getS3()).getCommType(),
////                                                                new Slot(u.getSlots().getS4()).getCommType(),
////                                                                new Slot(u.getSlots().getS5()).getCommType(),
////                                                                new Slot(u.getSlots().getS6()).getCommType() };
////
////                                                        if( (filters[0] && cts[0] == 1) ||
////                                                                (filters[1] && cts[0] == 3) ||
////                                                                (filters[2] && cts[0] == 7) )
////                                                            remove = false;
////                                                        if( (filters[0] && cts[1] == 1) ||
////                                                                (filters[1] && cts[1] == 3) ||
////                                                                (filters[2] && cts[1] == 7) )
////                                                            remove = false;
////                                                        if( (filters[0] && cts[2] == 1) ||
////                                                                (filters[1] && cts[2] == 3) ||
////                                                                (filters[2] && cts[2] == 7) )
////                                                            remove = false;
////                                                        if( (filters[0] && cts[3] == 1) ||
////                                                                (filters[1] && cts[3] == 3) ||
////                                                                (filters[2] && cts[3] == 7) )
////                                                            remove = false;
////                                                        if( (filters[0] && cts[4] == 1) ||
////                                                                (filters[1] && cts[4] == 3) ||
////                                                                (filters[2] && cts[4] == 7) )
////                                                            remove = false;
////                                                        if( (filters[0] && cts[5] == 1) ||
////                                                                (filters[1] && cts[5] == 3) ||
////                                                                (filters[2] && cts[5] == 7) )
////                                                            remove = false;
////                                                    }*/
////                            //}
////
////                            if(remove){
////                                final int indtx = j;
////                                runOnUiThread(new Runnable() {
////                                    @Override
////                                    public void run() {
////                                        linkBoxOriginators[indtx].setVisibility(View.GONE);
////                                    }
////                                });
////                            }
////                        }
////                    }
////                },
////                expandList = new Runnable() {
////                    @Override
////                    public void run() {
////                        String typed = ((TextView)findViewById(R.id.lilisrchtext)).getText().toString();
////                        for(int j = 0; j<linkBoxOriginators.length; j++) {
////                            if(linkBoxOriginators[j].getText().toString().toLowerCase().contains(typed.trim().toLowerCase())){
////                                final int indtx = j;
////                                runOnUiThread(new Runnable() {
////                                    @Override
////                                    public void run() {
////                                        String typed = ((EditText) findViewById(R.id.lilisrchtext)).getText().toString();
////                                        if(linkBoxOriginators[indtx].getText().toString().toLowerCase().contains(typed.toLowerCase().trim())){
////                                            linkBoxOriginators[indtx].setVisibility(View.VISIBLE);
////                                        }
////                                    }
////                                });
////                            }
////                        }
////                    }
////                };
////
////
////        // Links List
////        for (Link u : linksList) {
////            linkBoxes[i] = new ConstraintLayout(getApplicationContext());
////            linkBoxes[i].setLayoutParams(new LayoutParams(0, 0));
////            linkBoxes[i].getLayoutParams().width = dpToPx(305);
////            linkBoxes[i].setPadding(dpToPx(8), dpToPx(2), dpToPx(4), dpToPx(0));
////            linkBoxes[i].setAlpha(0f);
////            linkBoxes[i].setTag((""+i));
////
////            linkBoxBgs[i] = new ConstraintLayout(getApplicationContext());
////            linkBoxBgs[i].setId(View.generateViewId());
////            linkBoxMonIcos[i] = new ImageView(getApplicationContext());
////            linkBoxMonIcos[i].setId(View.generateViewId());
////            linkBoxCommTypes[i] = new TextView(getApplicationContext());
////            linkBoxCommTypes[i].setId(View.generateViewId());
////
////            linkBoxes[i].addView(linkBoxMonIcos[i]);
////            linkBoxes[i].addView(linkBoxBgs[i]);
////            linkBoxes[i].addView(linkBoxCommTypes[i]);
////
////            // Mon Ico
////            linkBoxMonIcos[i].setImageResource(R.drawable.sprite_agu);
////            linkBoxMonIcos[i].getLayoutParams().height = (dpToPx(40));
////            linkBoxMonIcos[i].getLayoutParams().width = (dpToPx(40));
////
////            // Box
////            linkBoxBgs[i].getLayoutParams().width = dpToPx(218);
////            linkBoxBgs[i].getLayoutParams().height = dpToPx(58);
////            linkBoxBgs[i].setBackgroundResource(R.drawable.shadow);
////
////            // Originator
////            linkBoxOriginators[i] = new TextView(getApplicationContext());
////            linkBoxOriginators[i].setId(View.generateViewId());
////            linkBoxOriginators[i].setText(u.getOriginator());
////            linkBoxOriginators[i].setTextSize(11);
////            linkBoxOriginators[i].setTextColor(0xE45E1800);
////            linkBoxBgs[i].addView(linkBoxOriginators[i]);
////
////            // Ranks
////            linkBoxRanks[i] = new TextView(getApplicationContext());
////            linkBoxRanks[i].setId(View.generateViewId());
////            linkBoxRanks[i].setText("B");
////            linkBoxRanks[i].setTextSize(40);
////            linkBoxRanks[i].setTextColor(0xE45E1800);
////            linkBoxBgs[i].addView(linkBoxRanks[i]);
////
////            // Levels
////            linkBoxLevels[i] = new TextView(getApplicationContext());
////            linkBoxLevels[i].setId(View.generateViewId());
////            linkBoxLevels[i].setText("Child");
////            linkBoxLevels[i].setTextSize(8);
////            linkBoxLevels[i].setTextColor(0x905E1800);
////            linkBoxBgs[i].addView(linkBoxLevels[i]);
////
////
////
////            // CommType
////            String text = "";
////            switch(u.getCommType()) {
////                case "1":
////                    text = "  PEN20  ";
////                    linkBoxCommTypes[i].setBackgroundResource(R.drawable.commtypebgpent);
////                    break;
////                case "3":
////                    text = "   DM20   ";
////                    linkBoxCommTypes[i].setBackgroundResource(R.drawable.commtypebgdmt);
////                    break;
////                case "7":
////                    text = "  DMOG  ";
////                    linkBoxCommTypes[i].setBackgroundResource(R.drawable.commtypebgdmog);
////                    break;
////            }
////            linkBoxCommTypes[i].setText(text);
////            linkBoxCommTypes[i].setRotation(90f);
////            linkBoxCommTypes[i].setTextColor(Color.WHITE);
////            linkBoxCommTypes[i].setTextSize(9);
////
////            //   Constraints
////
////            // LinkBox
////            ConstraintSet constraintSet = new ConstraintSet();
////            constraintSet.clone(linkBoxes[i]);
////            constraintSet.connect(linkBoxMonIcos[i].getId(), ConstraintSet.TOP, linkBoxBgs[i].getId(), ConstraintSet.TOP);
////            constraintSet.connect(linkBoxMonIcos[i].getId(), ConstraintSet.BOTTOM, linkBoxBgs[i].getId(), ConstraintSet.BOTTOM, dpToPx(10));
////            constraintSet.connect(linkBoxMonIcos[i].getId(), ConstraintSet.LEFT, linkBoxes[i].getId(), ConstraintSet.LEFT, dpToPx(4));
////
////            constraintSet.connect(linkBoxBgs[i].getId(), ConstraintSet.LEFT, linkBoxMonIcos[i].getId(), ConstraintSet.RIGHT, dpToPx(6));
////            constraintSet.connect(linkBoxBgs[i].getId(), ConstraintSet.TOP, linkBoxes[i].getId(), ConstraintSet.TOP);
////            constraintSet.connect(linkBoxBgs[i].getId(), ConstraintSet.BOTTOM, linkBoxes[i].getId(), ConstraintSet.BOTTOM);
////
////            constraintSet.connect(linkBoxCommTypes[i].getId(), ConstraintSet.TOP, linkBoxBgs[i].getId(), ConstraintSet.TOP);
////            constraintSet.connect(linkBoxCommTypes[i].getId(), ConstraintSet.BOTTOM, linkBoxBgs[i].getId(), ConstraintSet.BOTTOM, dpToPx(10));
////            constraintSet.connect(linkBoxCommTypes[i].getId(), ConstraintSet.LEFT, linkBoxBgs[i].getId(), ConstraintSet.LEFT, dpToPx(208));
////            constraintSet.applyTo(linkBoxes[i]);
////
////            // LinkBoxBG
////            ConstraintSet constraintSet2 = new ConstraintSet();
////            constraintSet2.clone(linkBoxBgs[i]);
////
////            //constraintSet.connect(linkBoxRanks[i].getId(), ConstraintSet.TOP, linkBoxBgs[i].getId(), ConstraintSet.BOTTOM);
////            constraintSet2.connect(linkBoxRanks[i].getId(), ConstraintSet.BOTTOM, linkBoxBgs[i].getId(), ConstraintSet.BOTTOM, dpToPx(7));
////            constraintSet2.connect(linkBoxRanks[i].getId(), ConstraintSet.LEFT, linkBoxBgs[i].getId(), ConstraintSet.LEFT, dpToPx(7));
////
////            constraintSet2.connect(linkBoxOriginators[i].getId(), ConstraintSet.TOP, linkBoxBgs[i].getId(), ConstraintSet.TOP, dpToPx(7));
////            constraintSet2.connect(linkBoxOriginators[i].getId(), ConstraintSet.LEFT, linkBoxRanks[i].getId(), ConstraintSet.RIGHT, dpToPx(9));
////
////            constraintSet2.connect(linkBoxLevels[i].getId(), ConstraintSet.TOP, linkBoxOriginators[i].getId(), ConstraintSet.BOTTOM);
////            constraintSet2.connect(linkBoxLevels[i].getId(), ConstraintSet.BOTTOM, linkBoxBgs[i].getId(), ConstraintSet.BOTTOM);
////            constraintSet2.connect(linkBoxLevels[i].getId(), ConstraintSet.LEFT, linkBoxOriginators[i].getId(), ConstraintSet.LEFT);
////
////            constraintSet2.applyTo(linkBoxBgs[i]);
////
////
////
////            ((LinearLayout) findViewById(R.id.lililinlayout)).addView(linkBoxes[i]);
////
////            linkBoxes[i].setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////                    anims("pulse", v);
////                    onClickLink(Integer.parseInt(v.getTag().toString()));
////                }
////            });
////
////            i++;
////        }
////
////        animateLinksLoad();
////    }
////    private void animateLinksLoad() {
////        for(int i = 0; i<linkBoxes.length; i++) {
//////            linkBoxes[i].setVisibility(View.VISIBLE);
////            linkBoxes[i].animate()
////                    .setStartDelay(450+(i*60))
////                    .setDuration(250)
////                    .alpha(1.0f)
////                    .start();
////        }
////    }
////    private void initialiseLinksSearch() {
////        genFakeLinksList();
////
////        lilisrchbar = ((EditText) findViewById(R.id.lilisrchtext));
////        lilisrchbar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
////            @Override
////            public void onFocusChange(View v, boolean hasFocus) {
////                if (hasFocus) {
////                    if(!hasLinksList) {
////                        hasLinksList = true;
////                        log("li", "+" + hasLinksList);
////
////                        int i = 0;
////                        usltxts = new TextView[linksList.length];
////                        uslspcrs = new TextView[linksList.length];
////
////
////                        // Setup filter
////                        filters = new boolean[3];
////                        for(int j = 0; j<filters.length; j++) filters[j] = true;
////                        final Runnable
////                            emptyList = new Runnable() {
////                                @Override
////                                public void run() {
////                                    // Filter out users according to filter bools
////                                    for(int j = 0; j<usltxts.length; j++) {
////                                        boolean remove = true;
////                                        //for(Link l : linksList) {
////                                            // commtype filter
////                                            /*if(u.getName().equals(usltxts[j].getText().toString())) {
////                                                int[] cts = {   new Slot(u.getSlots().getS1()).getCommType(),
////                                                        new Slot(u.getSlots().getS2()).getCommType(),
////                                                        new Slot(u.getSlots().getS3()).getCommType(),
////                                                        new Slot(u.getSlots().getS4()).getCommType(),
////                                                        new Slot(u.getSlots().getS5()).getCommType(),
////                                                        new Slot(u.getSlots().getS6()).getCommType() };
////
////                                                if( (filters[0] && cts[0] == 1) ||
////                                                        (filters[1] && cts[0] == 3) ||
////                                                        (filters[2] && cts[0] == 7) )
////                                                    remove = false;
////                                                if( (filters[0] && cts[1] == 1) ||
////                                                        (filters[1] && cts[1] == 3) ||
////                                                        (filters[2] && cts[1] == 7) )
////                                                    remove = false;
////                                                if( (filters[0] && cts[2] == 1) ||
////                                                        (filters[1] && cts[2] == 3) ||
////                                                        (filters[2] && cts[2] == 7) )
////                                                    remove = false;
////                                                if( (filters[0] && cts[3] == 1) ||
////                                                        (filters[1] && cts[3] == 3) ||
////                                                        (filters[2] && cts[3] == 7) )
////                                                    remove = false;
////                                                if( (filters[0] && cts[4] == 1) ||
////                                                        (filters[1] && cts[4] == 3) ||
////                                                        (filters[2] && cts[4] == 7) )
////                                                    remove = false;
////                                                if( (filters[0] && cts[5] == 1) ||
////                                                        (filters[1] && cts[5] == 3) ||
////                                                        (filters[2] && cts[5] == 7) )
////                                                    remove = false;
////                                            }*/
////                                        //}
////
////                                        if(remove){
////                                            final int indtx = j;
////                                            runOnUiThread(new Runnable() {
////                                                @Override
////                                                public void run() {
////                                                    usltxts[indtx].setVisibility(View.GONE);
////                                                    uslspcrs[indtx].setVisibility(View.GONE);
////                                                }
////                                            });
////                                        }
////                                    }
////                                }
////                            },
////                            expandList = new Runnable() {
////                                @Override
////                                public void run() {
////                                    String typed = ((TextView)findViewById(R.id.lilisrchtext)).getText().toString();
////                                    for(int j = 0; j<usltxts.length; j++) {
////                                        if(usltxts[j].getText().toString().toLowerCase().contains(typed.trim().toLowerCase())){
////                                            final int indtx = j;
////                                            runOnUiThread(new Runnable() {
////                                                @Override
////                                                public void run() {
////                                                    String typed = ((EditText) findViewById(R.id.lilisrchtext)).getText().toString();
////                                                    if(usltxts[indtx].getText().toString().toLowerCase().contains(typed.toLowerCase().trim())){
////                                                        usltxts[indtx].setVisibility(View.VISIBLE);
////                                                        uslspcrs[indtx].setVisibility(View.VISIBLE);
////                                                    }
////                                                }
////                                            });
////                                        }
////                                    }
////                                }
////                              };
////
////
////                        // Links List
////                        for (Link u : linksList) {
////                            ConstraintLayout cl = new ConstraintLayout(getApplicationContext());
////
////                            TextView tv = new TextView(getApplicationContext());
////                            tv.setText(u.getOriginator());
////                            tv.setTextSize(15);
////                            int bor = (int) dpToPx(16);
////                            tv.setPadding(10, bor, 0, bor);
////                            tv.setTextColor(0xE45E1800);
////                            usltxts[i] = tv;
////
////                            ((LinearLayout) findViewById(R.id.lilisrchscrllinlayout)).addView(tv);
////
////
////                            TextView sp = new TextView(getApplicationContext());
////
////                            LayoutParams spparams = new LayoutParams();
////                            spparams.width = 1000;
////                            spparams.height = 1;
////                            sp.setLayoutParams(spparams);
////                            sp.setBackgroundColor(0xB0521800);
////
////                            ((LinearLayout) findViewById(R.id.lilisrchscrllinlayout)).addView(sp);
////                            uslspcrs[i] = sp;
////
////                            i++;
////
////                            tv.setOnClickListener(new View.OnClickListener() {
////                                @Override
////                                public void onClick(View v) {
////                                    anims("pulse", v);
////                                }
////                            });
////                        }
////
////                        ((EditText)findViewById(R.id.lilisrchtext)).addTextChangedListener(new TextWatcher() {
////                            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
////                            @Override public void afterTextChanged(Editable s) {}
////
////                            @Override
////                            public void onTextChanged(CharSequence s, int start, int before, int count) {
////                                if(before<count) {
////                                    if(s.length() != 0) {
////                                        String ss = s.toString();
////                                        for(int j = 0; j<usltxts.length; j++) {
////                                            //log("TXT", ""+j+" : "+usltxts[j].getText());
////                                            if(!usltxts[j].getText().toString().toLowerCase().contains(ss.trim().toLowerCase())){
////                                                final int indtx = j;
////                                                runOnUiThread(new Runnable() {
////                                                    @Override
////                                                    public void run() {
////                                                        usltxts[indtx].setVisibility(View.GONE);
////                                                        uslspcrs[indtx].setVisibility(View.GONE);
////
////
////                                                    }
////                                                });
////                                            }
////                                        }
////                                    }
////                                }
////                                else {
////                                    emptyList.run();
////                                    expandList.run();
////                                }
////                            }
////                        });
////                    }
////
////                    log("FOC", "USED");
////                    anims("dropdownopen", findViewById(R.id.lilisrchscrl));
////                }
////            }
////        });
////    }
////
////
////    private void onClickLink(int index) {
////        UserInNode op = getUserFromList(linksList[index].getOriginator());
////        if(op == null) return;
////
////        anims("fadein", findViewById(R.id.lilibg));
////        anims("expand", findViewById(R.id.lililinkwin));
////
////        String r = op.getStats().getRank();
////        ((ImageView) findViewById(R.id.lilinkwinrank)).setImageResource(getImageId("card_tamercard_rank_"+r.toLowerCase()));
////
////        ((TextView)findViewById(R.id.lilinkwintitle)).setText(op.getName());
////        ((TextView)findViewById(R.id.lilinkwinlevel)).setText("Child");
////        ((TextView)findViewById(R.id.lilinkwinposition)).setText("#24");
////        ((TextView)findViewById(R.id.lilinkwinpoints)).setText("214");
////
////        switch(linksList[index].getCommType()){
////            case "1":
////                ((TextView)findViewById(R.id.lilinkwincommtype)).setText("  PEN20  ");
////                ((TextView)findViewById(R.id.lilinkwincommtype)).setBackgroundResource(R.drawable.commtypebgpent);
////                break;
////            case "3":
////                ((TextView)findViewById(R.id.lilinkwincommtype)).setText("  DM20  ");
////                ((TextView)findViewById(R.id.lilinkwincommtype)).setBackgroundResource(R.drawable.commtypebgdmt);
////
////                break;
////            case "7":
////                ((TextView)findViewById(R.id.lilinkwincommtype)).setText("  DMOG  ");
////                ((TextView)findViewById(R.id.lilinkwincommtype)).setBackgroundResource(R.drawable.commtypebgdmog);
////                break;
////        }
////    }
////
////    private void updateUser() {
////        FirebaseFirestore db = FirebaseFirestore.getInstance();
////        DocumentReference userRef = db.collection("Users").document(user.gUnm());
////
////        user.setMsg("!!!!");
////
////        userRef.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
////            @Override
////            public void onSuccess(Void aVoid) {
////                log("USER", "UPDATED");
////            }
////        }).addOnFailureListener(new OnFailureListener() {
////            @Override
////            public void onFailure(@NonNull Exception e) {
////                log("USER", "FAILED TO UPDATE", e);
////            }
////        });
////    }
////    private void userListener() {
////        FirebaseFirestore db = FirebaseFirestore.getInstance();
////        DocumentReference userRef = db.collection("Users").document(user.gUnm());
////        log("unm", user.gUnm());
////
////        userRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
////            @Override
////            public void onEvent(@Nullable DocumentSnapshot snapshot,
////                                @Nullable FirebaseFirestoreException e) {
////                if (e != null) {
////                    //fail
////                    return;
////                }
////                if (snapshot != null && snapshot.exists()) {
////                    User tuser = snapshot.toObject(User.class);   // Load user from db
////                    tuser.sUnm(snapshot.getId());
////                    if(!user.getMsg().equals(tuser.getMsg()) && !user.getMsg().equals("!!!!")) {
////                        log("UPDATE", "MSG");
////                        log("==========", "===================");
////                        //getMessages();
////                    }
////                } else {
////                    // data null
////                    log("NullUser", "NullUser");
////                }
////            }
////        });
////    }
////    private void notifyOpponent(final String name) {
////        String msgchk = ChallengeManager.messageStamp(new Date());
////
////        FirebaseFirestore db = FirebaseFirestore.getInstance();
////        DocumentReference userRef = db.collection("Users").document(name);
////        userRef.update("msg", msgchk).addOnCompleteListener(new OnCompleteListener<Void>() {
////            @Override
////            public void onComplete(@NonNull Task<Void> task) {
////                if(task.isSuccessful()) {
////                    log("msg", "chk");
////                } else {
////                    log("msg", "Failure");
////                }
////            }
////        });
////    }
////
////    private void getUsersList(final Runnable r) {
////        String URL = "http://104.248.224.184:1027/api/getUsers";
////
////        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
////        JsonObjectRequest obRe = new JsonObjectRequest(
////                Request.Method.GET,
////                URL,
////                null,
////                new Response.Listener<JSONObject>() {
////                    @Override
////                    public void onResponse(JSONObject response) {
////                        try {
////                            String jsonString = response.getString("users");
////                            Gson gson = new Gson();
////                            usersList = gson.fromJson(jsonString, UserInNode[].class);
////                        } catch (JSONException e) {
////                            e.printStackTrace();
////                        }
////                    }
////                },
////                new Response.ErrorListener() {
////                    @Override
////                    public void onErrorResponse(VolleyError error) {
////                        log("eRESP", error.toString());
////                    }
////                }
////        );
////
////        rQ.add(obRe);
////        runOnUiThread(r);
////    }
////    private void getUsersListFromString(String usersListString) {
////        Gson gson = new Gson();
////        usersList = gson.fromJson(usersListString, UserInNode[].class);
////    }
////    private UserInNode getUserFromList(String name) {
////        for(UserInNode u : usersList) {
////            if(u.getName().equals(name)) {
////                return u;
////            }
////        }
////        return null;
////    }
////
////    private void anims(String cmd, final View v) {
////        Animation
////                focus = AnimationUtils.loadAnimation(this, R.anim.clickfocus),
////                loseFocus = AnimationUtils.loadAnimation(this, R.anim.clicklosefocus),
////                pulse = AnimationUtils.loadAnimation(this, R.anim.pulse),
////                expand = AnimationUtils.loadAnimation(this, R.anim.expandone),
////                close = AnimationUtils.loadAnimation(this, R.anim.closeone),
////                expandslow = AnimationUtils.loadAnimation(this, R.anim.expandoneslow),
////                closefast = AnimationUtils.loadAnimation(this, R.anim.closefast),
////                closedelayed = AnimationUtils.loadAnimation(this, R.anim.closedelayed),
////                fade = AnimationUtils.loadAnimation(this, R.anim.fade),
////                fadein = AnimationUtils.loadAnimation(this, R.anim.fadein),
////                rcw = AnimationUtils.loadAnimation(this, R.anim.rotatecw),
////                rccw = AnimationUtils.loadAnimation(this, R.anim.rotateccw),
////                dropdownopen = AnimationUtils.loadAnimation(this, R.anim.dropdownopen),
////                dropdownclose = AnimationUtils.loadAnimation(this, R.anim.dropdownclose),
////                pulsering = AnimationUtils.loadAnimation(this, R.anim.pulsering);
////
////        if(v != null) v.setVisibility(View.VISIBLE);
////
////        switch(cmd) {
////            case "focus":
////                v.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200);
////                break;
////            case "loseFocus":
////                v.animate().scaleX(1.0f).scaleY(1.0f).setDuration(200);
////                break;
////            case "negFocus":
////                v.animate().scaleX(-1.2f).scaleY(1.2f).setDuration(200);
////                break;
////            case "negLoseFocus":
////                v.animate().scaleX(-1.0f).scaleY(1.0f).setDuration(200);
////                break;
////            case "pulse":
////                v.startAnimation(pulse);
////                break;
////            case "expand":
////                v.startAnimation(expand);
////                break;
////            case "close":
////                close.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        v.setVisibility(View.GONE);}
////                });
////                v.startAnimation(close);
////                break;
////            case "expandslow":
////                v.startAnimation(expandslow);
////                break;
////            case "closefast":
////                v.startAnimation(closefast);
////                v.setVisibility(View.GONE);
////                break;
////            case "closeDelayed":
////                anims("fade", findViewById(R.id.inbbg));
////                closedelayed.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        v.setVisibility(View.GONE);}
////
////                });
////                v.startAnimation(closedelayed);
////                break;
////            case "fade":
////                fade.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        v.setVisibility(View.GONE);}
////                });
////                v.startAnimation(fade);
////                break;
////            case "fadein":
////                v.startAnimation(fadein);
////                break;
////            case "rings":
////                ringsSpin = true;
////                findViewById(R.id.inbscanvpet).setVisibility(View.VISIBLE);
////                findViewById(R.id.inbinformation).setVisibility(View.VISIBLE);
////                findViewById(R.id.inbimghldr).setVisibility(View.VISIBLE);
////                ((ImageView) findViewById(R.id.inbcsml)).setVisibility(View.VISIBLE);
////                ((ImageView) findViewById(R.id.inbcbig)).setVisibility(View.VISIBLE);
////                ((ImageView) findViewById(R.id.inbcsml)).startAnimation(rcw);
////                ((ImageView) findViewById(R.id.inbcbig)).startAnimation(rccw);
////                break;
////            case "closeRings":
////                ringsSpin = false;
////                close.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        findViewById(R.id.inbcsml).setVisibility(View.GONE);
////                        findViewById(R.id.inbcbig).setVisibility(View.GONE);}
////                });
////                ((ImageView) findViewById(R.id.inbcsml)).startAnimation(close);
////                ((ImageView) findViewById(R.id.inbcbig)).startAnimation(close);
////                break;
////            case "ringsTwo":
////                ringsSpinTwo = true;
////                ((ImageView) findViewById(R.id.inbnccsml)).setVisibility(View.VISIBLE);
////                ((ImageView) findViewById(R.id.inbnccbig)).setVisibility(View.VISIBLE);
////                ((ImageView) findViewById(R.id.inbnccsml)).startAnimation(rcw);
////                ((ImageView) findViewById(R.id.inbnccbig)).startAnimation(rccw);
////                break;
////            case "closeRingsTwo":
////                ringsSpinTwo = false;
////                close.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        findViewById(R.id.sndcsml).setVisibility(View.GONE);
////                        findViewById(R.id.sndcbig).setVisibility(View.GONE);}
////                });
////                ((ImageView) findViewById(R.id.inbnccsml)).startAnimation(close);
////                ((ImageView) findViewById(R.id.inbnccbig)).startAnimation(close);
////                break;
////            case "fail":
////                close.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        findViewById(R.id.inbcsml).setVisibility(View.GONE);
////                        findViewById(R.id.inbcbig).setVisibility(View.GONE);
////                        findViewById(R.id.inbinfocenterico).setVisibility(View.GONE);}
////                });
////                ((ImageView) findViewById(R.id.inbcsml)).startAnimation(close);
////                ((ImageView) findViewById(R.id.inbcbig)).startAnimation(close);
////                break;
////            case "scs":
////                close.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        findViewById(R.id.inbcsml).setVisibility(View.GONE);
////                        findViewById(R.id.inbcbig).setVisibility(View.GONE);}
////                });
////                ((ImageView) findViewById(R.id.inbcsml)).startAnimation(close);
////                ((ImageView) findViewById(R.id.inbcbig)).startAnimation(close);
////                ((ImageView) findViewById(R.id.inbinfocenterico)).setVisibility(View.VISIBLE);
////                ((ImageView) findViewById(R.id.inbinfocenterico)).startAnimation(expand);
////                break;
////
////            case "dropdownopen":
////                ConstraintLayout constraintLayout = findViewById(R.id.livelinkbase);
////                ConstraintSet constraintSet = new ConstraintSet();
////                constraintSet.clone(constraintLayout);
////                constraintSet.connect(R.id.lilisearchbar,ConstraintSet.TOP,R.id.livelinkbase,ConstraintSet.TOP,0);
////                constraintSet.applyTo(constraintLayout);
////
////                v.startAnimation(dropdownopen);
////                dropDown = true;
////                findViewById(R.id.lilititle).setVisibility(View.GONE);
////                break;
////            case "dropdownclose":
////                findViewById(R.id.lilititle).setVisibility(View.VISIBLE);
////                dropdownclose.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        v.setVisibility(View.GONE);
////                    }
////                });
////                v.startAnimation(dropdownclose);
////                dropDown = false;
////
////                ConstraintLayout constraintLayouto = findViewById(R.id.livelinkbase);
////                ConstraintSet constraintSeto = new ConstraintSet();
////                constraintSeto.clone(constraintLayouto);
////                constraintSeto.connect(R.id.lilisearchbar,ConstraintSet.TOP,R.id.livelinkbase,ConstraintSet.TOP,(int) dpToPx(82));
////                constraintSeto.applyTo(constraintLayouto);
////                break;
////            case "pulsering":
////                findViewById(R.id.inbpresspulsering).setVisibility(View.VISIBLE);
////
////                pulsering.setAnimationListener(new Animation.AnimationListener() {
////                    @Override public void onAnimationStart(Animation animation) {}
////                    @Override public void onAnimationRepeat(Animation animation) {}
////                    @Override public void onAnimationEnd(Animation animation) {
////                        findViewById(R.id.inbpresspulsering).setVisibility(View.GONE);}
////                });
////
////                findViewById(R.id.inbpresspulsering).startAnimation(pulsering);
////                break;
////            case "openMessages":
////                ConstraintLayout mwo = findViewById(R.id.messageswin);
////
////                mwo.animate()
////                        .setStartDelay(220)
////                        .translationX(0)
////                        .setDuration(200)
////                        .start();
////
////                break;
////            case "closeMessages":
////                ConstraintLayout mwc = findViewById(R.id.messageswin);
////
////                mwc.animate()
////                        .setStartDelay(140)
////                        .translationX(mwc.getWidth())
////                        .setDuration(300);
////                break;
////            case "expandChallengeStats":
////                findViewById(R.id.msgchallengetopbg).setPivotY(0);
////                findViewById(R.id.msgchallengetopbg).animate()
////                        .scaleY(1.0f)
////                        .setDuration(200);
////
////                findViewById(R.id.messageschallengescorestats).animate()
////                        .alpha(1)
////                        .setDuration(150)
////                        .setStartDelay(240)
////                        .setListener(new AnimatorListenerAdapter() {
////                            @Override
////                            public void onAnimationStart(Animator animation) {
////                                findViewById(R.id.messageschallengescorestats).setVisibility(View.VISIBLE);
////                        }});
////
////                break;
////            case "closeChallengeStats":
////                findViewById(R.id.messageschallengescorestats).setVisibility(View.GONE);
////
////                findViewById(R.id.msgchallengetopbg).setPivotY(0);
////                findViewById(R.id.msgchallengetopbg).animate()
////                        .setStartDelay(150)
////                        .scaleY(0f)
////                        .setDuration(200);
////                break;
////            default:
////                break;
////        }
////    }
////    private void animateCountDown() {
////        ((ImageView) findViewById(R.id.inbinfocenterico))
////                .setImageResource(R.drawable.ic_cntdwn_three);
////
////        findViewById(R.id.inbcsml).clearAnimation();
////        findViewById(R.id.inbcbig).clearAnimation();
////
////        expOne = AnimationUtils.loadAnimation(this, R.anim.expandtwo);
////        expTwo = AnimationUtils.loadAnimation(this, R.anim.expandtwo);
////        expThree = AnimationUtils.loadAnimation(this, R.anim.expandtwo);
////
////        ((ImageView) findViewById(R.id.inbinfocenterico))
////                .setImageResource(R.drawable.ic_cntdwn_three);
////        ((ImageView) findViewById(R.id.inbinfocenterico))
////                .setVisibility(View.VISIBLE);
////
////        expOne.setAnimationListener(new Animation.AnimationListener() {
////            @Override public void onAnimationStart(Animation animation) {}
////            @Override public void onAnimationRepeat(Animation animation) {}
////            @Override public void onAnimationEnd(Animation animation) {
////                ((ImageView) findViewById(R.id.inbinfocenterico))
////                        .setImageResource(R.drawable.ic_cntdwn_two);
////                ((ImageView) findViewById(R.id.inbinfocenterico))
////                        .startAnimation(expTwo);
////            }
////        });
////
////        expTwo.setAnimationListener(new Animation.AnimationListener() {
////            @Override public void onAnimationStart(Animation animation) {}
////            @Override public void onAnimationRepeat(Animation animation) {}
////            @Override public void onAnimationEnd(Animation animation) {
////                ((ImageView) findViewById(R.id.inbinfocenterico))
////                        .setImageResource(R.drawable.ic_cntdwn_one);
////
////                ((ImageView) findViewById(R.id.inbinfocenterico))
////                        .startAnimation(expThree);
////            }
////        });
////
////        expThree.setAnimationListener(new Animation.AnimationListener() {
////            @Override public void onAnimationStart(Animation animation) {}
////            @Override public void onAnimationRepeat(Animation animation) {}
////            @Override public void onAnimationEnd(Animation animation) {
////                anims("close", findViewById(R.id.inbinfocenterico));
////                anims("expandslow", findViewById(R.id.inbimghldr));
////                anims("rings", null);
////            }
////        });
////
////
////        ((ImageView) findViewById(R.id.inbinfocenterico))
////                .startAnimation(expOne);
////
////    }
////
////
////    private int getImageId(String imageName) {
////        return this.getResources().getIdentifier("drawable/" + imageName, null, this.getPackageName());
////    }
////    private String getIMGFileName(String index, int commType) {
////        String name = "";
////        switch(commType) {
////            case 0: break;
////            case 1:
////                log("Pen20MonFromIndex", ""+index);
////                name = RomManager.getPen20MonFromIndex(index);
////                break;
////            case 2: break;
////            case 3:
////                name = RomManager.getDm20MonFromIndex(index);
////                break;
////            case 4: break;
////            case 5: break;
////            case 6: break;
////            case 7:
////                name = RomManager.getDmogMonFromIndex(index);
////                break;
////            case 8: break;
////            case 9: break;
////            case 10: break;
////            default: break;
////        }
////
////        return "sprite_" + name.trim();
////    }
////    private String getIMGFileNameFromRom(String rom, int commType) {
////        String name = "";
////        switch(commType) {
////            case 0: break;
////            case 1:
////                name = RomManager.getPen20MonFromRom(rom)[0];
////                break;
////            case 2: break;
////            case 3:
////                name = RomManager.getDm20MonFromRom(rom);
////                break;
////            case 4: break;
////            case 5: break;
////            case 6: break;
////            case 7:
////                name = RomManager.getDmogMonFromRom(rom);
////                break;
////            case 8: break;
////            case 9: break;
////            case 10: break;
////            default: break;
////        }
////
////        return "sprite_" + name.trim();
////    }
////    private String getIndex(String rom, int commType) {
////        switch(commType) {
////            case 1:
////                return ""+RomManager.getPen20Index(rom);
////            case 3:
////                return ""+RomManager.getDM20Index(rom);
////            case 7:
////                return RomManager.getDmogIndex(rom);
////        }
////        return "";
////    }
////    private String getName() {
////        String name = "";
////        switch(commType) {
////            case 0: break;
////            case 1:
////                name = RomManager.getPen20MonFromRom(rom)[0];
////                break;
////            case 2: break;
////            case 3:
////                name = RomManager.getDm20MonFromRom(rom);
////                break;
////            case 4: break;
////            case 5: break;
////            case 6: break;
////            case 7:
////                name = RomManager.getDmogMonFromRom(rom);
////                break;
////            case 8: break;
////            case 9: break;
////            case 10: break;
////            default: break;
////        }
////
////        if(name.contains("_")) {
////            String s = name.substring(1, 4);
////
////            if(s.equals("blu")) name = name+"";
////            else if(s.equals("gre")) name = name+"";
////            else if(s.equals("tai")) name = name.substring(5)+"mon";
////            else if(s.equals("yam")) name = name.substring(5)+"mon";
////            else if(s.equals("als")) name = "Alter-S "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
////            else if(s.equals("fdm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FDM";
////            else if(s.equals("plm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon PLM";
////            else if(s.equals("blm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon BLM";
////            else if(s.equals("ftm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FTM";
////            else if(s.equals("rgm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon RGM";
////            else if(s.equals("vcm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon VCM";
////            else if(s.equals("ory")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon ORY";
////            else if(s.equals("bla")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Blanc";
////            else if(s.equals("cie")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Ciel";
////            else if(s.equals("noi")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Noir";
////            else if(s.equals("bsh")) name = "Bushi "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
////            else if(s.equals("hak")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Hakase";
////
////
////
////            else return "err";
////
////            return name;
////        }
////        else return name.substring(0,1).toUpperCase() + name.substring(1) + "mon";
////    }
////    private String getMonName(String rom, int commType) {
////        String name = "";
////        switch(commType) {
////            case 0: break;
////            case 1:
////                name = RomManager.getPen20MonFromRom(rom)[0];
////                break;
////            case 2: break;
////            case 3:
////                name = RomManager.getDm20MonFromRom(rom);
////                break;
////            case 4: break;
////            case 5: break;
////            case 6: break;
////            case 7:
////                name = RomManager.getDmogMonFromRom(rom);
////                break;
////            case 8: break;
////            case 9: break;
////            case 10: break;
////            default: break;
////        }
////
////        if(name.contains("_")) {
////            String s = name.substring(1, 4);
////
////            if(s.equals("blu")) name = name+"";
////            else if(s.equals("gre")) name = name+"";
////            else if(s.equals("tai")) name = name.substring(5)+"mon";
////            else if(s.equals("yam")) name = name.substring(5)+"mon";
////            else if(s.equals("als")) name = "Alter-S "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
////            else if(s.equals("fdm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FDM";
////            else if(s.equals("plm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon PLM";
////            else if(s.equals("blm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon BLM";
////            else if(s.equals("ftm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FTM";
////            else if(s.equals("rgm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon RGM";
////            else if(s.equals("vcm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon VCM";
////            else if(s.equals("ory")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon ORY";
////            else if(s.equals("bla")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Blanc";
////            else if(s.equals("cie")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Ciel";
////            else if(s.equals("noi")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Noir";
////            else if(s.equals("bsh")) name = "Bushi "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
////            else if(s.equals("hak")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Hakase";
////
////
////
////            else return "err";
////
////            return name;
////        }
////        else return name.substring(0,1).toUpperCase() + name.substring(1) + "mon";
////    }
////    private String getMonNameFromIndex(String index, int commType) {
////        String name = "";
////        switch(commType) {
////            case 0: break;
////            case 1:
////                name = RomManager.getPen20MonFromIndex(index);
////                break;
////            case 2: break;
////            case 3:
////                name = RomManager.getDm20MonFromIndex(index);
////                break;
////            case 4: break;
////            case 5: break;
////            case 6: break;
////            case 7:
////                name = RomManager.getDmogMonFromIndex(index);
////                break;
////            case 8: break;
////            case 9: break;
////            case 10: break;
////            default: break;
////        }
////
////        if(name.contains("_")) {
////            String s = name.substring(1, 4);
////
////            if(s.equals("blu")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Blue";
////            else if(s.equals("gre")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Green";
////            else if(s.equals("tai")) name = name.substring(5)+"mon";
////            else if(s.equals("yam")) name = name.substring(5)+"mon";
////            else if(s.equals("als")) name = "Alter-S "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
////            else if(s.equals("fdm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FDM";
////            else if(s.equals("plm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon PLM";
////            else if(s.equals("blm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon BLM";
////            else if(s.equals("ftm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon FTM";
////            else if(s.equals("rgm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon RGM";
////            else if(s.equals("vcm")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon VCM";
////            else if(s.equals("ory")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon ORY";
////            else if(s.equals("bla")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Blanc";
////            else if(s.equals("cie")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Ciel";
////            else if(s.equals("noi")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Noir";
////            else if(s.equals("bsh")) name = "Bushi "+name.substring(5,6).toUpperCase()+name.substring(6)+"mon";
////            else if(s.equals("hak")) name = name.substring(5,6).toUpperCase()+name.substring(6)+"mon Hakase";
////
////
////
////            else return "err";
////
////            return name;
////        }
////        else return name.substring(0,1).toUpperCase() + name.substring(1) + "mon";
////    }
//
////    private boolean isA() {
////        if(msgs[msgSel].getA().equals(user.gUnm()) &&
////                !msgs[msgSel].getB().equals(user.gUnm())) return true;
////        else return false;
////    }
////    private boolean isA(Challenge ch) {
////        if(ch.getA().equals(user.gUnm()) &&
////                !ch.getB().equals(user.gUnm())) return true;
////        else return false;
////    }
////    private boolean isAway(int slot) {
////        for (Challenge ch : msgs) {
////            if ((isA(ch) && ch.gASlot() == slot && (ch.gState() == 1 || ch.gState() == 2 || ch.gState() == 5)) ||
////                    (!isA(ch) && ch.gBSlot() == slot && (ch.gState() == 4 || ch.gState() == 5 || ch.gState() == 2))) {
////
////                Log.e("AWAY", ""+slot+" "+ch.docId());
////                return true;
////            }
////        }
////        return false;
////    }
////    private boolean isActive(Challenge ch) {
////        if (ch.gState() == 1 || ch.gState() == 4 ||
////                ch.gState() == 2 || ch.gState() == 5 ){
////            Log.e("Active", ch.docId());
////            return true;
////        }
////        else return false;
////    }
////    private boolean isActiveRequiresAttention(Challenge ch) {
//////        if ((isA(ch) && (ch.gState() == 1 || ch.gState() == 5)) ||
//////           (!isA(ch) && (ch.gState() == 4 || ch.gState() == 2))) {
////        if (ch.gState() == 1 || (!isA(ch) && ch.gState() == 2) ||
////                ch.gState() == 4 || (isA(ch) && ch.gState() == 5)) {
////
////            Log.e("Active, requires attention", ch.docId());
////            return true;
////        }
////        else return false;
////    }
////
////
////    private void closeDropDown() {
////        runOnUiThread(new Runnable() {
////            @Override
////            public void run() {
////                hideKeyboard();
////                anims("dropdownclose", findViewById(R.id.lilisrchscrl));
////                backCnt = 0;
////                getCurrentFocus().clearFocus();
////            }
////        });
////    }
////    private void closeWindows() {
////        if(findViewById(R.id.lilibg).getVisibility() == View.VISIBLE && !del) {
//////            if(slotSel <= 6) {
//////                switch(slotSel){
//////                    case 0:
//////                        anims("loseFocus", findViewById(R.id.inbmlbone));
//////                        anims("loseFocus", findViewById(R.id.inbmlboneshadow));
//////                        anims("negLoseFocus", findViewById(R.id.inbmloneico));
//////                        break;
//////                    case 1:
//////                        anims("loseFocus", findViewById(R.id.inbmlbtwo));
//////                        anims("loseFocus", findViewById(R.id.inbmlbtwoshadow));
//////                        anims("negLoseFocus", findViewById(R.id.inbmltwoico));
//////                        break;
//////                    case 2:
//////                        anims("loseFocus", findViewById(R.id.inbmlbthree));
//////                        anims("loseFocus", findViewById(R.id.inbmlbthreeshadow));
//////                        anims("negLoseFocus", findViewById(R.id.inbmlthreeico));
//////                        break;
//////                    case 3:
//////                        anims("loseFocus", findViewById(R.id.inbmlbfour));
//////                        anims("loseFocus", findViewById(R.id.inbmlbfourshadow));
//////                        anims("negLoseFocus", findViewById(R.id.inbmlfourico));
//////                        break;
//////                    case 4:
//////                        anims("loseFocus", findViewById(R.id.inbmlbfive));
//////                        anims("loseFocus", findViewById(R.id.inbmlbfiveshadow));
//////                        anims("negLoseFocus", findViewById(R.id.inbmlfiveico));
//////                        break;
//////                    case 5:
//////                        anims("loseFocus", findViewById(R.id.inbmlbsix));
//////                        anims("loseFocus", findViewById(R.id.inbmlbsixshadow));
//////                        anims("negLoseFocus", findViewById(R.id.inbmlsixico));
//////                        break;
//////
//////                }
//////                slotSel = 12;
//////            }
////            anims("fade", findViewById(R.id.lilibg));
////
////            if(findViewById(R.id.lililinkwin).getVisibility() == View.VISIBLE)
////                anims("close", findViewById(R.id.lililinkwin));
////        }
////
////
//////        if(ringsSpin) {
//////            findViewById(R.id.inbcsml).clearAnimation();
//////            findViewById(R.id.inbcbig).clearAnimation();
//////
//////            findViewById(R.id.inbcsml).setVisibility(View.GONE);
//////            findViewById(R.id.inbcbig).setVisibility(View.GONE);
//////        }
//////        if(ringsSpinTwo) {
//////            findViewById(R.id.inbnccsml).clearAnimation();
//////            findViewById(R.id.inbnccbig).clearAnimation();
//////
//////            findViewById(R.id.inbnccsml).setVisibility(View.GONE);
//////            findViewById(R.id.inbnccbig).setVisibility(View.GONE);
//////        }
////        if(alpha != null) alpha.setkl();
////        srch = false;
////        if(ardu != null) {
////            if(ardu.isOpened()) ardu.close();
////        }
////        if (findThread != null) findThread.interrupt();
////        if(scanThread != null) {
////            scanThread.interrupt();
////        }
////        findThread = null;
////        scanThread = null;
////    }
////    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
////        closeWindows();
////        if (keyCode == KeyEvent.KEYCODE_BACK) {
////            if(dropDown) {
////                if(backCnt++ < 1) {
////                    closeDropDown();
//////                    anims("dropdownclose", findViewById(R.id.userslistscrl));
//////                    backCnt = 0;
//////                    getCurrentFocus().clearFocus();
////                }
////            }
////            else backCnt = 0;
////
////            return true;
////        }
////        return super.onKeyDown(keyCode, event);
////    }
////    @Override public boolean dispatchTouchEvent(MotionEvent event) {
////        if (event.getAction() == MotionEvent.ACTION_DOWN) {
////            View v = getCurrentFocus();
////            if ( v instanceof EditText) {
////                Rect outRect = new Rect(), listRect = new Rect(), srchicRect = new Rect();
////                //v.getGlobalVisibleRect(outRect);
////                findViewById(R.id.lilisearchbar).getGlobalVisibleRect(outRect);
////                findViewById(R.id.lilisrchscrl).getGlobalVisibleRect(listRect);
////                findViewById(R.id.lilisrchico).getGlobalVisibleRect(srchicRect);
////                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY()) &&
////                        !listRect.contains((int)event.getRawX(), (int)event.getRawY()) &&
////                        !srchicRect.contains((int)event.getRawX(), (int)event.getRawY()) ) {
////                    //log("focus", "touchevent");
////                    v.clearFocus();
////                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
////                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
////                    if(dropDown) {
////                        findViewById(R.id.lilisrchtext).clearFocus();
////                        anims("dropdownclose", findViewById(R.id.lilisrchscrl));
////                        backCnt = 0;
////                    }
////
////                }
////            }
////        }
////        return super.dispatchTouchEvent(event);
////    }
//    public void hideKeyboard() {
//        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//        //Find the currently focused view, so we can grab the correct window token from it.
//        View view = getCurrentFocus();
//        //If no view currently has focus, create a new one, just so we can grab a window token from it
//        if (view == null) {
//            view = new View(this);
//        }
//        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }
//    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        String item = parent.getItemAtPosition(position).toString();
//        switch(item) {
//            case "<Digimon Original VPet>":
//                commType = 7;
//                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
//                break;
//            case "<Digimon 20th VPet>":
//                commType = 3;
//                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
//                break;
//            case "<Pendulum 20th VPet>":
//                commType = 1;
//                ((TextView) findViewById(R.id.inbcommtext)).setText(item);
//                break;
//            default:
//                ((EditText)findViewById(R.id.opp)).setText(item);
//                break;
//        }
//    }
//    public void onNothingSelected(AdapterView<?> arg0) {
//        // TODO Auto-generated method stub
//    }
////    @Override public void onClick(View v) {
////        int id =  v.getId();
////        switch(id) {
////            case R.id.lilibg:
////                closeWindows();
////                break;
////            case R.id.lilisrchico:
////                anims("pulse", v);
////                break;
////            case R.id.lilinkwinlinkaccept:
////                anims("pulse", v);
////                closeWindows();
////                break;
////        }
////    }
//
//
//    public int pxToDp(final float px) {
//        return (int) (px / ((float) getApplicationContext().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
//    }
//    public int dpToPx(final float dp) {
//        return (int) (dp * ((float) getApplicationContext().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
//    }
//    private void sleep(int millis) {
//        try {
//            Thread.sleep(millis);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
//    public static LiveLink getInstance(){
//        return k;
//    }
//    public void setUser(UserN user) {  }
//
//    private void makeToast(final String t, final int len) {
//        TextView tv = new TextView(getApplicationContext());
//        tv.setGravity(Gravity.CENTER);
//        tv.setText(t);
//        tv.setPadding(30, 35, 35, 30);
//        tv.setTextColor(Color.WHITE);
//        tv.setBackgroundResource(R.drawable.toastbg);
//
//        Toast toast = new Toast(getApplicationContext());
//        toast.setView(tv);
//        toast.setDuration(len);
//        toast.show();
//    }
//    private void log(String T, String M) {
//        if(debugLog) Log.e(T, M);
//    }
//    private void log(String T, String M, Exception e) {
//        if(debugLog) Log.e(T, M, e);
//    }
//}