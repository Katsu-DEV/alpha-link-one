package com.alpha.alphalink;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.alpha.alphalink.LinkCore.NodeServer.AllConversations;
import com.alpha.alphalink.LinkCore.NodeServer.Message;
import com.alpha.alphalink.LinkCore.NodeServer.UserN;
import com.alpha.alphalink.LinkCore.SettingsStore;
import com.alpha.alphalink.LinkCore.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.jakewharton.processphoenix.ProcessPhoenix;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("InstanceVariableMayNotBeInitialized")
public class Settings extends AppCompatActivity implements View.OnClickListener {
    static Settings k;
    private SettingsStore settings;
    private ConstraintLayout[] options;
    private TextView[] titles, subtexts;
    private ConstraintLayout delwin, lowin;
    private Animation open, fadein, pulse, close, fade;
    private String FILENAME_CH = "filech.txt", FILENAME_DM = "filedm.txt";
    private UserN user;
    private Message selMess;
    private AllConversations reportedMessages;
    private int cnt = 0, opt = 0, selRepInd = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        k = this;

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Utility.catchCrash("[Settings : Uncaught]", paramThrowable, getApplicationContext());
                System.exit(2);
            }
        });

        Intent i = getIntent();
        user = (UserN) i.getSerializableExtra("User");
        init();
    }

    private void init() {
        // Initialise UI
        findViewById(R.id.settingsbg).setBackgroundColor(getResources().getColor(R.color.colorBg));

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = ((int) (dm.widthPixels - (50*dm.density))), height = dm.heightPixels;
        getWindow().setLayout(width, height);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.RIGHT;


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        // ...but notify us that it happened.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        delwin = findViewById(R.id.delwin);
        lowin = findViewById(R.id.lowin);

        findViewById(R.id.bg).setOnClickListener(this);
        findViewById(R.id.Main).setOnClickListener(this);

        findViewById(R.id.sobtn).setOnClickListener(this);
        findViewById(R.id.y).setOnClickListener(this);

        settings = loadSettings();
        populateOptions(0);
    }

    private void populateOptions(int anim) {
        options = new ConstraintLayout[6];
        titles = new TextView[6];
        subtexts = new TextView[6];

        for(int i = 0; i<options.length; i++) {
            //  Box
            options[i] = new ConstraintLayout(getApplicationContext());
            options[i].setId(View.generateViewId());
            options[i].setLayoutParams(new WindowManager.LayoutParams(0, 0));
            options[i].getLayoutParams().height = dpToPx(60);
            options[i].getLayoutParams().width = dpToPx(250);
            options[i].setPadding(dpToPx(4), dpToPx(3), dpToPx(4), dpToPx(2));
            options[i].setTag(Integer.toString(i));
            options[i].setAlpha(0f);

            // Title
            titles[i] = new TextView(getApplicationContext());
            titles[i].setId(View.generateViewId());
            titles[i].setTextSize(15);

            // Subtext
            subtexts[i] = new TextView(getApplicationContext());
            subtexts[i].setId(View.generateViewId());
            subtexts[i].setTextSize(10);

            options[i].addView(titles[i]);
            options[i].addView(subtexts[i]);


            // Constraints
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(options[i]);

            constraintSet.connect(titles[i].getId(), ConstraintSet.TOP, options[i].getId(), ConstraintSet.TOP);
            constraintSet.connect(titles[i].getId(), ConstraintSet.LEFT, options[i].getId(), ConstraintSet.LEFT);

            constraintSet.connect(subtexts[i].getId(), ConstraintSet.TOP, titles[i].getId(), ConstraintSet.BOTTOM, dpToPx(2));
            constraintSet.connect(subtexts[i].getId(), ConstraintSet.LEFT, options[i].getId(), ConstraintSet.LEFT);

            constraintSet.applyTo(options[i]);

            ((LinearLayout) findViewById(R.id.optionsLinLayout)).addView(options[i]);

            options[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    anims("pulse", v);
                    optionSelect(Integer.parseInt(v.getTag().toString()));
                }
            });
        }

        titles[0].setText("Delete Account");
        subtexts[0].setText("Permanently delete account");

        titles[1].setText("Sign Out");
        subtexts[1].setText("Sign out of account and return to login screen");

        titles[2].setText("Notification Settings");
        subtexts[2].setText("Mute or turn off notifications");

        titles[3].setText("Copy Serial Log");
        subtexts[3].setText("Copy Serial Log ");

        titles[4].setText("Copy Crash Log");
        subtexts[4].setText("Copy Crash Log ");

        if(user.getAccountlevel() == 9) {
            titles[options.length-1].setText("Moderator Options");
            subtexts[options.length-1].setText("Respond to reported messages");
        }
        else {
            titles[options.length-1].setVisibility(View.GONE);
            subtexts[options.length-1].setVisibility(View.GONE);
        }

        for(int i = 0; i<options.length; i++) {
            options[i].animate()
                    .setStartDelay(440+(i*60))
                    .setDuration(250)
                    .alpha(1.0f)
                    .start();
        }
    }

    private void populateReportedMessages() {
        String URL = "https://www.alphahub.site/alphalink/api/protected/getallmessages/reportMessage";

        final Runnable r = new Runnable() {
            @Override
            public void run() {
                if(reportedMessages != null) {
                    final LinearLayout messageslist = findViewById(R.id.repmesslin);
                    messageslist.removeAllViews();

                    int tag = 0;
                    for (Message message : reportedMessages.getMessages()) {
                        // Message
                        ConstraintLayout box = new ConstraintLayout(getApplicationContext());
                        box.setId(View.generateViewId());
                        box.setTag(""+tag++);

                        messageslist.addView(box);

                        TextView msg = new TextView(getApplicationContext());
                        msg.setId(View.generateViewId());

                        box.setBackgroundResource(R.drawable.messageleft);
                        msg.setText("Reported By: "+message.getFrom() + "\n" + message.getWhat().replace("\\", " "));

                        msg.setTextColor(Color.parseColor("#FFFFFF"));
                        msg.setPadding(dpToPx(17), dpToPx(6), dpToPx(17), dpToPx(6));
                        box.addView(msg);

                        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) box.getLayoutParams();
                        params.bottomMargin = (int) (dpToPx(15));
                        box.setLayoutParams(params);

                        ConstraintSet constraintSet = new ConstraintSet();
                        constraintSet.clone(box);
                        constraintSet.connect(msg.getId(), ConstraintSet.LEFT, box.getId(), ConstraintSet.LEFT, 0);
                        constraintSet.applyTo(box);

                        box.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //ArrayList<View> msgs = new ArrayList<>();
                                opt = 0;
                                for(int i = 0; i<messageslist.getChildCount(); i++) {
                                    messageslist.getChildAt(i).setBackgroundResource(R.drawable.messageleft);
                                    messageslist.getChildAt(i).setElevation(0.0f);
                                }
                                v.setBackgroundResource(R.drawable.messageleftreportedselected);
                                v.setElevation(10.0f);
                                selRepInd = Integer.parseInt(v.getTag().toString());
                                Message mm = reportedMessages.getMessages().get(Integer.parseInt(v.getTag().toString()));
                                //Message m = new Gson().fromJson(mm.getWhat(), Message.class);
                                selMess = mm;
                                Log.e("Sel", ": "+mm.toJsonString());
                            }
                        });
                    }
                }
            }
        };

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("FG", response.toString());
                        reportedMessages = (new Gson()).fromJson(response.toString(), AllConversations.class);
                        runOnUiThread(r);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Err Resp GCW", error.toString());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }

    private void onClickOptChkYes() {
        // Dismiss, Ban, Strike, Delete
        switch(opt) {
            case 1:
                dismissReport(new Runnable() {
                    @Override
                    public void run() {
                        hideReportedMessageView();
                        opt = 0;
                        selRepInd = -1;
                    }
                });
                break;
            case 2:
                banPlayer(new Gson().fromJson(selMess.getWhat().replace("{", "{\"").replace("}", "\"}").replace("from:", "from\":\"").replace(",to:", "\",\"to\":\"").replace(",what:", "\",\"what\":\"").replace(",when:", "\",\"when\":\""), Message.class).getFrom(), new Runnable() {
                    @Override
                    public void run() {
                        deleteReportedMessage(new Gson().fromJson(selMess.getWhat().replace("{", "{\"").replace("}", "\"}").replace("from:", "from\":\"").replace(",to:", "\",\"to\":\"").replace(",what:", "\",\"what\":\"").replace(",when:", "\",\"when\":\""), Message.class), new Runnable() {
                            @Override
                            public void run() {
                                dismissReport(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideReportedMessageView();
                                        opt = 0;
                                        selRepInd = -1;
                                    }
                                });
                            }
                        });
                    }
                });
                break;
            case 3:
                strikePlayer(new Gson().fromJson(selMess.getWhat().replace("{", "{\"").replace("}", "\"}").replace("from:", "from\":\"").replace(",to:", "\",\"to\":\"").replace(",what:", "\",\"what\":\"").replace(",when:", "\",\"when\":\""), Message.class).getFrom(), new Runnable() {
                    @Override
                    public void run() {
                        deleteReportedMessage(new Gson().fromJson(selMess.getWhat().replace("{", "{\"").replace("}", "\"}").replace("from:", "from\":\"").replace(",to:", "\",\"to\":\"").replace(",what:", "\",\"what\":\"").replace(",when:", "\",\"when\":\""), Message.class), new Runnable() {
                            @Override
                            public void run() {
                                dismissReport(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideReportedMessageView();
                                        opt = 0;
                                        selRepInd = -1;
                                    }
                                });
                            }
                        });
                    }
                });
                break;
            case 4:
                deleteReportedMessage(new Gson().fromJson(selMess.getWhat().replace("{", "{\"").replace("}", "\"}").replace("from:", "from\":\"").replace(",to:", "\",\"to\":\"").replace(",what:", "\",\"what\":\"").replace(",when:", "\",\"when\":\""), Message.class), new Runnable() {
                    @Override
                    public void run() {
                        dismissReport(new Runnable() {
                            @Override
                            public void run() {
                                hideReportedMessageView();
                                opt = 0;
                                selRepInd = -1;
                            }
                        });
                    }
                });
                break;
            default: break;
        }
    }

    private void banPlayer(String username, final Runnable task) {
        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/banuser/"+username,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            Log.e("BanPlayerScs", response);
                            if(task != null) task.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("BanPlayerErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }
        };
        rQ.add(strRe);
    }

    private void strikePlayer(String username, final Runnable task) {
        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/strikeuser/"+username,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            Log.e("StrikePlayerScs", response);
                            if(task != null) task.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("StrikePlayerErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }
        };
        rQ.add(strRe);
    }

    private void deleteReportedMessage(final Message m, final Runnable task) {
        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/deletemessage",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            Log.e("DeleteReportedMessageScs", response);
                            if(task != null) task.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("DeleteReportedMessageErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() { return m.toJsonString().getBytes(); }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        try {
            Log.e("Body", new String(strRe.getBody()));
        }
        catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        rQ.add(strRe);
    }

    private void dismissReport(final Runnable task) {
        Log.e("Sel", "Mes: "+selMess.toJsonString());
        RequestQueue rQ = Volley.newRequestQueue(BaseActivity.getInstance());
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/dismissreport/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            Log.e("DismissReportScs", response);
                            if(task != null) task.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("DismissReportErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() { return selMess.toJsonString().getBytes(); }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        try {
            Log.e("Body", new String(strRe.getBody()));
        }
        catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        rQ.add(strRe);
    }

    private void hideReportedMessageView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((LinearLayout) findViewById(R.id.repmesslin)).getChildAt(selRepInd).setVisibility(View.GONE);
            }
        });
    }

    private void deleteChallenges() {
        String URL = "https://www.alphahub.site/alphalink/api/protected/deleteChallengesByUsername/"
                + user.getName();

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.DELETE,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Delete Challenges", "SCS "+response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Delete Challenges", "ERR "+error.toString());

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }

    private void deleteMessages() {
        String URL = "https://www.alphahub.site/alphalink/api/protected/deleteallmessages/"
                + user.getName();

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.DELETE,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Delete Messages", "SCS "+response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Delete Messages", "ERR "+error.toString());

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };

        rQ.add(obRe);
    }

    private void deleteUser() {
        String URL = "https://www.alphahub.site/alphalink/api/protected/deleteUserByUsername/"
                + user.getName();
        settings = SettingsStore.getEmpty();
        saveSettings();
        saveChalChk(" ");
        saveDmChk(" ");

        RequestQueue rQ = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest obRe = new JsonObjectRequest(
                Request.Method.DELETE,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Delete User", "SCS "+response.toString());
                        terminate();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Delete User", "ERR "+error.toString());
                        deleteUser();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Token", "BckExPox0K1JqZSBuFrGlMgRNJsVuETo");
                return headers;
            }
        };
        rQ.add(obRe);
    }

    private void terminate() {
        BackgroundService.getInstance().stopListening();
        BaseActivity.getInstance().terminateService();

        GoogleSignInClient mGoogleSignInClient;
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        FirebaseAuth mAuth;
        if (acct != null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            mAuth = FirebaseAuth.getInstance();
            // Firebase sign out
            mAuth.signOut();
            // Google sign out
            mGoogleSignInClient.signOut().addOnCompleteListener(this,
                    new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            finish();

                            Utility.postDelayed(250, new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(Settings.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                    ProcessPhoenix.triggerRebirth(getApplicationContext(), intent);
                                }
                            });
                        }
                    });
        }
        else {
            // didnt log out
            makeToast("Something went wrong... [T0]");
            Log.e("Error", "Did not log out");
        }
    }

    private void switches(String whichSwitch) {
        switch(whichSwitch) {
            case "notify":
                if(((Switch) findViewById(R.id.switchNotify)).isChecked()) {
                    settings.setNotify("on");
                    settings.setNotificationsound("on");
                    settings.setNotificationvibrate("on");

                    ((Switch) findViewById(R.id.switchMuteSound)).setChecked(true);
                    ((Switch) findViewById(R.id.switchMuteVibrate)).setChecked(true);

                    ((Switch) findViewById(R.id.switchMuteSound)).setClickable(true);
                    ((Switch) findViewById(R.id.switchMuteVibrate)).setClickable(true);

                    ((Switch) findViewById(R.id.switchMuteSound)).setAlpha(0.7f);
                    ((Switch) findViewById(R.id.switchMuteVibrate)).setAlpha(0.7f);
                }
                else {
                    settings.setNotify("off");
                    settings.setNotificationsound("off");
                    settings.setNotificationvibrate("off");

                    ((Switch) findViewById(R.id.switchMuteSound)).toggle();
                    ((Switch) findViewById(R.id.switchMuteVibrate)).toggle();

                    ((Switch) findViewById(R.id.switchMuteSound)).setChecked(false);
                    ((Switch) findViewById(R.id.switchMuteVibrate)).setChecked(false);

                    ((Switch) findViewById(R.id.switchMuteSound)).setClickable(false);
                    ((Switch) findViewById(R.id.switchMuteVibrate)).setClickable(false);

                    ((Switch) findViewById(R.id.switchMuteSound)).setAlpha(0.3f);
                    ((Switch) findViewById(R.id.switchMuteVibrate)).setAlpha(0.3f);
                }
                break;
            case "sound":
                String soundSt = ((Switch) findViewById(R.id.switchMuteSound)).isChecked() ? "on" : "off";
                settings.setNotificationsound(soundSt);
                break;
            case "vibrate":
                String vibSt = ((Switch) findViewById(R.id.switchMuteVibrate)).isChecked() ? "on" : "off";
                settings.setNotificationvibrate(vibSt);
                break;
            default: break;
        }

        Log.e("SETS", settings.toJsonString());
    }

    private void anims(String cmd, final View view) {
        open = AnimationUtils.loadAnimation(this, R.anim.openone);
        fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);
        pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        close = AnimationUtils.loadAnimation(this, R.anim.closeone);
        fade = AnimationUtils.loadAnimation(this, R.anim.fade);

        switch(cmd) {
            case "delwinntc":
                findViewById(R.id.bg).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.delntc)).setText("Do you wish to delete your Alpha Link account?");
                ((TextView) findViewById(R.id.y)).setText("O K");
                findViewById(R.id.windows).setVisibility(View.VISIBLE);
                delwin.setVisibility(View.VISIBLE);
                lowin.setVisibility(View.GONE);
                findViewById(R.id.delwin).startAnimation(open);
                fadein.setDuration(200);
                findViewById(R.id.bg).startAnimation(fadein);
                break;
            case "delwinexe":
                delwin.setVisibility(View.GONE);
                ((TextView) findViewById(R.id.delntc)).setText("This will permanently erase your data.\nDo you wish to continue?");
                ((TextView) findViewById(R.id.y)).setText("D E L E T E");

                findViewById(R.id.windows).setVisibility(View.VISIBLE);
                delwin.setVisibility(View.VISIBLE);
                lowin.setVisibility(View.GONE);
                findViewById(R.id.delwin).startAnimation(open);
                fadein.setDuration(200);
                findViewById(R.id.bg).startAnimation(fadein);
                break;
            case "logout":
                findViewById(R.id.windows).setVisibility(View.VISIBLE);
                findViewById(R.id.bg).setVisibility(View.VISIBLE);
                delwin.setVisibility(View.GONE);
                lowin.setVisibility(View.VISIBLE);
                findViewById(R.id.lowin).startAnimation(open);
                fadein.setDuration(200);
                findViewById(R.id.bg).startAnimation(fadein);
                break;
            case "pulse":
                view.startAnimation(pulse);
                break;
            case "close":
                close.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        view.setVisibility(View.GONE);}
                });
                view.startAnimation(close);
                break;
            case "fade":
                fade.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        view.setVisibility(View.GONE);}
                });
                view.startAnimation(fade);
                break;
            case "notsets":
                findViewById(R.id.windows).setVisibility(View.VISIBLE);
                findViewById(R.id.bg).setVisibility(View.VISIBLE);
                findViewById(R.id.notsetswin).setVisibility(View.VISIBLE);
                findViewById(R.id.notsetswin).startAnimation(open);
                fadein.setDuration(200);
                findViewById(R.id.bg).startAnimation(fadein);

                ((Switch) findViewById(R.id.switchNotify)).setChecked(settings.getNotify().equals("on"));
                ((Switch) findViewById(R.id.switchMuteSound)).setChecked(settings.getNotificationsound().equals("on"));
                ((Switch) findViewById(R.id.switchMuteVibrate)).setChecked(settings.getNotificationvibrate().equals("on"));
                break;
            case "repmes":
                findViewById(R.id.windows).setVisibility(View.VISIBLE);
                findViewById(R.id.bg).setVisibility(View.VISIBLE);
                findViewById(R.id.reportedmessageswin).setVisibility(View.VISIBLE);
                findViewById(R.id.reportedmessageswin).startAnimation(open);
                fadein.setDuration(200);
                findViewById(R.id.bg).startAnimation(fadein);
                break;
            case "checkModOpt":
                findViewById(R.id.bg).setVisibility(View.VISIBLE);
                findViewById(R.id.windows).setVisibility(View.VISIBLE);
                findViewById(R.id.modoptchkwin).setVisibility(View.VISIBLE);
                findViewById(R.id.modoptchkwin).startAnimation(open);
                switch(opt) {
                    case 0:
                        ((TextView) findViewById(R.id.modoptchkntc)).setText("Error! Please press \"no\" and try again.");
                        break;
                    case 1:
                        ((TextView) findViewById(R.id.modoptchkntc)).setText("Would you like to Dismiss the Report?");
                        break;
                    case 2:
                        String neww = selMess.getWhat().replace("{", "{\"").replace("}", "\"}").replace("from:", "from\":\"").replace(",to:", "\",\"to\":\"").replace(",what:", "\",\"what\":\"").replace(",when:", "\",\"when\":\"");
                        String usr1 = new Gson().fromJson(neww, Message.class).getFrom();
                        ((TextView) findViewById(R.id.modoptchkntc)).setText("Would you like to suspend the account of player "+usr1+"?");
                        break;
                    case 3:
                        String usr2 = new Gson().fromJson(
                                selMess.getWhat().replace("{", "{\"")
                                        .replace("}", "\"}")
                                        .replace("from:", "from\":\"")
                                        .replace(",to:", "\",\"to\":\"")
                                        .replace(",what:", "\",\"what\":\"")
                                        .replace(",when:", "\",\"when\":\""), Message.class).getFrom();
                        ((TextView) findViewById(R.id.modoptchkntc)).setText("Would you like to add a strike to the account of player "+usr2+"?\nWarning: If the player already has four strikes, this action will result in a ban.");
                        break;
                    case 4:
                        ((TextView) findViewById(R.id.modoptchkntc)).setText("Would you like to delete the offending message?");
                        break;
                    default: break;
                }
                break;
            default:
                break;
        }
    }

    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            closeWindows();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    public static Settings getInstance(){
        return k;
    }
    public void setUser(UserN user) { this.user = user; }
    public UserN getUser() { return user; }
    private void closeWindows() {
        if(findViewById(R.id.bg).getVisibility() == View.VISIBLE) {
            anims("fade", findViewById(R.id.bg));

            if (findViewById(R.id.delwin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.delwin));

            if (findViewById(R.id.lowin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.lowin));

            if (findViewById(R.id.notsetswin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.notsetswin));

            if (findViewById(R.id.reportedmessageswin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.reportedmessageswin));

            if (findViewById(R.id.modoptchkwin).getVisibility() == View.VISIBLE)
                anims("close", findViewById(R.id.modoptchkwin));
        }
    }

    private void saveSettings() {
        String text = settings.toJsonString();
        if(text.equals("") || text == null) text="";
        FileOutputStream fos = null;
        try {
            fos = openFileOutput("settingsstore.txt", MODE_PRIVATE);
            fos.write(text.getBytes());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e("FOS", "FOS == NULL");
        }
    }
    private SettingsStore loadSettings() {
        String ret = "";
        FileInputStream fis = null;

        try {
            fis = openFileInput("settingsstore.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();

            while ( (ret = br.readLine()) !=null ) {
                sb.append(ret).append("\n");
            }
            ret = sb.toString();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fis != null) {
                try {
                    fis.close();

                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        SettingsStore retSets = null;
        if(ret == null || ret.equals("")) retSets = SettingsStore.getEmpty();
        else retSets = new Gson().fromJson(ret, SettingsStore.class);

        return retSets;
    }

    private void saveChalChk(String text) {
        if(text == null || text.equals("")) text="";
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(FILENAME_CH, MODE_PRIVATE);
            fos.write(text.getBytes());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e("FOS", "FOS == NULL");
        }
    }
    private void saveDmChk(String text) {
        if(text == null || text.equals("")) text="";
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(FILENAME_DM, MODE_PRIVATE);
            fos.write(text.getBytes());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e("FOS", "FOS == NULL");
        }
    }

    private int dpToPx(float dp) { return Utility.dpToPx(dp, this); }
    private int pxToDp(float px) { return Utility.pxToDp(px, this); }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.bg:
                closeWindows();
                break;
            case R.id.Main:
                if(!lowin.isShown()) findViewById(R.id.windows).setVisibility(View.GONE);
                cnt = 0;
                break;
            case R.id.box1:
                anims("pulse", view);
                anims("delwinntc", null);
                cnt = 0;
                break;
            case R.id.box2:
                anims("pulse", view);
                anims("logout", null);
                break;
            case R.id.box3:
                anims("pulse", view);
                anims("notsets", null);
                break;
            case R.id.y:
                if(cnt == 1) {
                    deleteChallenges();
                    deleteMessages();
                    deleteUser();
                }
                if(cnt == 0) {
                    anims("delwinexe", null);
                    cnt = 1;
                }
                break;
            case R.id.sobtn:
                anims("pulse", view);
                terminate();
                break;
            case R.id.notsetsbtn:
                anims("pulse", view);
                closeWindows();
                saveSettings();
                break;
            case R.id.switchNotify:
                anims("pulse", view);
                switches("notify");
                break;
            case R.id.switchMuteSound:
                anims("pulse", view);
                switches("sound");
                break;
            case R.id.switchMuteVibrate:
                anims("pulse", view);
                switches("vibrate");
                break;
            case R.id.reportedmessagesdismiss:
                anims("pulse", view);
                if(selMess != null) {
                    opt = 1;
                    anims("checkModOpt", null);
                }
                break;
            case R.id.reportedmessagesban:
                anims("pulse", view);
                if(selMess != null) {
                    opt = 2;
                    anims("checkModOpt", null);
                }
                break;
            case R.id.reportedmessagesstrike:
                anims("pulse", view);
                if(selMess != null) {
                    opt = 3;
                    anims("checkModOpt", null);
                }
                break;
            case R.id.reportedmessagesdelete:
                anims("pulse", view);
                if(selMess != null) {
                    opt = 4;
                    anims("checkModOpt", null);
                }
                break;
            case R.id.modoptchkyes:
                anims("pulse", view);
                anims("close", findViewById(R.id.modoptchkwin));
                onClickOptChkYes();
                opt = 0;
                break;
            case R.id.modoptchkno:
                anims("pulse", view);
                anims("close", findViewById(R.id.modoptchkwin));
                opt = 0;
                break;
            default: break;
        }
    }

    public void optionSelect(int option) {
        switch(option) {
            case 0:
                anims("delwinntc", null);
                cnt = 0;
                break;
            case 1:
                anims("logout", null);
                break;
            case 2:
                anims("notsets", null);
                break;
            case 3:
                Utility.copySerialLog();
                break;
            case 4:
                Utility.copyCrashLog();
                break;
            case 5:
                if(user.getAccountlevel() != 9) return;
                selMess = null;
                anims("repmes", null);
                populateReportedMessages();
                break;
            default: break;
        }
    }

    public void makeToast(final String t) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int duration = Toast.LENGTH_SHORT;
                Toast.makeText(k, t, duration).show();
            }
        });
    }
    private void makeToast(final String t, final int len) {
        TextView tv = new TextView(getApplicationContext());
        tv.setGravity(Gravity.CENTER);
        tv.setText(t);
        tv.setPadding(25, 30, 30, 25);
        tv.setTextColor(Color.WHITE);
        tv.setBackgroundResource(R.drawable.toastbg);

        Toast toast = new Toast(getApplicationContext());
        toast.setView(tv);
        toast.setDuration(len);
        toast.show();
    }
}
