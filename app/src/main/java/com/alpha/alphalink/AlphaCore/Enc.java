package com.alpha.alphalink.AlphaCore;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

/*
    Encryption and Decryption
    Authors: KaseyJoes and Katsu (Ben)
 */
public class Enc {
    private static char[] KEY = {'z', '&', 'X', '%', '$', 'a', '!', '?', '[', '+', 'n', 'r', '{', 't', '}'};
    private static ArrayList<Character> map;

    public static String encrypt(String dR)
    {
        formMap();
        char[] dr = dR.toCharArray();
        Random gen = new Random();
        char[] edr = new char[dr.length + 2];
        int temp, i, offset = Math.abs(gen.nextInt()) % (KEY.length);

        temp = (int) (offset / 10);
        edr[0] = (char) (temp + '0');
        temp = offset % 10;
        edr[1] = (char) (temp + '0');

        for(i = 0; i < dr.length; i++)
        {
            if (i == 0) temp = map.indexOf(dr[0]) + map.indexOf(KEY[offset]);
            else        temp = map.indexOf(dr[i]) + map.indexOf(KEY[(offset + i) % KEY.length]);

            temp %= map.size();
            edr[2+i] = (char) map.get(temp);
        }
        return new String(edr);
    }

    public static String decrypt(String dR)
    {
        formMap();
        int offset, temp, i = 0;
        char[]  edr = dR.toCharArray(),
                dr = new char[edr.length - 2];

        offset = (int) ( edr[0] - '0') * 10;
        offset += (int) (edr[1] - '0');

        Log.e("e", ""+i);
        for (i = 0; i < edr.length - 2; i++)
        {
            if (i == 0) temp = map.indexOf(edr[2+i]) - map.indexOf(KEY[offset]);
            else        temp = map.indexOf(edr[2+i]) - map.indexOf(KEY[(offset + i) % KEY.length]);

            if (temp < 0) temp += map.size();
            dr[i] = (char) map.get(temp);
        }

        return new String(dr);
    }

    private static void formMap() {
        map = new ArrayList<Character>();

        map.add('0');
        map.add('1');
        map.add('2');
        map.add('3');
        map.add('4');
        map.add('5');
        map.add('6');
        map.add('7');
        map.add('8');
        map.add('9');
        map.add('a');
        map.add('b');
        map.add('c');
        map.add('d');
        map.add('e');
        map.add('f');
        map.add('g');
        map.add('h');
        map.add('i');
        map.add('j');
        map.add('k');
        map.add('l');
        map.add('m');
        map.add('n');
        map.add('o');
        map.add('p');
        map.add('q');
        map.add('r');
        map.add('s');
        map.add('t');
        map.add('u');
        map.add('v');
        map.add('w');
        map.add('x');
        map.add('y');
        map.add('z');
        map.add('A');
        map.add('B');
        map.add('C');
        map.add('D');
        map.add('E');
        map.add('F');
        map.add('G');
        map.add('H');
        map.add('I');
        map.add('J');
        map.add('K');
        map.add('L');
        map.add('M');
        map.add('N');
        map.add('O');
        map.add('P');
        map.add('Q');
        map.add('R');
        map.add('S');
        map.add('T');
        map.add('U');
        map.add('V');
        map.add('W');
        map.add('X');
        map.add('Y');
        map.add('Z');
        map.add('!');
        map.add('?');
        map.add('$');
        map.add('&');
        map.add('%');
        map.add('(');
        map.add(')');
        map.add('[');
        map.add(']');
        map.add('{');
        map.add('}');
        map.add('+');
        map.add('=');
        map.add('-');
    }
}

