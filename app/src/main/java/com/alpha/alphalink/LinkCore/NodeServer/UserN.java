
package com.alpha.alphalink.LinkCore.NodeServer;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.EditText;

import com.alpha.alphalink.BackgroundService;
import com.alpha.alphalink.BaseActivity;
import com.alpha.alphalink.LinkCore.RomManager;
import com.alpha.alphalink.LinkCore.Utility;
import com.alpha.alphalink.MainActivity;
import com.alpha.alphalink.R;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class UserN implements Serializable {
    public static final long serialVersionUID = 8892168273359866657L;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("uid")
    @Expose
    private String uid;

    @SerializedName("slots")
    @Expose
    private SlotN[] slots;

    @SerializedName("stats")
    @Expose
    private Stats stats;

    @SerializedName("joined")
    @Expose
    private long joined;

    @SerializedName("lastseen")
    @Expose
    private long lastseen;

    @SerializedName("dmchk")
    @Expose
    private String dmchk;

    @SerializedName("chalchk")
    @Expose
    private String chalchk;

    @SerializedName("commTypes")
    @Expose
    private String commTypes;

    @SerializedName("theme")
    @Expose
    private int theme;

    @SerializedName("accountlevel")
    @Expose
    private int accountlevel;

    @SerializedName("banned")
    @Expose
    private long banned;

    @SerializedName("aid")
    @Expose
    private String aid;

    public UserN() {}

    public UserN(String name, String uid) {
        setName(name);
        setUid(uid);
        joined = Utility.getTime();;
        updateLastseen();
        theme = 0;
        setSlots(new SlotN[]{});
        setStats(new Stats());
        setAccountlevel(0);
        setBanned(0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public SlotN[] getSlots() {
        return slots;
    }

    public SlotN getSlotFromIdentifier(long identifier) {
        for(SlotN s : slots) {
            if(s.getBirthdate() == Math.abs(identifier)) return s;
        }
        return null;
    }

    public int getSlotIndexFromIdentifier(long identifier) {
        int i;
        for(i = 0; i<slots.length && slots[i].getBirthdate() != Math.abs(identifier); i++) {
            Log.e("GG"+i, ""+identifier + " | " + slots[i].getBirthdate());
        }
        return i;
    }

    public SlotN getSlotFromIdentifierTwo(long identifier) {
        return slots[getSlotIndexFromIdentifier(identifier)];
    }

    public void setSlots(SlotN[] slots) {
        this.slots = slots;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public long getJoined() {
        return joined;
    }

    public void setJoined(long joined) {
        this.joined = joined;
    }

    public long getLastseen() {
        return lastseen;
    }

    public void setLastseen(long lastseen) {
        this.lastseen = lastseen;
    }

    public void updateLastseen() {
        Log.e("UpdateLastSeen", "| "+lastseen+" | "+Utility.getTime());
        lastseen = Utility.getTime();;
    }

    public String getDmchk() {
        return dmchk;
    }

    public void setDmchk(String dmchk) {
        this.dmchk = dmchk;
    }

    public String getChalchk() {
        return chalchk;
    }

    public void setChalchk(String chalchk) {
        this.chalchk = chalchk;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getCommTypes() {
        return commTypes;
    }

    public void setCommTypes(String commTypes) {
        this.commTypes = commTypes;
    }

    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public int getAccountlevel() {
        return accountlevel;
    }

    public void setAccountlevel(int accountlevel) {
        this.accountlevel = accountlevel;
    }

    public long getBanned() {
        return banned;
    }

    public void setBanned(long banned) {
        this.banned = banned;
    }

    public void updateSlot(int index, SlotN slot) {
        Log.e("SUb", ""+slots.length);
        List<SlotN> alSlots = new ArrayList<SlotN>();
        for(SlotN s : slots) alSlots.add(s);
        alSlots.add(index, slot);
        slots = alSlots.toArray(new SlotN[slots.length-1]);
        Log.e("SUa", ""+slots.length);
    }
    public void deleteSlot(int slotSel) {
        Log.e("SIb", ""+slots.length);
        List<SlotN> alSlots = new ArrayList<SlotN>();
        for(SlotN s : slots) alSlots.add(s);
        alSlots.remove(slotSel);
        slots = alSlots.toArray(new SlotN[slots.length-1]);
        Log.e("SIa", ""+slots.length);
    }
    public void addSlot(SlotN slot) {
        SlotN[] newSlots = new SlotN[slots.length+1];
        for(int i = 0; i<slots.length; i++) newSlots[i] = slots[i];
        newSlots[newSlots.length-1] = slot;
        slots = newSlots;
        generateCommTypes();
    }

    //public void winsUp() { this.winsUp("E"); }
    public void winsUp(String oppRank) {
        int pointsUp = 5, myRN = 0, oppRN = 0, diff = 0;
        switch(stats.getRank().toUpperCase()) {
            case "E":
                myRN = 0;
                break;
            case "D":
                myRN = 1;
                break;
            case "C":
                myRN = 2;
                break;
            case "B":
                myRN = 3;
                break;
            case "A":
                myRN = 4;
                break;
            case "S":
                myRN = 5;
                break;
            default:
                break;
        }
        switch(oppRank.toUpperCase()) {
            case "E":
                myRN = 0;
                break;
            case "D":
                myRN = 1;
                break;
            case "C":
                myRN = 2;
                break;
            case "B":
                myRN = 3;
                break;
            case "A":
                myRN = 4;
                break;
            case "S":
                myRN = 5;
                break;
            default:
                break;
        }

        diff = oppRN - myRN;

        if(diff > 0) pointsUp += (diff * 9);

        stats.setWin(stats.getWin()+1);
        stats.setScore(stats.getScore()+pointsUp);
        stats.genRank();
    }
    public void lossUp() {
        stats.setLoss(stats.getLoss()+1);
        stats.setScore(stats.getScore()+1);
        stats.genRank();
    }
    public void drawUp() {
        stats.setDraw(stats.getDraw()+1);
        stats.setScore(stats.getScore()+3);
        stats.genRank();
    }

    public String getPosition(UserN[] leaderboard) {
        if(leaderboard != null && leaderboard.length > 0){
            for(int i = 0; i< leaderboard.length; i++)
                if(leaderboard[i].getName().equals(name)) return RomManager.leadingZeros(""+i, 3);
        }
        return "XXX";
    }

    public void generateCommTypes() {
        String cts = "000000000000000";

        for(SlotN s : slots)  {
            if(s != null) {
                switch(s.getCommType()) {
                    case 1:
                        cts = cts.substring(0, 14) + "1";
                        break;
                    case 3:
                        cts = cts.substring(0, 13) + "1" + cts.substring(14);
                        break;
                    case 7:
                        cts = cts.substring(0,12) + "1" + cts.substring(13);
                        break;
                    default: break;
                }
            }
        };

        commTypes = Integer.toString(Integer.parseInt(cts, 2), 32);
    }

    public SlotN getMostBattledPartner() {
        if(slots.length == 0) return null;
        else if (slots.length == 1) return slots[0];
        else {
            int crsr = 0, battles = 0;
            for(int i = 0; i<slots.length; i++) {
                if(slots[i] != null) {
                    int total = slots[i].getWins() + slots[i].getLoss() + slots[i].getDraw();
                    if (battles < total) {
                        battles = total;
                        crsr = i;
                    }
                }
            }

            return slots[crsr];
        }
    }

    public boolean hasPartners() {
        return (slots != null && slots.length > 0);
    }

    public String toJsonString() {
        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create()
                .toJson(this);
    }

    public static UserN fromJson(String json) { return new Gson().fromJson(json, UserN.class); }

    public void updateUserInDatabase() { updateUserInDatabase(null);}
    public void updateUserInDatabase(final Runnable task) {
        updateLastseen();
        Log.e("USRN", toJsonString());
        Context context = (BaseActivity.getInstance() != null) ? BaseActivity.getInstance() : MainActivity.getInstance();
        RequestQueue rQ = Volley.newRequestQueue(context);
        StringRequest strRe = new StringRequest(Request.Method.POST, "https://www.alphahub.site/alphalink/api/protected/updateUser",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null) {
                            Log.e("UpdateUserInDatabaseScs", response);
                            if(BackgroundService.getInstance() != null) BackgroundService.getInstance().getUserFromDatabase(null, false);
                            if(task != null) task.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("UpdateUserInDatabaseErr", error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() { return toJsonString().getBytes(); }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        try {
            Log.e("Body", new String(strRe.getBody()));
        }
        catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        rQ.add(strRe);
    }

    ///api/protected/updateLastSeen/
}