
package com.alpha.alphalink.LinkCore.NodeServer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Conversation {

    @SerializedName("messages")
    @Expose
    private List<Message> messages = null;

    public List<Message> getMessages() {
        return messages;
    }

    public List<Message> getShortMessages() {
        return (messages != null) ?
                messages
                .subList(messages.size() - (messages.size() > 30 ? 30:messages.size()),
                         messages.size()) :
                null;
    }


    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void appendMessage(Message m) {
        if(messages == null) messages = new ArrayList<>();
        messages.add(m);
    }
    
    public void sortMessages() {
        if (messages != null) {
            if (messages.size() == 2) {
                if ((messages.get(0).getWhen() < messages.get(1).getWhen())) {
                    Message a = messages.get(0);
                    messages.add(0, messages.get(1));
                    messages.add(1, a);
                }
            } else {
                for (int i = messages.size() - 1; i > 1; i--) {
                    for (int j = 0; j < i; j++) {
                        if (messages.get(j).getWhen() < messages.get(j + 1).getWhen()) {
                            Message c = messages.get(j);
                            messages.add(j, messages.get(j + 1));
                            messages.add(j + 1, c);
                        }
                    }
                }
            }
        }
    }

    public String getConversant(String user) {
        return (messages != null) ?
                messages.get(0).getFrom().equals(user) ? messages.get(0).getTo():messages.get(0).getFrom() : null;
    }

    public Message getLastMessage() {
        return (messages != null) ? messages.get(messages.size()-1) : null;
    }

    public Message getMessageFromWhen(String from, long when) {
        if(messages != null) {
            for(Message m : messages) {
                if(m.getWhen() == when && m.getFrom().equals(from)) return m;
            }
        }
        return null;
    }

    public boolean hasMessages() {
        return messages != null && messages.size() > 0;
    }
}
