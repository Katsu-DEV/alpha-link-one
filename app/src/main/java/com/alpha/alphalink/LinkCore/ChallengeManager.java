package com.alpha.alphalink.LinkCore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ChallengeManager {
    public static String increaseAWins(String C) {
        int c = Integer.parseInt(C.substring(0,2), 36)+1;
        String z = leadingZeros(Integer.toString(c, 36), 2);
        return z + C.substring(2);
    }
    public static String increaseBWins(String C) {
        int c = Integer.parseInt(C.substring(6,8), 36)+1;
        String z = leadingZeros(Integer.toString(c, 36), 2);
        return C.substring(0, 6) + z + C.substring(8);
    }
    public static String increaseALoss(String C) {
        int c = Integer.parseInt(C.substring(2, 4), 36)+1;
        String z = leadingZeros(Integer.toString(c, 36), 2);
        return C.substring(0,2) + z + C.substring(4);
    }
    public static String increaseBLoss(String C) {
        int c = Integer.parseInt(C.substring(8, 10), 36)+1;
        String z = leadingZeros(Integer.toString(c, 36), 2);
        return C.substring(0,8) + z + C.substring(10);
    }
    public static String increaseADraws(String C) {
        int c = Integer.parseInt(C.substring(4, 6), 36)+1;
        String z = leadingZeros(Integer.toString(c, 36), 2);
        return C.substring(0,4) + z + C.substring(6);
    }
    public static String increaseBDraws(String C) {
        int c = Integer.parseInt(C.substring(10, 12), 36)+1;
        String z = leadingZeros(Integer.toString(c, 36), 2);
        return C.substring(0,10) + z + C.substring(12);
    }
    public static String setASlot(String C, int slot) {
        String sa = String.valueOf(slot);
        return C.substring(0, 12) + sa + C.substring(13);
    }
    public static String setBSlot(String C, int slot) {
        String sb = String.valueOf(slot);
        return C.substring(0, 13) + sb + C.substring(14);
    }
    public static String setCommType(String C, int commType) {
        String ct = leadingZeros(Integer.toString(commType, 36), 2);
        return C.substring(0, 14) + ct;
    }

    //  ====== C ======
    //  AA BB CC DD EE FF G H II
    //  AWins, ALoss, ADraw, BWins, BLoss, BDraw, ASlot, BSlot, CommType
    public static int getAWins(String C) {      // AA
        return Integer.parseInt(C.substring(0,2), 36);
    }
    public static int getALoss(String C) {      // BB
        return Integer.parseInt(C.substring(2,4), 36);
    }
    public static int getADraws(String C) {     // CC
        return Integer.parseInt(C.substring(4,6), 36);
    }
    public static int getBWins(String C) {      // DD
        return Integer.parseInt(C.substring(6,8), 36);
    }
    public static int getBLoss(String C) {      // EE
        return Integer.parseInt(C.substring(8,10), 36);
    }
    public static int getBDraws(String C) {     // FF
        return Integer.parseInt(C.substring(10,12), 36);
    }
    public static int getASlot(String C) {      // G
        return Integer.parseInt(C.substring(12, 13));
    }
    public static int getBSlot(String C) {      // H
        return Integer.parseInt(C.substring(13, 14));
    }
    public static int getCommType(String C) {   // II
        return Integer.parseInt(C.substring(14, 16), 36);
    }

    public static String messageStamp(Date date) {
        SimpleDateFormat    Mf = new SimpleDateFormat("MM", Locale.getDefault()),
                            df = new SimpleDateFormat("DD", Locale.getDefault()),
                            hf = new SimpleDateFormat("HH", Locale.getDefault()),
                            mf = new SimpleDateFormat("mm", Locale.getDefault()),
                            sf = new SimpleDateFormat("ss", Locale.getDefault());

        String  M = Mf.format(date).substring(1,2),
                d = df.format(date).substring(1,2),
                h = hf.format(date).substring(1,2),
                m = mf.format(date).substring(1,2),
                s = sf.format(date);

        String op = M+d+h+m+s;
        long i = Long.parseLong(op);
        return leadingZeros( Long.toString(i, 36) , 4);
    }

    public static String condenseDate(Date date) {
        SimpleDateFormat    Yf = new SimpleDateFormat("YY", Locale.getDefault()),
                            Rf = new SimpleDateFormat("MMDDHHmmss", Locale.getDefault());

        String  Y = Yf.format(date).substring(1,2),
                R = Rf.format(date).substring(0,10);

        String op = Y+R;
        long i = Long.parseLong(op);
        return leadingZeros( Long.toString(i, 36) , 7);
    }

    public static int getState(String D) {
        return Integer.parseInt(D.substring(0,1), 36);
    }
    public static String setState(String D, int state) {
        return Integer.toString(state, 36) + D.substring(1);
    }
    public static String getRom(String D) {
        return D.substring(1);
    }
    public static String setRom(String D, String rom) {
        return D.substring(1);
    }


    public static String expandC(String C) {


        String s =
                "\nA Wins: " + getAWins(C) + "   A Loss: " + getALoss(C) +  "   A Draw: "+ getADraws(C) +
                "\nB Wins: " + getBWins(C) + "   B Loss: " + getBLoss(C) +  "   B Draw: "+ getBDraws(C) +
                "\ncommType: " + getCommType(C);

        return s;
    }
    public static String leadingZeros(String input, int length) {
        if(input.length() < length){
            for(int j = input.length(); j<length ; j++) input = "0"+input;
        }
        return input;
    }


    public static long daysSince(String d) {
        String da = "2"+d;
        Date date = new Date();

        try { date = new SimpleDateFormat("yyMMddHHmmss").parse(da);
        } catch (ParseException e) { e.printStackTrace(); }

        long diff = date.getTime() - new Date().getTime();
        diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        return diff;
    }




}
