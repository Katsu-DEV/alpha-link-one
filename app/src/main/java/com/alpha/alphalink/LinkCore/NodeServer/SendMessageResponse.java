
package com.alpha.alphalink.LinkCore.NodeServer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendMessageResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("rowid")
    @Expose
    private Integer rowid;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

}
