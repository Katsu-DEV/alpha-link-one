
package com.alpha.alphalink.LinkCore.NodeServer;

import android.util.Log;

import com.alpha.alphalink.BaseActivity;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class AllChallengesN {
    @SerializedName("allchallenges")
    @Expose
    private List<ChallengeN> allchallenges = null;

    private ArrayList<String> names;
    private int counterA = 0, counterB = 0;

    private void genNameList(String user) {
        sortChallengesByLastEvent();
        names = new ArrayList<>();
        if(allchallenges != null) {
            for(ChallengeN m : allchallenges) {
                String n = m.getA().equals(user) ? m.getB():m.getA();
                if(!names.contains(n)) names.add(n);
            }
        }
    }

    public void sortChallengesByLastEvent() {
        if(allchallenges != null) {
            //BaseActivity.getInstance().makeToast("CB: "+counterB++);
            if(allchallenges.size() != 2) {
                Collections.sort(allchallenges, new Comparator<ChallengeN>() {
                    @Override
                    public int compare(ChallengeN u1, ChallengeN u2) {
                        if (u1.getLastseen() < u2.getLastseen()) return 1;
                        else if (u1.getLastseen() > u2.getLastseen()) return -1;
                        else return 0;
                    }
                });
            }
        }
    }

    public void sortChallengesByActivity() {
        if(allchallenges != null) {
            if(allchallenges.size() == 2) {
                //BaseActivity.getInstance().makeToast("CA: "+counterA++);
                if(allchallenges.get(0).isActiveRequiresAttention() && allchallenges.get(1).isActiveRequiresAttention()) sortChallengesByLastEvent();
                else if((!allchallenges.get(0).isActiveRequiresAttention() && allchallenges.get(1).isActiveRequiresAttention()) &&
                    (allchallenges.get(0).getLastseen() > allchallenges.get(1).getLastseen()))
                {
                    ChallengeN a = allchallenges.get(0);
                    allchallenges.set(0, allchallenges.get(1));
                    allchallenges.set(1, a);
                }
            }
            else {
                sortChallengesByLastEvent();
                List<ChallengeN> aF = new ArrayList<ChallengeN>();

                for(ChallengeN ac : allchallenges) {
                    if(ac.isActiveRequiresAttention()) aF.add(ac);
                }
                for(ChallengeN ac : allchallenges) {
                    if(!ac.isActiveRequiresAttention()) aF.add(ac);
                }

                allchallenges = aF;
            }
        }
    }

    public List<ChallengeN> getAllChallenges() {
        return allchallenges == null ? new ArrayList<ChallengeN>() : allchallenges;
    }
    public void setChallenges(List<ChallengeN> challenges) {
        this.allchallenges = challenges;
    }

    public LinkedHashMap<String, ChallengeN> getChallengesArrayOrderedByEvent() {
        if(allchallenges != null) {
            LinkedHashMap<String, ChallengeN> ret = new LinkedHashMap<>();

            if(allchallenges.size() != 2) {
                Collections.sort(allchallenges, new Comparator<ChallengeN>() {
                    @Override
                    public int compare(ChallengeN u1, ChallengeN u2) {
                        if (u1.getLastseen() < u2.getLastseen()) return 1;
                        else if (u1.getLastseen() > u2.getLastseen()) return -1;
                        else return 0;
                    }
                });
            }

            for(ChallengeN ch : allchallenges) ret.put(ch.getOpp(), ch);
            return ret;
        }
        else return new LinkedHashMap<>();
    }

    public LinkedHashMap<String, ChallengeN> getChallengesArrayOrderedByActivity() {
        if(allchallenges != null) {
            LinkedHashMap<String, ChallengeN> ret = new LinkedHashMap<>();

            if(allchallenges.size() == 2) {
                //BaseActivity.getInstance().makeToast("CA: "+counterA++);
                if(allchallenges.get(0).isActiveRequiresAttention() && allchallenges.get(1).isActiveRequiresAttention()) sortChallengesByLastEvent();
                else if((!allchallenges.get(0).isActiveRequiresAttention() && allchallenges.get(1).isActiveRequiresAttention()) &&
                        (allchallenges.get(0).getLastseen() > allchallenges.get(1).getLastseen()))
                {
                    ChallengeN a = allchallenges.get(0);
                    allchallenges.set(0, allchallenges.get(1));
                    allchallenges.set(1, a);
                }
                ret.put(allchallenges.get(0).getOpp(), allchallenges.get(0));
                ret.put(allchallenges.get(1).getOpp(), allchallenges.get(1));
            }
            else {
                Collections.sort(allchallenges, new Comparator<ChallengeN>() {
                    @Override
                    public int compare(ChallengeN u1, ChallengeN u2) {
                        if (u1.getLastseen() < u2.getLastseen()) return 1;
                        else if (u1.getLastseen() > u2.getLastseen()) return -1;
                        else return 0;
                    }
                });

                for(ChallengeN ac : allchallenges) {
                    if(ac.isActiveRequiresAttention()) ret.put(ac.getOpp(), ac);
                }
                for(ChallengeN ac : allchallenges) {
                    if(!ac.isActiveRequiresAttention()) ret.put(ac.getOpp(), ac);
                }
            }

            return ret;
        }
        else return new LinkedHashMap<>();
    }

    public ChallengeN getChallengeWith(String opponent) {
        if(allchallenges != null){
            for(ChallengeN ch : allchallenges) if(ch.getA().equals(opponent) || ch.getB().equals(opponent)) return ch;
        }
        return null;
    }

    public void addChal(ChallengeN m) {
        boolean fnd = false;

        if(allchallenges != null && allchallenges.size() != 0){
            for(int i = 0; i < allchallenges.size(); i++) {
                if(allchallenges.get(i).getA().equals(m.getA()) && allchallenges.get(i).getB().equals(m.getB())) {
                    allchallenges.add(i, m);
                    fnd = true;
                    break;
                }
            }
            if(!fnd){
                List<ChallengeN> a = new ArrayList<>();
                a.addAll(allchallenges);
                a.add(m);
                allchallenges = a;
            }
            sortChallengesByLastEvent();
        }
        else {
            allchallenges = new ArrayList<ChallengeN>();
            allchallenges.add(m);
        }
    }

    public ArrayList<String> getNamesList() {
        genNameList(BaseActivity.getInstance().getUser().getName());
        return names;
    }

    public String toJsonString() {
        return new Gson().toJson(this);
    }
}

/*

    public ChallengeN[] getChallengesArrayOrderedByEvent() {
        if(allchallenges != null) {
            sortChallengesByLastEvent();
            ChallengeN[] cc = new ChallengeN[allchallenges.size()];
            return allchallenges.toArray(cc);
        }
        else return new ChallengeN[]{};
    }

    public ChallengeN[] getChallengesArrayOrderedByActivity() {
        if(allchallenges != null) {
            sortChallengesByActivity();
            ChallengeN[] cc = new ChallengeN[allchallenges.size()];
            return allchallenges.toArray(cc);
        }
        else return new ChallengeN[]{};
    }


 */
