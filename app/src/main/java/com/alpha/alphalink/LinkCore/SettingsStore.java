
package com.alpha.alphalink.LinkCore;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class SettingsStore {

    @SerializedName("theme")
    @Expose
    private int theme;

    @SerializedName("notificationsound")
    @Expose
    private String notificationsound = "on";

    @SerializedName("notificationvibrate")
    @Expose
    private String notificationvibrate = "on";

    @SerializedName("notify")
    @Expose
    private String notify = "on";


    public SettingsStore() {};

    public SettingsStore(int theme) {
        setTheme(theme);
        setNotificationsound(notificationsound);
    }

    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public String getNotificationsound() {
        return notificationsound;
    }

    public void setNotificationsound(String notificationsound) {
        this.notificationsound = notificationsound;
    }

    public String getNotificationvibrate() {
        return notificationvibrate;
    }

    public void setNotificationvibrate(String notificationvibrate) {
        this.notificationvibrate = notificationvibrate;
    }

    public String getNotify() {
        return notify;
    }

    public void setNotify(String notify) {
        this.notify = notify;
    }

    public String toJsonString() {
        return new Gson().toJson(this);
    }

    public static SettingsStore getEmpty() {
        return new SettingsStore(0);
    }
}
